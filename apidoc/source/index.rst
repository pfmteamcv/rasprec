.. Rasprec documentation master file, created by
   sphinx-quickstart on Tue May  7 17:13:20 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Rasprec's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Rasprec Detector
==================
.. automodule:: Detector
   :members:

.. automodule:: Heatmap
   :members:

.. automodule:: Classifier
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
