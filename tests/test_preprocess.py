# -*- coding: utf-8 -*- 
import cv2
import os
from Preprocessor import BasicPreprocessor

"""
	Pruebas sobre el preprocesador.
	Lee todas las imágenes de prueba de la BD de Yale, las preprocesa y las guarda en el directorio ./preprocessed
"""


#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
lst_dir = os.walk(path + "00_original/")
preprocessor = BasicPreprocessor()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)

		pp_image = preprocessor.prepare(image)
		cv2.imwrite(path + "01_preprocessed/" + file, pp_image)

		cv2.imshow("text2", pp_image)
		cv2.waitKey(0)
