# -*- coding: utf-8 -*- 
from Pipeline import Pipeline


from Provider import FileProvider
#from Provider import CameraProvider
from Preprocessor import BasicPreprocessor
from Detector import HaarDetector
from Processor import HaarProcessor
import os

project_path = os.getenv("HOME") + "/fr/rasprec/"


provider = FileProvider(project_path + "facial_databases/user01/train")
#provider = CameraProvider()
preprocessor = BasicPreprocessor()
detector = HaarDetector()
processor = HaarProcessor()
#trainer = EigenTrainer()

pipeline = Pipeline(provider, preprocessor, detector, processor)
pipeline.start()
