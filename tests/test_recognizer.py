# -*- coding: utf-8 -*- 
import cv2
import os
from Recognizer import *

r = EigenfacesRecognizer()

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/06_separados/"
for i in range(1, 10):
	lst_dir = os.walk(path + "subject0" + str(i) + "/")
	for root, dirs, files in lst_dir:
		for file in files:
			image = cv2.imread(root + file)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			r.add_face(image, i)
r.train()

lst_dir = os.walk(path + "recog" + "/")
for root, dirs, files in lst_dir:
		for file in files:
			image = cv2.imread(root + file)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			print "-->" + str(file)
			print r.predict(image)
			
			print " "