# -*- coding: utf-8 -*- 
import cv2
import os
from Processor import HaarProcessor

face_processor = HaarProcessor()

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/Yale_Face_Database/faces_pgm/"
lst_dir = os.walk(path)


for root, dirs, files in lst_dir:
	for file in files:
		print "--> " + str(path) + str(file)
		image = cv2.imread(path + file)
		#if image is None:
		#	continue
		image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		print type(image)

		print image.shape
		
		centers =  face_processor.load(image)
		if not centers == False:
			print "---->" + str(centers)
			x, y = centers[0]

			img = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
			cv2.circle(img,centers[0], 2,(0,255,0),1)
			cv2.circle(img,centers[1], 2,(0,255,0),1)
			
			#cv2.rectangle(image,corner1, corner2,(0,255,0),1)
			cv2.imshow("text2", img)
			cv2.waitKey(0)
			
