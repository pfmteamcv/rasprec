# -*- coding: utf-8 -*- 
from Pipeline import Pipeline


from Provider import FileProvider
#from Provider import CameraProvider
from Preprocessor import BasicPreprocessor
from Detector import HaarDetector
from Processor import HaarProcessor
import os

project_path = os.getenv("HOME") + "/fr/rasprec/"
print project_path
database = os.getenv("HOME") + '/resources/facial_databases/BioID_Face_Database/'
print "--> " +  database
provider = FileProvider(database)


#provider = CameraProvider()
preprocessor = BasicPreprocessor()
detector = HaarDetector()
processor = HaarProcessor()

pipeline = Pipeline(provider, preprocessor, detector, processor)
pipeline.start()
