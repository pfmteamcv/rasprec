# -*- coding: utf-8 -*- 
import cv2
import os
from Detector import HaarDetector

"""
	Pruebas sobre el detector.
	Leemos las imágenes de prueba ya preprocesadas de la BD de Yale y detectamos la cara
"""

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
lst_dir = os.walk(path + "01_preprocessed/")
detector = HaarDetector()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)
		detected_image = detector.detect(image)

		rects = detector.get_rects_as_coords()

		# Si se ha detectado alguna cara
		if len(rects) > 0:
			#img_rgb = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
			# Iteramos sobre las caras encontradas

			for f in detector.faces:
				cv2.imwrite(path + "02_detected/" + file, f)

			for r in rects:
				# Mostramos la posición de la cara detectada
				cv2.rectangle(image, r[0], r[1], (0, 255, 0), 2)

			#cv2.imshow("text2", image)
			#cv2.waitKey(0)

