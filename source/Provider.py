"""
	Implementa un iterador que va devolviendo todas las imágenes.
"""

from abc import ABCMeta
from abc import abstractmethod

class Provider:
	__metaclass__ = ABCMeta

	# Modela un proveedor de imágenes, puede ser una base de datos, un directorio o de la webcam
	def __init__(self, verbose=False):
		self.verbose = verbose
		if self.verbose:
			print ("[INFO] Inicializando la clase Provider.")

