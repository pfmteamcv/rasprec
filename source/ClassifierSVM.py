"""
	ClassifierSVM.py

	Implementa el clasificador Support Vector Machines (SVM)


	Parámetros
	----------


	Enlaces
	-------
	https://www.pyimagesearch.com/2014/11/10/histogram-oriented-gradients-object-detection/
		Pasos para entrenar SVM, habla de hard minig

	https://www.pyimagesearch.com/2014/11/17/non-maximum-suppression-object-detection-python/
		Non Maximum Suppresion

	http://cs.brown.edu/people/pfelzens/papers/lsvm-pami.pdf
		PAPER. Mejoras para SVM

	http://www.cs.cmu.edu/~efros/exemplarsvm-iccv11.pdf
		PAPER. Exemplar SVM

	https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_ml/py_svm/py_svm_basics/py_svm_basics.html#svm-understanding
		Explicación de SVM en OpenCV

	https://docs.opencv.org/2.4/modules/ml/doc/support_vector_machines.html
		Referencia de SVM en OpenCV

	https://www.learnopencv.com/support-vector-machines-svm/
		Explicación de los parámetros de SVM
"""

import cv2
from Classifier import Classifier
from Provider import Provider
from Utils import Utils
import numpy as np
import imutils

class ClassifierSVM(Classifier):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, features=None, params=None, verbose=False):
		
		Classifier.__init__(self, features, params, verbose)

		self.__svm = None					# Clasificador SVM
		
		self._faces_descriptors = []			# Descriptores de las caras de entrenamiento
		self._nfaces_descriptors = []		# Descriptores de las no caras de entrenamiento
		self.__hard_mining_descriptors = []	# Descriptores para el hard mining

		# Parámetros del clasificador
		self.__type = cv2.ml.SVM_C_SVC
		self.__kernel_type = cv2.ml.SVM_LINEAR
		self.__degree = 5
		self.__gamma = 0.50625
		self.__c = 12.5
		self.__term_crit = (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6)

		""" Los posibles valores de los parámetros son:
				type:
					cv2.ml.C_SVC
					cv2.ml.NU_SVC
					cv2.ml.ONE_CLASS
					cv2.ml.EPS_SVR
					cv2.ml.NUSVR
				kernel_type:
					cv2.ml.SVM_LINEAR
					cv2.ml.SVM_POLY
					cv2.ml.SVM_RBF
					cv2.ml.CVM_SIGMOID
				term_crit:
					cv2.TERM_CRITERIA_COUNT
					cv2.TERM_CRITERIA_EPS
					cv2.TERM_CRITERIA_MAX_ITER
		"""

		if params:
			self.set_params(params)
		else:
			self.__initialize()

		if self._verbose:
			print ("[INFO] Se ha inicializado la clase ClassifierSVM.")


	def __initialize(self):
		# Inicializamos el clasificador SVM
		
		self.__svm = cv2.ml.SVM_create()
		self.__svm.setGamma(self.__gamma)
		self.__svm.setC(self.__c)
		self.__svm.setKernel(self.__kernel_type)
		self.__svm.setType(self.__type)
		self.__svm.setDegree(self.__degree)
		self.__svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6))


	def set_params(self, params):
		""" Carga los parámetros en el clasificador
		"""
		if 'kernel_type' in params:
			self.__kernel_type = params['kernel_type']

		if 'type' in params:
			self.__type = params['type']

		if 'gamma' in params:
			self.__gamma = params['gamma']

		if 'c' in params:
			self.__c = params['c']

		if 'degree' in params:
			self.__degree = params['degree']

		if 'term_crit' in params:
			self.__term_crit = params['term_crit']

		self.__initialize()


	def get_params(self):
		return self.params



	"""
		ESTA PARECE SER QUE ES LA REFERENCIA BUENA: https://docs.opencv.org/4.0.0/d1/d2d/classcv_1_1ml_1_1SVM.html
	"""


	def train(self, hard_mining_provider=None, hard_mining_num_images=0, hard_mining_width=70, hard_mining_height=70):
		""" Entrena el clasificador a partir de los datos precargados

		Entrena el clasificador SVM tomando los datos que tienen que haber sido cargados
		previamente con la función load_data().

		Devuelve:
			response
				Array de respuesta, cada elemento corresponde con un vector de características
				de train_data e indica si es cara (1.) o no cara (0.)
			train_data
				Array con los vectores de características de las imágenes de entrenamiento

		"""
		num_faces_train = len(self._faces_descriptors)
		num_nfaces_train = len(self._nfaces_descriptors)

		# Cargamos el clasificador. 1 caras, 0 no caras
		response = np.concatenate((np.ones((num_faces_train), dtype=np.int32),
								   np.zeros((num_nfaces_train), dtype=np.int32)))

		a = self._faces_descriptors
		b = self._nfaces_descriptors

		#self._faces_descriptors = np.asarray(self._faces_descriptors)
		#self._nfaces_descriptors = np.asarray(self._nfaces_descriptors)

		train_data=np.concatenate((self._faces_descriptors, self._nfaces_descriptors), axis=0)


		self._num_trained_faces = num_faces_train
		self.num_trained_nfaces = num_nfaces_train
		self._num_dimensions = train_data[0].shape[0]

		if self._verbose:
			print ("[INFO] Entrenado el clasificador SVM. " + \
				str(self._num_trained_faces) + " caras y " + \
				str(self.num_trained_nfaces) + " no caras con " + \
				str(self._num_dimensions) + " dimensiones.")

		print ("***")
		print ("Kernel: " + str(self.__kernel_type))
		a = self.__svm.train(train_data, cv2.ml.ROW_SAMPLE, response)

		if self._verbose:
			print ("[INFO] El clasificador SVM ha sido entrenado.")

		if not a:
			print ("[ERROR] Ha habido un problema al entrenar el clasificador.")

		if self._verbose:
			print ("[INFO] Se ha entrenado el clasificador SVM.")

		self._is_trained = True
		
		if not hard_mining_provider is None:
			self.apply_hard_mining(hard_mining_provider, hard_mining_num_images, hard_mining_width, hard_mining_height)
			# Actualizamos los datos con los falsos positivos
			num_faces_train = len(self._faces_descriptors)
			num_nfaces_train = len(self._nfaces_descriptors)

			# Cargamos el clasificador. 1 caras, 0 no caras
			response = np.concatenate((np.ones((num_faces_train), dtype=np.int32),
									   np.zeros((num_nfaces_train), dtype=np.int32)))
			train_data=np.concatenate((self._faces_descriptors, self._nfaces_descriptors), axis=0)

			self._num_trained_faces = num_faces_train
			self.num_trained_nfaces = num_nfaces_train
			self._num_dimensions = train_data[0].shape[0]

			a = self.__svm.train(train_data, cv2.ml.ROW_SAMPLE, response)
			if not a:
				print ("[ERROR] Ha habido un problema al entrenar el clasificador.")

		return response, train_data


	def save (self, filename=None):

		if self._is_trained:
			if filename is None:
				filename = self._path + self._default_filename + '.svm'
			else:
				filename = self._path + filename

			self.__svm.save(filename)

			if self._verbose:
				print ("[INFO] Se ha guardado el fichero " + str(filename) + ".")


	def train_and_save(self, filename=None, hard_mining_data=None, hard_mining_num_images=0):

		response, train_data = self.train(hard_mining_data, hard_mining_num_images)
		self.save(filename)


	def load_training(self, filename="train_files/default.svm"):
		
		if filename is None:
			filename = self._path + self._default_filename + '.svm'
		else:
			filename = self._path + filename

		self.__svm = cv2.ml.SVM_load(filename)
		self._is_trained = True

		
	def is_face(self, image):
		if self._is_trained:
			# Se ha pasado una imagen, por tanto el primer paso es obtener sus características
			feat = self._features.get_features(image)

			# SVM recoge un array de vectores de características. Como solo queremos evaluar
			# una cara debemos crear el array y cargar el vector pasado como primer (y único)
			# elemento del mismo
			samples = np.asarray([feat])
			ret = self.__svm.predict(samples)
			"""
				predict devuelve un array con 3 datos:
					- Un float que no se lo que es (por ahora es 0.0)
					- Array con las predicciones
					- Tipo de datos de las predicciones (dtype=float32) 
			"""


			return int(np.ravel(ret)[0])
			"""
				predict devuelve un array con 3 datos:
					- Un float que no se lo que es (por ahora es 0.0)
					- Array con las predicciones
					- Tipo de datos de las predicciones (dtype=float32) 
			"""


	def are_faces(self, images):
		""" Evalúa un conjunto de imágenes para verificar si son caras

		Se le pasa un conjunto de imágenes y las evalúa para predecir si corresponden a caras o no caras

		Parámetros:
			images
				Array donde cada elemento es un array que corresponde a una imagen

		Devuelve:
			Array en el que cada elemento corresponde a cada una de las imágenes que se le han pasado
			y que es 0 si predice que la imagen no es cara y 1 si corresponde a una cara.

		"""
		if self._is_trained:
			if isinstance(images, Provider):
				images = images.get_array()
				samples = []
				for i in images:
					img = cv2.imread(i)
					feat = self._features.get_features(img)
					samples.append(feat)

				samples = np.asarray(samples)

				ret, preds = self.__svm.predict(samples)

				return np.ravel(preds).tolist()

			elif isinstance(images, list):    
				#samples = np.asarray(images)	
				samples = []		
				for i in images:
					feat = self._features.get_features(i)
					samples.append(feat)

				samples = np.asarray(samples)
				ret, preds = self.__svm.predict(samples)
				ret = np.ravel(preds)

				return np.ravel(preds).tolist()

			elif isinstance(images, tuple):
				# Se ha pasado una lista de imágenes
				samples = []
				# Calculamos las características de todas las imágenes
				for i in images:
					feat = self._features.get_features(i)
					samples.append(feat)

				samples = np.asarray(samples)

				ret, preds = self.__svm.predict(samples)

				return np.ravel(preds).tolist()


	def apply_hard_mining(self, data, num_images, width, height):
		"""
			data - objeto de la clase Provider con imágenes de fondo
			num_images - número de imágenes para el hardminig
		"""

		data = data.get_array()

		if self._verbose:
			print ("[INFO] Comienza la carga de " + str(len(data)) + " imágenes para hard mining.")

		print ("Comienza el hard mining")
		# print (self.are_faces(self.__hard_mining_descriptors))
		
		# Cargamos las imágenes para hard mining y calculamos los descriptores
		hm_descriptors = []
		temp_images = []
		num_bg_images = 0
		for d in data:
			img = cv2.imread(d)
			# Aquí redimensionamos la imagen

			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

			win = np.array(Utils.get_sliding_windows(img, min_height=height, min_width=width))
			images = win[:,0]	# Eliminamos la posición de la ventana en la imagen original
			for i in images:
				# sliding_windows devolverá imágene de diversos tamaños. Se reducen a 70x70
				
				i = imutils.resize(i, width=80)
				temp_images.append(i)
				feat = self._features.get_features(i)
				hm_descriptors.append(feat)
				num_bg_images += 1
				if num_bg_images >= num_images:
					break
			if num_bg_images >= num_images:
					break

		fp = self.are_faces(hm_descriptors)
		fp_descriptors = []

		count = 0
		for i in fp:
			if i == 1.:
				print ("detectado falso positivo")
				img = temp_images[count]
				fp_descriptors.append(hm_descriptors[count])
				cv2.imshow("Falso positivo", img)
				cv2.waitKey(50)
			count += 1

		if self._verbose:
			print ("[INFO] Se han detectado " + str(len(fp_descriptors)) + " falsos positivos tras analizar " + str(num_images) + " imágenes.")
			print ("[INFO] Re-entrenando el clasificador.")

		# Añadimos los descriptores de los falsos positivos a los descriptores de las no caras de entrenamiento
		self._nfaces_descriptors = self._nfaces_descriptors + fp_descriptors


		"""
		# Se cargan las caras
		n = 0
		if faces:
			for i in faces:
				if num_faces != 0 and n >= num_faces:
					break
				n += 1
				img = cv2.imread(i)
				feat = self._features.get_features(img)
				self._faces_descriptors.append(feat)
		if self._verbose:
			print ("[INFO] Se han cargado las caras en el clasificador.")

		if self._verbose:
			print ("[INFO] Comienza la carga de " + str(len(nfaces)) + " NO caras de entrenamiento.")
		# Se cargan las no caras
		n = 0
		if nfaces:
			for i in nfaces:
				if num_faces != 0 and n >= num_nfaces:
					break
				n += 1
				img = cv2.imread(i)
				feat = self._features.get_features(img)
				self._nfaces_descriptors.append(feat)
		if self._verbose:
			print ("[INFO] Se han cargado las NO caras en el clasificador.")
		"""

	def show_parameters(self):
		"""
			Esta clase solamente muestra información interna de SVM
		"""

		#print (self.__svm.getSupportVectors())
		print ()
		print ("Parámetros de SVM")
		print ("Type:  " + str(self.__svm.getType()))
		print ("C:     " + str(self.__svm.getC()))
		print ("Gamma: " + str(self.__svm.getGamma()))
		print ()



	