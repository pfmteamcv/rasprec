""" Clase base para el detector

CAMBIO - TAL VEZ NO HEREDE NADA DE ESTA CLASE SINO QUE LOS ELEMENTOS QUE SE LE PASAN DETERMINAN EL TIPO DE DETECTOR
Esta clase contiene los métodos virtuales y métodos reutilizables por todas las
clases que hereden de ella. Pretende establecer la estructura común que deberán
tener todos los detectores de caras.
El objetivo final de esta clase es recoger una imagen en b/n de un tamaño arbitrario
y devolver una lista con los rectángulos que contienen las caras en la misma.

Attributes
---------
faces_dir : str
	Directorio con las imágenes de caras para el entrenamiento
faces_ndir : str
	Directorio con las imágenes de NO caras para el entrenamiento
faces : list
	Lista con rutas de todas las imágenes de caras para el entrenamiento
nfaces : list
	Lista con rutas de todas las imágenes de NO caras para el entrenamiento

Methods
-------
detect_multiscale(image) : 

"""

""" IDEAS SUELTAS DE COMO DEBERÍA FUNCIONAR LA CLASE

	El detector requiere una combinación de características y clasificador (clases Features y Classifier)
	Se entiende que el clasificador debe estar entrenado.

	Por coherencia con OpenCV se puede llamar al método principal detect_multiscale(image), que dada una imagen 
	devuelve una lista con los rectángulos que contienen caras.

	¿Preparado para trabajar con un tamaño fijo de caras de 70x70?????

	Ninguna clase hereda de esta, a fin de cuentas la única diferencia es el clasificador que se pase como 
	parámetro al constructor.



"""

from abc import ABCMeta
from abc import abstractmethod

class Detector:
	__metaclass__ = ABCMeta

	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, verbose=False):

		self.verbose = verbose

		
	# -------------------- METODOS VIRTUALES --------------------
	
	@abstractmethod
	def set_params(self, params):
		raise NotImplementedError()


	@abstractmethod
	def detect_multiscale(self, image):
		raise NotImplementedError()


	@abstractmethod
	def set_debug(self, debug):
		raise NotImplementedError()

	# -------------------- METODOS PÚBLICOS -------------------- 

	def get_params(self):
		
		return self.params

