from Features import Features
import cv2
import numpy as np
import math 
import sys

# https://ianlondon.github.io/blog/how-to-sift-opencv/


class FeaturesBoWSIFT(Features):

	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, params=None, verbose=False):

		self._verbose = verbose

		self.__sift = cv2.xfeatures2d.SIFT_create()

		self.__dictionary = None
		self.__dict_size = None
		self.has_dictionary = False

		self.__flann = None
		self.__bow_dict = None

	# --------------------- METODOS PRIVADOS ---------------------

	def __init_dictionary(self):
		""" Inicializa el matcher con el diccionario
		"""

		FLANN_INDEX_KDTREE = 0
		index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
		search_params = dict(checks=50)
		self.__flann = cv2.FlannBasedMatcher(index_params, search_params)

		self.__bow_dict = cv2.BOWImgDescriptorExtractor(self.__sift, cv2.BFMatcher(cv2.NORM_L2))
		self.__bow_dict.setVocabulary(self.__dictionary)
		if self._verbose:
			print ("[INFO] Se ha creado el diccionario de palabras visuales.")



	# --------------------- METODOS PUBLICOS ---------------------

	def get_params(self):
		pass


	def set_params(self, params):
		pass


	"""
	def get_features(self, image):
		sift = cv2.xfeatures2d.SIFT_create()
		kp, desc = sift.detectAndCompute(image, None)

		print (kp)
		print ()
		print (desc)

		imgkp = cv2.drawKeypoints(image, kp, cv2.cvtColor(image, cv2.COLOR_GRAY2BGR))
		cv2.imshow("SIFT", imgkp)
		cv2.waitKey(0)

		return kp, desc
	"""

	def get_dimensions(self, image):

		return self.__dict_size



	def draw_features(self, image):

		# Obtenemos los keypoints y los descriptores
		kp, desc = self.__sift.detectAndCompute(image, None)

		img2 = cv2.drawKeypoints(image=image, outImage=image, keypoints=kp,
			flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS, color=(0, 0, 255))

		cv2.imshow("SOFT Keypoints", img2)

		k = kp[0]
		print("")
		print ("EJEMPLO DE INFORMACIÓN ASOCIADA A UN KEYPOINT")
		print ("=============================================")
		print ("Punto de interés SIFT")
		print ("---------------------")
		print ("Ángulo:   " + str(k.angle))
		#print ("Clase:    " + str(k.class_id))
		#print ("Convert:  " + str(k.convert))
		#print ("Octave:   " + str(k.octave))
		#print ("Overlap:  " + str(k.overlap))
		print ("Posición: " + str(k.pt))
		#print ("Response: " + str(k.response))
		print ("Tamaño:     " + str(k.size))
		print ("Descriptor SIFT")
		print ("---------------------")
		print (desc[0])
		cv2.waitKey(0)
		cv2.destroyAllWindows()


	def generate_dictionary(self, provider, dict_size = 5, preprocessor=None):
		""" 
			Genera el diccionario de palabras visuales a partir de un conjunto de 
			imágenes
		"""
		# PASO 1: CREACIÓN DEL DICCIONARIOS DE PALABRAS VISUALES
		
		self.__dict_size = dict_size
		bow = cv2.BOWKMeansTrainer(self.__dict_size)

		for filename in provider: 
			img = cv2.imread(filename)
			if not preprocessor is None:
				img = preprocessor.adjust_color(img)
			else:
				img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			kp, desc = self.__sift.detectAndCompute(img, None)
			bow.add(desc)

		if self._verbose:
			dc = bow.descriptorsCount()
			print ("[INFO] Se han cargado " + str(dc) + " descriptores.")

		# Dividimos en cluster todas las imágenes. Cada cluster será una palabra del 
		# diccionario.
		self.__dictionary = bow.cluster()
		self.has_dictionary = True
		self.__init_dictionary()

		# Aquí acaba el proceso de construcción del diccionario. Ya tenemos las 
		# palabras que lo conforman.
		# Es un proceso offline, habría que guardarlo en fichero


	def get_features(self, image):
		"""
			Genera el histograma de palabras visuales según el diccionario creado.
		"""
		if not self.has_dictionary:
			print("[ERROR] No se ha definido un diccionario de palabras visuales")
			sys.exit()
		else:
			if type(image) == np.ndarray:
				desc = self.__bow_dict.compute(image, self.__sift.detect(image))
				# Hay ocasiones en que no se encuentran keypoints (por ejemplo, en una
				# imagen de color continuo), por lo que es necesario devolver un
				# descriptor falso
				if desc is None:
					desc = np.zeros((self.__dict_size), dtype=np.float32)
					return desc
				else:
					return desc[0]




		


		# https://answers.opencv.org/question/183596/how-exactly-does-bovw-work-for-python-3-open-cv3/