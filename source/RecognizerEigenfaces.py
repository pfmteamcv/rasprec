import sys
from pathlib import Path
from Recognizer import Recognizer
import cv2
import numpy as np
import pickle


class RecognizerEigenfaces(Recognizer):
	"""
		Reconocedor facial basado en EigenFaces
	"""
	def __init__(self, verbose=False, num_components=10, threshold=6000):
		Recognizer.__init__(self, verbose)

		self._face_recognizer = cv2.face.EigenFaceRecognizer_create(num_components=num_components, threshold=threshold)
		self.verbose = verbose

		if self.verbose:
			print (dir(self._face_recognizer))


	def train(self):
		#self._face_recognizer.setNumComponents(16)
		#self._face_recognizer.setThreshold(6000)
		#self._face_recognizer.setLabelInfo(1, 'hola')
		#self._face_recognizer.setLabelInfo(2, 'adios')
		self._face_recognizer.train(self._training_set, np.array(self._training_labels))
		self._is_trained = True
				

	def save(self, filename):

		file = self._data_dir + filename + '.eigen'
		self._face_recognizer.save(file) 
		with open(file + '.lbl', 'wb') as fp:
			pickle.dump(self._labels, fp)


	def load(self, filename):

		file = self._data_dir + filename + '.eigen'
		self._face_recognizer.read(file)
		with open(file+'.lbl', 'rb') as fp:
			self._labels = pickle.load(fp)
		self._is_trained = True



	def predict(self, image):

		prediction = self._face_recognizer.predict(image)
		
		#print (prediction)

		#print (self._face_recognizer.getEigenVectors().shape)
		
		if prediction[0] == -1:
			lbl = None
		else:
			lbl = self._labels[prediction[0]]

		if prediction[1] > 3000:
			lbl = 'Desconocido'

		return lbl


	def get_info(self):

		info = {
			'Is trained': 	self._is_trained,
			'Algorithm': 	'Eigenfaces',
			'Faces loaded': len(self._training_set),
			'People loaded': len(self._labels),
			'People labels': self._labels,
			'Num. components': self._face_recognizer.getNumComponents(),
			'Threshold': self._face_recognizer.getThreshold()
		}

		return info



	def show_info(self):

		print ("****")
		print ("Default name: " + str(self._face_recognizer.getDefaultName()))
		print ("****")
		print ("Eigen Values: " + str(self._face_recognizer.getEigenValues()))
		print ("****")
		print ("Eigen Vectors: " + str(self._face_recognizer.getEigenVectors()))
		print ("****")
		print ("Label Info: " + str(self._face_recognizer.getLabelInfo(1)))
		print ("****")
		print ("Labels: " + str(self._face_recognizer.getLabels()))
		print ("****")
		print ("Labels by string: " + str(self._face_recognizer.getLabelsByString('1')))
		print ("****")
		print ("Mean: " + str(self._face_recognizer.getMean()))
		print ("****")
		print ("Num components: " + str(self._face_recognizer.getNumComponents()))
		#print ("****")
		#print ("Projections: " + str(self._face_recognizer.getProjections()))
		print ("****")
		print ("Threshold: " + str(self._face_recognizer.getThreshold()))

		ev = self._face_recognizer.getEigenVectors()
		print (len(ev))


	def draw(self):
		eigen_vectors = np.asarray(self._face_recognizer.getEigenVectors())
		#print (eigen_vectors.shape)
		com = self._face_recognizer.getNumComponents()
		ev = eigen_vectors.reshape(70, 70, com)
		print (ev.shape)
		images = []
		for i in range(ev.shape[-1]):
			img = ev[...,i]
			print (img.max())
			img *= 255.0 / img.max()
			img = img.astype(np.uint8)
			images.append(img)

			print (type(img[0][0]))

			im_color = cv2.applyColorMap(img, cv2.COLORMAP_JET)

			cv2.imshow(str(i), im_color)
			cv2.waitKey(0)


	def draw_eigenvalues(self):

		print (str(dir (self._face_recognizer)))
		print ("")
		eigen_values = self._face_recognizer.getEigenValues()
		eigen_vectors = self._face_recognizer.getEigenVectors()
		mean = self._face_recognizer.getMean()

		print ("")
		print(eigen_values)
		print(eigen_values.shape)
		print ("")
		print(eigen_vectors)
		print(eigen_vectors.shape)
		
