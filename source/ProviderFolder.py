import os
from pathlib import Path
from Provider import Provider
import cv2
import sys

class ProviderFolder(Provider):
	def __init__(self, path):

		self.__path = path	# Ruta a la carpeta
		self.__len = 0		# Número de imágenes en la carpeta	
		self.__images = []	# Imágenes de la carpeta
		self.__actual = 0		# Siguiente imagen para el iterador
		
		for f in Path(self.__path).iterdir():
			img = cv2.imread(str(f))
			if not img is None:
				self.__images.append(img)

		self.__len = len(self.__images)
		


	def __iter__(self):
		self.__actual = 0
		return self


	def __next__(self):
		if self.__actual == self.__len:
			raise StopIteration()
		else:
			i = self.__images[self.__actual]
			self.__actual += 1
			return i

	