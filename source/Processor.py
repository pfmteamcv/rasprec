from EyeDetector import LandmarkEyeDetector
from EyeDetector import HaarEyeDetector

import cv2
import dlib
from pathlib import Path
import rasprec
import numpy as np
import imutils
import math
import time

class Processor:

	def __init__(self, verbose=False, debug=[], eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_68, histogram=rasprec.HISTO_PARTS):
		# Almacenamos los frames intermedios para documentación
		# Imágenes de los diferentes pasos
		self.__face = None						# Frame original
		self.__face_geom_rotate = None			# Tras aplicar rotación en transformaciones geométricas 
		self.__face_geom_scale = None			# Tras aplicar escalado en transformaciones geométricas 
		self.__face_geom_translate = None		# Tras aplicar traslación en transformaciones geométricas 
		self.__face_geom_crop = None			# Tras aplicar recorte en transformaciones geométricas 
		self.__face_hist = None					# Tras ecualización de histograma independiente
		self.__face_smooth = None				# Tras suavizado
		self.__face_mask = None					# Tras aplicar la máscara
		self.__face_processed = None			# Frame tras aplicar todas las operaciones

		self.__histo = histogram

		self.__desired_width = 70				# Ancho de la cara ya recortada
		self.__desired_height = 70			# Alto de la cara ya recortada
		self.__desired_left_eye_x = 0.21 * self.__desired_width		# Posición X en que se debe encontrar el ojo izquierdo
		self.__desired_right_eye_x = 0.79 * self.__desired_width	# Posición Y en que se debe encontrar el ojo derecho
		self.__desired_eyes_y = 0.14 * self.__desired_height		# Posición Y en que se deben encontrar los ojos

		self.__x1 = None						# Coordenada X del ojo izquierdo
		self.__y1 = None						# Coordenada Y del ojo izquierdo
		self.__x2 = None						# Coordenada X del ojo derecho
		self.__y2 = None						# Coordenada Y del ojo derecho

		self.__width = None						# Ancho de la imagen
		self.__height = None					# Alto de la imagen

		self.__debug = debug					# Modo depuración

		self.__debug_eye_detections = {
			'left': 0,
			'right': 0,
			'both': 0,
			'total': 0
		}

		self.__all_times = {
			'detect_eyes': None,
			'geometric': None,
			'histogram':None,
			'smooth_mask': None
		}

		self.__eye_detect_method = eye_detect_method

		# Tarda un poco en cargalo, por lo que se carga aquí una vez en lugar de cada vez que se llame
		self.__predictor68 = dlib.shape_predictor(str(Path.home()) + "/TFM/rasprec/resources/facial_landmarks/shape_predictor_68_face_landmarks.dat")
		self.__predictor5 = dlib.shape_predictor(str(Path.home()) + "/TFM/rasprec/resources/facial_landmarks/shape_predictor_5_face_landmarks.dat")

		self.__verbose = verbose

		#self.left_eye_coords = None
		#self.right_eye_coords = None


	def set_debug(self, debug):

		self.__debug = debug


	def get_count_eyes_detected(self):
		return self.__debug_eye_detections


	def __detect_eyes(self):

		if self.__eye_detect_method == rasprec.EYES_FACIAL_LANDMARKS_68:
			self.ed = LandmarkEyeDetector(self.__predictor68, self.__face)
			self.ed.detect()
			landmarks = self.ed.get_landmarks()

			# Nos fijamos en los puntos de referencia obtenidos de los ojos. Cada ojo tiene
			# 6 puntos de referencia de los que nos quedamos los del extremo exterior e interior.
			# Son los valores 37 y 40 para el ojo izquierdo y 43 y 46 para el ojo derecho
			fl37 = landmarks[36]
			fl40 = landmarks[39]
			fl43 = landmarks[42]
			fl46 = landmarks[45]

			# Calculamos los centros de ambos ojos
			self.__x1 = int((fl37[0]+fl40[0])/2)
			self.__y1 = int((fl37[1]+fl40[1])/2)
			self.__x2 = int((fl43[0]+fl46[0])/2)
			self.__y2 = int((fl43[1]+fl46[1])/2)

			# self.full_processing()

			if rasprec.DEBUG_EYES_DETECTION in self.__debug:
				# Modo de depuración para Facial Landmarks 68
				# Se dibujan todos los puntos, remarcando los ojos y
				# la línea que une ambos centros
				img = np.copy(self.__face)
				img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
				# Línea que une los centros de ambos ojos
				img = cv2.line(img, (self.__x1, self.__y1), (self.__x2, self.__y2), (0, 255, 255), 1)
				# Todos los landmarks
				for x, y in landmarks:
					img = cv2.rectangle(img, (x, y), (x + 1, y + 1), (0, 255, 0), 1)
				# Landmarks que corresponden a los extremos de los ojos
				img = cv2.rectangle(img, (fl37[0], fl37[1]), (fl37[0] + 1, fl37[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl40[0], fl40[1]), (fl40[0] + 1, fl40[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl43[0], fl43[1]), (fl43[0] + 1, fl43[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl46[0], fl46[1]), (fl46[0] + 1, fl46[1] + 1), (255, 0, 255), 2)
				cv2.imshow("Facial landmarks 68", img)

			return True

		elif self.__eye_detect_method == rasprec.EYES_FACIAL_LANDMARKS_5:
			# Modo de depuración para Facial Landmarks 5
			# Se fibujan los 5 puntos, remarcango los ojos y 
			# la línea que une ambos centros
			self.ed = LandmarkEyeDetector(self.__predictor5, self.__face)
			self.ed.detect()
			landmarks = self.ed.get_landmarks()

			# Nos fijamos en los puntos de referencia obtenidos de los ojos. 
			fl1 = landmarks[0]
			fl2 = landmarks[1]
			fl3 = landmarks[2]
			fl4 = landmarks[3]

			# Calculamos los centros de ambos ojos
			self.__x2 = int((fl1[0]+fl2[0])/2)
			self.__y2 = int((fl1[1]+fl2[1])/2)

			self.__x1 = int((fl3[0]+fl4[0])/2)
			self.__y1 = int((fl3[1]+fl4[1])/2)

			if rasprec.DEBUG_EYES_DETECTION in self.__debug:
				img = np.copy(self.__face)
				img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
				# Línea que une los centros de ambos ojos
				img = cv2.line(img, (self.__x1, self.__y1), (self.__x2, self.__y2), (0, 255, 255), 1)
				# Todos los landmarks
				for x, y in landmarks:
					img = cv2.rectangle(img, (x, y), (x + 1, y + 1), (0, 255, 0), 1)
				# Landmarks que corresponden a los extremos de los ojos
				img = cv2.rectangle(img, (fl1[0], fl1[1]), (fl1[0] + 1, fl1[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl2[0], fl2[1]), (fl2[0] + 1, fl2[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl3[0], fl3[1]), (fl3[0] + 1, fl3[1] + 1), (255, 0, 255), 2)
				img = cv2.rectangle(img, (fl4[0], fl4[1]), (fl4[0] + 1, fl4[1] + 1), (255, 0, 255), 2)
				cv2.imshow("Facial landmarks 5", img)
				cv2.waitKey(1)

			return True

		elif self.__eye_detect_method == rasprec.EYES_VIOLA:
			self.ed = HaarEyeDetector(self.__face)
			self.ed.detect(xml_file="HAARCASCADE_EYE")		

			# Si está el modo depuración se cuentan las detecciones
			if rasprec.DEBUG_COUNT_DETECTIONS in self.__debug:
				r = self.ed.right_eye_center
				l = self.ed.left_eye_center
				self.__debug_eye_detections['total'] += 1
				if not r is None:
					self.__debug_eye_detections['right'] += 1
				if not l is None: 
					self.__debug_eye_detections['left'] += 1
				if (not r is None) and (not l is None):
					self.__debug_eye_detections['both'] += 1

			# Guardamos las coordenadas centrales de los ojos
			if not (self.ed.right_eye_center is None or self.ed.left_eye_center is None):
				self.__x1, self.__y1 = self.ed.left_eye_center
				self.__x2, self.__y2 = self.ed.right_eye_center
				eyes_detected = True
			else:
				self.__x1 = None
				self.__y1 = None
				self.__x2 = None
				self.__y2 = None
				eyes_detected = False

			# A efectos de documentación se guardan imágenes de los puntos encontrados
			# Estas imágenes son:
			# 	self.__face_areas: se marcan las áreas donde se buscan los ojos
			#	self.__face_eyes:  se marcan las áreas donde se han encontrado ojos
			#	self.__face_full:  se marcan tanto donde se buscan como dónde se encuentran, así como 
			#					 la recta que une ambos centros
			if rasprec.DEBUG_EYES_DETECTION:

				self.__face_areas = cv2.cvtColor(self.__face, cv2.COLOR_GRAY2BGR)
				cv2.rectangle(self.__face_areas,
							  (self.ed.left_eye_x1, self.ed.left_eye_y1),
							  (self.ed.left_eye_x2, self.ed.left_eye_y2),
							  (255, 0, 0), 2)
				cv2.rectangle(self.__face_areas,
							  (self.ed.right_eye_x1, self.ed.right_eye_y1),
							  (self.ed.right_eye_x2, self.ed.right_eye_y2), 
							  (255, 0, 0), 2)
				self.__face_full = self.__face_areas

				# Dibujamos los ojos encontrados
				self.__face_eyes = cv2.cvtColor(self.__face, cv2.COLOR_GRAY2BGR)

				ler = self.ed.left_eye_rect
				if not ler is None:
					cv2.rectangle(self.__face_eyes, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)
					cv2.rectangle(self.__face_full, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)

				rer = self.ed.right_eye_rect
				if not rer is None:
					cv2.rectangle(self.__face_eyes, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)
					cv2.rectangle(self.__face_full, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)

				"""
				# Calculamos los puntos centrales
				if (not ler is None) and (not rer is None):
					lex = int(ler[0] + ((ler[1]-ler[0])/2))		# Left eye X
					ley = int(ler[2] + ((ler[3]-ler[2])/2))		# Left eye Y
					rex = int(rer[0] + ((rer[1]-rer[0])/2))		# Left eye X
					rey = int(rer[2] + ((rer[3]-rer[2])/2))		# Left eye Y
					cv2.line(self.__face_full, (lex, ley), (rex, rey), (0, 255, 255), 1)
				"""

				if eyes_detected:
					cv2.line(self.__face_eyes, (self.__x1, self.__y1), (self.__x2, self.__y2), (0, 255, 255), 1)
					cv2.rectangle(self.__face_eyes, (self.__x1, self.__y1), (self.__x1+1, self.__y1+1), (255, 0, 255), 2)
					cv2.rectangle(self.__face_eyes, (self.__x2, self.__y2), (self.__x2+1, self.__y2+1), (255, 0, 255), 2)

				cv2.imshow("Eyes Viola", self.__face_eyes)

			return eyes_detected


	def __geometric(self):
		""" Endereza la cara en función de la posición de los ojos

		Si se han detectado ambos ojos utiliza sus coordenadas para enderezar la cara
		"""

		# Calculamos la pendiente de la recta que une ambos ojos
		slope =  float(self.__y2-self.__y1)/(self.__x2-self.__x1)
		

		if not slope is None:
			if self.__verbose:
				print ("[INFO] Detectada pendiente de " + str(slope))
			# Primer paso: rotación de la imagen para que los ojos estén en la horizontal
			self.__face_geom_rotate = imutils.rotate_bound(self.__face, math.degrees(slope)*-1)

			# Segundo paso: se redimensiona la imagen para que los ojos se encuentren en las posiciones deseadas
			# Calculamos el ratio para que la distancia entre ambos ojos sea 0.68
			ratio = abs(float(self.__desired_right_eye_x - self.__desired_left_eye_x)/(self.__x2-self.__x1))
			if self.__width <= self.__height:
				self.__face_geom_scale = imutils.resize(self.__face_geom_rotate, width=abs(int(self.__width*ratio)))
			else: 
				self.__face_geom_scale = imutils.resize(self.__face_geom_rotate, height=abs(int(self.__height*ratio)))
				
			new_width = int(self.__width*ratio)
			new_height = int(self.__height*ratio)

			# Recortamos la imagen para que el ojo izquierdo se encuentre en (0.16, 0.14)
			new_x1 = self.__x1 * ratio
			new_y1 = self.__y1 * ratio

			crop_left =  int(new_x1 - self.__desired_left_eye_x)				# Lo que ha que recortar por la izquierda
			crop_top =   int(new_y1 - self.__desired_eyes_y)					# Lo que hay que recortar por arriba
			crop_right = int(new_width - self.__desired_width - crop_left)	# Lo que hay que recortar por la derecha
			crop_down =  int(new_height - self.__desired_height - crop_top) 	# Lo que hay que recortar por debajo

			# Hay ocasiones en que para centrar los ojos nos salimos de la cara. Con esto se soluciona aunque quede
			# ligeramente descentrado
			"""
			if new_height-crop_down-crop_top < 70:
				print "Corrigiendo"
				crop_down-=1
			"""
			if crop_down <= 0:
				crop_top += crop_down - 1
				crop_down = 1
			if crop_right <= 0:
				crop_left += crop_right - 1
				crop_right = 1

			#self.frame_geom_crop = self.frame_geom_scale[crop_left:self.__width-crop_right, crop_top:self.__width-crop_down]
			#self.frame_geom_crop = self.frame_geom_scale[crop_top:new_height-crop_down, crop_left:new_width-crop_right]
			self.__face_geom_crop = self.__face_geom_scale[crop_top:crop_top+self.__desired_height, crop_left:crop_left+self.__desired_width]
			
			if rasprec.DEBUG_GEOMETRIC in self.__debug:
				cv2.imshow("Rotación", self.__face_geom_rotate)
				cv2.imshow("Escalado", self.__face_geom_scale)
				cv2.imshow("Recortado", self.__face_geom_crop)

			     
	def __histogram_parts(self):

		""" Realiza el histograma por partes sobre la imagen
		"""
		# Se ecualiza la cara completa y también las mitades por separado
		self.__face_hist = cv2.equalizeHist(self.__face_geom_crop)
		face_left = self.__face_geom_crop[:, :int(self.__desired_width/2)]
		face_left_hist = cv2.equalizeHist(face_left)

		face_right = self.__face_geom_crop[:, int(self.__desired_width/2):]
		face_right_hist = cv2.equalizeHist(face_right)

		# Y se combinan
		# ---> Comparar velocidad con np.item y np.itemset
		div_left   = int(self.__desired_width*0.25)	# Punto donde comienza la transición entre izquierda y centro
		div_center = int(self.__desired_width*0.50)	# Punto central
		div_right  = int(self.__desired_width*0.75)	# Punto donde comienza la transición entre centro y derecha
		step = 1./(div_center-div_left)				# Cuánto se debe incrementar o decremenar blend en la parte central
		blend = 0									# Cuánto se deben mezclar las imágenes
		for x in range (0, self.__desired_width):
			# Calculamos el ratio de mezcla en cada columna
			if x < div_left or x > div_right:
				blend = 0
			else:
				blend += step * (-1 if x > div_center else 1)

			for y in range (0, self.__desired_height):
				# Parte izquierda
				if x < div_center:
					self.__face_hist[y, x] = int(face_left_hist[y, x]*(1. - blend) + self.__face_hist[y, x]*blend)
				else:
					self.__face_hist[y, x] = int(face_right_hist[y, x-div_center]*(1. - blend) + self.__face_hist[y, x]*blend)

		#return face_left, face_right, face_left_hist, face_right_hist, face_hist_tmp

		if rasprec.DEBUG_HISTOGRAM in self.__debug:
			cv2.imshow("Histograma por partes", self.__face_hist)



	def __histogram_parts_simple(self):

		""" Realiza el histograma por partes sobre la imagen dividiendo por mitades
		"""
		# Se ecualiza la cara completa y también las mitades por separado
		#self.__face_hist = cv2.equalizeHist(self.__face_geom_crop)
		
		face_left = self.__face_geom_crop[:, :int(self.__desired_width/2)]
		face_left_hist = cv2.equalizeHist(face_left)
		face_right = self.__face_geom_crop[:, int(self.__desired_width/2):]
		face_right_hist = cv2.equalizeHist(face_right)

		self.__face_hist = np.concatenate((face_left_hist, face_right_hist), axis=1)


		"""
		# Y se combinan
		# ---> Comparar velocidad con np.item y np.itemset
		div_left   = int(self.__desired_width*0.25)	# Punto donde comienza la transición entre izquierda y centro
		div_center = int(self.__desired_width*0.50)	# Punto central
		div_right  = int(self.__desired_width*0.75)	# Punto donde comienza la transición entre centro y derecha
		step = 1./(div_center-div_left)				# Cuánto se debe incrementar o decremenar blend en la parte central
		blend = 0									# Cuánto se deben mezclar las imágenes
		for x in range (0, self.__desired_width):
			# Calculamos el ratio de mezcla en cada columna
			if x < div_left or x > div_right:
				blend = 0
			else:
				blend += step * (-1 if x > div_center else 1)

			for y in range (0, self.__desired_height):
				# Parte izquierda
				if x < div_center:
					self.__face_hist[y, x] = int(face_left_hist[y, x]*(1. - blend) + self.__face_hist[y, x]*blend)
				else:
					self.__face_hist[y, x] = int(face_right_hist[y, x-div_center]*(1. - blend) + self.__face_hist[y, x]*blend)

		#return face_left, face_right, face_left_hist, face_right_hist, face_hist_tmp
		"""

		if rasprec.DEBUG_HISTOGRAM in self.__debug:
			cv2.imshow("Histograma por partes", self.__face_hist)
			cv2.waitKey(1)



	def __histogram_CLAHE(self):

		clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
		self.__face_hist = clahe.apply(self.__face_geom_crop)

		if rasprec.DEBUG_HISTOGRAM in self.__debug:
			cv2.imshow("Histograma CLAHE", self.__face_hist)
			cv2.waitKey(1)



	def __histogram_mirror(self):

		face_left = self.__face_geom_crop[:, :int(self.__desired_width/2)]
		face_left_hist = cv2.equalizeHist(face_left)
		face_mirror = np.flip(face_left_hist, 1)

		self.__face_hist = np.concatenate((face_left_hist, face_mirror), axis=1)

		if rasprec.DEBUG_HISTOGRAM in self.__debug:
			cv2.imshow("Histograma espejo", self.__face_hist)
			cv2.waitKey(1)


	def __smoothing(self):
		""" Aplica el suavizado sobre la imagen para eliminar los ruidos introducidos por el histograma.
		"""

		# Los valores son los indicados en el libro (hay que verificarlo y probar con otros tipos de filtro
		self.__face_smooth = cv2.bilateralFilter(self.__face_hist, 0, 20., 2.)

		if rasprec.DEBUG_SMOOTHING in self.__debug:
			cv2.imshow("Suavizado", self.__face_smooth)


	def __elliptical_mask(self):
		""" Aplica una máscara elíptica para eliminar los elementos periféricos.
		"""
		center = (int(self.__desired_width*0.5), int(self.__desired_height*0.4))
		size = (int(self.__desired_width * 0.5), int(self.__desired_height * 0.8))
		mask = np.zeros((self.__desired_width, self.__desired_height))
		mask = cv2.ellipse(mask, center, size, 0, 0, 360, 175, -1)
		self.__face_mask =  np.copy(self.__face_smooth)
		self.__face_mask[mask==0] = 96

		if rasprec.DEBUG_ELLIPTICAL in self.__debug:
			cv2.imshow("Máscara eliptica", self.__face_mask)

		self.__face_processed = self.__face_mask



	def get_times(self):

		return self.__all_times



	def load(self, face):
		""" Carga una imagen en el procesador de caras

		El procesador de caras tiene que realizar una serie de operaciones para preparar la cara
		para el reconocedor.
		Uno de los puntos más importantes de este procesamiento es intentar que todos los rasgos
		de todas las caras se encuentren en la misma posición, y esto se consigue localizando puntos
		clave de las caras, como por ejemplo los ojos, y aplicando transformaciones geométricas para
		que estos rasgos se encuentren siempre en las mismas posiciones.
		Esta función carga la imagen de la cara en el procesador y localiza los rasgos clave de la
		cara.
		Para ello por ahora hay implementados dos algoritmos:

			- Cascadas de Haar: utiliza el método de Viola Jones con cascadas de Haar para localizar
							    el centro de ambos ojos
							    El valor de method tiene que ser
							    	rasprec.EYES_VIOLA
			- Facial landmarks: utiliza Facial Landmarks para detectar 68 puntos clave de la cara. A 
								partir de estos puntos se puede obtener la posición central de los ojos
								y tal vez algún otro punto destacable (p.e. nariz?)
								El valor de method tiene que ser
									rasprec.EYES_FACIAL_LANDMARKS_68
								 	rasprec.EYES_FACIAL_LANDMARKS_5
		
		Parámetros:
			face - 	 imagen con la cara
			method - método que se va a utilizar para detectar los ojos. 
					 Valores aceptados: "Processor.EYES_HAAR" y "Processor.EYES_FACIAL_LANDMARKS"

		Devuelve:
			ATENCIÓN: los datos que devuelve esta función varían según el método utilizado ya que solo 
			se utilizan para la representación en pantalla de los resultados obtenidos.

			HAAR - Devuelve centers, rects donde centers son las coordenadas de los ojos encontrados de
				   la forma (x1, y1, x2, y2) y rects son las coordenadas de las zonas donde buscó los ojos

		"""

		if (len(face.shape)) == 3:	# Si está en RGB
			face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
		self.__face = face
		self.__width, self.__height = face.shape

		t0 = time.process_time()
		eyes_detected = self.__detect_eyes()

		if eyes_detected:
			t1 = time.process_time()
			self.__geometric()
			t2 = time.process_time()

			# Hay veces que detecta mal los ojos y aplica muy mal el recorte
			if self.__face_geom_crop.shape != (70, 70):
				return None
			if self.__histo == rasprec.HISTO_PARTS:
				self.__histogram_parts()
			elif self.__histo == rasprec.HISTO_CLAHE:
				self.__histogram_CLAHE()
			elif self.__histo == rasprec.HISTO_PARTS_SIMPLE:
				self.__histogram_parts_simple()
			elif self.__histo == rasprec.HISTO_MIRROR:
				self.__histogram_mirror()
			else:
				self.__face_hist = self.__face_geom_crop
			t3 = time.process_time()
			self.__smoothing()
			self.__elliptical_mask()
			t4 = time.process_time()
			self.__all_times['detect_eyes'] = t1-t0
			self.__all_times['geometric'] = t2-t1
			self.__all_times['histogram'] = t3-t2
			self.__all_times['smooth_mask'] = t4-t3

			return self.__face_processed
		else:
			return None



			#return ((self.__x1, self.__y1), (self.__x2, self.__y2)), None, landmarks


		"""

		if method == "HAAR":
			self.ed = HaarEyeDetector(self.__face)
			self.ed.detect(xml_file="HAARCASCADE_EYE")		

			# A efectos de documentación se guardan imágenes de los puntos encontrados
			# Estas imágenes son:
			# 	self.__face_areas: se marcan las áreas donde se buscan los ojos
			#	self.__face_eyes:  se marcan las áreas donde se han encontrado ojos
			#	self.__face_full:  se marcan tanto donde se buscan como dónde se encuentran, así como 
			#					 la recta que une ambos centros
			if self.__debug == True:
				self.__face_areas = cv2.cvtColor(self.__face, cv2.COLOR_GRAY2BGR)
				cv2.rectangle(self.__face_areas,
							  (self.ed.left_eye_x1, self.ed.left_eye_y1),
							  (self.ed.left_eye_x2, self.ed.left_eye_y2),
							  (255, 0, 0), 2)
				cv2.rectangle(self.__face_areas,
							  (self.ed.right_eye_x1, self.ed.right_eye_y1),
							  (self.ed.right_eye_x2, self.ed.right_eye_y2), 
							  (255, 0, 0), 2)
				self.__face_full = self.__face_areas

				# Dibujamos los ojos encontrados
				self.__face_eyes = cv2.cvtColor(self.__face, cv2.COLOR_GRAY2BGR)

				ler = self.ed.left_eye_rect
				if not ler is None:
					cv2.rectangle(self.__face_eyes, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)
					cv2.rectangle(self.__face_full, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)

				rer = self.ed.right_eye_rect
				if not rer is None:
					cv2.rectangle(self.__face_eyes, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)
					cv2.rectangle(self.__face_full, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)

				# Calculamos los puntos centrales
				if (not ler is None) and (not rer is None):
					lex = int(ler[0] + ((ler[1]-ler[0])/2))		# Left eye X
					ley = int(ler[2] + ((ler[3]-ler[2])/2))		# Left eye Y
					rex = int(rer[0] + ((rer[1]-rer[0])/2))		# Left eye X
					rey = int(rer[2] + ((rer[3]-rer[2])/2))		# Left eye Y
					cv2.line(self.__face_full, (lex, ley), (rex, rey), (0, 255, 255), 1)


			# El algoritmo puede encontrar 1, 2 o ningún ojo:
			# 	- Si se han encontrado los dos ojos se puede realizar el procesamiento de la imagen.
			#   - Si se encuentra un único ojo se presupone que la posición del otro es simétrica y
			#	  teniendo en cuenta esta asunción se continúa con el procesamiento
			#	- Si no se ha detectado ningún ojo no se puede realizar ningún tipo de procesamiento
			if self.ed.detected == 2:
				centers = self.ed.get_eyes_centers()
				(self.__x1, self.__y1), (self.__x2, self.__y2) = centers
				self.eyes_detected = True
				# Se puede realizar el procesado completo por lo que se lanza desde aquí
				self.full_processing()
				areas = self.ed.get_eyes_area()
				return centers, areas, None
			elif self.ed.detected == 1:
				# Si se detecta un único ojo se presupone que la posición del otro es simétrica.
				lec, rec = self.ed.get_eyes_centers()	# Uno de los 2 es None
				if lec is None:
					# Se han localizado las coordenadas del ojo derecho
					self.__x2 = rec[0]
					self.__y2 = rec[1]
					self.__x1 = self.__width - rec[0]	# Deducimos la posición simétrica
					self.__y1 = rec[1]				# y misma altura
				else:
					# Se han localizado las coordenadas del ojo izquierdo
					self.__x1 = lec[0]
					self.__y1 = lec[1]
					self.__x2 = self.__width - lec[0]	# Deducimos la posición simétrica
					self.__y2 = lec[1]				# y la misma altura

				self.full_processing()
				areas = self.ed.get_eyes_area()
				return [(self.__x1, self.__y1), (self.__x2, self.__y2)], areas, None

			else:
				# Si se ha detectado 1 o ningún ojo
				self.eyes_detected = False
				areas = self.ed.get_eyes_area()
				return None, areas, None
		elif method == "FACIAL_LANDMARKS":
			self.ed = LandmarkEyeDetector(self.predictor, self.__face)
			self.ed.detect()
			landmarks = self.ed.get_landmarks()

			# Nos fijamos en los puntos de referencia obtenidos de los ojos. Cada ojo tiene
			# 6 puntos de referencia de los que nos quedamos los del extremo exterior e interior.
			# Son los valores 37 y 40 para el ojo izquierdo y 43 y 46 para el ojo derecho
			fl37 = landmarks[36]
			fl40 = landmarks[39]
			fl43 = landmarks[42]
			fl46 = landmarks[45]

			# Calculamos los centros de ambos ojos
			self.__x1 = int((fl37[0]+fl40[0])/2)
			self.__y1 = int((fl37[1]+fl40[1])/2)
			self.__x2 = int((fl43[0]+fl46[0])/2)
			self.__y2 = int((fl43[1]+fl46[1])/2)

			self.full_processing()

			return ((self.__x1, self.__y1), (self.__x2, self.__y2)), None, landmarks

		"""