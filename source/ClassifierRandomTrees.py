"""
	ClassifierRandomTrees.py

	Implementa el clasificador Random Forests


	Parámetros
	----------


"""

import cv2
from Classifier import Classifier
from Provider import Provider
import numpy as np

"""
	REFERENCIA DE RANDOM TREES: https://docs.opencv.org/3.4.3/d0/d65/classcv_1_1ml_1_1RTrees.html
"""

class ClassifierRandomTrees(Classifier):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, features=None, params=None, verbose=False):
		Classifier.__init__(self, features, params, verbose)

		self.rt = None					# Clasificador Random Trees
		
		self.faces_descriptors = []		# Descriptores de las caras de entrenamiento
		self.nfaces_descriptors = []	# Descriptores de las no caras de entrenamiento

		if params:
			self.set_params(params)
		else:
			self.__initialize()

		if self.verbose:
			print ("[INFO] Se ha inicializado la clase ClassifierRandomTrees.")


	def __initialize(self):

		self.bayes = cv2.ml.NormalBayesClassifier_create()
		#self.bayes = cv2.NormalBayesClassifier()


	def set_params(self, params):
		# TODO: Mirar a ver si tiene parámetros
		if 'neighs' in params:
			self.neighs = params['neighs']

		self.__initialize()



	def get_params(self):
		return self.params




	def train(self):
		""" Entrena el clasificador a partir de los datos precargados

		Entrena el clasificador Bayes tomando los datos que tienen que haber sido cargados
		previamente con la función load_data().

		Devuelve:
			response
				Array de respuesta, cada elemento corresponde con un vector de características
				de train_data e indica si es cara (1.) o no cara (0.)
			train_data
				Array con los vectores de características de las imágenes de entrenamiento

		"""

		num_faces_train = len(self.faces_descriptors)
		num_nfaces_train = len(self.nfaces_descriptors)

		# Cargamos el clasificador. 1 caras, 0 no caras
		response = np.concatenate((np.ones((num_faces_train), dtype=np.int32),
								   np.zeros((num_nfaces_train), dtype=np.int32)))
		train_data=np.concatenate((self.faces_descriptors, self.nfaces_descriptors), axis=0)

		if self.verbose:
			print ("[INFO] Entrenando el clasificador...")
		b = self.bayes.train(train_data, cv2.ml.ROW_SAMPLE, response)
		if self.verbose:
			print ("[INFO] Se ha entrenado el clasificador")

		if not b:
			print ("[ERROR] Ha habido un problema al entrenar el clasificador.")		

		self.is_trained = True
		self.num_trained_faces = num_faces_train
		self.num_trained_nfaces = num_nfaces_train
		self.num_dimensions = train_data[0].shape[0]

		if self.verbose:
			print ("[INFO] Entrenado el clasificador Bayes. " + \
				str(self.num_trained_faces) + " caras y " + \
				str(self.num_trained_nfaces) + " no caras con " + \
				str(self.num_dimensions) + " dimensiones.")

		return response, train_data



	def train_and_save(self, filename=None):

		raise NotImplementedError()




	def load_training(self, filename="train_files/default.svm"):
		
		raise NotImplementedError()


	def is_face(self, image):
		if self.is_trained:
			# Se ha pasado el vector de características
			if type(image) == np.ndarray:
				# Bayes recoge un array de vectores de características. Como solo queremos evaluar
				# una cara debemos crear el array y cargar el vector pasado como primer (y único)
				# elemento del mismo
				samples = np.asarray([image])

				retval, results = self.svm.predict(samples)

				print ("retval: " + str(retval))
				print ("results: " + str(results))
				"""
					predict devuelve un array con 3 datos:
						- Un float que no se lo que es (por ahora es 0.0)
						- Array con las predicciones
						- Tipo de datos de las predicciones (dtype=float32) 
				"""


				return int(np.ravel(ret)[0])
				"""
					predict devuelve un array con 3 datos:
						- Un float que no se lo que es (por ahora es 0.0)
						- Array con las predicciones
						- Tipo de datos de las predicciones (dtype=float32) 
				"""


	def are_faces(self, images):
		""" Evalúa un conjunto de imágenes para verificar si son caras

		Se le pasa un conjunto de imágenes y las evalúa para predecir si corresponden a caras o no caras

		Parámetros:
			images
				Array donde cada elemento es un array que corresponde a una imagen

		Devuelve:
			Array en el que cada elemento corresponde a cada una de las imágenes que se le han pasado
			y que es 0 si predice que la imagen no es cara y 1 si corresponde a una cara.

		"""

		if self.is_trained:
			if isinstance(images, Provider):
				images = images.get_array()
				samples = []
				for i in images:
					img = cv2.imread(i)
					feat = self.features.get_features(img)
					samples.append(feat)

				samples = np.asarray(samples)

				ret = self.bayes.predict(samples)

				ret = np.ravel(ret[1])
				num_faces = np.count_nonzero(ret == 1)

				return num_faces


				"""

				ret, results, neighbours, dist = self.knn.findNearest(samples, 35)
				
				if threshold != 0:
					results = []
					for r in neighbours:
						if np.count_nonzero(r == 1) >= threshold:
							results.append([1.])
						else:
							results.append([0.])
					results = np.asarray(results)

				c = np.count_nonzero(results == 1)
				t = results.shape[0]
				# print ("Total: " + str(c) + " de " + str(t))
				return c
				"""
