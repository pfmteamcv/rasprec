from Detector import Detector
from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN


class DetectorHOGkNN(Detector):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, params = None, verbose=False, train_file=None):

		Detector.__init__(self, verbose)

		self.features = FeaturesHOG(verbose=self.verbose)
		self.classifier = ClassifierkNN(verbose=self.verbose)

		if self.verbose:
			print ("[INFO] Sa ha inicializado la clase DetectorHOGkNN.")
			print ("[INFO] Clasificador del tipo " + str(type(self.classifier)) + ".")
			print ("[INFO] Características del tipo " + str(type(self.features)) + ".")


		# Cargamos los parámetros por defecto en el clasificador y el generador de características
		self.classifier.set_params({
			"neighs": 1
			})
		self.features.set_params({
			"win_size": (70, 70),
			"block_size": (14, 14),
			"block_stride": (7, 7),
			"cell_size": (7, 7),
			"nbins": 9
			})

		if not train_file is None:
			# Si se le pasa un fichero de entrenamiento implica que se debe entrenar
			self.classifier.load_training(train_file)
			if self.verbose:
				print ("[INFO] Se ha cargado el fichero de entrenamiento " + str(train_file))
