""" Clase base para la la implementación del clasificador

Esta clase contiene los métodos virtuales y métodos reutilizables por todas las
clases que hereden de ella. Pretende establecer la estructura común que deberán
tener todos los clasificadores.

Parámetros
----------
	features : Features
		Generador de características que utilizará el clasificador. Tiene que pertenecer a una clase
		hijo de Classifier.
	params : dict
		Diccionario con los parámetros con que se quiere configurar el clasificador, en el caso
		de que algún parámetro no se especifique se utilizarán los parámetros por defecto.
	verbose: boolean
		Muestra información extendida por pantalla.

Atributos
---------
	features : Features
		Generador de características
	faces_descriptors : list
		Lista donde se guardarán los vectores de características de las caras de entrenamiento
	nfaces_descriptors : list
		Lista donde se guardarán los vectores de características de las no caras de entrenamiento
	is_trained : boolean
		Indica si el clasificador ya ha sido entrenado
	num_dimensions : int
		Número de dimensiones de los vectores de características. Este valor depende de los parámetros
		escogidos para el generador de características
	num_trained_faces : int
		Número de caras del conjunto de entrenamiento
	num_trained_nfaces : int
		Número de no caras del conjunto de entrenamiento
	path : string
		Ruta por defecto donde se guardarán los ficheros de entrenamiento
	default_filename : string
		Nombre por defecto del fichero de entrenamiento

Métodos virtuales
-----------------
	__initialize()
		Inicializa el clasificador

	set_params()
		Recoge los parámetros a aplicar al clasificador y los aplica

	get_params()
		Devuelve un diccionario con los parámetros del clasificador

	train()
		Entrena el clasificador. Requiere que se hayan cargado ya las imágenes. 

	train_and_save(filename)
		Entrena el clasificador y guarda el entrenamiento en un fichero.un

	load_training(filename)
		Carga el entrenamiento del clasificador de un fichero

	is_face(image)
		Devuelve 1 si la imagen corresponde a una cara y 0 en caso contrario

	are_faces(images)
		Recoge un array de imágenes y devuelve una lista con 1s para las caras y 0s para
		las no caras


Métodos
-------
	load_data(self, faces, nfaces, num_faces, num_nfaces)
		Carga los datos para el entrenamiento del clasificador

"""


from abc import ABCMeta
from abc import abstractmethod
from ProviderDataset import ProviderDataset
from Provider import Provider
from pathlib import Path
import cv2
import time


class Classifier:
	__metaclass__ = ABCMeta

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, features=None, params=None, verbose=False):

		self._verbose = verbose			# Si es True muestra más información por pantalla

		self._features = features
		self._faces_descriptors = []			# Descriptores de caras que se han cargado
		self._nfaces_descriptors = []			# Descriptores de no caras que se han cargado
		self._is_trained = False				# Indica si está entrenado o no
		self._num_dimensions = None 			# Núm. de dimensiones de los vectores de características
		self._num_trained_faces = 0			# Núm. de imágenes de caras con que se ha entrenado
		self._num_trained_nfaces = 0			# Núm. de imágenes de no caras con que se ha entrenado
		self._path = str(Path.home()) + "/TFM/rasprec/source/train_files/"		# Ruta por defecto donde se guardan los ficheros
		self._default_filename = "default" 	# Nombre por defecto del fichero

		num_faces = 0	# Número de caras que se han cargado 
		num_nfaces = 0  # Número de no caras que se han cargado

		if self._verbose:
			print ("[INFO] Se ha inicializado la clase Classifier.")
		


	# -------------------- METODOS VIRTUALES --------------------
	
	@abstractmethod
	def __initialize(self):
		raise NotImplementedError()


	@abstractmethod
	def set_params(self, params):
		raise NotImplementedError()


	@abstractmethod
	def get_params(self):
		raise NotImplementedError()


	@abstractmethod
	def train(self):
		raise NotImplementedError()


	@abstractmethod
	def train_and_save(self, filename=None):
		raise NotImplementedError()


	@abstractmethod
	def load_training(self, filename=None):
		raise NotImplementedError


	@abstractmethod
	def is_face(self, image):
		raise NotImplementedError


	@abstractmethod
	def evaluate(self, images):
		raise NotImplementedError


	@abstractmethod
	def get_num_of_faces(self, images):
		raise NotImplementedError


	# --------------------- METODOS PÚBLICOS _---------------------


	def set_features(self, features):
		""" Carga el generador de características que utilizará el clasificador

		"""

		self._features = features
		

	def load_data(self, faces=None, nfaces=None, num_faces=0, num_nfaces=0, preprocessor=None):
		"""Carga las imágenes para el posterior entrenamiento.

		Recoge las imágenes de caras y de no caras para el entrenador y genera los
		descriptores de las mismas. 
		Las imágenes se pueden proporcionar de diferentes formas:
			* Lista donde cada elemento es la ruta absoluta de una imagen
			* Un elemento de la clase ProviderDataset
		Se puede indicar el número de imágenes que se cargarán. Por ahora carga las
		n primeras imágenes.
		TODO: permitir que cargue n imágenes aleatorias

		Parámetros:
			faces      		-- Imágenes de caras
			nfaces     		-- Imágenes de no caras
			num_faces  		-- Número de imágenes de caras que se cargarán
			num_nfaces 		-- Número de imágenes de no caras que se cargarán
			preprocessor	-- Si es necesario realizar algún procesamiento sobre las imágenes

		Devuelve:
			Nada

		"""
		# Se debe pasar una lista de rutas de las imágenes
		if isinstance(faces, Provider):
			faces = faces.get_array()
		if isinstance(nfaces, Provider):
			nfaces = nfaces.get_array()

		

		if self._verbose:
			print ("[INFO] Comienza la carga de " + str(len(faces)) + " caras de entrenamiento.")
		# Se cargan las caras
		n = 0
		if faces:
			for i in faces:
				if num_faces != 0 and n >= num_faces:
					break
				n += 1
				img = cv2.imread(i)
				if not preprocessor is None:
					img = preprocessor.prepare(img)
				feat = self._features.get_features(img)
				self._faces_descriptors.append(feat)

		if self._verbose:
			print ("[INFO] Se han cargado las caras en el clasificador.")

		if self._verbose:
			print ("[INFO] Comienza la carga de " + str(len(nfaces)) + " NO caras de entrenamiento.")
		# Se cargan las no caras
		n = 0
		if nfaces:
			for i in nfaces:
				if num_faces != 0 and n >= num_nfaces:
					break
				n += 1
				img = cv2.imread(i)
				if not preprocessor is None:
					img = preprocessor.prepare(img)
				feat = self._features.get_features(img)
				self._nfaces_descriptors.append(feat)
		if self._verbose:
			print ("[INFO] Se han cargado las NO caras en el clasificador.")



	def precision_and_recall(self, faces, nfaces, preprocessor=None):
		"""
			Evalúa el rendimiento del clasificador según la métrica de
			precisión y exhaustividad.
			Parámetros:
				- faces: imágenes de caras (clase Provider)
				- nfaces: imágenes de no caras (clase Provider)
				- preprocessor: preprocesador para las imágenes
		"""


		if self._is_trained:

			# Preparamos las caras
			faces = faces.get_array()
			ppfaces = []
			for f in faces:
				img = cv2.imread(f)
				if not preprocessor is None:
					img = preprocessor.prepare(img)
				ppfaces.append(img)

			# Preparamos las no caras
			nfaces = nfaces.get_array()
			ppnfaces = []
			for nf in nfaces:
				img = cv2.imread(nf)
				if not preprocessor is None:
					img = preprocessor.prepare(img)
				ppnfaces.append(img)

			# Realizamos las predicciones
			t1 = time.perf_counter()
			pred_faces = self.are_faces(ppfaces)
			pred_nfaces = self.are_faces(ppnfaces)
			t2 = time.perf_counter()
			
			# Generamos los valores de la métrica
			total_faces = len(pred_faces)
			total_nfaces = len(pred_nfaces)
			total_images = total_faces + total_nfaces
			time_per_image = (t2 - t1) * 1000 / total_images
			
			tp = pred_faces.count(True)
			tn = pred_nfaces.count(False)
			fp = total_nfaces - tn
			fn = total_faces - tp
			precision = tp / (tp + fp)
			recall = tp / (tp + fn)
			accuracy = (tp + tn) / total_images

			data = {
				'total_faces': total_faces,
				'total_nfaces': total_faces,
				'total_images': total_images,
				'tp': tp,
				'tn': tn,
				'fp': fp, 
				'fn': fn,
				'precision': precision,
				'recall': recall,
				'accuracy': accuracy,
				'time_per_image': time_per_image
			}

			return data



