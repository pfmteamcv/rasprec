import os
from pathlib import Path
from Provider import Provider
import cv2
import sys

class ProviderDataset(Provider):
	def __init__(self, dataset, num_images=0):
		"""
			num_images es el número de imágenes que se tomarán del dataset. Si es 0
			se toman todas las imágenes.
		"""
		self.__path = None		# Ruta del dataset que se utilizará
		self.__files = None		# Listado de imágenes del dataset sobre las que se itera
		self.__length = None		# Número de imágenes en el dataset
		self.__actual = 0

		datasets = {"bioid": "/TFM/datasets/BioID/",
					"yale":  "/TFM/datasets/Yale/faces/",
					"helen": "/TFM/datasets/HELEN/images/",
					"mit":   "/TFM/datasets/MIT_CBCL/train/face/",
					"caltech_back": "/TFM/datasets/caltech/backgrounds/",
					"stanford_bg": "/TFM/datasets/stanford/backgrounds/",
					"training_faces": "/TFM/datasets/train/faces/",
					"training_nfaces": "/TFM/datasets/train/nfaces/",
					"ciberextruder": "/TFM/datasets/Ciberextruder/",
					"training_faces_rect": "/TFM/datasets/train_75x56/faces/",	# Ojo, necesitan redimensionado, están a 75x56
					"training_nfaces_rect": "/TFM/datasets/train_75x56/nfaces/"
					}
		
		dataset=dataset.lower()
		if dataset in datasets:
			home = str(Path.home())
			self.__path = home + datasets[dataset]
		else:
			print ("[ERROR] No se reconoce el dataset indicado.")
			sys.exit()

		# Guardamos la ruta de todas las imágenes
		lst_dir = os.walk(self.__path)

		self.__files = []
		num = 0
		for root, dirs, files in lst_dir:
			for f in files:
				if f[-3:] in ["pgm", "jpg", "png"]:
					if num_images == 0 or num < num_images:
						self.__files.append(root+f)
						num += 1
		self.__length = len(self.__files)



	def get_array(self):
		"""Devuelve el array con todas las direcciones
		"""
		return self.__files


	def len(self):
		"""Devuelve el número de imágenes en el dataset
		"""
		return self.len


	def __iter__(self):
		self.__actual=0
		return self


	def __next__(self):
		
		if self.__actual >= len(self.__files):
			raise StopIteration()
		else:
			i = self.__files[self.__actual]
			self.__actual += 1
			return i

			
"""

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
lst_dir = os.walk(path + "01_preprocessed/")
detector = HaarDetector()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)
		detected_image = detector.detect(image)
"""