""" Proveedor de imágenes para entrenamiento y prueba

El objetivo de esta clase es crear un iterador sobre las imágenes de un dataset que se utilizarán 
para el entrenamiento de un algoritmo y otro para, sobre el mismo dataset, que itere sobre las
imágenes de prueba. La idea es que ambos conjuntos de imágenes sean disjuntos para que el
clasificador no arroje falsos resultados.

Para ello, es necesario crear dos instancias de esta clase, proporcionando una el iterador para el
conjunto de entrenamiento y la otra el del conjunto de prueba. Para ello:
	- El valor del parámetro seed del constructor de ambas instancias debe ser el mismo.
	- El valor del parámetro train del constructor debe ser True en el caso del iterador para el 
	  entrenamiento y False para el iterador de pruebas.

Parámetros
----------
dataset : str
	Nombre de la base de datos de imágenes que se utilizará
seed : int
	Semilla que se utilizará en el generador pseudoaleatorio para escoger aleatoriamente las imágenes
	para entrenamiento y prueba. El mismo valor siempre generará el mismo resultado.
train_percent : float
	Valor entre 0. y 1. que indica el porcentaje de imágenes que se utilizarán para el entrenamiento.
	Este valor se aplica sobre el número de imágenes indicado mediante el parámetro num_images, no el 
	número de imágenes totales en el dataset
num_images : int
	Número de imágenes del dataset que se van a utilizar. Un valor de 0 indica que se utilizarán todas
	las imágenes disponibles en el dataset
train : boolean
	Un valor de True indica que el iterador devolverá las imágenes seleccionadas para el entrenamiento, 
	mientras que un valor de False devolverá las imágenes para prueba.

Atributos
---------


Métodos
-------
get_files()
	Recorre los directorios de imágenes de entrenamiento y carga todas las imágenes
needs_trainig() : boolean
	Indica si el clasificador requiere entrenamiento o no

"""


from ProviderDataset import ProviderDataset
import numpy as np

class ProviderDatasetTrainTest (ProviderDataset):
	
	def __init__(self, dataset, seed, train_percent, num_images, train, verbose=False):
		""" Inicializa la clase

		Parametros:
			- dataset: conjunto de imágenes utilizado. Referencia en la clase padre
			- seed: seed, semilla, imprescindible para que funcione bien
			- train_percent: porcenaje de imágenes que se tomarán para entrenamiento (entre 0 y 1)
			- num_images: número de imágenes que se tomarán del dataset
			- train: booleano que indicará si se van a devolver las imágenes de entrenamiento o de test.
		"""
		ProviderDataset.__init__(self, dataset, num_images)

		# Inicializamos el generador de números pseudoaleatorios
		np.random.seed(seed)

		num_train = int(num_images * train_percent)
		num_test = num_images - num_train

		tr = np.ones(num_train, dtype=np.uint32)
		ts = np.zeros(num_test, dtype=np.uint32)

		resp = np.concatenate((tr, ts))
		np.random.shuffle(resp)

		if train:
			self.files=np.asarray(self.files)[np.asarray(resp)==1].tolist()
			self.len=num_train 
		else:
			self.files=np.asarray(self.files)[np.asarray(resp)==0].tolist()
			self.len=num_test

		if verbose:
			print ("[INFO] Se ha inicializado la clase ProviderDatasetTrainTest.")
		