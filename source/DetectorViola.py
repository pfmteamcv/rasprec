"""
	Wrapper para el detector Viola Jones de OpenCV
"""


from Detector import Detector
import cv2
from pathlib import Path

class DetectorViola(Detector):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, classifier_file=None, verbose=False, params=None, scale_factor=None,
		min_neighbors=None, min_size=None):

		Detector.__init__(self, verbose)

		if classifier_file == None:
			self.__classifier_file = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'
		else:
			self.__classifier_file = classifier_file

		self.face_cascade = cv2.CascadeClassifier(self.__classifier_file)

		# Valores por defecto para los parámetros
		self.__scale_factor = 1.1
		self.__min_neighbors = 3
		self.__min_size = (0, 0)

		self.set_params(params=params, scale_factor=scale_factor, min_neighbors=min_neighbors, min_size=min_size)

		"""
		# Los valores por defecto de OpenCV son:
		self.params = {
			'file':			 None,
			'scale_factor':  1.1,
			'min_neighbors': 3,
			'min_size':		 (0, 0),
			'max_size'		 None		# Este se puede ignorar
		}
		"""

	# --------------------- METODOS HEREDADOS ---------------------

	def set_params(self, classifier_file=None, scale_factor=None, min_neighbors=None, min_size=None, params=None):

		if not classifier_file is None:
			self.__classifier_file = classifier_file
			self.face_cascade = cv2.CascadeClassifier(self.__classifier_file)
		if not scale_factor is None:
			self.__scale_factor = scale_factor
		if not min_neighbors is None:
			self.__min_neighbors = min_neighbors
		if not min_size is None:
			self.__min_size = min_size

		if not params is None:
			if 'classifier_file' in params:
				self.__classifier_file = params['classifier_file']
				self.face_cascade = cv2.CascadeClassifier(self.__classifier_file)
			if 'scale_factor' in params:
				self.__scale_factor = params['scale_factor']
			if 'min_neighbors' in params:
				self.__min_neighbors = params['min_neighbors']
			if 'min_size' in params:
				self.__min_size = params['min_size']


	def set_debug(self, debug):
		pass


	def detect_multiscale(self, image):
		"""
			Devuelve lista de rectángulos con coordenadas de la forma (x1, y1, x2, y2)
		"""

		faces = self.face_cascade.detectMultiScale(
									image, 
									scaleFactor=self.__scale_factor,
									minNeighbors=self.__min_neighbors, 
									minSize=self.__min_size)

		rects = []
		# Por algún motivo, en ocasiones devuelve rectángulos solapados que corresponden a la misma cara
		# Hay que eliminar estos rectángulos solapados
		if len(faces) > 1: 
			i = 0
			while i < len(faces):
				overlaps = False
				x, y, w, h = faces[i]
				cx = int(x + (w/2))
				cy = int(y + (h/2))
				for xr, yr, wr, hr in rects:
					if (cx > x) and (cx < x + w) and (cy > y) and (cy < y + h):
						overlaps = True
				if not overlaps:
					rects.append ( (x, y, x+w, y+h))
				i += 1
		else:
			for x, y, w, h in faces:
				rects.append( (x, y, x+w, y+h))

		return rects

"""
		faces = face_cascade.detectMultiScale(gray, 1.3, 5)
for (x,y,w,h) in faces:
    cv.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
        cv.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
cv.imshow('img',img)
cv.waitKey(0)
cv.destroyAllWindows()

"""