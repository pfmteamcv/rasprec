# Módulo para las variables

# Opciones de depuración
# ----------------------
DEBUG_HEATMAP = 1				# Muestra imagen con los mapas de calor
DEBUG_EYES_DETECTION = 2		# Muestra imagen con la detección de los ojos
DEBUG_BOUNDING_BOXES = 3		# Muestra imagen con todos los positivos solapados de la imagen
DEBUG_FPS = 4					# 
DEBUG_EYES = 5
DEBUG_GEOMETRIC = 6				# Muestra imagen tras rotar, recortar y escalar en procesado
DEBUG_HISTOGRAM = 7				# Muestra imagen tras aplicar histograma por partes
DEBUG_SMOOTHING = 8				# Muestra imagen tras aplicar el suavizado
DEBUG_ELLIPTICAL = 9			# Muestra imagen tras aplicar máscara elíptica
DEBUG_FULL_PROCESS	= 10		# Combina los cuatro pasos anteriores en una imagen
DEBUG_COUNT_DETECTIONS = 11		# Cuenta el número de detecciones de ojos con Viola Jones


EYES_VIOLA = 20					# Utiliza Viola-Jones para la detección de los ojos
EYES_FACIAL_LANDMARKS_68 = 21	# Utiliza Facial Landmarks con 68 puntos para la detección de los ojos
EYES_FACIAL_LANDMARKS_5 = 22	# Utiliza Facial Landmarks con 5 puntos para la detección de los ojos

FUSION_HEATMAP 	= 30			# Modo de fusión para el detector basado en mapas de calor
FUSION_NMS 		= 31			# Modo de fusión para el detector basado en Non Maxima Suppression

# Normalización en preprocesamiento
NORM_HISTO     	= 40			# Ecualización de histograma
NORM_RANGE     	= 41			# Normalización de rango
NORM_CLAHE     	= 42			# Ecualización de histograma adaptativo
GRAY_EQUAL     	= 43			# Conversión a gris con mismo peso en todos los canales
GRAY_VALUE     	= 44			# Conversión a gris tomando el canal valor de HSV
GRAY_WEIGHTED 	= 45			# Conversión a gris con pesos ponderados en cada canal

HISTO_PARTS		= 50			# Histograma por partes en el procesamiento
HISTO_CLAHE		= 51			# Histograma CLAHE en el procesamiento
HISTO_NONE		= 52			# Ningún histograma en el procesamiento
HISTO_PARTS_SIMPLE = 53			# Histograma por partes sin fusionar
HISTO_MIRROR	= 54			# Se refleja la mitad de la cara

# Identificadores de los datasets
BIOID 			= 'bioid'
YALE			= 'yale'
HELEN			= 'helen'
MIT				= 'mit'
CALTECH_BACK	= 'caltech_back'
STANFORD_BG		= 'stanford_bg'
TRAINING_FACES 	= 'training_faces'
TRAINING_NFACES	= 'training_nfaces'