import imutils
import cv2
from abc import ABCMeta
from abc import abstractmethod
import numpy as np
import sys
import rasprec


class Preprocessor:
	__metaclass__ = ABCMeta
	"""
	Clase base para el preprocesador de imágenes
	"""

	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, verbose=False, gray=rasprec.GRAY_VALUE,
		norm=None,face_size=None, width=480):

		self.width = width
		self.gray = gray
		self.norm = norm

		if face_size is None:
			self.face_size = (70, 70)
		else:
			self.face_size = face_size

		self.verbose = verbose
		if self.verbose:
			print ("[INFO] Inicializado el preprocesador.")



	def adjust_color(self, img):

		# Conversión a escala de grises
		if img.ndim == 3:
			if self.gray == rasprec.GRAY_EQUAL:
				img = img.astype(np.int16)
				img = (img[:,:,0] + img[:,:,1] + img[:,:,2]) / 3
				img = img.astype(np.uint8)
			elif self.gray == rasprec.GRAY_WEIGHTED:
				img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			elif self.gray == rasprec.GRAY_VALUE:
				i = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
				img = i[:,:,2]
			else:
				print ("[ERROR] Parámetro inválido.")
				sys.exit()

		# Normalización de la imagen
		if self.norm == rasprec.NORM_HISTO:
			img = self.hist_norm(img)
		elif self.norm == rasprec.NORM_RANGE:
			img = self.range_norm(img)
		elif self.norm == rasprec.NORM_CLAHE:
			img = self.clahe(img)

		return img



	def range_norm(self, image):

		img = (image - np.amin(image))/(np.amax(image) - np.amin(image)) * 255.
		img = img.astype(np.uint8)
		return img


	def hist_norm(self, image):

		img = cv2.equalizeHist(image)
		return img


	def clahe(self, image, clip_limit=2.0, tile_grid_size=(8, 8)):
		clahe = cv2.createCLAHE(clipLimit=clip_limit, tileGridSize=tile_grid_size)
		cl1 = clahe.apply(image)
		return cl1



	# -------------------- METODOS VIRTUALES --------------------
	
	@abstractmethod
	def prepare(self, image):
		raise NotImplementedError()





class PreprocessorBasic(Preprocessor):
	"""
	Preprocesador básico. Realiza las siguientes operaciones:
	- Convertir a escala de grises
	- Redimensionar la imagen
	- Opcionalmente histograma

	"""

	def __init__(self, verbose=False, gray=rasprec.GRAY_EQUAL,
		norm=None, face_size=None, width=480):
		""" Inicializa el preprocesador

		Parámetros:
			verbose
				Muestra información extendida por pantalla
			gray:
				Indica el modo de conversión a escala de grises utilizado. Los valores son:
					rasprec.GRAY_EQUAL:		Mismo peso en cada uno de los 3 canales RGB
					rasprec.GRAY_WEIGHTED:	Diversos pesos para aproximar a la vista humana
					rasprec.GRAY_VALUE:		Canal valor de HSV
			norm
				Indica el tipo de normalización utilizada. Los posibles valores son:
					None: 					No hay normalización sobre la imagen
					rasprec.NORM_HISTO:		Ecualización de histograma
					rasprec.NORM_RANGE:		Normalización de rango
					rasprec.NORM_CLAHE:		Ecualización de histograma adaptativo

		"""
		Preprocessor.__init__(self, verbose, face_size, width)
		self.width = width
		self.gray = gray
		self.norm = norm


	def prepare(self, img):

		# Redimensionado
		if img.shape[1] != self.width:
			img = imutils.resize(img, width=self.width)
		
		# Conversión a escala de grises
		if img.ndim == 3:
			if self.gray == rasprec.GRAY_EQUAL:
				img = img.astype(np.int16)
				img = (img[:,:,0] + img[:,:,1] + img[:,:,2]) / 3
				img = img.astype(np.uint8)
			elif self.gray == rasprec.GRAY_WEIGHTED:
				img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			elif self.gray == rasprec.GRAY_VALUE:
				i = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
				img = i[:,:,2]
			else:
				print ("[ERROR] Parámetro inválido.")
				sys.exit()

		# Normalización de la imagen
		if self.norm == rasprec.NORM_HISTO:
			img = self.hist_norm(img)
		elif self.norm == rasprec.NORM_RANGE:
			img = self.range_norm(img)
		elif self.norm == rasprec.NORM_CLAHE:
			img = self.clahe(img)

		return img




	"""
	def mean_norm(self, image):
		img = (image - np.mean(image))
		return img


	def std_norm(self, image):
		img = (image - np.mean(image))/np.std(image)
		img = (img - np.amin(img))/(np.amax(img) - np.amin(img)) * 255
		img = img.astype(np.uint8)
		return img

	"""

	


	def prepare_single_face(self, face):
		# Se convierte a escala de grises
		face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)

		# Se recorta y redimensiona si es necesario
		# Si se quiere dejar cuadrado
		if self.face_size[0] == self.face_size[1]:
			y, x = face.shape
			if x != self.face_size[1] or y != self.face_size[0]:
				if x > y:
					dif = int((x - y)/2)
					face = face[:, dif:dif+x]
				elif y > x:
					dif = int((y - x)/2)
					face = face[dif:dif+y, :]
				face = imutils.resize(face, width=face_size[1])
		else:
			# Si es rectangular
			h = self.face_size[1]	# Tamaño que debe tener al final
			w = self.face_size[0]
			y, x = face.shape

			# Redimensionamos de alto o ancho para ajustar en el lado en que menor pierda
			# Y del otro se recorta
			if w/x < h/y:
				face = imutils.resize(face, height=h)
				y, x = face.shape 	# Volvemos a calcular ancho y alto
				b = int((x-w)/2)
				face = face[:,b:b+w]
			else:
				face = imutils.resize(face, width=w)
				y, x = face.shape
				b = int((y-h)/2)
				face = face[b:b+h, :]
			
		# Se aplica el histograma si se indica así
		if self.with_histo:
			face = cv2.equalizeHist(face)

		return face






