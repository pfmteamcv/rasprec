import cv2
from threading import Thread

class Player():
	
	def __init__(self):
		print ("Inicializando el reproductor de vídeo")
		
		self.frame = None
		self.stopped = False
		cv2.namedWindow("Video", flags=cv2.WINDOW_AUTOSIZE)
		
		Thread(target=self.show, args=()).start()
		
		
	def write(self, image):
		self.frame = image
		
		
	def show(self):
		print ("en show")
		while not self.stopped:
			if not self.frame is None:
				cv2.imshow("Video", self.frame)


	def stop(self):
		print ("Esto se ha parado")
		self.stopped = True
