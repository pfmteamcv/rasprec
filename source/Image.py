""" Esta clase encapsula una imagen que va a ser tratada por un clasificador

	El objetivo de la misma es mantener una uniformidad de datos así como poder
	almacenar metadatos con cada imagen.
	Son las imágenes con las que trabajan los clasificadores, por lo que se 
	cumple que:
		- Tienen un tamaño de 70x70 píxeles
		- Están en blanco y negro

"""

import numpy as np

class Image:

	def __init__(self, image):
		""" Constructor, recoge una imagen como parámetro

		"""

		self.data = None	# Contiene la imagen propiamente dicha

		if (type(image) == list):
			self.data = np.asarray(image, dtype=np.int32)
		elif type(image) == string:
			cv.imread("../images/faces.jpg")
			image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
		else:
			self.data=image


