""" Clase base para la obtención de características

Esta clase contiene los métodos virtuales y métodos reutilizables por todas las
clases que hereden de ella. Pretende establecer la estructura común que deberán
tener todos los generadores de características.

Attributes
---------


Methods
-------



"""


from abc import ABCMeta
from abc import abstractmethod


class Features:
	__metaclass__ = ABCMeta

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, params=None, verbose=False):

		self._verbose = verbose

		if self._verbose:
			print("[INFO] Se ha inicializado la clase Features.")		


	# -------------------- METODOS VIRTUALES --------------------
	
	@abstractmethod
	def __initialize(self):
		raise NotImplementedError()


	@abstractmethod
	def set_params(self, params):
		raise NotImplementedError()


	@abstractmethod
	def get_params(self):
		raise NotImplementedError()


	@abstractmethod
	def get_features(self, image):
		raise NotImplementedError()


	@abstractmethod
	def draw_features(self, image):
		raise NotImplementedError()

	# --------------------- METODOS PÚBLICOS _---------------------

