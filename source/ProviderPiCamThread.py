from Provider import Provider
from threading import Thread
import cv2
import time

from picamera.array import PiRGBArray
from picamera import PiCamera


class ProviderPiCamThread(Provider):
	
	def __init__(self, resolution=(320, 240), framerate=32):
		
		# Inicializamos la cámara y el stream
		self.__camera = PiCamera()
		self.__camera.resolution = resolution
		self.__camera.framerate = framerate
		self.__raw_capture = PiRGBArray(self.__camera, size=resolution)
		self.__stream = self.__camera.capture_continuous(self.__raw_capture,
			format="bgr", use_video_port=True)
			
		# Inicializamos el frame y la señal de parada
		self.__frame = None
		self.__stopped = False
	
		
		# Lanzamos la cámara
		self.start()
		time.sleep(2.0)
		
		
	def start(self):
		
		# Lanzamos un hilo que va leyendo los frames
		Thread(target=self.update, args=()).start()
		return self
		
	
	def update(self):
		
		for f in self.__stream:
			self.__frame = f.array
			self.__raw_capture.truncate(0)
			
			if self.__stopped:
				self.__stream.close()
				self.__raw_capture.close()
				self.__camera.close()
				return
				
				
	def read(self):
		
		return self.__frame
		
		
	def stop(self):
		
		self.__stopped = True
		
		
	def __iter__(self):
		
		return self
		
	
	def __next__(self):
		
		return self.__frame


	
