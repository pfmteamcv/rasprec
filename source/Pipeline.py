# -*- coding: utf-8 -*- 
import cv2
import os
import time
import numpy as np
import rasprec
import time
import statistics

class Pipeline: 
	"""
	Esta es la clase que encapsula todo el proceso de procesamiento de la imagen.
	Los pasos son:
		1.- Obtención de las imágenes. 	Instancia de la clase Provider
		2.- Preprocesamiento.			Instancia de la clase Preprocessor
		3.- Detección de las caras.		Instancia de la clase Detector
		4.- Procesamiento de las caras.	Instancia de la clase FaceProcessor
		5.- Reconocimiento facial.		
				5.1.- Entrenamiento. 	Instancia de la clase Trainer
				5.2.- Reconocimiento. 	Instancia de la clase Recognizer
	"""

	def __init__ (self, provider, preprocessor=None,
		detector=None, face_processor=None,
		recognizer=None, player=None, debug=[],
		time_measure=False):

		# Etapas del procesado de imágenes
		self.__provider = provider
		self.__preprocessor = preprocessor
		self.__detector = detector
		self.__face_processor = face_processor
		self.__recognizer = recognizer
		self.__player = player
		self.__debug = debug

		# Se calculan los FPS medio de los últimos X frames
		self.__max_fps_index = 10
		self.__fps = np.zeros(self.__max_fps_index)	
		self.__fps_index = 0

		self.__num_frames = 0

		self.__heatmap = None

		self.__time_measure = time_measure

		# Si hay algún modo de depuración que dependa de las clases se le envía la señal
		self.__detector.set_debug(self.__debug)
		if not self.__face_processor is None:
			self.__face_processor.set_debug(self.__debug)

		self.__all_times = {
			'provider':      [],	# Obtención de la imagen
			'preprocessing': [],	# Preprocesamiento
			'detection':     [],	# Fase de detección de la cara en la imagen
			'processing':    [],	# Procesamiento de la cara para prepararla para el reconocimiento
			'recognition':   [],	# Reconocimiento de la cara
			'detect_eyes':   [],	# Procesamiento: detección de los ojos
			'geometric':     [],	# Procesamiento: rotación, escalado y recortado de la cara
			'histogram':     [],	# Procesamiento: histograma por partes
			'smooth_mask':   [],	# Procesamiento: suavizado y máscara circular
			'total':         []		# Tiempo total en procesar una imagen
		}



# Tal vez se puede definir Pipeline como un iterador
# En ese caso devolvería en cada elemento:
#	- Imagen correspondiente al frame, habría que mirar a ver exactamente qué imagen sería
#	- Lista con coordenadas e identificador de cada cara detectada en la imagen

	def start(self):
		img_number=0
		while self.__provider.has_next:
			# Se obtiene la siguiente imagen
			self.original_image = self.__provider.next()


	def __iter__(self):
		#self.actual=0
		return self


	def __next__(self):
		"""
			Devuelve:
				image: ndarray
					La imagen que ha obtenido del proveedor
				faces: list
					Lista donde cada elemento es cada una de las detecciones de caras en la imagen. Cada elemento es un 
					diccionario que contiene información sobre la cara. Sus claves son:
						'rect': coordenadas de la forma [x1, y1, x2, y2] del rectángulo que contiene la cara
						'id': identificador del usuario que ha reconocido
		"""
		# Comenzamos el recorrido por totas las fases del Pipeline

		# Medimos el tiempo y contamos los frames
		start_time = time.time()
		self.__num_frames += 1
		
		# Leemos una imagen del proveedor
		t1 = time.process_time()
		image = next(self.__provider)
		if isinstance(image, str):
			image = cv2.imread(image)
		# Pasamos la imagen por el preprocesador
		t2 = time.process_time()
		self.__all_times['provider'].append(t2-t1)
		
		# ########## 1.- PREPROCESAMIENTO ##########
		t1 = time.process_time()
		image = self.__preprocessor.prepare(image)
		t2 = time.process_time()
		self.__all_times['preprocessing'].append(t2-t1)

		# ########## 2.- DETECCIÓN        ##########
		t1 = time.process_time()
		rects = self.__detector.detect_multiscale(image)
		t2 = time.process_time()
		self.__all_times['detection'].append(t2-t1)

		#if self.__debug and not image is None:
		#	self.__heatmap = self.__detector.detect_multiscale(image, debug='heatmap')

		# Ahora pasamos al procesador de caras
		faces = []
		for x1, y1, x2, y2 in rects:
			face = image[y1:y2, x1:x2]

			# ########## 3.- PROCESAMIENTO    ##########
			t1 = time.process_time()
			if not self.__face_processor is None:
				face_processed = self.__face_processor.load(face)

			t2 = time.process_time()
			self.__all_times['processing'].append(t2-t1)
			# También extraemos el tiempo de cada fase del procesado
			t = self.__face_processor.get_times()
			for key, value in t.items():
				self.__all_times[key].append(value)

			if not self.__recognizer is None and not face_processed is None:
				prediction = self.__recognizer.predict(face_processed)
						
				faces.append({
					'rect': (x1, y1, x2, y2),
					'face': face_processed,
					'id': prediction
					})
			else:
				faces.append({
					'rect': (x1, y1, x2, y2),
					'face': face_processed,
					'id': 'Reconocedor no configurado'
					})

		# Formato faces = [{'rect': (10, 10, 150, 150), 'face': [], 'id': 'Victor'}]

		end_time = time.time()
		self.__all_times['total'].append(end_time-start_time)

		if rasprec.DEBUG_FPS:
			t = end_time - start_time

			if t != 0.0:
				self.__fps[self.__fps_index] = 1. / t
				if self.__fps_index < self.__max_fps_index-1:
					self.__fps_index += 1
				else:
					self.__fps_index = 0
			fps = 'FPS: ' + str(np.mean(self.__fps, dtype=np.uint32))
			font = cv2.FONT_HERSHEY_SIMPLEX
			h, w = image.shape
			cv2.putText(image, fps, (w-110, h-30), font, 0.7, 255, 1, cv2.LINE_AA)


		if not self.__player is None:
			self.__player.write(image)

		return image, faces



	def get_heatmap(self):
		return self.__heatmap


	def debug_info(self):

		if self.__face_processor.eye_detect_method == rasprec.EYES_VIOLA and rasprec.DEBUG_COUNT_DETECTIONS in self.__debug:
			detections = self.__face_processor.get_count_eyes_detected()
			total = detections['total']
			left = detections['left']
			right = detections['right']
			both = detections['both']
			one = left + right - (both * 2)
			none = total - both - one
			print ("[DEBUG] Caras procesadas: " + str(total))
			print ("Ojo izquierdo: " + str(left))
			print ("Ojo derecho: " + str(right))
			print ("[DEBUG] Ambos ojos detectados: " + str(both) + "  (" + str(both/total*100) + "%)")
			print ("[DEBUG] Un solo ojo detectado: " + str(one) + "  (" + str(one/total*100) + "%)")
			print ("[DEBUG] Ningún ojo detectado: " + str(none) + "  (" + str(none/total*100) + "%)")


	def get_times(self):
		"""
			Devuelve un diccionario con los tiempos de cada una de las etapas medidos en milisegundos
		"""

		t = {}
		for key, value in self.__all_times.items():
			if len(value) != 0:
				t[key] = statistics.mean(value) * 1000
			else:
				t[key] = 0.00

		return t


"""

		self.original_image = None		# Imagen original
		self.preprocessed_image = None	# Imagen tras el preprocesado
		self.output = None				# Imagen que se mostrará al final
		self.output_face = None			# Imagen que mostrará el procesamiento de la cara

		if preprocessor is None:
			print "Warning: No se ha definido un preprocesador"
		if detector is None:
			print "Warning: No se ha definido un detector"
		if face_processor is None:
			print "Warning: No se ha definido un procesador facial"
		if recognizer is None:
			print "Warning: No se ha definido un reconocedor"

		print "Inicializamos el pipeline"


	def start(self):
		img_number=0
		#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
		while self.__provider.has_next:			
			# Obtenemos la siguiente imagen
			start = time.time()
			self.original_image = self.__provider.next()
			self.gui.set_image(self.original_image)

			self.output = self.original_image

			# Preprocesamiento
			self.preprocessed_image = self.__preprocessor.prepare(self.original_image)
			
			# Detección
			self.__detector.detect(self.preprocessed_image)
			end = time.time()
			t = (end-start)*1000
			print "Tiempo de detección: " + str(t) + " ms."

			while self.__detector.more_faces:
				start = time.time()
				f, r = self.__detector.next_face()	# Imagen de la cara y coordenadas donde se encuentra
				(face_x1, face_y1), (face_x2, face_y2) = r
				
				self.gui.set_face(f)
				self.gui.set_face_rect(r)

				self.output_face = cv2.cvtColor(f, cv2.COLOR_GRAY2BGR)

				# Preprocesado
				self.output = cv2.rectangle(self.output, (face_x1, face_y1), (face_x2, face_y2), (255, 0, 0), 2)

				img_number += 1
				# Esto itera por cada cara encontrada en la imagen

				method = "HAAR"
				method = "FACIAL_LANDMARKS"
				eyes, areas, landmarks = self.__face_processor.load(f, method)
				self.gui.set_eyes(eyes, areas, landmarks)
				print "---------------"

				cface, hface, sface, pface = self.__face_processor.get_processed_faces()
				self.gui.set_processed_faces(cface, hface, sface, pface)
				end = time.time()
				t = (end - start)*1000
				print "Tiempo de procesado: " + str(t) + " ms."
				print " "

				if self.train_mode:
					# Modo entrenamiento
					# Por ahora recogemos las caras a procesar
					lbl = self.__provider.next_label

					if pface is not None:
						self.train_faces.append(pface)
						self.train_labels.append(lbl)
					else:
						print "Cara no válida"

				# Mostramos las caras seleccionadas en la interface
				self.gui.set_training_faces(self.train_faces)

			
			self.gui.draw()

			#cv2.imshow("Output", self.output)
			#cv2.imshow("Face", self.output_face)
			#cv2.waitKey(1500)

		# Se han leido todas las imágenes, se puede proceder al entrenamiento
		if self.train_mode:
			trainer = Trainer.FisherTrainer(self.train_faces, "Victor")
			trainer.train()

		cv2.waitKey(0)
		cv2.destroyAllWindows()

"""