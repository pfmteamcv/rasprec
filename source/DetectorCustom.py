"""
	Implementa un detector utilizando características y clasificador personalizados.
"""
 
from Detector import Detector
import rasprec
import imutils
import numpy as np
from Heatmap import Heatmap
import cv2
import multiprocessing
import threading

class DetectorCustom(Detector):
	# -------------------- VARIABLES DE CLASE --------------------


	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, params=None, classifier=None, verbose=False, train_file=None,
		concurrent=None, debug=(), fusion_mode=None):

		Detector.__init__(self, verbose)

		self.verbose = verbose
		self.__debug = debug
		self.__classifier = classifier
		self.__concurrent = concurrent

		if fusion_mode is None:
			self.__fusion_mode = rasprec.FUSION_HEATMAP
		else:
			self.__fusion_mode = fusion_mode

		# Parámetros que se aplicarán por defecto
		self.__default_params = {
			'scale_factor': 1.5,
			'min_size': 70,
			'min_width': 70,
			'min_height': 70,
			'disp': 35,
			'heatmap_threshold': 3
		}

		self.set_params(self.__default_params)

		if not params is None:
			self.set_params(params)
			
		if self.verbose:
			print ("[INFO] Se ha inicializado la clase Detector.")


	# --------------------- METODOS PÚBLICOS ---------------------


	def set_classifier(self, classifier):
		self.__classifier = classifier


	def set_params(self, params=None, scale_factor=None, min_size=None, min_width=None, min_height=None, disp=None, heatmap_threshold=None):

		if not params is None:
			if 'scale_factor' in params:
				self.scale_factor = params['scale_factor']
			if 'min_size' in params:
				self.min_size = params['min_size']
			if 'min_width' in params:
				self.min_width = params['min_width']
			if 'min_height' in params:
				self.min_height = params['min_height']
			if 'disp' in params:
				self.disp = params['disp']
			if 'heatmap_threshold' in params:
				self.heatmap_threshold = params['heatmap_threshold']

		if not scale_factor is None:
			self.scale_factor = scale_factor
		if not min_size is None:
			self.min_size = min_size
		if not min_width is None:
			self.min_width = min_width
		if not min_height is None:
			self.min_height = min_height
		if not disp is None:
			self.disp = disp
		if not heatmap_threshold is None:
			self.heatmap_threshold = heatmap_threshold


	def set_debug(self, debug):
		self.__debug = debug


	def get_sliding_windows(self, image):
		#  Implementa las ventanas deslizantes
		# 	
		# Este método implementa las ventanas deslizantes.
		# A partir de una imagen devuelve una lista donde cada elemento es otro 
		# array que incluye todas las sub-imágenes de un determinado tamaño. Cada
		# elemento de este array contiene a su vez un par de la forma:
		# 	- Array con una subimagen
		# 	- Lista de la forma (x, y, w, h) con las coordenadas, ancho y alto
		# 	  de la subimagen sobre la imagen original
		
		height, width = image.shape

		ratio = 1.
		windows = []
		
		#while width > self.min_size and height > self.min_size:
		while width > self.min_width and height > self.min_height:
			#extend / append
			windows.extend(self.__get_fixed_size_windows(image, ratio))
			width = int(width / self.scale_factor)
			image = imutils.resize(image, width=width)
			height, width = image.shape
			ratio *= self.scale_factor
		return windows


	def __get_fixed_size_windows(self, image, ratio):
		""" Devuelve una lista con todas las subimágenes del tamaño indicado.
		"""

		total_height, total_width = image.shape
		windows = []

		x = 0
		y = 0

		"""
		size = int(self.min_size * ratio)

		while y + self.min_size < total_height:
			while x + self.min_size < total_width:
				i = image[y:y+self.min_size, x:x+self.min_size]
				windows.append ((i, (int(x*ratio), int(y*ratio), int(x*ratio) + size, int(y*ratio) + size)))
				x += self.disp
			y += self.disp
			x = 0
		"""

		width = int(self.min_width * ratio)
		height = int(self.min_height * ratio)

		while y + self.min_height < total_height:
			while x + self.min_width < total_width:
				i = image[y:y+self.min_height, x:x+self.min_width]
				windows.append((i, (int(x*ratio), int(y*ratio), int(x*ratio) + width, int(y*ratio) + height)))
				x += self.disp
			y += self.disp
			x = 0

		return windows



	def detect_multiscale(self, image):
		""" Detecta las caras que hay en la imagen que se le pasa

		Parámetros:
			- image: imagen (array numpy)

		Devuelve:
		"""
		height, width = image.shape
		subimages = self.get_sliding_windows(image)

		if self.verbose:
			print ("[INFO] Detectando en imagen de tamaño " + str(width) + " x " + str(height) + " píxeles.")
			print ("[INFO] Imagen descompuesta en " + str(len(subimages)) + " subimágenes.")

		if self.__concurrent == 'threads' or self.__concurrent == True:
			if self.verbose:
				print ("[INFO] Paralelizando mediante hilos")
			# Concurrencia mediante la librería threading
			num_cpus = multiprocessing.cpu_count()
			core = []
			data = []
			num_subimages = len(subimages)
			step = int(num_subimages/num_cpus)
			i = 0
			while i < num_cpus:
				core.append (subimages[i*step:(i+1)*step])
				i += 1

			#processes = [multiprocessing.Process(target=self.__concurrent_detect, args=(core[i], rects )) for i in range(3)]
			processes = [threading.Thread(target=self.__concurrent_thread_detect, args=(core[i], data )) for i in range(num_cpus-1)]
			[process.start() for process in processes]
			[process.join() for process in processes]
			rects = data
		elif self.__concurrent == 'processes':
			# Concurrencia mediante la librería multiprocessing (da problemas en Windows) <-------------
			if self.verbose:
				print ("[INFO] Paralelizando mediante procesos")
			num_cpus = multiprocessing.cpu_count()
			core = []
			data = []
			num_subimages = len(subimages)
			step = int(num_subimages/num_cpus)
			i = 0
			while i < num_cpus:
				core.append (subimages[i*step:(i+1)*step])
				i += 1

			# pool = multiprocessing.Pool(processes=3)
			# pool.map(self.__concurrent_process_detect, core)
			#result = pool.apply_async(self.__concurrent_detect, core)
			#print (result.get(timeout=1))

			# https://stackoverflow.com/questions/1816958/cant-pickle-type-instancemethod-when-using-multiprocessing-pool-map
			pool = ProcessingPool(nodes=num_cpus)
			pool.map(self.__concurrent_process_detect, core)
			#pool.freeze_support()
			#Spool.map(Detector.test, core)
			
		else:
			if self.verbose:
				print ("[INFO] Sin paralelización")
			rects = self.__detect(subimages)
			rects=rects.tolist()


		# En este punto tenemos múltiples identificaciones, por lo que hay que simplificarlas
		# Los algoritmos para esto son: mapas de calor y NMS
		if self.__fusion_mode == rasprec.FUSION_HEATMAP:
			# Ahora toca el heatmap para acotar el número de rectángulos detectados
			hm = Heatmap(height, width, debug=self.__debug, threshold=self.heatmap_threshold)	# Inicializamos el mapa de calor
			hm.add_multiple(rects)
			bboxes = hm.get_bounding_boxes()

			if rasprec.DEBUG_BOUNDING_BOXES in self.__debug:
				img_bb = np.copy(image)
				max_area = 0	# Vamos a dibujar de otro color el mayor rectángulo detectado
				x1max = 0
				x2max = 0
				y1max = 0
				y2max = 0
				for x1, y1, x2, y2 in rects:
					img_bb = cv2.rectangle(img_bb, (x1, y1), (x2, y2), 255, 1)
					area = (x2-x1)*(y2-y1)
					if area > max_area:
						x1max = x1
						x2max = x2
						y1max = y1
						y2max = y2
						max_area = area
				# Dibujamos de otro color el mayor rectángulo detectado
				#img_bb = cv2.cvtColor(img_bb, cv2.COLOR_GRAY2BGR)
				#img_bb = cv2.rectangle(img_bb, (x1max, y1max), (x2max, y2max), (255, 0, 255), 2)
				cv2.imshow("Debug bounding boxes", img_bb)



			return bboxes
			
		elif self.__fusion_mode == Detector.FUSION_NMS:
			print ("Utilizando non maxima suppression")
			nms = NMS()
			nms.add_multiple(rects)
			bboxes = nms.get_bounding_boxes()
			return bboxes

		

	def test(data):
		print (data)


	def concurrent_process_detect(self,subimages):
		
		
		print("PID: %s, Process Name: %s, Thread Name: %s" % (
        	os.getpid(),
        	multiprocessing.current_process().name,
        	threading.current_thread().name)
    	)
		

		print ("Comenzando proceso concurrente")

		# Separamos las imágenes de sus coordenadas
		imgs, coords = list(zip(*subimages))

		# Evaluamos las caras. Devuelve una lista con una elemento para cada imagen que se pasó siendo
		# su valor 1 si es cara y 0 si no lo es.
		predictions = self.__classifier.are_faces(imgs)

		# Extraemos las coordenadas de las subimágenes que corresponden a caras		
		predictions = np.asarray(predictions)
		coords = np.asarray(coords)
		rects = coords[predictions == 1]
		
		data.extend(rects.tolist())



	def __concurrent_thread_detect(self,subimages, data):
		"""
			Recoge un grupo de imágenes y determina cuáles corresponden a caras.
			Hay que tener en cuenta que se le pasa lo generado por get_sliding_windows, que es una lista 
			donde cada elemento es un par que incluye la imagen y las coordenadas de la misma en la imagen original
			Es decir, la entrada es de la forma ( (ndarray_img, coords), (ndarray_img, coords), ...)
			Las coordenadas se proporcionan de la forma (x1, y1, x2, y2)
		"""
		
		"""
		print("PID: %s, Process Name: %s, Thread Name: %s" % (
        	os.getpid(),
        	multiprocessing.current_process().name,
        	threading.current_thread().name)
    	)
		"""


		# Separamos las imágenes de sus coordenadas
		imgs, coords = list(zip(*subimages))

		# Evaluamos las caras. Devuelve una lista con una elemento para cada imagen que se pasó siendo
		# su valor 1 si es cara y 0 si no lo es.
		predictions = self.__classifier.are_faces(imgs)

		# Extraemos las coordenadas de las subimágenes que corresponden a caras		
		predictions = np.asarray(predictions)
		coords = np.asarray(coords)
		rects = coords[predictions == 1]
		
		data.extend(rects.tolist())





	def __detect(self,subimages):
		"""
			Recoge un grupo de imágenes y determina cuáles corresponden a caras.
			Hay que tener en cuenta que se le pasa lo generado por get_sliding_windows, que es una lista 
			donde cada elemento es un par que incluye la imagen y las coordenadas de la misma en la imagen original
			Es decir, la entrada es de la forma ( (ndarray_img, coords), (ndarray_img, coords), ...)
			Las coordenadas se proporcionan de la forma (x1, y1, x2, y2)
		"""
	
		
		# Separamos las imágenes de sus coordenadas
		imgs, coords = list(zip(*subimages))

		# Evaluamos las caras. Devuelve una lista con una elemento para cada imagen que se pasó siendo
		# su valor 1 si es cara y 0 si no lo es.
		predictions = self.__classifier.are_faces(imgs)

		# Extraemos las coordenadas de las subimágenes que corresponden a caras		
		predictions = np.asarray(predictions)
		coords = np.asarray(coords)
		rects = coords[predictions == 1]
		
		return rects


	