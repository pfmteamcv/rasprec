"""
	Realiza el procesado de las caras antes de la etapa de reconocimiento

"""

from EyeDetector import HaarEyeDetector
from EyeDetector import LandmarkEyeDetector
import cv2
import imutils
import math
import numpy as np
import dlib
import os

class Processor:
	"""
	Clase base para el procesador de imágenes que prepara las caras antes de su reconocimiento
	"""
	def __init__(self):
		# Almacenamos los frames intermedios para documentación
		print "inicializando el constructor"
		# Imágenes de los diferentes pasos
		self.face = None					# Frame original
		self.face_geom_rotate = None		# Tras aplicar rotación en transformaciones geométricas 
		self.face_geom_scale = None			# Tras aplicar escalado en transformaciones geométricas 
		self.face_geom_translate = None		# Tras aplicar traslación en transformaciones geométricas 
		self.face_geom_crop = None			# Tras aplicar recorte en transformaciones geométricas 
		self.face_hist = None				# Tras ecualización de histograma independiente
		self.face_smooth = None				# Tras suavizado
		self.face_mask = None				# Tras aplicar la máscara
		self.face_processed = None			# Frame tras aplicar todas las operaciones

		self.desired_width = 70				# Ancho de la cara ya recortada
		self.desired_height = 70			# Alto de la cara ya recortada
		self.desired_left_eye_x = 0.21 * self.desired_width		# Posición X en que se debe encontrar el ojo izquierdo
		self.desired_right_eye_x = 0.79 * self.desired_width	# Posición Y en que se debe encontrar el ojo derecho
		self.desired_eyes_y = 0.14 * self.desired_height		# Posición Y en que se deben encontrar los ojos

		self.x1 = None						# Coordenada X del ojo izquierdo
		self.y1 = None						# Coordenada Y del ojo izquierdo
		self.x2 = None						# Coordenada X del ojo derecho
		self.y2 = None						# Coordenada Y del ojo derecho

		self.width = None					# Ancho de la imagen
		self.height = None					# Alto de la imagen

		self.debug = True					# Modo depuración

		# Tarda un poco en cargalo, por lo que se carga aquí una vez en lugar de cada vez que se llame
		self.predictor = dlib.shape_predictor(os.getenv("HOME") + "/resources/shape_predictor_68_face_landmarks.dat")

		#self.left_eye_coords = None
		#self.right_eye_coords = None


	def get_processed_faces(self):
		"""
		Para efectos de mostrar por pantalla y depuración devuelve los pasos intermedios del procesamiento
			- Cara tras aplicar las transfomaciones
			- Cara tras aplicar el histograma
			- Cara tras aplicar el suavizado
			- Cara tras finalizar el procesamiento
		"""
		return self.face_geom_crop, self.face_hist, self.face_smooth, self.face_processed



	def do_geometric(self):
		""" Endereza la cara en función de la posición de los ojos

		Si se han detectado ambos ojos utiliza sus coordenadas para enderezar la cara
		"""

		# Calculamos la pendiente de la recta que une ambos ojos
		slope =  float(self.y2-self.y1)/(self.x2-self.x1)

		if not slope is None:
			# Primer paso: rotación de la imagen para que los ojos estén en la horizontal
			self.face_geom_rotate = imutils.rotate(self.face, math.degrees(slope))

			# Segundo paso: se redimensiona la imagen para que los ojos se encuentren en las posiciones deseadas
			# Calculamos el ratio para que la distancia entre ambos ojos sea 0.68
			ratio = float(self.desired_right_eye_x - self.desired_left_eye_x)/(self.x2-self.x1)
			if self.width <= self.height:
				self.face_geom_scale = imutils.resize(self.face_geom_rotate, width=int(self.width*ratio))
			else: 
				self.face_geom_scale = imutils.resize(self.face_geom_rotate, height=int(self.height*ratio))
				
			new_width = int(self.width*ratio)
			new_height = int(self.height*ratio)

			# Recortamos la imagen para que el ojo izquierdo se encuentre en (0.16, 0.14)
			new_x1 = self.x1 * ratio
			new_y1 = self.y1 * ratio

			crop_left = int(new_x1 - self.desired_left_eye_x)					# Lo que ha que recortar por la izquierda
			crop_top = int(new_y1 - self.desired_eyes_y)							# Lo que hay que recortar por arriba
			crop_right = int(new_width - self.desired_width - crop_left)	# Lo que hay que recortar por la derecha
			crop_down = int(new_height - self.desired_height - crop_top) 	# Lo que hay que recortar por debajo

			# Hay ocasiones en que para centrar los ojos nos salimos de la cara. Con esto se soluciona aunque quede
			# ligeramente descentrado
			"""
			if new_height-crop_down-crop_top < 70:
				print "Corrigiendo"
				crop_down-=1
			"""
			if crop_down <= 0:
				crop_top += crop_down - 1
				crop_down = 1
			if crop_right <= 0:
				crop_left += crop_right - 1
				crop_right = 1

			#self.frame_geom_crop = self.frame_geom_scale[crop_left:self.width-crop_right, crop_top:self.width-crop_down]
			#self.frame_geom_crop = self.frame_geom_scale[crop_top:new_height-crop_down, crop_left:new_width-crop_right]
			self.face_geom_crop = self.face_geom_scale[crop_top:crop_top+self.desired_height, crop_left:crop_left+self.desired_width]
			     

	def do_histogram(self):
		""" Realiza el histograma por partes sobre la imagen
		"""
		# Se ecualiza la cara completa y también las mitades por separado
		self.face_hist = cv2.equalizeHist(self.face_geom_crop)
		# face_hist_tmp = self.face_hist 	# Solo a efectos de generación de capturas para documentación
		face_left = self.face_geom_crop[:, :int(self.desired_width/2)]
		face_right = self.face_geom_crop[:, int(self.desired_width/2):]

		face_left_hist = cv2.equalizeHist(face_left)
		face_right_hist = cv2.equalizeHist(face_right)

		# Y se combinan
		# ---> Comparar velocidad con np.item y np.itemset
		div_left   = int(self.desired_width*0.25)	# Punto donde comienza la transición entre izquierda y centro
		div_center = int(self.desired_width*0.50)	# Punto central
		div_right  = int(self.desired_width*0.75)	# Punto donde comienza la transición entre centro y derecha
		step = 1./(div_center-div_left)				# Cuánto se debe incrementar o decremenar blend en la parte central
		blend = 0									# Cuánto se deben mezclar las imágenes
		for x in range (0, self.desired_width):
			# Calculamos el ratio de mezcla en cada columna
			if x < div_left or x > div_right:
				blend = 0
			else:
				blend += step * (-1 if x > div_center else 1)

			for y in range (0, self.desired_height):
				# Parte izquierda
				if x < div_center:
					self.face_hist[y, x] = int(face_left_hist[y, x]*(1. - blend) + self.face_hist[y, x]*blend)
				else:
					self.face_hist[y, x] = int(face_right_hist[y, x-div_center]*(1. - blend) + self.face_hist[y, x]*blend)

		#return face_left, face_right, face_left_hist, face_right_hist, face_hist_tmp


	def do_smoothing(self):
		""" Aplica el suavizado sobre la imagen para eliminar los ruidos introducidos por el histograma.
		"""

		# Los valores son los indicados en el libro (hay que verificarlo y probar con otros tipos de filtro
		self.face_smooth = cv2.bilateralFilter(self.face_hist, 0, 20., 2.)


	def do_elliptical_mask(self):
		""" Aplica una máscara elíptica para eliminar los elementos periféricos.
		"""
		center = (int(self.desired_width*0.5), int(self.desired_height*0.4))
		size = (int(self.desired_width * 0.5), int(self.desired_height * 0.8))
		mask = np.zeros((self.desired_width, self.desired_height))
		mask = cv2.ellipse(mask, center, size, 0, 0, 360, 175, -1)
		self.face_mask =  np.copy(self.face_smooth)
		self.face_mask[mask==0] = 96

		self.face_processed = self.face_mask


	def load(self, face, method):
		""" Carga una imagen en el procesador de caras

		El procesador de caras tiene que realizar una serie de operaciones para preparar la cara
		para el reconocedor.
		Uno de los puntos más importantes de este procesamiento es intentar que todos los rasgos
		de todas las caras se encuentren en la misma posición, y esto se consigue localizando puntos
		clave de las caras, como por ejemplo los ojos, y aplicando transformaciones geométricas para
		que estos rasgos se encuentren siempre en las mismas posiciones.
		Esta función carga la imagen de la cara en el procesador y localiza los rasgos clave de la
		cara. Para ello por ahora hay implementados dos algoritmos:

			- Cascadas de Haar: utiliza el método de Viola Jones con cascadas de Haar para localizar
							    el centro de ambos ojos
			- Facial landmarks: utiliza Facial Landmarks para detectar 68 puntos clave de la cara. A 
								partir de estos puntos se puede obtener la posición central de los ojos
								y tal vez algún otro punto destacable (p.e. nariz?)
		
		Parámetros:
			face - 	 imagen con la cara
			method - método que se va a utilizar para detectar los ojos. 
					 Valores aceptados: "HAAR" y "FACIAL_LANDMARKS"

		Devuelve:
			ATENCIÓN: los datos que devuelve esta función varían según el método utilizado ya que solo 
			se utilizan para la representación en pantalla de los resultados obtenidos.

			HAAR - Devuelve centers, rects donde centers son las coordenadas de los ojos encontrados de
				   la forma (x1, y1, x2, y2) y rects son las coordenadas de las zonas donde buscó los ojos

		"""

		self.face = face
		self.width, self.height = face.shape


		if method == "HAAR":
			self.ed = HaarEyeDetector(self.face)
			self.ed.detect(xml_file="HAARCASCADE_EYE")		

			# A efectos de documentación se guardan imágenes de los puntos encontrados
			# Estas imágenes son:
			# 	self.face_areas: se marcan las áreas donde se buscan los ojos
			#	self.face_eyes:  se marcan las áreas donde se han encontrado ojos
			#	self.face_full:  se marcan tanto donde se buscan como dónde se encuentran, así como 
			#					 la recta que une ambos centros
			if self.debug == True:
				self.face_areas = cv2.cvtColor(self.face, cv2.COLOR_GRAY2BGR)
				cv2.rectangle(self.face_areas,
							  (self.ed.left_eye_x1, self.ed.left_eye_y1),
							  (self.ed.left_eye_x2, self.ed.left_eye_y2),
							  (255, 0, 0), 2)
				cv2.rectangle(self.face_areas,
							  (self.ed.right_eye_x1, self.ed.right_eye_y1),
							  (self.ed.right_eye_x2, self.ed.right_eye_y2), 
							  (255, 0, 0), 2)
				self.face_full = self.face_areas

				# Dibujamos los ojos encontrados
				self.face_eyes = cv2.cvtColor(self.face, cv2.COLOR_GRAY2BGR)

				ler = self.ed.left_eye_rect
				if not ler is None:
					cv2.rectangle(self.face_eyes, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)
					cv2.rectangle(self.face_full, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)

				rer = self.ed.right_eye_rect
				if not rer is None:
					cv2.rectangle(self.face_eyes, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)
					cv2.rectangle(self.face_full, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)

				# Calculamos los puntos centrales
				if (not ler is None) and (not rer is None):
					lex = int(ler[0] + ((ler[1]-ler[0])/2))		# Left eye X
					ley = int(ler[2] + ((ler[3]-ler[2])/2))		# Left eye Y
					rex = int(rer[0] + ((rer[1]-rer[0])/2))		# Left eye X
					rey = int(rer[2] + ((rer[3]-rer[2])/2))		# Left eye Y
					cv2.line(self.face_full, (lex, ley), (rex, rey), (0, 255, 255), 1)


			# El algoritmo puede encontrar 1, 2 o ningún ojo:
			# 	- Si se han encontrado los dos ojos se puede realizar el procesamiento de la imagen.
			#   - Si se encuentra un único ojo se presupone que la posición del otro es simétrica y
			#	  teniendo en cuenta esta asunción se continúa con el procesamiento
			#	- Si no se ha detectado ningún ojo no se puede realizar ningún tipo de procesamiento
			if self.ed.detected == 2:
				centers = self.ed.get_eyes_centers()
				(self.x1, self.y1), (self.x2, self.y2) = centers
				self.eyes_detected = True
				# Se puede realizar el procesado completo por lo que se lanza desde aquí
				self.full_processing()
				areas = self.ed.get_eyes_area()
				return centers, areas, None
			elif self.ed.detected == 1:
				# Si se detecta un único ojo se presupone que la posición del otro es simétrica.
				lec, rec = self.ed.get_eyes_centers()	# Uno de los 2 es None
				if lec is None:
					# Se han localizado las coordenadas del ojo derecho
					self.x2 = rec[0]
					self.y2 = rec[1]
					self.x1 = self.width - rec[0]	# Deducimos la posición simétrica
					self.y1 = rec[1]				# y misma altura
				else:
					# Se han localizado las coordenadas del ojo izquierdo
					self.x1 = lec[0]
					self.y1 = lec[1]
					self.x2 = self.width - lec[0]	# Deducimos la posición simétrica
					self.y2 = lec[1]				# y la misma altura

				self.full_processing()
				areas = self.ed.get_eyes_area()
				return [(self.x1, self.y1), (self.x2, self.y2)], areas, None

			else:
				# Si se ha detectado 1 o ningún ojo
				self.eyes_detected = False
				areas = self.ed.get_eyes_area()
				return None, areas, None
		elif method == "FACIAL_LANDMARKS":
			self.ed = LandmarkEyeDetector(self.predictor, self.face)
			self.ed.detect()
			landmarks = self.ed.get_landmarks()

			# Nos fijamos en los puntos de referencia obtenidos de los ojos. Cada ojo tiene
			# 6 puntos de referencia de los que nos quedamos los del extremo exterior e interior.
			# Son los valores 37 y 40 para el ojo izquierdo y 43 y 46 para el ojo derecho
			fl37 = landmarks[36]
			fl40 = landmarks[39]
			fl43 = landmarks[42]
			fl46 = landmarks[45]

			# Calculamos los centros de ambos ojos
			self.x1 = int((fl37[0]+fl40[0])/2)
			self.y1 = int((fl37[1]+fl40[1])/2)
			self.x2 = int((fl43[0]+fl46[0])/2)
			self.y2 = int((fl43[1]+fl46[1])/2)

			self.full_processing()

			return ((self.x1, self.y1), (self.x2, self.y2)), None, landmarks



	def full_processing(self):
		"""
			Cuando se detectan ambos ojos se puede realizar un procesado completo
			que pasa por todas las etapas del procesado
		"""

		self.do_geometric()
		self.do_histogram()
		self.do_smoothing()
		self.do_elliptical_mask()
		#self.frame_processed = self.frame_mask



# -------------------------------------------------------------------
# CASCADAS DE HAAR
# -------------------------------------------------------------------

class HaarProcessor(Processor):
	"""
	Cascadas de Haar para detectar los ojos
	"""

	def __init__(self):
		""" Inicializa el procesador de caras
		"""
		Processor.__init__(self)	# Llamamos al constructor del padre
		
		self.ed = None						# Detector 

		self.face_areas	= None				# Imagen con las áreas donde se buscan ojos marcadas
		self.face_eyes	= None				# Imagen con los ojos marcados
		self.face_full	= None				# imagen con los ojos y las áreas marcadas


	def load(self, face, method):
		""" Carga una imagen en el procesador de caras

		El procesador de caras tiene que realizar una serie de operaciones para preparar la cara
		para el reconocedor.
		Uno de los puntos más importantes de este procesamiento es intentar que todos los rasgos
		de todas las caras se encuentren en la misma posición, y esto se consigue localizando puntos
		clave de las caras, como por ejemplo los ojos, y aplicando transformaciones geométricas para
		que estos rasgos se encuentren siempre en las mismas posiciones.
		Esta función carga la imagen de la cara en el procesador y localiza los rasgos clave de la
		cara. Para ello por ahora hay implementados dos algoritmos:

			- Cascadas de Haar: utiliza el método de Viola Jones con cascadas de Haar para localizar
							    el centro de ambos ojos
			- Facial landmarks: utiliza Facial Landmarks para detectar 68 puntos clave de la cara. A 
								partir de estos puntos se puede obtener la posición central de los ojos
								y tal vez algún otro punto destacable (p.e. nariz?)
		
		Parámetros:
			face - 	 imagen con la cara
			method - método que se va a utilizar para detectar los ojos. 
					 Valores aceptados: "HAAR" y "FACIAL_LANDMARKS"

		Devuelve:
			ATENCIÓN: los datos que devuelve esta función varían según el método utilizado ya que solo 
			se utilizan para la representación en pantalla de los resultados obtenidos.

			HAAR - Devuelve centers, rects donde centers son las coordenadas de los ojos encontrados de
				   la forma (x1, y1, x2, y2) y rects son las coordenadas de las zonas donde buscó los ojos

		"""

		self.face = face
		self.width, self.height = face.shape


		if method == "HAAR":
			self.ed = HaarEyeDetector(self.face)
			self.ed.detect(xml_file="HAARCASCADE_EYE")		

			# A efectos de documentación se guardan imágenes de los puntos encontrados
			# Estas imágenes son:
			# 	self.face_areas: se marcan las áreas donde se buscan los ojos
			#	self.face_eyes:  se marcan las áreas donde se han encontrado ojos
			#	self.face_full:  se marcan tanto donde se buscan como dónde se encuentran, así como 
			#					 la recta que une ambos centros
			if self.debug == True:
				self.face_areas = cv2.cvtColor(self.face, cv2.COLOR_GRAY2BGR)
				cv2.rectangle(self.face_areas,
							  (self.ed.left_eye_x1, self.ed.left_eye_y1),
							  (self.ed.left_eye_x2, self.ed.left_eye_y2),
							  (255, 0, 0), 2)
				cv2.rectangle(self.face_areas,
							  (self.ed.right_eye_x1, self.ed.right_eye_y1),
							  (self.ed.right_eye_x2, self.ed.right_eye_y2), 
							  (255, 0, 0), 2)
				self.face_full = self.face_areas

				# Dibujamos los ojos encontrados
				self.face_eyes = cv2.cvtColor(self.face, cv2.COLOR_GRAY2BGR)

				ler = self.ed.left_eye_rect
				if not ler is None:
					cv2.rectangle(self.face_eyes, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)
					cv2.rectangle(self.face_full, (ler[0], ler[2]), (ler[1], ler[3]), (0, 0, 255), 1)

				rer = self.ed.right_eye_rect
				if not rer is None:
					cv2.rectangle(self.face_eyes, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)
					cv2.rectangle(self.face_full, (rer[0], rer[2]), (rer[1], rer[3]), (0, 0, 255), 1)

				# Calculamos los puntos centrales
				if (not ler is None) and (not rer is None):
					lex = int(ler[0] + ((ler[1]-ler[0])/2))		# Left eye X
					ley = int(ler[2] + ((ler[3]-ler[2])/2))		# Left eye Y
					rex = int(rer[0] + ((rer[1]-rer[0])/2))		# Left eye X
					rey = int(rer[2] + ((rer[3]-rer[2])/2))		# Left eye Y
					cv2.line(self.face_full, (lex, ley), (rex, rey), (0, 255, 255), 1)


			# El algoritmo puede encontrar 1, 2 o ningún ojo:
			# 	- Si se han encontrado los dos ojos se puede realizar el procesamiento de la imagen.
			#   - Si se encuentra un único ojo se presupone que la posición del otro es simétrica y
			#	  teniendo en cuenta esta asunción se continúa con el procesamiento
			#	- Si no se ha detectado ningún ojo no se puede realizar ningún tipo de procesamiento
			if self.ed.detected == 2:
				centers = self.ed.get_eyes_centers()
				(self.x1, self.y1), (self.x2, self.y2) = centers
				self.eyes_detected = True
				# Se puede realizar el procesado completo por lo que se lanza desde aquí
				self.full_processing()
				areas = self.ed.get_eyes_area()
				return centers, areas, None
			elif self.ed.detected == 1:
				# Si se detecta un único ojo se presupone que la posición del otro es simétrica.
				lec, rec = self.ed.get_eyes_centers()	# Uno de los 2 es None
				if lec is None:
					# Se han localizado las coordenadas del ojo derecho
					self.x2 = rec[0]
					self.y2 = rec[1]
					self.x1 = self.width - rec[0]	# Deducimos la posición simétrica
					self.y1 = rec[1]				# y misma altura
				else:
					# Se han localizado las coordenadas del ojo izquierdo
					self.x1 = lec[0]
					self.y1 = lec[1]
					self.x2 = self.width - lec[0]	# Deducimos la posición simétrica
					self.y2 = lec[1]				# y la misma altura

				self.full_processing()
				areas = self.ed.get_eyes_area()
				return [(self.x1, self.y1), (self.x2, self.y2)], areas, None

			else:
				# Si se ha detectado 1 o ningún ojo
				self.eyes_detected = False
				areas = self.ed.get_eyes_area()
				return None, areas, None
		elif method == "FACIAL_LANDMARKS":
			self.ed = LandmarkEyeDetector(self.predictor, self.face)
			self.ed.detect()
			landmarks = self.ed.get_landmarks()

			# Nos fijamos en los puntos de referencia obtenidos de los ojos. Cada ojo tiene
			# 6 puntos de referencia de los que nos quedamos los del extremo exterior e interior.
			# Son los valores 37 y 40 para el ojo izquierdo y 43 y 46 para el ojo derecho
			fl37 = landmarks[36]
			fl40 = landmarks[39]
			fl43 = landmarks[42]
			fl46 = landmarks[45]

			# Calculamos los centros de ambos ojos
			self.x1 = int((fl37[0]+fl40[0])/2)
			self.y1 = int((fl37[1]+fl40[1])/2)
			self.x2 = int((fl43[0]+fl46[0])/2)
			self.y2 = int((fl43[1]+fl46[1])/2)

			self.full_processing()

			return ((self.x1, self.y1), (self.x2, self.y2)), None, landmarks



	def full_processing(self):
		"""
			Cuando se detectan ambos ojos se puede realizar un procesado completo
			que pasa por todas las etapas del procesado
		"""

		self.do_geometric()
		self.do_histogram()
		self.do_smoothing()
		self.do_elliptical_mask()
		#self.frame_processed = self.frame_mask

