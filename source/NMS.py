import cv2
import numpy as np

class NMS:

	def __init__(self, verbose=False):
		if verbose:
			print ("[INFO] Inicializando la clase NMS")

		self.__generated = False
		self.__boxes = []
		self.__bounding_boxes = []
		self.__verbose = verbose

		
	def __generate(self):
		# Aquí guardaremos los rectángulos válidos
		pick = []

		overlapThresh = 0.3

		# Tomamos las coordenadas de cada caja
		"""
		x1 = self.__boxes[:,0]
		x2 = self.__boxes[:,1]
		y1 = self.__boxes[:,2]
		y2 = self.__boxes[:,3]
		"""
		x1 = self.__boxes[:,0]
		y1 = self.__boxes[:,1]
		x2 = x1 + self.__boxes[:,2]
		y2 = y1 + self.__boxes[:,3]

		print ("**************************************************")
		print ("**************************************************")

		print (self.__boxes)
		print ("")
		print ("x1: " + str(x1))
		print ("x2: " + str(x2))
		print ("y1: " + str(y1))
		print ("y2: " + str(y2))


		print ("**************************************************")
		print ("**************************************************")
		print ("")

		# Calculamos el área de cada caja y las ordenamos según la esquina inferior derecha
		# Observa que np.argsort devuelve los índices de los elementos ordenados, no los propios elementos
		area = (x2 - x1 + 1) * (y2 - y1 + 1)
		idxs = np.argsort(y2)
		print ("area:" + str(area))
		
		while len(idxs) > 0:
			last = len(idxs) - 1
			i = idxs[last]
			pick.append(i)
			suppress = [last]
			print ("idxs: " + str(idxs))
			print ("suppress: "  + str(suppress))
			# Iteramos sobre todos los índices en la lista de índices
			for pos in range(0, last):

				# Tomamos un índice
				j = idxs[pos]
				print ("")
				print("Iteración. pos: " + str(pos))
				print ("i: " + str(i))
				print ("j: " + str(j))
				xx1 = max(x1[i], x1[j])
				yy1 = max(y1[i], y1[j])
				xx2 = min(x2[i], x2[j])
				yy2 = min(y2[i], y2[j])				

				# Calculamos el ancho y alto de la caja de solapamiento
				w = max(0, xx2 - xx1 + 1)
				h = max(0, yy2 - yy1 + 1)

				
				print ("xx1: " + str(xx1))
				print ("xx2: " + str(xx2))
				print ("yy1: " + str(yy1))
				print ("yy2: " + str(yy2))
				
				print ("w: " + str(w))
				print ("h: " + str(h))

				print ("w: " + str(w))
				print ("h: " + str(h))
				

				#Calculamos el ratio de solapamiento entre la caja calculada y la que está en la lista de áreas
				overlap = float(w*h)/area[j]

				print ("overlap: " + str(overlap))


				# Si hay suficiente solapamiento, eliminamos la caja actual
				if overlap > overlapThresh:
					suppress.append(pos)

			# Borramos de la lista de índices todos los índices que estén en la lista de suprimidos
			idxs = np.delete(idxs, suppress)

		# Devolvemos solo las cajas seleccionadas
		self.__bounding_boxes = self.__boxes[pick].tolist()



	def add(self, coords):
		""" Añade un rectángulo a la imagen
		"""

		self.__boxes.append(coords)
		self.__generated = False

		if self.__verbose:
			print ("[INFO] Añadiendo caja con coordenadas " + str(coords))


	def add_multiple(self, rects):
		"""
			Añade múltiples rectángulos
		"""
		self.__boxes = np.array(rects)
		print ("")
		print (self.__boxes)
		print("--")
		self.__generated = False



	def get_bounding_boxes(self):

		if type(self.__boxes) == list:
			self.__boxes = np.array(self.__boxes)
		if not self.__generated:
			self.__generate()

		return self.__bounding_boxes