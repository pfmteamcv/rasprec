"""
	Clase base para los reconocedores faciales
"""
import cv2
import numpy as np
import time
import os
import imutils
import rasprec

from abc import ABCMeta
from abc import abstractmethod
from pathlib import Path
from Preprocessor import PreprocessorBasic
from DetectorViola import DetectorViola
from Processor import Processor


class Recognizer:
	__metaclass__ = ABCMeta

	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, verbose=False):

		self.verbose = verbose

		self._training_set = []
		self._training_labels = []
		self._face_recognizer = None

		self._labels = []
		self._label_index = 0

		self._is_trained = False

		self._data_dir = str(Path.home()) + '/TFM/rasprec/resources/train_data/recognizer/'

		if self.verbose:
			print("[INFO] Inicializando la clase Recognizer")


	# -------------------- METODOS VIRTUALES --------------------
	@abstractmethod
	def train(self):
		raise NotImplementedError()


	@abstractmethod
	def predict(self, face):
		raise NotImplementedError()


	@abstractmethod
	def get_info(self):
		raise NotImplementedError()

	# -------------------- METODOS PÚBLICOS -------------------- 

	def add_face(self, face, label):
		""" Añade una cara al conjunto de entrenamiento

		Se puede añadir una cara de la clase Face, una imagen o la ruta de un fichero en disco
		"""

		# Si la etiqueta ya está registrada
		if label in self._labels:
			lbl = self._labels.index(label)
		else:
			lbl = len(self._labels)
			self._labels.append(label)

		self._training_set.append(face)
		self._training_labels.append(lbl)



	def train_from_dir(self, directory=None):
		""" Entrena el reconocedor a partir de las imágenes contenidas en un directorio
		"""

		if directory is None:
			directory = str(Path.home()) + "/TFM/rasprec/resources/people/"

		preprocessor = PreprocessorBasic()
		detector = DetectorViola(scale_factor=1.4)
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5)
		images = []			# Lista que contiene las imágenes
		labels = []			# Lista que contiene las etiquetas
		train_faces  = {}	# Diccionario que contiene las caras

		direc, subdirs, f = next(os.walk(directory))
		for s in subdirs: 
			d, sd, files = next(os.walk(directory + s))
			faces = []
			prep_faces = []
			for f in files:
				file = directory + s + '/' + f
				img = cv2.imread(file)

				labels.append(s)	# Guardamos la etiqueta

				img2 = preprocessor.prepare(img)
				rects = detector.detect_multiscale(img2)

				for x1, y1, x2, y2 in rects:
					face = img2[y1:y2, x1:x2]
					face = imutils.resize(face, height=100)
					faces.append(face)
					pface = processor.load(face)
					prep_faces.append(pface)

			train_faces[s] = prep_faces

			# Para mostrarlas
			cfaces=np.hstack(faces)
			cpfaces = np.hstack(prep_faces)

		# Tenemos un diccionario donde cada etiqueta contiene una lista de caras de una persona
		for name, faces in train_faces.items():
			for f in faces:
				self.add_face(f, name)

		#info = self.get_info()
		self.train()
