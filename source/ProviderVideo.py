from Provider import Provider
import cv2

class ProviderVideo(Provider):
	
	def __init__(self, filename):
		self.__video = cv2.VideoCapture(filename)
		self.__next_frame = None

		if not self.__video.isOpened():
			raise Exception('[ERROR] No se pudo abrir el fichero' + filename)


	def __iter__(self):
		#self.actual=0
		return self


	def __next__(self):
		if self.__video.isOpened():
			ret, frame = self.__video.read()
			if not frame is None:
				return frame
			else:
				self.__video.release()
				raise StopIteration
		else:
			raise StopIteration()




		"""
		if self.actual == self.len:
			raise StopIteration()
		else:
			i = self.files[self.actual]
			self.actual += 1
			return i

		"""