

from Recognizer import Recognizer
import cv2
import numpy as np
import pickle


class RecognizerLBP(Recognizer):
	"""
		Reconocedor facial basado en EigenFaces
	"""
	def __init__(self, verbose=False, radius=1, neighbors=8, grid_x=8, grid_y=8, threshold=6000):

		Recognizer.__init__(self, verbose)

		self._face_recognizer = cv2.face.LBPHFaceRecognizer_create(radius=radius, neighbors=neighbors, grid_x=grid_x, grid_y=grid_y, threshold=threshold)
		self.verbose = verbose






	def train(self):
		"""
		self._face_recognizer.setNumComponents(16)
		self._face_recognizer.setThreshold(6000)
		self._face_recognizer.setLabelInfo(1, 'hola')
		self._face_recognizer.setLabelInfo(2, 'adios')
		"""
		self._face_recognizer.train(self._training_set, np.array(self._training_labels))
		self._is_trained = True


	def save(self, filename):

		file = self._data_dir + filename + '.lbp'
		self._face_recognizer.save(file) 
		with open(file + '.lbl', 'wb') as fp:
			pickle.dump(self._labels, fp)


	def load(self, filename):

		file = self._data_dir + filename + '.lbp'
		self._face_recognizer.read(file)
		with open(file+'.lbl', 'rb') as fp:
			self._labels = pickle.load(fp)
		self._is_trained = True


	def predict(self, image):
		prediction = self._face_recognizer.predict(image)
		
		#print (prediction)

		#print (self._face_recognizer.getEigenVectors().shape)
		
		lbl = self._labels[prediction[0]]
		
		if prediction[1] > 3000:
			lbl = 'Desconocido'

		return lbl


	def get_info(self):

		info = {
			'Is trained': 	 self._is_trained,
			'Algorithm': 	 'LBP',
			'Faces loaded':  len(self._training_set),
			'People loaded': len(self._labels),
			'People labels': self._labels,
			#'Num. components': self._face_recognizer.getNumComponents(),
			'Threshold':     self._face_recognizer.getThreshold()
		}

		return info
