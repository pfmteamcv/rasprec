import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Preprocessor import PreprocessorBasic
import cv2
import rasprec
import time
import imutils

file = str(Path.home()) + "/TFM/rasprec/source/images/faces.png"
pp1 = PreprocessorBasic(gray=rasprec.GRAY_EQUAL)
pp2 = PreprocessorBasic(gray=rasprec.GRAY_WEIGHTED)
pp3 = PreprocessorBasic(gray=rasprec.GRAY_VALUE)

img = cv2.imread(file)
img = imutils.resize(img, width=480)

# Tiempos de conversión a gris
print ("---------- TIEMPOS DE CONVERSIÓN A GRIS ----------")
start = time.perf_counter()
for i in range(1000):
	img1 = pp1.prepare(img)
end = time.perf_counter()
t = end-start
print ("Mismo peso: " + str(t) + " ms.")

start = time.perf_counter()
for i in range(1000):
	img2 = pp2.prepare(img)
end = time.perf_counter()
t = end-start
print ("Ponderado : " + str(t) + " ms.")

start = time.perf_counter()
for i in range(1000):
	img3 = pp3.prepare(img)
end = time.perf_counter()
t = end-start
print ("HSV       : " + str(t) + " ms.")
print("")
# Tiempos de normalización, partimos de imagen en gris

img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

pp4 = PreprocessorBasic(norm=rasprec.NORM_HISTO)
pp5 = PreprocessorBasic(norm=rasprec.NORM_RANGE)
pp6 = PreprocessorBasic(norm=rasprec.NORM_CLAHE)


print ("---------- TIEMPOS DE NORMALIZACIÓN ----------")
start = time.perf_counter()
for i in range(1000):
	img4 = pp4.prepare(img)
end = time.perf_counter()
t = end-start
print ("Histograma: " + str(t) + " ms.")

start = time.perf_counter()
for i in range(1000):
	img5 = pp5.prepare(img)
end = time.perf_counter()
t = end-start
print ("Rango     : " + str(t) + " ms.")

start = time.perf_counter()
for i in range(1000):
	img6 = pp6.prepare(img)
end = time.perf_counter()
t = end-start
print ("CLAHE     : " + str(t) + " ms.")
