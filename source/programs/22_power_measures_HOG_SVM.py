import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import time
import os
import rasprec
import cv2 
import psutil

from ProviderVideo import ProviderVideo
from ProviderDataset import ProviderDataset
from Preprocessor import PreprocessorBasic
from Processor import Processor
from RecognizerFisherfaces import RecognizerFisherfaces
from Pipeline import Pipeline
from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from DetectorCustom import DetectorCustom


def cls():
	# Menú para elegir el reconocedor
	if os.name == 'posix':
		os.system('clear')
	else:
		os.system('cls')

cls()
print ("Idle (3 segundos)")
time.sleep(3)
print ("Preparación del entorno")

cpu1 = psutil.cpu_percent()

provider = ProviderVideo(str(Path.home()) + "/TFM/rasprec/resources/videos/portatil_03.avi")
preprocessor = PreprocessorBasic(width=320)

train_faces = ProviderDataset("training_faces", num_images=50)
train_nfaces = ProviderDataset("training_nfaces", num_images=100)
p = {
	"cell_size": (10, 10),
	"block_size": (20,20),
	"block_stride": (10, 10),
	"nbins": 9
	}
features = FeaturesHOG(p, verbose=True)
classifier = ClassifierSVM(features=features, params={'kernel_type': cv2.ml.SVM_POLY}, verbose=False)
classifier.load_data(train_faces, train_nfaces)
classifier.train()

p = {
	'scale_factor': 1.4,
	'disp': 20,
	'heatmap_threshold': 1
}
detector = DetectorCustom(classifier=classifier, params=p, debug=[rasprec.DEBUG_HEATMAP], concurrent=True)

processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_NONE)
print ("Entrenamiento del reconocedor")
recognizer = RecognizerFisherfaces(verbose=True)
recognizer.train_from_dir()

# Inicializamos el pipeline
pipeline = Pipeline(
	provider       = provider,
	preprocessor   = preprocessor,
	detector       = detector,
	face_processor = processor,
	recognizer     = recognizer,
	debug 		   = []#[rasprec.DEBUG_HEATMAP]
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)

frames = 0
cv2.namedWindow('video', cv2.WINDOW_NORMAL)
cv2.resizeWindow('video', 320, 240)

cpu2 = psutil.cpu_percent()
print ("Porcentaje empleado para entrenamiento y carga: " + str (cpu2 - cpu1))

print ("Idle (3 segundos)")
time.sleep(3)
print ("Procesando el vídeo")

cpu = psutil.cpu_percent()
for img, data in pipeline:
	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)

	if cv2.waitKey(1) == ord('q'):
		break
cpu = psutil.cpu_percent(percpu=True)
print (cpu)

print ("")
print (" Porcentajes de uso del procesador - HOG/SVM")
print (" -------------------------------------------")
i = 1
for core in cpu:
	print (" Core " + str(i) + ": " + '{:05.3f}'.format(core))
	i += 1
print ("")
cad = '{:05.3f}'.format(sum(cpu) / len(cpu))
print (" Media: " + cad)
print ("")

print ("Idle (3 segundos)")
time.sleep(3)
print ("Finalizado")