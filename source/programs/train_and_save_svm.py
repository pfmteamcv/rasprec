import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Processor import Processor
from ProviderDataset import ProviderDataset
from Preprocessor import PreprocessorBasic
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from ClassifierSVM import ClassifierSVM
from FeaturesHOG import FeaturesHOG
import cv2

provider = ProviderDataset("training_faces_rect")
pp = PreprocessorBasic(face_size=(60, 40))

"""
for p in provider:
	img = cv2.imread(p)
	print (img.shape)
	img = pp.prepare_single_face(img)
	print (img.shape)
	cv2.imshow("img", img)
	cv2.waitKey(50)
"""
# Vamos a usar características de HOG

width = 80	
height = 100
cell_size = 20 
block_size = 40
block_stride = 20

hog_params = {
	'win_size': (width, height),
	'block_size': (block_size, block_size),
	'block_stride': (block_stride, block_stride),
	'cell_size': (cell_size, cell_size),
	'nbins': 9
}
face_size = (width, height)

svm_params = {
	'kernel_type': cv2.ml.SVM_LINEAR,
	'type': cv2.ml.SVM_C_SVC,
	'gamma': 2.,
	'c': 10.505,
	'term_crit': (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6),
}


""" Los posibles valores de los parámetros son:
				type:
					cv2.ml.C_SVC
					cv2.ml.NU_SVC
					cv2.ml.ONE_CLASS
					cv2.ml.EPS_SVR
					cv2.ml.NUSVR
				kernel_type:
					cv2.ml.SVM_LINEAR
					cv2.ml.SVM_POLY
					cv2.ml.SVM_RBF
					cv2.ml.CVM_SIGMOID
				term_crit:
					cv2.TERM_CRITERIA_COUNT
					cv2.TERM_CRITERIA_EPS
					cv2.TERM_CRITERIA_MAX_ITER
		"""

hf = FeaturesHOG(verbose=True, params=hog_params)

train_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
	seed=0,
	train_percent=1.,
	num_images=2000,
	train=True,
	verbose=True)
train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
	seed=0,
	train_percent=1., 
	num_images=2000,
	train=True,
	verbose=True)
hard_mining_data = ProviderDataset("stanford_bg")
pp = PreprocessorBasic(face_size=face_size)

print ("Entrenando y guardando HOG + SVM")
classifier_SVM =  ClassifierSVM(features=hf, params=svm_params, verbose=True)
classifier_SVM.load_data(train_faces_svm, train_nfaces_svm, preprocessor=pp)
filename = 'hog.rectangular.svm.xml'
classifier_SVM.train_and_save(filename)
print ("[INFO] Se ha guardado el fichero " + str(filename))