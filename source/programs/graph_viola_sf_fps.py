import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from DetectorViola import DetectorViola
from Pipeline import Pipeline
from Preprocessor import PreprocessorBasic
from ProviderVideo import ProviderVideo
import cv2
import time
import rasprec
import numpy as np

import matplotlib.pyplot as plt

filename = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'


sf_vector = (1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.)
width_vector = (320, 480, 640, 800)


plt_tp = []
plt_fp = []

results = {}

# Iteramos con diferentes valores de scale factor
for w in width_vector:
	plt_time = []
	print ("Ancho: " + str(w))
	for sf in sf_vector:
		print ("Scale factor: " + str(sf))
		provider = ProviderVideo('../data/portatil_03.avi')
		preprocessor = PreprocessorBasic(width=w)
		detector = DetectorViola(filename, scale_factor=sf)

		# Inicializamos el pipeline
		pipeline = Pipeline(
			provider = provider,
			preprocessor = preprocessor,
			detector = detector,
			face_processor = None,
			recognizer = None,
			debug = ()
			)

		start = time.perf_counter()
		tp = 0
		fp = 0

		for img, data in pipeline:
			num_detected = len(data)	# Caras que se han detectado en el frame
			if num_detected == 1:
				tp += 1
			elif num_detected > 1:
				tp += 1
				fp += num_detected - 1

		end = time.perf_counter()
		total_time=end-start
		nframes = pipeline.num_frames


		plt_time.append(total_time/nframes)

	results[str(w)] = plt_time
		

	"""
		print ("--------------------------------------------")
		print ("Número total de frames: " + str(nframes))
		print ("Tiempo empleado: " + str(total_time))
		print ("Frames por segundo medio:" + str(nframes/total_time))
		print ("True positives: " + str(tp))
		print ("False positives: " + str(fp))
		print ("")
	"""

for w, v in results.items():
	h = int(float(w) / 1.33333333333)
	lbl = str(w) + 'x' + str(h) + ' píxeles'
	plt.plot(sf_vector, v, label=lbl)


plt.xlabel('Factor de escala')
plt.ylabel('Segundos')
plt.title('Influencia del factor de escala en el tiempo')

plt.legend()

plt.show()

