"""
	Prueba de la clase Detector. 
	Entrena un algoritmo HOG + kNN y localiza las caras en una imagen. Muestra todos los candidatos
	a cara, el mapa de calor y el resultado final tras simplificar el resultado.

"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
from ProviderVideo import ProviderVideo
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Preprocessor import PreprocessorBasic
from Processor import Processor
from Detector import Detector
from Pipeline import Pipeline
import rasprec

import multiprocessing 

import time
import cv2
import numpy as np


if __name__ == "__main__":
	# Vamos a usar características de HOG
	hf = FeaturesHOG(verbose=False)

	# Clasificador kNN con 7 vecinos
	classifier = ClassifierkNN(features=hf, params={"neighs": 25}, verbose=False)
	classifier.load_training('hog.14-28-14.knn')

	# Inicializamos los componentes
	provider = ProviderVideo('../data/portatil_01.avi')
	preprocessor = PreprocessorBasic()
	detector = Detector(classifier = classifier, verbose=False, concurrent='threads',
		fusion_mode=Detector.FUSION_HEATMAP)
	#detector = Detector(classifier = classifier, verbose=False, concurrent='threads')
	detector.set_params({
		'scale_factor': 1.3, 
		'min_size': 70,
		'disp': 15
		})
	processor = Processor(verbose=True)

	# Inicializamos el pipeline
	pipeline = Pipeline(
		provider = provider,
		preprocessor = preprocessor,
		detector = detector,
		face_processor = processor,
		recognizer = None,
		debug = (rasprec.DEBUG_BOUNDING_BOXES, rasprec.DEBUG_HEATMAP )
		)

	start = time.time()
	for img, data in pipeline:
		for d in data:
			x1, y1, x2, y2 = d['rect']
			img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		#hm = pipeline.get_heatmap()
		#cv2.imshow('Heatmap', hm)
		cv2.imshow("img", img)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	end = time.time()
	total_time=end-start
	print ("Tiempo empleado: " + str(total_time))

	"""

	d = Detector(classifier = classifier, verbose=True)

	# Estos son los parámetros por defecto
	d.set_params({
		'scale_factor': 1.25, 
		'min_size': 70,
		'disp': 20
		})

	"""


	"""
	img = cv2.imread('../images/faces.png')
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rects_all = d.detect_multiscale(img, debug='bounding_boxes')
	heatmap = d.detect_multiscale(img, debug='heatmap')
	rects = d.detect_multiscale(img)

	print (rects)

	img_all_boxes = np.copy(img)
	for x1, y1, x2, y2 in rects_all:
		img_all_boxes = cv2.rectangle(img_all_boxes, (x1, y1), (x2, y2), 255, 1)

	for x1, y1, x2, y2 in rects:
		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)



	cv2.imshow("Antes de heatmap", img_all_boxes)
	cv2.imshow("Despues de heatmap", img)
	cv2.imshow("Heatmap", heatmap)
	cv2.waitKey(0)
	"""