import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import time
import os
import rasprec
import cv2 
import psutil

from ProviderVideo import ProviderVideo
from Preprocessor import PreprocessorBasic
from DetectorViola import DetectorViola
from Processor import Processor
from RecognizerFisherfaces import RecognizerFisherfaces
from Pipeline import Pipeline



def cls():
	# Menú para elegir el reconocedor
	if os.name == 'posix':
		os.system('clear')
	else:
		os.system('cls')

cls()
cpu0 = psutil.cpu_percent(percpu=True)
print (cpu0)
print ("Idle (3 segundos)")
cpu1 = psutil.cpu_percent(percpu=True)
print (cpu1)
time.sleep(3)
print ("Preparación del entorno")

provider = ProviderVideo(str(Path.home()) + "/TFM/rasprec/resources/videos/portatil_03.avi")
preprocessor = PreprocessorBasic(width=320)
detector = DetectorViola(scale_factor=1.4)
processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_CLAHE)
print ("Entrenamiento del reconocedor")
recognizer = RecognizerFisherfaces(verbose=True)
recognizer.train_from_dir()

# Inicializamos el pipeline
pipeline = Pipeline(
	provider       = provider,
	preprocessor   = preprocessor,
	detector       = detector,
	face_processor = processor,
	recognizer     = recognizer,
	debug 		   = [] #[rasprec.DEBUG_HISTOGRAM]
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)

frames = 0
cv2.namedWindow('video', cv2.WINDOW_NORMAL)
cv2.resizeWindow('video', 320, 240)

#cpu2 = psutil.cpu_percent()
#print ("Porcentaje empleado para entrenamiento y carga: " + str (cpu2 - cpu1))

print ("Idle (3 segundos)")
time.sleep(3)
print ("Procesando el vídeo")

cpu = psutil.cpu_percent(percpu=True)
for img, data in pipeline:
	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)

	if cv2.waitKey(1) == ord('q'):
		break

cpu = psutil.cpu_percent(percpu=True)
print (cpu)

print ("")
print (" Porcentajes de uso del procesador - Viola Jones")
print (" -----------------------------------------------")
i = 1
for core in cpu:
	print (" Core " + str(i) + ": " + '{:05.3f}'.format(core))
	i += 1
print ("")
cad = '{:05.3f}'.format(sum(cpu) / len(cpu))
print (" Media: " + cad)
print ("")


print ("Idle (3 segundos)")
time.sleep(3)
print ("Finalizado")