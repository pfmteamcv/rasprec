import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from ProviderDatasetMetadata import ProviderDatasetMetadata
from ProviderDataset import ProviderDataset
from Preprocessor import PreprocessorBasic
from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from DetectorCustom import DetectorCustom
from DetectorViola import DetectorViola

import numpy as np
import matplotlib.pyplot as plt

import cv2
import rasprec
import time
import os


def autolabel(ax, rects):
	for rect in rects:
		height = rect.get_height()
		ax.annotate('{0:.3f}'.format(height),
				xy=(rect.get_x() + rect.get_width()/2, height),
				xytext=(0, 3),
				textcoords='offset points',
				ha='center',
				va='bottom')


def show_pyc_graph(data, title):
	"""
		Gráfica comparativa con diferentes parámetros
	"""

	scales_lst    = data['scale_factor']
	neigh_lst     = data['neighbours']
	precision_lst = np.asarray(data['precision'])
	recall_lst    = np.asarray(data['recall'])
	accuracy_lst  = np.asarray(data['recall'])
	tp 		      = np.asarray(data['tp'])
	fp            = np.asarray(data['fp'])
	fn            = np.asarray(data['fn'])
	print (data['total_images'])
	total_images = data['total_images'][0]
	tp = tp / total_images
	fp = fp / total_images
	fn = fn / total_images
	time_lst      = data['time']

	i = 0
	labels = []
	while i < len(scales_lst):
		labels.append("Escala: " + str(scales_lst[i]))# + '\n' + "Vecinos: " + str(neigh_lst[i]))
		i += 1

	x = np.arange(len(labels))
	width = 0.20

	fig, ax = plt.subplots()

	plt.ylim(top=1.1)
	plt.ylim(bottom=0)

	rects1 = ax.bar(x=x-width, height=tp.astype(np.float32), 
		width=width, label="Verdaderos positivos")
	rects2 = ax.bar(x=x, height=fp.astype(np.float32), 
		width=width, label="Falsos positivos")
	rects3 = ax.bar(x=x+width, height=fn.astype(np.float32), 
		width=width, label="Falsos negativos")

	ax.set_ylabel("Valor")
	ax.set_title(title, fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=11, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)
	autolabel(ax, rects3)

	fig.tight_layout()

	plt.show()



def show_time_graph(data, title):
	"""
		Gráfica comparativa con diferentes parámetros
	"""
	scales_lst    = data['scale_factor']
	neigh_lst     = data['neighbours']
	time_lst      = np.asarray(data['time'])

	i = 0
	labels = []
	while i < len(scales_lst):
		labels.append("Escala: " + str(scales_lst[i]))# + '\n' + "Vecinos: " + str(neigh_lst[i]))
		i += 1

	x = np.arange(len(labels))
	width = 0.35

	fig, ax = plt.subplots()

	#plt.ylim(top=1.1)
	#plt.ylim(bottom=0)

	rects = ax.bar(x=x, height=time_lst.astype(np.float32), 
		width=width, label="Tiempo por imagen")

	ax.set_ylabel("Milisegundos")
	ax.set_title(title, fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=11, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects)

	fig.tight_layout()

	plt.show()


def is_detected(face, eyes):
	"""
		A partir de la cara detectada y las coordenadas de los ojos indica si la detección es correcta.
	"""
	fx1, fy1, fx2, fy2 = face
	(lx, ly), (rx, ry) = eyes

	# Por ahora nos conformamos con que ambos ojos estén dentro de la cara detectada
	detected = False

	if fx1 < min(lx, rx) and fx2 > max(lx, rx) and fy1 < min(ly, ry) and fy2 > max(ly, ry):
		detected = True

	return detected


def test_detector(preprocessor, provider, detector, visualization=3):
	"""
	Parámetros:
		- preprocessor: preprocesador que se utilziará
		- provider:	tiene que ser de la clase ProviderDatasetMetada
		- detector
		- visualization: nivel de visualización de resultados
	"""

	# Inicializamos contadores
	num_images = 0
	total_time = 0	
	tp = 0
	fp = 0
	tn = 0
	fn = 0

	# Iteramos sobre las imágenes y los puntos del proveedor
	for file, metadata  in provider:
		points = metadata['points']
		((lx, ly),(rx, ry)) = points
		num_images += 1

		# Detectamos las caras en la imagen
		img = cv2.imread(file)
		img_gray = preprocessor.prepare(img)
		start_time = time.perf_counter()
		rects = detector.detect_multiscale(img_gray)
		end_time = time.perf_counter()
		total_time += end_time - start_time

		true_detected = 0
		false_detected = 0

		# Para cada rectángulo detectado en la imagen
		for r in rects:
			x1, y1, x2, y2 = r
			detected = is_detected(r, points)
			if detected:
				true_detected += 1
			else:
				false_detected += 1

			# Dibujamos las caras detectadas
			if visualization >= 3:
				if detected:
					img = cv2.rectangle(img, (x1, y1), (x2, y2), color=[0, 255, 0], thickness=2)
				else:
					img = cv2.rectangle(img, (x1, y1), (x2, y2), color=[0, 0, 255], thickness=3)

		# Actualizamos los contadores
		tp = tp + true_detected
		fp = fp + false_detected
		# ¿Qué hacemos con los true negatives?
		if true_detected == 0:
			fn += 1

		if visualization >= 3:
			img = cv2.rectangle(img, (lx-1, ly-1), (lx+1, ly+1), color=[0, 0, 255], thickness=3)
			img = cv2.rectangle(img, (rx-1, ry-1), (rx+1, ry+1), color=[0, 0, 255], thickness=3)
			cv2.imshow("Imagen", img)
			cv2.waitKey(0)

	precision = tp / (tp + fp)
	recall = tp / (tp + fn)
	accuracy = (tp + tn) / num_images

	print ("Total imágenes:    " + str(num_images))
	print ("Tiempo por imagen: " + str((total_time/num_images)*1000))
	print ("True positives:    " + str(tp))
	print ("False positives:   " + str(fp))
	print ("True negatives:    " + str(tn))
	print ("False negatives:   " + str(fn))
	print ("")
	print ("Precisión:     " + str(precision))
	print ("Exhaustividad: " + str(recall))
	print ("Exactitud:     " + str(accuracy))

	cv2.destroyAllWindows()








def hog_svm(scale_factor, disp, conc, visualization):

	pp = PreprocessorBasic(norm=rasprec.NORM_CLAHE, width=384)
	bioid = ProviderDatasetMetadata(dataset='bioid')

	# Entrenamos HOG + SVM
	train_faces = ProviderDataset("training_faces", num_images=1500)
	train_nfaces = ProviderDataset("training_nfaces", num_images=1500)
	p = {
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (10, 10),
		"nbins": 9
	}
	hf = FeaturesHOG(p, verbose=True)
	classifier = ClassifierSVM(features=hf, params={'kernel_type': cv2.ml.SVM_POLY}, verbose=True)
	classifier.load_data(train_faces, train_nfaces)
	print ("Entrenando el clasificador...")
	classifier.train()
	print ("Clasificador entrenado.")
	debug = []
	if visualization == 5:
		debug.append(rasprec.DEBUG_HEATMAP)
	if visualization >= 4:
		debug.append(rasprec.DEBUG_BOUNDING_BOXES)
	detector = DetectorCustom(classifier=classifier, params={'scale_factor': scale_factor, 'disp': disp}, debug=debug, concurrent=conc)

	test_detector(pp, bioid, detector, visualization)




def viola(scale_factor, neighs, visualization):
	"""
	Parámetros:
		- visualization: nivel de visualización de los resultados.
	"""

	pp = PreprocessorBasic(norm=rasprec.NORM_CLAHE, width=384)
	bioid = ProviderDatasetMetadata(dataset='bioid')
	detector = DetectorViola(scale_factor=scale_factor, min_neighbors=neighs)

	test_detector(pp, bioid, detector, visualization)



def collect_all_data(detector, title_pyr, title_time):

	#sf = np.arange(1.05, 2., 0.05)
	sf = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0]
	neighs = [3]

	pp = PreprocessorBasic(norm=rasprec.NORM_CLAHE, width=384)
	bioid = ProviderDatasetMetadata(dataset='bioid', num_images=250)
	
	data = {
		'scale_factor': [],
		'neighbours': [],
		'total_images': [],
		'tp': [],
		'fp': [],
		'tn': [],
		'fn': [],
		'precision': [],
		'recall': [], 
		'accuracy': [],
		'time': []
	}

	for scale in sf:
		print ("")
		print("Factor de escala: " + str(scale))		
		detector.set_params(scale_factor=scale)

		num_images = 0
		tp = 0
		fp = 0
		tn = 0
		fn = 0
		total_time = 0

		for file, metadata  in bioid:
			eyes = metadata['points']
			((lx, ly),(rx, ry)) = eyes
			num_images += 1

			# Detectamos las caras en la imagen
			img = cv2.imread(file)
			img_gray = pp.prepare(img)
			start_time = time.perf_counter()
			rects = detector.detect_multiscale(img_gray)
			end_time = time.perf_counter()
			total_time += end_time - start_time

			true_detected = 0
			false_detected = 0

			for r in rects:
				x1, y1, x2, y2 = r
				detected = is_detected(r, eyes)
				if detected:
					true_detected += 1
				else:
					false_detected += 1

			# Actualizamos los contadores
			tp = tp + true_detected
			fp = fp + false_detected
			# ¿Qué hacemos con los true negatives?
			if true_detected == 0:
				fn += 1

		precision = tp / (tp + fp)
		recall = tp / (tp + fn)
		accuracy = (tp + tn) / num_images

		data['scale_factor'].append(scale)
		data['total_images'].append(num_images)
		data['tp'].append(tp)
		data['fp'].append(fp)
		data['tn'].append(tn)
		data['fn'].append(fn)
		data['precision'].append(precision)
		data['recall'].append(recall)
		data['accuracy'].append(accuracy)
		data['time'].append(total_time)
	print (data)

	show_pyc_graph(data, title_pyr)
	show_time_graph(data, title_time)










def main_menu():
	if os.name == 'posix':
		os.system('clear')
	else:
		os.system('cls')

	print ("ELIGE EL DETECTOR")
	print ("-------------------------")
	print ("1.- Viola Jones")
	print ("2.- HOG - SVM")
	print ("")
	opf = input("Elige una opción: ")
	print ("")
	print ("ELIGE EL NIVEL DE VISUALIZACIÓN DE RESULTADOS")
	print ("---------------------------------------------")
	print ("0.- Gráfica para diferentes valores de los parámetros")
	print ("1.- Resultados finales en texto")
	print ("2.- Resultados finales de forma gráfica (¿se puede aplicar aquí la matriz de confusión?)")
	print ("3.- Resultados sobre las imágenes")
	print ("4.- Mostrar todas las detecciones")
	print ("5.- Mostrar mapa de calor")
	print ("")
	opr = input("Elige una opción: ")

	# Iteraciones sobre varios valores de parámetros. Muestra gráfica P&R y gráfica de tiempos
	if opr == '0':
		if opf == '1':
			# Se itera sobre varios tamaños de parámetro y se muestra una gráfica
			detector = DetectorViola()
			collect_all_data(detector, "Precisión y Exhaustividad - Viola Jones", "Tiempo por imagen (384x286) - Viola Jones")

		elif opf == '2':
			# Se itera sobre varios tamaños de parámetro y se muestra una gráfica
			train_faces  = ProviderDataset("training_faces", num_images=1500)
			train_nfaces = ProviderDataset("training_nfaces", num_images=1500)
			p = {
				"cell_size": (10, 10),
				"block_size": (20,20),
				"block_stride": (10, 10),
				"nbins": 9
			}
			hf = FeaturesHOG(p, verbose=True)
			classifier = ClassifierSVM(features=hf, params={'kernel_type': cv2.ml.SVM_POLY}, verbose=True)
			classifier.load_data(train_faces, train_nfaces)
			print ("Entrenando el clasificador...")
			classifier.train()
			print ("Clasificador entrenado.")

			if os.name == 'posix':
				os.system('clear')
			else:
				os.system('cls')
			print ("SELECCIONA LOS PARÁMETROS")
			print ("-------------------------")
			d = input ("Indica el valor del desplazamiento[35]: ")
			if d != '':
				disp = int(d)
			else:
				disp = 35
			conc = input ("Quieres habilitar la concurrencia? [y|N]: ")
			if conc == 'y' or conc == 'Y':
				conc = True
			else:
				conc = False

			detector = DetectorCustom(classifier=classifier, params={'disp': disp}, concurrent=conc)
			title = "Precisión y Exhaustividad - HOG SVM (desp=" + str(d) + ")"
			title_time = "Tiempo por imagen (384x286) - HOG SVM (desp=" + str(d) + ")"
			collect_all_data(detector, title, title_time)

	# Resultados preguntando por parámetros y visualización de salida
	else:
		# Viola - Jones
		if opf == '1':
			print("")
			print ("PARÁMETROS")
			print ("----------")
			sf = input("Factor de escala [1.2]: ")
			if sf != '':
				sf = float(sf)
			else:
				sf = 1.2
			neigh = input("Número de vecinos [3]: ")
			if neigh != '':
				neigh = int(neigh)
			else:
				neigh = 3
			print ("")

			viola(sf, neigh, int(opr))

		# HOG - SVM
		elif opf == '2':
			print ("")
			print ("PARÁMETROS")
			print ("----------")
			sf = input("Factor de escala [1.2]: ")
			if sf != '':
				sf = float(sf)
			else:
				sf = 1.2
			disp = input("Desplazamiento [35]: ")
			if disp != '':
				disp = int(disp)
			else:
				disp = 35
			conc = input ("Quieres habilitar la concurrencia? [y|N]: ")
			if conc == 'y' or conc == 'Y':
				conc = True
			else:
				conc = False

			hog_svm(sf, disp, conc, int(opr))




main_menu()