import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import os
import rasprec
import cv2
import imutils
import random
import time

import matplotlib.pyplot as plt
import numpy as np

from ProviderDatasetMetadata import ProviderDatasetMetadata
from ProviderFolder import ProviderFolder
from Preprocessor import PreprocessorBasic
from DetectorViola import DetectorViola
from Processor import Processor
from RecognizerEigenfaces import RecognizerEigenfaces
from RecognizerFisherfaces import RecognizerFisherfaces
from RecognizerLBP import RecognizerLBP


def prepare_files():
	"""
	Extrae las imágenes del proveedor con los metadatos, localiza las caras y las guarda en un directorio temporal.
	"""

	prev_id = ''
	num = 0
	for file, metadata in provider:
		id = metadata['id']
		if id != prev_id:
			num = 0
			prev_id = id
		img = cv2.imread(file)
		gray = preprocessor.prepare(img)
		img = imutils.resize(img, width=384)
		rects = detector.detect_multiscale(gray)
		for x1, y1, x2, y2 in rects:
			img = cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 255))
			face = img[y1:y2, x1:x2]
			processed_face = processor.load(face)
			cv2.imshow("cara", processed_face)
			filename = 'temp/' + id + '_' + '{:03d}'.format(num) + '.png'
			print (num)
			num += 1
			cv2.imwrite(filename, face)
		print (rects)
		id = metadata['id']
		#cv2.imshow(id, img)
		cv2.waitKey(50)
	#recognizer.load('test01')



def compare():

	print ("Calculando Fisherfaces")
	recognizer_fisher = RecognizerFisherfaces()
	results_fisherfaces = test(recognizer_fisher)
	print ("Calculando Eigenfaces")
	recognizer_eigen = RecognizerEigenfaces()
	results_eigenfaces = test(recognizer_eigen)
	print ("Calculando LBP")
	recognizer_LBP = RecognizerLBP()
	results_LBP = test(recognizer_LBP)
	print ("Mostrando resultados")

	labels = ['Correcto', 'Incorrecto', 'Desconocido']
	fracs_fisherfaces = [
		results_fisherfaces['correct'],
		results_fisherfaces['wrong'],
		results_fisherfaces['unknown']
		]
	fracs_eigenfaces = [
		results_eigenfaces['correct'],
		results_eigenfaces['wrong'],
		results_eigenfaces['unknown']
		]
	fracs_LBP = [
		results_LBP['correct'],
		results_LBP['wrong'],
		results_LBP['unknown']
		]

	fig, axs = plt.subplots(1, 3)

	axs[0].pie(fracs_fisherfaces, labels=labels, shadow=True)
	axs[1].pie(fracs_eigenfaces, labels=labels, shadow=True)
	axs[2].pie(fracs_LBP, labels=labels, shadow=True)

	plt.show()


def autolabel(ax, rects):
	for rect in rects:
		height = rect.get_height()
		ax.annotate('{0:.3f}'.format(height),
				xy=(rect.get_x() + rect.get_width()/2, height),
				xytext=(0, 3),
				textcoords='offset points',
				ha='center',
				va='bottom')


def draw_time(x_values, process_time, recog_time, title):

	fig, ax = plt.subplots()
	x = np.arange(len(x_values))
	width=0.35

	process_time = np.asarray(process_time, dtype=np.float32)
	recog_time = np.asarray(recog_time, dtype=np.float32)

	rects1 = ax.bar(x=x, height=process_time, width=width, label="Tiempo de procesamiento")
	rects2 = ax.bar(x=x, height=recog_time, width=width, bottom=process_time, label="Tiempo de reconocimiento")

	ax.set_ylabel("Milisegundos")
	ax.set_title(title, fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(x_values, fontsize=11, fontweight="demibold")
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)

	plt.show()



def draw_graph(x_values, correct, wrong, unknown, title):

	fig, ax = plt.subplots()
	x = np.arange(len(x_values))
	width = 0.20

	correct = np.asarray(correct, dtype=np.float32)
	wrong = np.asarray(wrong, dtype=np.float32)
	unknown = np.asarray(unknown, dtype=np.float32)

	rects1 = ax.bar(x=x-width/2, height=correct, width=width, label="Correctas")
	rects2 = ax.bar(x=x+width/2, height=wrong, width=width, label="Incorrectas")
	rects3 = ax.bar(x=x+width/2, height=unknown, bottom=wrong, width=width, label="Desconocido")

	ax.set_ylabel("Porcentaje")
	ax.set_title(title, fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(x_values, fontsize=11, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)
	autolabel(ax, rects3)

	#fig.tight_layout()

	plt.show()



def test_LBP_radius():
	radius = [1, 2, 3, 5, 7, 10, 15, 20]
	neighs = [2, 4, 6, 8, 10, 12, 14, 16]

	analized = []
	correct = []
	wrong = []
	unknown = []
	process_time = []
	prediction_time = []

	for r in radius:
	#for n in neighs:
	#for th in threshold:
		#recognizer = RecognizerEigenfaces(num_components=c)
		recognizer = RecognizerLBP(radius=r)

		data = test(recognizer)
		print (data)
		analized.append(data['analized'])
		correct.append(data['correct']/data['analized'])
		wrong.append(data['wrong']/data['analized'])
		unknown.append(data['unknown']/data['analized'])
		process_time.append(data['process_time'])
		prediction_time.append(data['prediction_time'])

	draw_graph(neighs, correct, wrong, unknown, "Influencia del radio en LBP")

	draw_time(neighs, process_time, prediction_time, "Consumo CPU en LBP según el radio")


def test_LBP_neighs():

	neighs = [2, 4, 6, 8, 10, 12, 14, 16]

	analized = []
	correct = []
	wrong = []
	unknown = []
	process_time = []
	prediction_time = []

	for n in neighs:
		recognizer = RecognizerLBP(neighbors=n)

		data = test(recognizer)
		print (data)
		analized.append(data['analized'])
		correct.append(data['correct']/data['analized'])
		wrong.append(data['wrong']/data['analized'])
		unknown.append(data['unknown']/data['analized'])
		process_time.append(data['process_time'])
		prediction_time.append(data['prediction_time'])

	draw_graph(neighs, correct, wrong, unknown, "Influencia del número de vecinos en LBP")
	draw_time(neighs, process_time, prediction_time, "Consumo CPU en LBP según el número de vecinos")



def test_fisherfaces():
	
	components = [10, 20, 30, 40, 50, 75, 100, 125, 150, 200, 250] #, 110, 130, 150, 170, 190, 210, 230]
	#threshold = [10, 30, 50, 100, 250, 500, 1000, 2000, 4000, 6000]
	analized = []
	correct = []
	wrong = []
	unknown = []
	process_time = []
	prediction_time = []

	for c in components:
	#for th in threshold:
		#recognizer = RecognizerEigenfaces(num_components=c)
		recognizer = RecognizerFisherfaces(num_components=c, threshold=1000)

		data = test(recognizer)
		print (data)
		analized.append(data['analized'])
		correct.append(data['correct']/data['analized'])
		wrong.append(data['wrong']/data['analized'])
		unknown.append(data['unknown']/data['analized'])
		process_time.append(data['process_time'])
		prediction_time.append(data['prediction_time'])

	draw_graph(components, correct, wrong, unknown, "Influencia del número de componentes en Fisherfaces")

	draw_time(components, process_time, prediction_time, "Consumo CPU en Fisherfaces según número de componentes")



def test_eigenfaces():

	components = [10, 20, 30, 40, 50, 75, 100, 125, 150, 200, 250] #, 110, 130, 150, 170, 190, 210, 230]
	threshold = [10, 30, 50, 100, 250, 500, 1000, 2000, 4000, 6000]
	analized = []
	correct = []
	wrong = []
	unknown = []
	process_time = []
	prediction_time = []

	for c in components:
	#for th in threshold:
		#recognizer = RecognizerEigenfaces(num_components=c)
		recognizer = RecognizerEigenfaces(num_components=c)

		data = test(recognizer)
		print (data)
		analized.append(data['analized'])
		correct.append(data['correct']/data['analized'])
		wrong.append(data['wrong']/data['analized'])
		unknown.append(data['unknown']/data['analized'])
		process_time.append(data['process_time'])
		prediction_time.append(data['prediction_time'])

	draw_graph(components, correct, wrong, unknown, "Influencia del número de componentes en Eigenfaces")
	draw_time(components, process_time, prediction_time, "Consumo CPU en Eigenfaces según número de componentes")

	
def test(recognizer, verbose=False, num_train=10):

	path = str(Path.home()) + "/TFM/rasprec/source/programs/temp/" 

	subjects = {}
	faces_train = []
	labels_train = []
	faces_test = []
	labels_test = []
	
	lst_dir = os.walk(path)

	for root, dirs, files in lst_dir:
		for f in files:
			id = f[:10]
			filename = root + f
			if not id in subjects:
				subjects[id] = []
			subjects[id].append(filename)

	for subject in subjects:
		faces = subjects[subject]
		random.shuffle(faces)
		for train in faces[:num_train]:
			face = cv2.imread(train)
			faces_train.append(face)
			labels_train.append(subject)
		for test in faces[num_train:]:
			face = cv2.imread(test)
			faces_test.append(face)
			labels_test.append(subject)

	# En este punto tenemos faces_train/labels_train para el entrenamiento y faces_test/labels_test para pruebas

	# Procesamos las imágenes de entrenamiento
	faces_processed_train = []
	for face in faces_train:
		pface = processor.load(face)
		faces_processed_train.append(pface)
		
	# Añadimos las caras de entrenamiento al reconocedor
	i = 0
	while i < len(faces_processed_train):
		recognizer.add_face(faces_processed_train[i], labels_train[i])
		i += 1
	recognizer.train()

	recognizer.get_info()
	# Ahora a por las caras de prueba
	i = 0
	ok = 0
	wrong = 0
	unknown = 0
	while i < len(faces_test):
		face = faces_test[i]
		label = labels_test[i]
		i += 1
		time1 = time.perf_counter()
		pface = processor.load(face)
		time2 = time.perf_counter()
		pred = recognizer.predict(pface)
		time3 = time.perf_counter()

		if pred == "Desconocido":
			unknown += 1
		else:
			if label != pred:
				res = ' - NO'
				wrong += 1
			else:
				res = ''
				ok += 1

		if verbose:
			print ("Prediccion: " + str(pred) + '  -  ' + "Real: " + str(label) + res)
		
	#if verbose:
	print ("")
	print ("RESULTADOS")
	print ("--------------------------")
	print ("Algoritmo: " + str(type(recognizer)))
	print ("")
	print ("Caras analizadas:               " + str(i))
	print ("Identificaciones correctas:     " + str(ok))
	print ("Identificaciones incorrectas:   " + str(wrong))
	print ("Identificaciones no realizadas: " + str(unknown))

	results = {
		'algorithm': type(recognizer),
		'analized': i,
		'correct': ok,
		'wrong': wrong,
		'unknown': unknown,
		'process_time': (time2 - time1) * 1000,
		'prediction_time': (time3 - time2) * 1000
	}

	return results

	
		

	

#provider = ProviderDatasetMetadata(dataset='caltech_faces')
#for p, metadata in provider:
#	print (p)
#	print (metadata)

filename = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'

provider = ProviderDatasetMetadata(dataset='caltech_faces')
preprocessor = PreprocessorBasic(width=380, norm=rasprec.NORM_CLAHE)
detector = DetectorViola(filename, scale_factor=1.2)
processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5) #, debug=[rasprec.DEBUG_EYES_DETECTION])


# Menú para elegir el reconocedor
if os.name == 'posix':
	os.system('clear')
else:
	os.system('cls')

print ("Elige un reconocedor")
print ("--------------------")
print ("1.- Eigenfaces (según componentes)")
print ("2.- Fisherfaces (según componentes)")
print ("3.- Local Binary Patterns (según componentes)")
print ("4.- Local Binary Patterns (según vecinos)")
print ("5.- Comparar reconocedores")
print ("")
print ("0.- Preparar ficheros para el test")
print ("")
op = input("Elige una opción: ")

if op == '1':
	test_eigenfaces()
elif op == '2':
	test_fisherfaces()
elif op == '3':
	test_LBP_radius()
elif op == '4':
	test_LBP_neighs()
elif op == '5':
	compare()
elif op == '0':
	prepare_files()


