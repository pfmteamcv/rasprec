

import cv2
import dlib
from pathlib import Path
from imutils import face_utils
import time


p5 = dlib.shape_predictor(str(Path.home()) + "/TFM/rasprec/resources/facial_landmarks/shape_predictor_5_face_landmarks.dat")
p68 = dlib.shape_predictor(str(Path.home()) + "/TFM/rasprec/resources/facial_landmarks/shape_predictor_68_face_landmarks.dat")
classifier = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'

img = cv2.imread("../images/faces.png")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = []
detector = cv2.CascadeClassifier(classifier)
rects = detector.detectMultiScale(img)

for x, y, w, h in rects:
	f = img[y:y+h, x:x+w]
	faces.append (f)

iterations = 250

start = time.perf_counter()
for a in range(iterations):
	for f in faces:
		w, h = f.shape
		rect = dlib.rectangle(0, 0, w-1, h-1)
		landmarks = p68(f, rect)
		landmarks = face_utils.shape_to_np(landmarks)
end = time.perf_counter()

time68 = end - start
print ("Facial Landmarks 68 puntos")
print ("--------------------------")
print ("Tiempo total: " + str(time68))
print ("Tiempo por iteración: " + str(time68/(iterations*len(faces))))
print ("")


start = time.perf_counter()
for a in range(iterations):
	for f in faces:
		w, h = f.shape
		rect = dlib.rectangle(0, 0, w-1, h-1)
		landmarks = p5(f, rect)
		landmarks = face_utils.shape_to_np(landmarks)
end = time.perf_counter()

time5 = end-start
print ("Facial Landmarks 5 puntos")
print ("--------------------------")
print ("Tiempo total: " + str(time5))
print ("Tiempo por iteración: " + str(time5/(iterations*len(faces))))
print ("")


