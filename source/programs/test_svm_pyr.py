"""
	Pruebas de HOG + SVM con Precission and Recall
"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from ProviderDataset import ProviderDataset
from ProviderVideo import ProviderVideo
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Preprocessor import PreprocessorBasic
from Detector import Detector
from Pipeline import Pipeline
from Processor import Processor

import multiprocessing

import rasprec
import time
import cv2
import numpy as np


if __name__ == "__main__":
	# Vamos a usar características de HOG
	width = 80	
	height = 100
	cell_size = 20 
	block_size = 40
	block_stride = 20

	hog_params = {
		'win_size': (width, height),
		'block_size': (block_size, block_size),
		'block_stride': (block_stride, block_stride),
		'cell_size': (cell_size, cell_size),
		'nbins': 9
	}
	detector_params = {
		'scale_factor': 1.35,
		'min_width': width,
		'min_height': height,
		'disp': 20
	}
	face_size = (width, height)

	svm_params = {
		'kernel_type': cv2.ml.SVM_LINEAR,
		'type': cv2.ml.SVM_C_SVC,
		'gamma': 0.5,
		'c': 1.,
		'term_crit': (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6),
	}

	hf = FeaturesHOG(verbose=False, params=hog_params)

	train_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
		seed=0,
		train_percent=0.5,
		num_images=2000,
		train=True,
		verbose=True)
	test_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
		seed=0,
		train_percent=0.5,
		num_images=2000,
		train=False,
		verbose=True)
	train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
		seed=0,
		train_percent=0.5, 
		num_images=2000,
		train=True,
		verbose=True)
	test_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
		seed=0,
		train_percent=0.5, 
		num_images=2000,
		train=False,
		verbose=True)
	hard_mining_data = ProviderDataset("stanford_bg")
	pp = PreprocessorBasic(face_size=face_size)

	classifier_SVM =  ClassifierSVM(features=hf, params=svm_params, verbose=True)
	classifier_SVM.load_data(train_faces_svm, train_nfaces_svm, preprocessor=pp)
	classifier_SVM.train()

	faces = []
	for f in test_faces_svm:
		img = cv2.imread(f)
		img = pp.prepare_single_face(img)
		faces.append(img)

	nfaces = []
	for nf in test_nfaces_svm:
		img = cv2.imread(nf)
		img = pp.prepare_single_face(img)
		nfaces.append(img)

	classifier_SVM.show_parameters()

	preds = classifier_SVM.are_faces(faces)
	print ("Total de caras de test: " + str(len(preds.tolist())))
	num_fn = preds.tolist().count([0.0])
	print ("Falsos negativos: " + str(num_fn))
	
	preds = classifier_SVM.are_faces(nfaces)
	print ("Total de no caras de test: " + str(len(preds.tolist())))
	num_fp = preds.tolist().count([1.0])
	print ("Falsos positivos: " + str(num_fp))