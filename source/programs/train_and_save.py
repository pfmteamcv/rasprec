"""
	Este programa permite entrenar los difentes clasificadores y guardar los datos en
	un fichero.
"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from ProviderDataset import ProviderDataset
from ClassifierkNN import ClassifierkNN
from ClassifierSVM import ClassifierSVM
from Preprocessor import PreprocessorBasic

import os

def train_svm_hard_mining():
	# --------------------
	# Entrenamiento de SVM
	# --------------------
	train_faces_svm = ProviderDatasetTrainTest("training_faces",  
		seed=0,
		train_percent=1.,
		num_images=2000,
		train=True,
		verbose=True)
	train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces", 
		seed=0,
		train_percent=1., 
		num_images=2000,
		train=True,
		verbose=True)
	hard_mining_data = ProviderDataset("stanford_bg")

	print ("Entrenando y guardando HOG + SVM")
	pp = PreprocessorBasic(with_histo=True)
	classifier_SVM =  ClassifierSVM(features=hf, verbose=True)
	classifier_SVM.load_data(train_faces_svm, train_nfaces_svm, preprocessor=pp)
	classifier_SVM.train_and_save('hog.14-28-14.hm.100000.svm.xml', hard_mining_data, 100000)


def train_svm():
	# --------------------
	# Entrenamiento de SVM
	# --------------------
	train_faces_svm = ProviderDatasetTrainTest("training_faces",  
		seed=0,
		train_percent=1.,
		num_images=2000,
		train=True,
		verbose=True)
	train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces", 
		seed=0,
		train_percent=1., 
		num_images=2000,
		train=True,
		verbose=True)
	hard_mining_data = ProviderDataset("stanford_bg")

	print ("Entrenando y guardando HOG + SVM")
	pp = PreprocessorBasic(with_histo=True)
	classifier_SVM =  ClassifierSVM(features=hf, verbose=True)
	classifier_SVM.load_data(train_faces_svm, train_nfaces_svm, preprocessor=pp)
	classifier_SVM.train_and_save('hog.14-28-14.svm')


def train_knn():
	train_faces_knn = ProviderDatasetTrainTest("training_faces",  
		seed=0,
		train_percent=1.,
		num_images=400,
		train=True,
		verbose=True)
	train_nfaces_knn = ProviderDatasetTrainTest("training_nfaces", 
		seed=0,
		train_percent=1., 
		num_images=200,
		train=True,
		verbose=True)

	print ("Entrenando y guardando HOG + kNN")
	classifier_kNN = ClassifierkNN(features=hf, verbose=True)
	classifier_kNN.load_data(train_faces_knn, train_nfaces_knn)
	classifier_kNN.train_and_save('hog.14-28-14.knn')


if __name__ == "__main__":
	# Vamos a usar características de HOG
	hf = FeaturesHOG(verbose=True)
	# Inicializamos las características con los parámetros
	hf.set_params({
		"win_size": (48, 64)
		"cell_size": (8, 8),
		"block_size": (16, 16),
		"block_stride": (8, 8),
		"nbins": 9
		})

	os.system("cls")
	print ("ENTRENAMIENTO Y GRABACIÓN DE DATOS")
	print ("==================================")
	print ("Selecciona una opción:")
	print ("1.- Entrenar HOG + kNN")
	print ("2.- Entrenar HOG + SVM")
	print ("3.- Entrenar HOG + SVM con hard mining")
	opt = input("Selecciona una opción: ")


	if opt == '1':
		train_knn()
	elif opt == '2':
		train_svm()
	elif opt == '3':
		train_svm_hard_mining()