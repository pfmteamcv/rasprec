"""
	Para el algoritmo de detección facial HOG+kNN, evalúa lso resultados obtenidos con vectores de características
	con diferente número de dimensiones. 
	Para ello, se utilizan diferentes tamaños de celda, bloque, desplazamiento de bloque y número de bins.
"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

print(sys.path)

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
from DetectorHOGkNN import DetectorHOGkNN

from ProviderDatasetTrainTest import ProviderDatasetTrainTest

hf = FeaturesHOG(verbose=True)


params = [
	{
		"cell_size": (35, 35),
		"block_size": (35,35),
		"block_stride": (35, 35),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (7,7),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (14, 14),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (5, 5),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	]

# Preparamos los datasets
# 300 imágenes para entrenamiento y 700 imágenes para test
# Mismas imágenes para cada iteración
num_images = 2000
seed = 35
train_percent = 0.05
num_images_train = int(num_images * train_percent)
num_images_test = num_images - num_images_train

# Preparamos el dataset para entrenamiento y test
train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
test_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)

results = []

for p in params:
	for b in [5, 7, 9]:
		p["nbins"] = b

		# Aquí comienza cada iteración
		hf.set_params(p)
		classifier = ClassifierkNN(features=hf, params={"neighs": 1}, verbose=True)

		classifier.load_data(train_faces, train_nfaces)
		classifier.train()

		print ("")
		print ("-------------------------------------------------------------")
		print ("Parámetros: " + str(p))
		dimensions = hf.get_dimensions("../images/img.png")
		print ("Dimensiones: " + str(str(dimensions[0])))
		print ("")

		tp = classifier.are_faces(test_faces)
		fp = num_images_test - tp
		print ("Total caras evaluadas: " + str(num_images_test))
		print ("True positives: " + str(tp))
		print ("False positives: " + str(fp))

		print ("")
		fn = classifier.are_faces(test_nfaces)
		tn = num_images_test - fn
		print ("Total NO caras evaluadas: " + str(num_images_test))
		print ("True negatives: " + str(tn))
		print ("False negatives: " + str(fn))

		print ("")
		print ("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

		# Guardamos los resultados en un diccionario
		results .append({
			"cell_size":       p["cell_size"][0],
			"block_size":      p["block_size"][0],
			"block_stride":    p["block_stride"][0],
			"nbins":           p["nbins"],
			"dimensions":      dimensions[0],
			"eval_faces":      num_images_test,
			"true positives":  tp,
			"false positives": fp,
			"true negatives":  tn,
			"false negatives": fn
			})


print ("")
print ("")

print (results)

# Vamos a guardarlo en un fichero CSV
f = open("../data/hogknn.csv", "w")
f.write("cell_size,block_size,block_stride,nbins,dimensions,tp,fp,tn,fn\n")
for l in results:
	line = str(l["cell_size"]) + "," +\
		str(l["block_size"]) + "," +\
		str(l["block_stride"]) + "," +\
		str(l["nbins"]) + "," +\
		str(l["dimensions"]) + "," +\
		str(l["true positives"]) + "," +\
		str(l["false positives"]) + "," +\
		str(l["true negatives"]) + "," +\
		str(l["false negatives"]) + "\n"
	f.write(line)

f.close()


"""
print (p)

f=hf.get_features("images/img.png")
d = hf.get_dimensions("images/img.png")
print ("Dimensiones: " + str(d[0]))
print ("")
"""
