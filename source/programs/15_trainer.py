import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Preprocessor import PreprocessorBasic
from PreprocessorFace import PreprocessorFace
from ProviderDataset import ProviderDataset
from FeaturesHOG import FeaturesHOG
from FeaturesBoWSIFT import FeaturesBoWSIFT
from FeaturesBoWSURF import FeaturesBoWSURF
from FeaturesBoWORB import FeaturesBoWORB
from ClassifierkNN import ClassifierkNN
from ClassifierSVM import ClassifierSVM
from ClassifierBayes import ClassifierBayes
from DetectorViola import DetectorViola
from DetectorCustom import DetectorCustom

import rasprec
import os
import cv2

import matplotlib.pyplot as plt
import numpy as np

train_faces = ProviderDatasetTrainTest("training_faces", seed=20, 
	train_percent=0.2, num_images=2000, train=True)
test_faces = ProviderDatasetTrainTest("training_faces", seed=20, 
	train_percent=0.2, num_images=2000, train=False)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", seed=20, 
	train_percent=0.2, num_images=2000, train=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", seed=20, 
	train_percent=0.2, num_images=2000, train=False)


def print_cm(tn, fp, fn, tp, title, str_feat, str_clas, str_params_feat, str_params_clas,
	precision=None, recall=None, accuracy=None):

	total = tn + tp + fn + fp
	cm  = [[tp, fp], [fn, tn]]

	# Valores como porcentajes
	ptn = tn / (tn + fp)	
	ptp = tp / (tp + fn)
	pfn = fn / (fn + tp)
	pfp = fp / (fp + tn)
	pcm = [[ptp, pfp], [pfn, ptn]]
	
	# Métrica P&R
	if precision is None:
		precision = tp / (tp + fp)
	if recall is None:
		recall = tp / (tp + fn)
	if accuracy is None:
		accuracy = (tp + tn) / total

	classNames = ['Cara', 'No cara']

	plt.figure(figsize=(10, 6))
	plt.suptitle("Algoritmo: " + title + "\n", fontsize=18, fontweight='bold')

	# Gráfica con TN, TP, FN y FP
	plt.subplot(121)
	plt.imshow(cm, interpolation='nearest', cmap=plt.cm.coolwarm)
	classNames = ['Cara', 'No cara']
	plt.title('Matriz de confusión\n', fontsize=17, fontweight='bold', 
		color='dimgray')
	plt.ylabel('Estimado\n', fontsize=14, fontweight='demibold')
	plt.xlabel('\nRealidad', fontsize=14, fontweight='demibold')
	tick_marks = np.arange(len(classNames))
	plt.xticks(tick_marks, classNames, fontsize=13)
	plt.yticks(tick_marks, classNames, fontsize=13,
				rotation=90, verticalalignment='center')
	s = [['TP', 'FP'], ['FN', 'TN']]
	for i in range(2):
		for j in range(2):
			txt = str(s[i][j] + ' = ' + str(cm[i][j])) + '\n' + \
				"{0:.4f}".format(pcm[i][j]*100) + '%'
			plt.text(j, i, txt, fontsize=13,
				horizontalalignment='center', verticalalignment='center',
				fontweight='demibold', color='white')

	# Subimagen derecha
	plt.subplot(122, frameon=False)
	plt.axis('off')
	plt.text(0.20, 0.80, "Precisión: ", fontsize=14)
	plt.text(0.65, 0.80, "{0:.3f}".format(precision), fontsize=14, fontweight='demibold')

	plt.text(0.20, 0.64, "Exhaustividad: ", fontsize=14)
	plt.text(0.65, 0.64, "{0:.3f}".format(recall), fontsize=14, fontweight='demibold')

	plt.text(0.20, 0.48, "Exactitud: ", fontsize=14)
	plt.text(0.65, 0.48, "{0:.3f}".format(accuracy), fontsize=14, fontweight='demibold')

	# Parámetros del algoritmo
	plt.text(0.5, 0.26, "Características: " + str_feat, fontsize=12,
		fontweight='demibold', color='darkslateblue', horizontalalignment='center')
	plt.text(0.5, 0.22, str_params_feat, fontsize=12,
		color='slateblue', horizontalalignment='center')

	plt.text(0.5, 0.16, "Clasificador: " + str_clas, fontsize=12,
		fontweight='demibold', color='darkslateblue', horizontalalignment='center')
	plt.text(0.5, 0.12, str_params_clas, fontsize=12,
		color='slateblue', horizontalalignment='center')

	plt.show()




def train_hog_knn(params_feat, params_clas):

	# Entrenamiento de HOG + kNN

	hf = FeaturesHOG(params_feat, verbose=True)
	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	classifier = ClassifierkNN(features=hf, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("hog_train_defaults.knn")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Celda: ' + str(params_feat['cell_size']) + \
			' Bloque: ' + str(params_feat['block_size']) + \
			' Desp.: ' + str(params_feat['block_stride']) + \
			' Nbins: ' + str(params_feat['nbins'])

	s_clas = 'Vecinos: ' + str(params_clas['neighs']) + \
			' Umbral: '

	if params_clas['threshold'] == 0:
		s_clas += str(int((params_clas['neighs']/2)+1))
	else:
		s_clas += str(params_clas['threshold'])

	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'], title="HOG + kNN",
		str_feat='Histogram of Gradients',
		str_clas='k-Nearest Neighbours',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def train_hog_svm(params_feat, params_clas):

	hf = FeaturesHOG(params_feat, verbose=False)
	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	classifier = ClassifierSVM(features=hf, params=params_clas, verbose=True)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save('hog_train_defaults.svm')

	# Se prueba el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)
	
	s_feat = 'Celda: ' + str(params_feat['cell_size']) + \
			' Bloque: ' + str(params_feat['block_size']) + \
			' Desp.: ' + str(params_feat['block_stride']) + \
			' Nbins: ' + str(params_feat['nbins'])

	s_clas = 'Kernel: ' 
	if params_clas['kernel_type'] == cv2.ml.SVM_LINEAR:
		s_clas += 'Lineal - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_POLY:
		s_clas += 'Polinomial - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_SIGMOID:
		s_clas += 'Sigmoideo - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_RBF:
		s_clas += 'RBF - '

	if params_clas['kernel_type'] != cv2.ml.SVM_LINEAR:
		s_clas += ' Gamma: ' + str(params_clas['gamma']) + ' - '

	s_clas += 'C: ' + str(params_clas['c']) + '.'
		
	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'], title="HOG + SVM",
		str_feat='Histogram of Gradients',
		str_clas='Support Vector Machines',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])



def train_hog_bayes(params_feat, params_clas):

	# Entrenamiento de HOG + kNN

	hf = FeaturesHOG(params_feat, verbose=True)
	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	classifier = ClassifierBayes(features=hf, params=params_clas, verbose=True)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("hog_train_defaults.bayes")

	print("------------------")
	print ("Hasta aquí el entrenamiento")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Celda: ' + str(params_feat['cell_size']) + \
			' Bloque: ' + str(params_feat['block_size']) + \
			' Desp.: ' + str(params_feat['block_stride']) + \
			' Nbins: ' + str(params_feat['nbins'])

	s_clas = 'Vecinos: ' + str(params_clas['neighs']) + \
			' Umbral: '

	if params_clas['threshold'] == 0:
		s_clas += str(int((params_clas['neighs']/2)+1))
	else:
		s_clas += str(params_clas['threshold'])

	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'], title="HOG + SVM",
		str_feat='Histogram of Gradients',
		str_clas='k-Nearest Neighbours',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def train_bow_sift_knn(params_feat, params_clas):

	# Entrenamiento de BoW SIFT + kNN
	fbs = FeaturesBoWSIFT()
	fbs.generate_dictionary(train_faces, dict_size=params_feat['dict_size'])

	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	
	classifier = ClassifierkNN(features=fbs, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("sift_train_defaults.knn")


	print("------------------")
	print ("Hasta aquí el entrenamiento")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Tamaño diccionario: ' + str(params_feat['dict_size'])

	s_clas = 'Vecinos: ' + str(params_clas['neighs']) + \
			' Umbral: '

	if params_clas['threshold'] == 0:
		s_clas += str(int((params_clas['neighs']/2)+1))
	else:
		s_clas += str(params_clas['threshold'])

	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'],
		title="BoW SIFT + kNN",
		str_feat='BoW SIFT',
		str_clas='k-Nearest Neighbours',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])



def train_bow_sift_svm(params_feat, params_clas):

	# Entrenamiento de BoW SIFT + kNN
	fbs = FeaturesBoWSIFT()
	fbs.generate_dictionary(train_faces, dict_size=params_feat['dict_size'])

	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	
	classifier = ClassifierSVM(features=fbs, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("sift_train_defaults.svm")

	print ("[INFO] Entrenamiento finalizado.")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Tamaño diccionario: ' + str(params_feat['dict_size'])

	s_clas = 'Kernel: ' 
	if params_clas['kernel_type'] == cv2.ml.SVM_LINEAR:
		s_clas += 'Lineal - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_POLY:
		s_clas += 'Polinomial - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_SIGMOID:
		s_clas += 'Sigmoideo - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_RBF:
		s_clas += 'RBF - '

	if params_clas['kernel_type'] != cv2.ml.SVM_LINEAR:
		s_clas += ' Gamma: ' + str(params_clas['gamma']) + ' - '

	s_clas += 'C: ' + str(params_clas['c']) + '.'
	
	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'],
		title="BoW SIFT + SVM",
		str_feat='BoW SIFT',
		str_clas='Support Vector Machines',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def train_bow_surf_knn(params_feat, params_clas):

	# Entrenamiento de BoW SURF + kNN
	fbs = FeaturesBoWSURF(verbose=True)
	fbs.generate_dictionary(train_faces, dict_size=params_feat['dict_size'])

	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	
	classifier = ClassifierkNN(features=fbs, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("surf_train_defaults.knn")

	print ("[INFO] Entrenamiento finalizado.")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Tamaño diccionario: ' + str(params_feat['dict_size'])

	s_clas = 'Vecinos: ' + str(params_clas['neighs']) + \
			' Umbral: '

	if params_clas['threshold'] == 0:
		s_clas += str(int((params_clas['neighs']/2)+1))
	else:
		s_clas += str(params_clas['threshold'])

	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'],
		title="BoW SURF + kNN",
		str_feat='BoW SURF',
		str_clas='k-Nearest Neighbours',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def train_bow_surf_svm(params_feat, params_clas):

	# Entrenamiento de BoW SURF + kNN
	fbs = FeaturesBoWSURF()
	fbs.generate_dictionary(train_faces, dict_size=params_feat['dict_size'])

	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	
	classifier = ClassifierSVM(features=fbs, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("surf_train_defaults.knn")

	print ("[INFO] Entrenamiento finalizado.")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Tamaño diccionario: ' + str(params_feat['dict_size'])

	s_clas = 'Kernel: ' 
	if params_clas['kernel_type'] == cv2.ml.SVM_LINEAR:
		s_clas += 'Lineal - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_POLY:
		s_clas += 'Polinomial - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_SIGMOID:
		s_clas += 'Sigmoideo - '
	elif params_clas['kernel_type'] == cv2.ml.SVM_RBF:
		s_clas += 'RBF - '

	if params_clas['kernel_type'] != cv2.ml.SVM_LINEAR:
		s_clas += ' Gamma: ' + str(params_clas['gamma']) + ' - '

	s_clas += 'C: ' + str(params_clas['c']) + '.'
	
	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'],
		title="BoW SURF + SVM",
		str_feat='BoW SURF',
		str_clas='Support Vector Machines',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def train_bow_orb_knn(params_feat, params_clas):

	# Entrenamiento de BoW SURF + kNN
	fbs = FeaturesBoWORB(verbose=True)
	fbs.generate_dictionary(train_faces, dict_size=params_feat['dict_size'])

	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm=rasprec.NORM_CLAHE)
	
	classifier = ClassifierSVM(features=fbs, params=params_clas, verbose=False)
	classifier.load_data(train_faces, train_nfaces, preprocessor=pp)
	classifier.train_and_save("surf_train_defaults.knn")

	print ("[INFO] Entrenamiento finalizado.")

	# Probamos el clasificador
	data = classifier.precision_and_recall(test_faces, test_nfaces, preprocessor=pp)
	print (data)

	s_feat = 'Tamaño diccionario: ' + str(params_feat['dict_size'])

	s_clas = 'Vecinos: ' + str(params_clas['neighs']) + \
			' Umbral: '

	if params_clas['threshold'] == 0:
		s_clas += str(int((params_clas['neighs']/2)+1))
	else:
		s_clas += str(params_clas['threshold'])

	print_cm(tn=data['tn'], fp=data['fp'], fn=data['fn'], tp=data['tp'],
		title="BoW ORB + kNN",
		str_feat='BoW ORB',
		str_clas='k-Nearest Neighbours',
		str_params_feat= s_feat,
		str_params_clas= s_clas,
		precision=data['precision'], recall=data['recall'], accuracy=data['accuracy'])


def test_viola(params):

	detector = DetectorViola(params=params)

	# Al probarlo de forma directa con el detector y no con el clasificador hay que hacerlo diferente
	pp = PreprocessorFace(gray=rasprec.GRAY_VALUE, norm= rasprec.NORM_RANGE)

	for f in test_faces:
		img = cv2.imread(f)
		img = pp.prepare(img)
		cv2.imshow('asdfasdf', img)
		cv2.waitKey(100)
		rect = detector.detect_multiscale(img)
		print (rect)


#print_cm(tn=15, fp=2, fn=13, tp=0, title="HOG + SVM")

def main_menu():
	if os.name == 'posix':
		os.system('clear')
	else:
		os.system('cls')

	print ("ELIGE LAS CARACTERÍSTICAS")
	print ("-------------------------")
	print ("1.- HOG")
	print ("2.- BoW SIFT")
	print ("3.- BoW SURF")
	print ("")
	opf = input("Elige una opción: ")

	params_feat = {}

	if opf == "1":	# HOG
		params_feat = {
			"cell_size": (7, 7),
			"block_size": (14,14),
			"block_stride": (7, 7),
			"nbins": 9
		}
		print ("")
		print ("Parámetros de HOG")
		print ("-----------------")
		cs = input ("Tamaño de celda [7]: ")
		if cs != '':
			params_feat['cell_size'] = (int(cs), int(cs))
		bs = input ("Tamaño de bloque [14]: ")
		if bs != '':
			params_feat['block_size'] = (int(bs), int(bs))
		bst = input ("Desplazamiento de bloque [7]: ")
		if bst != '':
			params_feat['block_stride'] = (int(bst), int(bst))
		nb = input ("Número de rangos [9]: ")
		if nb != '':
			params_feat['nbins'] = int(nb)

		print ("")
		print ("Tamaño de celda:          " + str(params_feat['cell_size']))
		print ("Tamaño de bloque:         " + str(params_feat['block_size']))
		print ("Desplazamiento de bloque: " + str(params_feat['block_stride']))
		print ("Número de rangos:         " + str(params_feat['nbins']))
		print ("")

	elif opf == "2":	# BoW SIFT
		params_feat = {
			'dict_size': 128
		}
		print ("")
		print ("Parámetros de BoW SIFT")
		print ("----------------------")
		ds = input("Tamaño del diccionario [128]: ")
		if ds != '':
			params_feat['dict_size'] = int(ds)

	elif opf == "3":	# BoW SURF
		params_feat = {
			'dict_size': 128
		}
		print ("")
		print ("Parámetros de BoW SURF")
		print ("----------------------")
		ds = input("Tamaño del diccionario [128]: ")
		if ds != '':
			params_feat['dict_size'] = int(ds)

	elif opf == "4":	# Viola-Jones
		params = {
			'scale_factor': 1.0005,
			'min_neighbors': 0,
			'min_size': (65, 65)
		}
		

	if not opf == "4":	# Si no es Viola Jones se elige el clasificador
		print ("")
		print ("ELIGE EL CLASIFICADOR")
		print ("---------------------")
		print ("1.- k-Nearest Neighbours (kNN)")
		print ("2.- Support Vector Machines (SVM)")
		print ("")
		opc = input("Elige una opción: ")

		params_clas = {}

		if opc == '1':		# kNN
			params_clas = {
				"neighs": 7,
				"threshold" : 0
			}
			print ("")
			print ("Parámetros de kNN")
			print ("-----------------")
			ne = input ("Número de vecinos [7]: ")
			if ne != '':
				params_clas['neighs'] = int(ne)
			th = input ("Número de vecinos para se considerado cara [0=Mayoría]: ")
			if th != '':
				params_clas['threshold'] = int(th)
			
		elif opc == '2':	# SVM
			params_clas = {
				'kernel_type': cv2.ml.SVM_LINEAR,
				'type': cv2.ml.SVM_C_SVC,
				'gamma': 0.5, 
				'c': 12.5,
				'degree': 5,
				'term_crit': (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6)
				}

			""" Los posibles valores de los parámetros son:
					type:
						cv2.ml.SVM_C_SVC
						cv2.ml.SVM_NU_SVC
						cv2.ml.SVM_ONE_CLASS
						cv2.ml.SVM_EPS_SVR
						cv2.ml.SVM_NUSVR
					kernel_type:
						cv2.ml.SVM_LINEAR
						cv2.ml.SVM_POLY
						cv2.ml.SVM_RBF
						cv2.ml.CVM_SIGMOID
					term_crit:
						cv2.TERM_CRITERIA_COUNT
						cv2.TERM_CRITERIA_EPS
						cv2.TERM_CRITERIA_MAX_ITER
			"""
			print ("")
			print ("Parámetros de SVM")
			print ("-----------------")
			kt = input ("Tipo de kernel ([1: Lineal]. 2:Polinomial. 3: RBF. 4: Sigmoideo): ")
			if kt != '':
				if kt == '1':
					params_clas['kernel_type'] = cv2.ml.SVM_LINEAR
				elif kt == '2':
					params_clas['kernel_type'] = cv2.ml.SVM_POLY
				elif kt == '3':
					params_clas['kernel_type'] = cv2.ml.SVM_RBF
				elif kt == '4':
					params_clas['kernel_type'] = cv2.ml.SVM_SIGMOID
			if params_clas['kernel_type'] != cv2.ml.SVM_LINEAR:
				g = input("Valor gamma [0.5]: ")
				if g != '':
					params_clas['gamma'] = float(g)

			if params_clas['kernel_type'] == cv2.ml.SVM_POLY:
				d = input("Grado [5]: ")
				if d != '':
					params_clas['degree'] = float(d)

			c = input("Valor C [12.5]: ")
			if c != '':
				params_clas['c'] = float(c)

		elif opc == '3':	# Naive Bayes
			print ("No tiene parámetros.")

	print ("")

	# SE LANZA EL ALGORITMO CORRESPONDIENTE
	if opf == '4':
		# Viola Jones
		test_viola(params)
	elif opf == '1' and opc == '1':
		# HOG + kNN
		train_hog_knn(params_feat, params_clas)
	elif opf == '1' and opc == '2':
		# HOG + SVM
		train_hog_svm(params_feat, params_clas)
	elif opf == '1' and opc == '3':
		# HOG + Bayes
		train_hog_bayes(params_feat, params_clas)
	elif opf == '2' and opc == '1':
		# BoW SIFT + kNN
		train_bow_sift_knn(params_feat, params_clas)
	elif opf == '2' and opc == '2':
		# BoW SIFT + SVM
		train_bow_sift_svm(params_feat, params_clas)
		pass
	elif opf == '2' and opc == '3':
		# BoW SIFT + Bayes
		pass
	elif opf == '3' and opc == '1':
		# BoW SURF + kNN
		train_bow_surf_knn(params_feat, params_clas)
	elif opf == '3' and opc == '2':
		# BoW SURF + SVM
		train_bow_surf_svm(params_feat, params_clas)
	elif opf == '3' and opc == '3':
		# BoW SURF + Bayes
		pass
	elif opf == '4' and opc == '1':
		# BoW ORB + kNN
		train_bow_orb_knn(params_feat, params_clas)
	elif opf == '4' and opc == '2':
		# BoW ORB + SVM
		pass
	elif opf == '4' and opc == '3':
		# BoW ORB + Bayes
		pass





main_menu()