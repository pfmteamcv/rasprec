"""
	Para el algoritmo de detección facial HOG+SVM, evalúa los resultados obtenidos con vectores de características
	con diferente número de dimensiones. 
	Para ello, se utilizan diferentes tamaños de celda, bloque, desplazamiento de bloque y número de bins.
"""

import sys
from pathlib import Path
import os

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from ClassifierBayes import ClassifierBayes
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
import threading

from ProviderDatasetTrainTest import ProviderDatasetTrainTest

hf = FeaturesHOG(verbose=True)

params = [
	{
		"cell_size": (35, 35),
		"block_size": (35,35),
		"block_stride": (35, 35),
		"nbins": 9
	},
	{
		"cell_size": (14, 14),
		"block_size": (14,14),
		"block_stride": (14, 14),
		"nbins": 9
	},
	{
		"cell_size": (14, 14),
		"block_size": (28,28),
		"block_stride": (14, 14),
		"nbins": 9
	},
	{
		"cell_size": (14, 14),
		"block_size": (28,28),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (7,7),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (14, 14),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (5, 5),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	]


seed = 35			# Semilla para el generados pseudo-aleatorio que escoge los grupos de test/entrenamiento
results = []		# Lista donde se guardan los resultados 

os.system("cls")
print ("Características de HOG")
print ("----------------------")
num_images = input("Número de imágenes a procesar (max. 2000) [2000]: ")
if not num_images or int(num_images) > 2000:
	num_images = 2000
else:
	num_images = int(num_images
		)
train_percent = input("Porcentaje de imágenes para entrenamiento (entre 0. y 1.) [0.05]: ")
if not train_percent or float(train_percent) > 1.0:
	train_percent = 0.05
else:
	train_percent = float(train_percent)

num_images_train = int(num_images*train_percent)
num_images_test  = int(num_images - num_images_train)


# Preparamos el dataset para entrenamiento y test
train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
test_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)


print("Imágenes para entrenamiento: " + str(num_images_train))
print("Imágenes para test: " + str(num_images_test))
print()
print("Clasificador")
print("------------")
print("a) Normal Bayes")
print("b) k Nearest Neighbours (kNN)")
print("c) Support Vector Machines (SVM)")
print("q) Salir")
print()
op = None
while op not in ["a", "b", "c", "q"]:
	op = input ("Escoge el clasificador: ")
print ()


for p in params:
	for b in [5, 7, 9]:

		p["nbins"] = b

		# Aquí comienza cada iteración
		hf.set_params(p)
		if op == "a" or op == "A":
			classifier = ClassifierBayes(features=hf, verbose=True)
		elif op == "b" or op == "B":
			classifier = ClassifierkNN(features=hf, verbose=True)
		elif op == "c" or op == "C":
			classifier = ClassifierSVM(features=hf, verbose=True)
		else:
			sys.exit()

		classifier.load_data(train_faces, train_nfaces)


		classifier.train()

		dimensions = hf.get_dimensions("../images/img.png")
		tp = classifier.are_faces(test_faces)
		fp = num_images_test - tp
		fn = classifier.are_faces(test_nfaces)
		tn = num_images_test - fn


		print ("[RESULTS] Parámetros: " + str(p) + "    Dimensiones: " + str(dimensions[0]))
		print ("[RESULTS] True positives: " + str(tp) + "   False positives: " + str(fp)
			+ "   True negatives: " + str(tn) + "   False negatives: " + str(fn))
		print ()


		# Guardamos los resultados en un diccionario
		results .append({
			"cell_size":       p["cell_size"][0],
			"block_size":      p["block_size"][0],
			"block_stride":    p["block_stride"][0],
			"nbins":           p["nbins"],
			"dimensions":      dimensions[0],
			"eval_faces":      num_images_test,
			"true positives":  tp,
			"false positives": fp,
			"true negatives":  tn,
			"false negatives": fn
			})



print ("")
print ("")

print (results)

# Vamos a guardarlo en un fichero CSV
if op == "a" or op == "A":
	filename="hogbayes.csv"
elif op == "b" or op == "B":
	filename="hogknn.csv"
elif op == "c" or op == "C":
	filename="hogsvm.csv"


f = open("../data/" + filename, "w")
f.write("cell_size,block_size,block_stride,nbins,dimensions,tp,fp,tn,fn\n")
for l in results:
	line = str(l["cell_size"]) + "," +\
		str(l["block_size"]) + "," +\
		str(l["block_stride"]) + "," +\
		str(l["nbins"]) + "," +\
		str(l["dimensions"]) + "," +\
		str(l["true positives"]) + "," +\
		str(l["false positives"]) + "," +\
		str(l["true negatives"]) + "," +\
		str(l["false negatives"]) + "\n"
	f.write(line)

f.close()


