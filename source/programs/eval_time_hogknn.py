"""
	Este programa evalúa el tiempo medio necesario para determinar si una imagen de 70x70 píxeles
	es una cara o no utilizando el algoritmo HOG+kNN

	Variaciones:
		- Según el número de características
		- Según el valor de k

"""
 
import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
from DetectorHOGkNN import DetectorHOGkNN
from ProviderDatasetTrainTest import ProviderDatasetTrainTest

import time

hf = FeaturesHOG(verbose=True)

params = [
	{
		"cell_size": (35, 35),
		"block_size": (35,35),
		"block_stride": (35, 35),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (10, 10),
		"nbins": 9
	},
	{
		"cell_size": (10, 10),
		"block_size": (20,20),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (7,7),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (7, 7),
		"nbins": 9
	},
	{
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (14, 14),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (5, 5),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (5, 5),
		"nbins": 9
	},
	{
		"cell_size": (5, 5),
		"block_size": (10,10),
		"block_stride": (10, 10),
		"nbins": 9
	},
	]

# Preparamos los datasets
# 300 imágenes para entrenamiento y 700 imágenes para test
# Mismas imágenes para cada iteración
num_images = 2000		# Imágenes totales del dataset
seed = 177				# Semilla para que todas las pruebas utilicen las mismas imágenes
train_percent = 0.05	# Porcentaje de imágenes de entrenamiento. 0.05 = 100
num_images_train = int(num_images * train_percent)
num_images_test = num_images - num_images_train

# Realizamos 5 iteraciones con diferentes conjuntos de imágenes
for i in range(5):

	# Preparamos el dataset para entrenamiento y test
	train_faces = ProviderDatasetTrainTest("training_faces",  
		seed=i, train_percent=train_percent,
		num_images=num_images, train=True, verbose=True)
	test_faces = ProviderDatasetTrainTest("training_faces",  
		seed=i, train_percent=train_percent, 
		num_images=num_images, train=False, verbose=True)
	train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
		seed=i, train_percent=train_percent, 
		num_images=num_images, train=True, verbose=True)
	test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
		seed=i, train_percent=train_percent, 
		num_images=num_images, train=False, verbose=True)

	results = {}

	# ##############################################
	# Según el número de características (7 vecinos)
	# ##############################################
	for p in params:
		for b in [5, 7, 9]:
			p["nbins"] = b

			# Aquí comienza cada iteración
			hf.set_params(p)
			classifier = ClassifierkNN(features=hf, params={"neighs": 7}, verbose=True)

			classifier.load_data(train_faces, train_nfaces)
			classifier.train()

			# print ("")
			# print ("-------------------------------------------------------------")
			# print ("Parámetros: " + str(p))
			dimensions = hf.get_dimensions("../images/img.png")
			# print ("Dimensiones: " + str(str(dimensions[0])))
			# print ("")

			faces_start = time.time()
			tp = classifier.are_faces(test_faces)
			faces_end = time.time()
			fp = num_images_test - tp
			faces_total_time = faces_end - faces_start
			faces_number = num_images_test
			faces_indiv_time = faces_total_time / faces_number

			# print ("Total caras evaluadas: " + str(faces_number))
			# print ("Tiempo total empleado: " + str(faces_total_time))
			# print ("Tiempo por imagen:     " + str(faces_indiv_time))

			# print ("")
			nfaces_start = time.time()
			fn = classifier.are_faces(test_nfaces)
			nfaces_end = time.time()
			tn = num_images_test - fn
			nfaces_total_time = nfaces_end - nfaces_start
			nfaces_number = num_images_test
			nfaces_indiv_time = nfaces_total_time / nfaces_number

			# print ("Total NO caras evaluadas: " + str(nfaces_number))
			# print ("Tiempo total empleado: " + str(nfaces_total_time))
			# print ("Tiempo por imagen:     " + str(nfaces_indiv_time))

			# print ("")
			# print ("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

			k = str(dimensions[0])
			t = (faces_indiv_time + nfaces_indiv_time) / 2
			if k in results:
				results[k] = (results[k] + t ) / 2
			else:
				results[k] = t


# Vamos a guardarlo en un fichero CSV
f = open("../data/hogknn_times.csv", "w")
f.write("dimensions,time\n")
for k,v in results.items():
	line = str(k) + "," + str(v) + "\n"
	f.write(line)

f.close()

