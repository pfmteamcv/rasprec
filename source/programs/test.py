import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")
import cv2

from FeaturesBoWSIFT import FeaturesBoWSIFT
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from ProviderDataset import ProviderDataset
from Preprocessor import PreprocessorBasic
from ClassifierSVM import ClassifierSVM
import rasprec
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
	
	prec = []
	rec  = []
	ds = [5, 10, 20, 40, 60, 80, 100, 125, 150, 200, 250, 350, 400, 500]

	for dict_size in ds:
		# Preparamos los provedores de caras y no caras
		print ("Preparando los proveedores...")

		train_percent = 0.25
		num_images = 2000
		#dict_size = 1000

		train_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
			seed=0,
			train_percent=train_percent,
			num_images=num_images,
			train=True,
			verbose=False)
		train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
			seed=0,
			train_percent=train_percent,
			num_images=num_images,
			train=True,
			verbose=False)
		test_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
			seed=0,
			train_percent=train_percent,
			num_images=num_images,
			train=False,
			verbose=False)
		test_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
			seed=0,
			train_percent=train_percent,
			num_images=num_images,
			train=False,
			verbose=False)

		# Preparamos el preprocesador
		print ("Preparando el preprocesador...")
		pp = PreprocessorBasic(verbose=False, norm=rasprec.NORM_CLAHE)

		# Preparamos el generador de características
		print ("Preparando el generador de características...")
		fbs = FeaturesBoWSIFT(verbose=False)
		prv = ProviderDataset('training_faces', num_images=200)
		fbs.generate_dictionary(prv, preprocessor=pp, dict_size=dict_size)

		# Preparamos el clasificador
		print ("Preparando el clasificador...")
		svm_params = {
			'kernel_type': cv2.ml.SVM_LINEAR,
			'type': cv2.ml.SVM_C_SVC,
			'gamma': 10.5,
			'c': .15,
			'term_crit': (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6),
		}
		svm = ClassifierSVM(features=fbs, params=svm_params, verbose=False)
		svm.load_data(train_faces_svm, train_nfaces_svm)
		svm.train()
		#svm.train(hard_mining_provider=hard_mining_data, hard_mining_num_images=50000, hard_mining_width=width, hard_mining_height=height)

		# Análisis de resultados
		print ("Analizando resultados...")
		dimensions = fbs.get_dimensions("../images/img.png")
		num_images = int(num_images * (1 - train_percent))
		faces_results = svm.are_faces(test_faces_svm)
		tp = np.count_nonzero(faces_results == 1.)
		fn = num_images - tp
		
		nfaces_results = svm.are_faces(test_nfaces_svm)
		tn = np.count_nonzero(nfaces_results == 0.)
		fp = num_images - tn

		print ("Número total de imágenes: " + str(num_images))
		print ("True positives: "  + str(tp))
		print ("False positives: " + str(fp))
		print ("True negatives: "  + str(tn))
		print ("False negatives: " + str(fn))

		"""
		print ("[RESULTS] True positives: " + str(tp) + "   False positives: " + str(fp)
			+ "   True negatives: " + str(tn) + "   False negatives: " + str(fn))
		print ()
		"""
		print ("")
		precision = tp / (tp + fp)
		recall = tp / (tp + fn)
		print ("Tamaño diccionario: " + str(dict_size))
		print ("Precisión:          " + str(precision))
		print ("Exahustividad:      " + str(recall))
		print ("")

		prec.append(precision)
		rec.append(recall)

plt.plot(ds, prec, label="Precisión")
plt.plot(ds, rec, label="Exahustividad")
#plt.legend()
plt.show()
