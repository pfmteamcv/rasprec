import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Preprocessor import PreprocessorBasic
from pathlib import Path
from ProviderVideo import ProviderVideo
from ProviderPiCamThread import ProviderPiCamThread
from Player import Player
import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt
import time

provider = ProviderPiCamThread()
pp = PreprocessorBasic()
player = Player()

frames = 0
start = time.perf_counter()
for frame in provider:
	frame = pp.prepare(frame)
	frames += 1
	player.write(frame)
	#cv2.imshow("Camera", frame)
	key = cv2.waitKey(1) & 0xFF
	
	
	# Paramos si se pulsa q(113), Q(81) o ESC(27)
	if key in [113, 81, 27]:
		provider.stop()
		player.stop()
		break

end = time.perf_counter()
print("Frames mostrados: " + str(frames))
print("FPS: " + str(frames/(end-start)) + " frames por segundo")

cv2.destroyAllWindows()
