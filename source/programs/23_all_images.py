import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import cv2
import imutils
import rasprec

from Preprocessor import PreprocessorBasic
from FeaturesHOG import FeaturesHOG
from FeaturesBoWSIFT import FeaturesBoWSIFT
from FeaturesBoWSURF import FeaturesBoWSURF

img = cv2.imread(str(Path.home()) + "/TFM/rasprec/source/images/miguel.jpg")
img = imutils.resize(img, width=640)

cv2.imshow("Original", img)

pp1 = PreprocessorBasic(gray=rasprec.GRAY_EQUAL, norm=None)
pp2 = PreprocessorBasic(gray=rasprec.GRAY_WEIGHTED, norm=None)
pp3 = PreprocessorBasic(gray=rasprec.GRAY_VALUE, norm=None)

img1 = pp1.prepare(img)
img2 = pp2.prepare(img)
img3 = pp3.prepare(img)

cv2.imshow("Mismo valor", img1)
cv2.imshow("Ponderado", img2)
cv2.imshow("HSV", img3)

pp4 = PreprocessorBasic(gray=rasprec.GRAY_WEIGHTED, norm=rasprec.NORM_HISTO)
pp5 = PreprocessorBasic(gray=rasprec.GRAY_WEIGHTED, norm=rasprec.NORM_RANGE)
pp6 = PreprocessorBasic(gray=rasprec.GRAY_WEIGHTED, norm=rasprec.NORM_CLAHE)

img4 = pp4.prepare(img)
img5 = pp5.prepare(img)
img6 = pp6.prepare(img)

cv2.imshow("Ecualización", img4)
cv2.imshow("Rango", img5)
cv2.imshow("CLAHE", img6)

cv2.waitKey(1)
cv2.destroyAllWindows()

# Pasamos a la fase de detección con Ponderado y CLAHE
img = img6
# Cogemos solo la cara, a ojo del todo
img = img[95:240, 175:320]
img = imutils.resize(img, width=70)

fh = FeaturesHOG()
fh.draw_features(img)

img = imutils.resize(img, width=640)
fs = FeaturesBoWSIFT()
fs.draw_features(img)

fsurf =FeaturesBoWSURF()
fsurf.draw_features(img)

