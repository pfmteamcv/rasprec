import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Detector import Detector
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ClassifierSVM import ClassifierSVM
from Heatmap import Heatmap
import cv2
import numpy as np
import Image


def alpha_rect (image, top_left, bottom_right, color, alpha):
	""" Dibuja un rectángulo sobre una imagen con el grado de transparencia indicado.

	*************** ESTO SE PODÍA PASAR A UN MÓDULO CON UTILIDADES ******************
	"""

	overlay = image.copy()
	output = image.copy()

	# Dibujamos el rectángulo en la copia
	cv2.rectangle(overlay, top_left, bottom_right, color, -1)

	# Y la superponemos con transparencia
	cv2.addWeighted(overlay, alpha, output, 1 - alpha, 0, output)

	return output
		





hf = FeaturesHOG(verbose=True)
cl = ClassifierkNN(features=hf, verbose=True)

train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=100, train_percent=0.1,
	num_images=2000, train=True, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=100, train_percent=1., 
	num_images=2000, train=True, verbose=True)


cl.load_data(train_faces, train_nfaces)
cl.train()


image = cv2.imread("../images/faces.png")
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

d = Detector(cl, verbose=True)
hm = Heatmap(700, 1050)

win = d.get_sliding_windows(gray_image) 

i=0
for w in win:
	img = np.asarray(w[0])	# El primer elemento es la image
	coords = w[1]			# El segundo las coordenadas
	x1, y1, x2, y2 = w[1]
	
	is_face = cl.is_face(img, threshold=15)
	
	i += 1
	if is_face:
		#image = alpha_rect(image, (coords[0], coords[1]), (coords[0]+coords[2], coords[1]+coords[3]), (0, 0, 255), 0.2)
		#hm.add((coords[0], coords[1], coords[0]+coords[2], coords[1]+coords[3]))
		image = alpha_rect(image, (x1, y1), (x2, y2), (0, 0, 255), 0.2)
		hm.add((x1, y1, x2, y2))
		#cv2.rectangle(image, (coords[0], coords[1]), (coords[0]+coords[2], coords[1]+coords[3]), 255, 1)
		#cv2.imshow("imagen", img)
		#cv2.waitKey(0)

		# Esto habrá que implementarlo dentro de la clase Detector

print (hm.boxes)

cv2.imshow("imagen", image)
cv2.imshow("Heatmap", hm.get_colormap())
cv2.imshow("threshold", hm.get_thresholded_map())
lm=hm.get_labeled_map()
lm=cv2.applyColorMap(lm*40, cv2.COLORMAP_HOT)
cv2.imshow("Labeled", lm)

bboxes = hm.get_bounding_boxes()

for ul, br in bboxes:
	image = cv2.rectangle(image, ul, br, (0, 255, 0), 2)

cv2.imshow("imagen boxes", image)

cv2.waitKey(0)
cv2.destroyAllWindows()



