import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import os
import rasprec
import cv2
import time

from Preprocessor import PreprocessorBasic
from DetectorViola import DetectorViola
from Processor import Processor
from RecognizerFisherfaces import RecognizerFisherfaces
from Pipeline import Pipeline
from threading import Thread


def cls():
	# Menú para elegir el reconocedor
	if os.name == 'posix':
		os.system('clear')
	else:
		os.system('cls')


def show(img, data):

	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)


# Objetos que se usarán
provider = None
preprocessor = PreprocessorBasic(width=320)
detector = DetectorViola(scale_factor=1.4)
processor = None
recognizer = RecognizerFisherfaces()

cls()
print ("--------------------------------------")
print ("RECONOCIMIENTO FACIAL CON RASPBERRY PI")
print ("--------------------------------------")
print ("")

print ("Elige el tipo de histograma para el procesamiento")
print ("-------------------------------------------------")
print ("  1.- Histograma por partes")
print ("  2.- Histograma por partes simple")
print ("  3.- Histograma CLAHE para toda la imagen")
print ("  4.- Reflejo de un lado de la cara")
print ("  5.- Sin histograma")
print ("  0.- Salir")

op = None
while op not in ['0', '1', '2', '3', '4', '5']:
	op = input("  Escoge una opción: ")
	if op == '1':
		# Histograma por partes
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_PARTS)
	elif op == '2':
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_PARTS_SIMPLE)
	elif op == '3':
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_CLAHE)
	elif op == '4':
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_MIRROR)
	elif op == '5':
		processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5, histogram=rasprec.HISTO_NONE) 
	elif op == '0':
		sys.exit()


cls()
print ("--------------------------------------")
print ("RECONOCIMIENTO FACIAL CON RASPBERRY PI")
print ("--------------------------------------")
print ("")

print ("Elige el origen de los datos")
print ("----------------------------")
print ("  1.- Fichero de vídeo")
print ("  2.- Webcam")
print ("  3.- Cámara de la Raspberry Pi")
print ("  0.- Salir")
print ("")

op = None
while op not in ['0', '1', '2', '3']:
	op = input("  Escoge una opción: ")
	if op == '1':
		# Origen desde vídeo
		print ("")
		print ("  Videos disponibles")
		print ("  ------------------")

		path = str(Path.home()) + "/TFM/rasprec/resources/videos/" 
		lst_dir = os.walk(path)
		for root, dirs, files in lst_dir:
			i = 0
			for f in files:
				print ("    " + str(i) + ".- " + f)
				i += 1

		print ("")
		op = input ("    Escoge un vídeo: ")
		filename = root + files[int(op)]

		from ProviderVideo import ProviderVideo
		provider = ProviderVideo(filename)
		print ("")
		print ("    El origen de los datos será el vídeo " + filename)
		print ("")

	elif op == '2':
		# Origen desde Webcam
		from ProviderWebcam import ProviderWebcam
		provider = ProviderWebcam()
		print ("")
		print ("  El origen de los datos será una Webcam")
		print ("")

	elif op == '3':
		# Origen desde la cámara de la Raspberry
		from ProviderPiCamThread import ProviderPiCamThread
		provider = ProviderPiCamThread()
		print ("  El origen de los datos será la cámara de la Raspberry Pi")
		print ("")

	elif op == '0':
		# Salir
		sys.exit()

print ("Elige un como quieres entrenar el reconocedor")
print ("---------------------------------------------")
print ("  1.- Fichero de entrenamiento")
print ("  2.- Carpeta de imágenes")
print ("  0.- Salir")
print ("")

op = None
while not op in ['0', '1', '2']:
	op = input("  Elige una opción: ")

	if op == '1':
		# Fichero de entrenamiento
		print ("")
		print ("  Ficheros disponibles")
		print ("  --------------------")

		path = str(Path.home()) + "/TFM/rasprec/resources/train_data/recognizer/" 
		lst_dir = os.walk(path)
		for root, dirs, files in lst_dir:
			i = 0
			fisher_files = []
			for f in files:
				if f[-6:] == 'fisher':
					print ("    " + str(i) + ".- " + f)
					fisher_files.append(f[:-7])
					i += 1

			print ("")
			op = input ("    Escoge un fichero: ")
			filename = root + fisher_files[int(op)]
			print ("")
			print ("    El fichero escogido es : " + filename)
			print ("")

			recognizer.load(fisher_files[int(op)])

	elif op == '2':
		# Entrenamiento desde carpeta con imágenes
		print ("")
		print ("  El reconocedor se entrenará desde la carpeta con imágenes")
		print ("")
		recognizer.train_from_dir()

	elif op == '0':
		# Salir
		sys.exit()



# Inicializamos el pipeline
pipeline = Pipeline(
	provider       = provider,
	preprocessor   = preprocessor,
	detector       = detector,
	face_processor = processor,
	recognizer     = recognizer,
	#debug 		   = [] #[rasprec.DEBUG_HISTOGRAM]
	debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC,
	rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)

frames = 0
cv2.namedWindow('video', cv2.WINDOW_NORMAL)
cv2.resizeWindow('video', 320, 240)
start = time.perf_counter()

identifications = {
	'correct': 0,
	'wrong':   0,
	'unknown': 0
}

frame = 0
for img, data in pipeline:
	print ("Frame: " + str(frame))
	frame += 1

	for d in data:
		id = d['id']
		if id == 'Victor':
			identifications['correct'] += 1
		elif id == 'Desconocido':
			identifications['unknown'] += 1
		else:
			identifications['wrong'] += 1

	frames += 1
	et = time.perf_counter()
	t = et-start
	#Thread(target=show, args=(img, data)).start()

	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)

	if cv2.waitKey(0) == ord('q'):

		break

t = pipeline.get_times()
k=[]
v=[]
#for key, value in t.items():
#	print ('{:>15}: {:011.7f}'.format(key, float(value)))
	
end = time.perf_counter()
t = end - start

cv2.destroyAllWindows()

print ("RESULTADOS")
print ("======================")
print ("Tiempos")
print ("----------------------")
print ("Tiempo total: " + str(t) + " segundos.")
print ("Frames procesados: " + str(frames) + " frames.")
print ("FPS: " + str(frames/t) + " imágenes por segundo.")
print ("")
print ("Identificaciones")
print ("----------------------")
print ("Correctas:    " + str(identifications['correct']))
print ("Erróneas:     " + str(identifications['wrong']))
print ("Desconocidas: " + str(identifications['unknown']))

