import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Preprocessor import PreprocessorBasic

from ProviderVideo import ProviderVideo
from ProviderDataset import ProviderDataset
#from ProviderPiCamThread import ProviderPiCamThread
from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from DetectorViola import DetectorViola
from DetectorCustom import DetectorCustom
from Processor import Processor
from RecognizerEigenfaces import RecognizerEigenfaces
from Player import Player
from Pipeline import Pipeline

import rasprec

import cv2
import numpy as np
import imutils
import time

from matplotlib import pyplot as plt
from pathlib import Path
from threading import Thread


def show(img, data):

	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)



# Selección de Detector
det = "HOGkNN"
if det == "Viola":
	detector = DetectorViola(scale_factor=1.4)		# Detector Viola Jones
else:
	train_faces = ProviderDataset("training_faces", num_images=50)
	train_nfaces = ProviderDataset("training_nfaces", num_images=100)
	p = {
		"cell_size": (7, 7),
		"block_size": (14,14),
		"block_stride": (7, 7),
		"nbins": 9
	}
	hf = FeaturesHOG(p, verbose=True)
	classifier = ClassifierkNN(features=hf, params={"neighs": 7}, verbose=False)
	classifier.load_data(train_faces, train_nfaces)
	classifier.train()
	detector = DetectorCustom(classifier=classifier)

# Inicializamos los componentes
#provider = ProviderPiCamThread()
provider = ProviderVideo('../data/portatil_01.avi')
preprocessor = PreprocessorBasic()						# Preprocesador
processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5)
recognizer = RecognizerEigenfaces()
recognizer.load('test01')

# Inicializamos el pipeline
pipeline = Pipeline(
	provider       	= provider,
	preprocessor   	= preprocessor,
	detector       	= detector,
	face_processor 	= processor,
	recognizer     	= recognizer,
	player 			= None,
	debug 			= []
	#debug          	= []
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_GEOMETRIC]#, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)

cv2.namedWindow('video', cv2.WINDOW_NORMAL)

for img, data in pipeline:
	show(img, data)
	Thread(target=show, args=(img, data)).start()
	cv2.waitKey(1)
	
cv2.destroyAllWindows()

"""
frames = 0
start = time.perf_counter()
for frame in provider:
	frame = pp.prepare(frame)
	frames += 1
	player.write(frame)
	#cv2.imshow("Camera", frame)
	key = cv2.waitKey(1) & 0xFF
	
	
	# Paramos si se pulsa q(113), Q(81) o ESC(27)
	if key in [113, 81, 27]:
		provider.stop()
		player.stop()
		break

end = time.perf_counter()
print("Frames mostrados: " + str(frames))
print("FPS: " + str(frames/(end-start)) + " frames por segundo")

cv2.destroyAllWindows()
"""