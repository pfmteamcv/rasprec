"""
	Este programa analiza cuál es el mejor método para seleccionar si una imagen es cara o no
	evaluando las distancias de los vecinos hasta el candidato.

"""

import sys
from pathlib import Path
import os

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
import threading
import time 

from ProviderDatasetTrainTest import ProviderDatasetTrainTest

hf = FeaturesHOG(verbose=True)



# Preparamos el dataset para entrenamiento y test
seed = 100
num_images = 2000
train_percent = 0.1
train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
test_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)


p = {
		"cell_size": (14, 14),
		"block_size": (28,28),
		"block_stride": (14, 14),
		"nbins": 9
	}
hf.set_params(p)
classifier = ClassifierkNN(features=hf, verbose=True)

# Entrenamos el clasificador
classifier.load_data(train_faces, train_nfaces)
classifier.train()

# Estos tienen que ser todo caras
start = time.time()
results, neighs, dists = classifier.evaluate(test_faces)
end = time.time()

# Vamos a mostrar únicamente los que se han evaluado incorrectamente
i = 0
for f in results:
	if f == 0:	# Si es un falso negativo
		print ("")
		print ("Vecinos:    " + str(neighs[i].tolist()))
		print ("Distancias: " + str(dists[i].tolist()))
	i += 1

print ("Elapsed time: " + str(end-start))

"""
nf, resultsnf = classifier.evaluate(test_nfaces)
print (nf)
print (resultsnf)
"""