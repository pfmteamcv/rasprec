import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from DetectorViola import DetectorViola
from Pipeline import Pipeline
from Preprocessor import PreprocessorBasic
from ProviderVideoThread import ProviderVideoThread
from ProviderVideo import ProviderVideo
from ProviderDataset import ProviderDataset
from Processor import Processor
from threading import Thread
from FeaturesHOG import FeaturesHOG
from DetectorCustom import DetectorCustom
from ClassifierkNN import ClassifierkNN
from RecognizerEigenfaces import RecognizerEigenfaces
from RecognizerFisherfaces import RecognizerFisherfaces
from RecognizerLBP import RecognizerLBP
import cv2
import time
import os
import rasprec
import numpy as np

import matplotlib.pyplot as plt


def show(img, data):

	for d in data:
		x1, y1, x2, y2 = d['rect']
		name = d['id']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		cv2.putText(img, name, (x1, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 200)

	cv2.imshow('video', img)


filename = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'


#provider = ProviderVideoThread('../data/portatil_01.avi')
provider = ProviderVideo('../data/portatil_01.avi')
#provider = ProviderWebcam()
#provider = ProviderWebcamThread()
preprocessor = PreprocessorBasic(width=640)
detector = DetectorViola(filename, scale_factor=1.5)
processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5)
#processor = Processor(eye_detect_method=rasprec.EYES_VIOLA)

frames = 0
 

#detector = DetectorViola()

# Menú para elegir el reconocedor
if os.name == 'posix':
	os.system('clear')
else:
	os.system('cls')

print ("Elige un reconocedor")
print ("--------------------")
print ("1.- Eigenfaces")
print ("2.- Fisherfaces")
print ("3.- Local Binary Patterns")
print ("")
op = input("Elige una opción: ")

if op == '1':
	recognizer = RecognizerEigenfaces()
	recognizer.load('test01')
elif op == '2':
	recognizer = RecognizerFisherfaces()
	recognizer.load('test01')
elif op == '3':
	recognizer = RecognizerLBP()
	recognizer.load('test01')


# Inicializamos el pipeline
pipeline = Pipeline(
	provider       = provider,
	preprocessor   = preprocessor,
	detector       = detector,
	face_processor = processor,
	recognizer     = recognizer,
	debug          = [rasprec.DEBUG_HEATMAP, rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_ELLIPTICAL]
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)


cv2.namedWindow('video', cv2.WINDOW_NORMAL)
start = time.perf_counter()
for img, data in pipeline:
	#show(img, data)
	frames += 1
	et = time.perf_counter()
	t = et-start
	#print ('Frames: {:>5}  Tiempo: {:07.5f}'.format(frames, t/frames))
	Thread(target=show, args=(img, data)).start()
	cv2.waitKey(1)
	#show(img, data)

#pipeline.debug_info()

t = pipeline.get_times()
k=[]
v=[]
#for key, value in t.items():
#	print ('{:>15}: {:011.7f}'.format(key, float(value)))
	
end = time.perf_counter()
t = end - start
#print ("Tiempo total: " + str(t))




############################################################################
############################################################################
############################################################################
############################################################################
# https://drive.google.com/drive/folders/0B7EVK8r0v71pWEZsZE9oNnFzTm8