import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")
import cv2

from FeaturesBoWSIFT import FeaturesBoWSIFT
from ProviderDataset import ProviderDataset

file = str(Path.home()) + '/TFM/rasprec/source/images/image.jpg'

fbs = FeaturesBoWSIFT(verbose=True)
provider = ProviderDataset('training_faces', num_images=2)
provider_face = ProviderDataset('training_faces', num_images=6)
provider_nface = ProviderDataset('training_nfaces', num_images=6)

fbs.generate_dictionary(provider) 

print ("Caras")
for filename in provider_face:
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	feat = fbs.get_features(img)
	print (feat)

print ("No caras")
for filename in provider_nface:
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	feat = fbs.get_features(img)
	print (feat)

