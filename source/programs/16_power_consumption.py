import csv

from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt


data = {
	'time': [],
	'voltage': [],
	'current': [],
	'power': []
}

with open('power_idle_full_4.csv', mode='r') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=';')
	line_count = 0
	for row in csv_reader:
		if line_count != 0:
			time = row[0]
			voltage = float(row[1])
			current = float(row[2])
			power = voltage*current

			data['time'].append(line_count)
			data['voltage'].append(voltage)
			data['current'].append(current)
			data['power'].append(round(power, 4))
		line_count += 1


print (data)

host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

par1 = host.twinx()
par2 = host.twinx()

offset = 60
new_fixed_axis = par2.get_grid_helper().new_fixed_axis
par2.axis['right'] = new_fixed_axis(loc='right',
	axes=par2, offset=(offset, 0))

par1.axis['right'].toggle(all=True)
par2.axis['right'].toggle(all=True)

host.set_xlim(min(data['time']), max(data['time']))
host.set_ylim(1, 3)	# Potencia

host.set_xlabel("Tiempo (seg)")
host.set_ylabel("Potencia (W)")
par1.set_ylabel("Voltaje (V)")
par2.set_ylabel("Corriente (A)")

p1, = host.plot(data['time'], data['power'], label='Potencia', aa=True)
p2, = par1.plot(data['time'], data['voltage'], label='Voltaje', aa=True)
p3, = par2.plot(data['time'], data['current'], label='Corriente', aa=True)

par1.set_ylim(0, 10)	# Voltaje
par2.set_ylim(0, 2) 	# Intensidad

host.legend()

host.axis['left'].label.set_color(p1.get_color())
par1.axis['right'].label.set_color(p2.get_color())
par2.axis['right'].label.set_color(p3.get_color())

plt.show()
	