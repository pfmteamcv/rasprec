

"""
El cometido de este fichero es generar caras útiles para el aprendizaje de clasificadores
partiendo de un dataset e identificando las caras con el algoritmo de Viola Jones integrado
en OpenCV.
Las caras requeriran una comprobación manual.
"""

import cv2
from pathlib import Path 
from DetectorViola import DetectorViola
from DatasetProvider import DatasetProvider
from Preprocessor import Preprocessor
import imutils
import dlib
from EyeDetector import LandmarkEyeDetector

home = str(Path.home())
dir_lm = home + "/TFM/rasprec/resources/facial_landmarks/"

dp = DatasetProvider("BioID")	# Origen de los datos
pp = Preprocessor(color_space="proportional")
dv = DetectorViola()

#predictor = dlib.shape_predictor(dir_lm + "shape_predictor_68_face_landmarks.dat")
predictor = dlib.shape_predictor(dir_lm + "shape_predictor_5_face_landmarks.dat")

img_number=0	# Contador para obtener el nombre de archivo
for file in dp:
	img = cv2.imread(file)
	img = pp.prepare(img)
	rects = dv.detect_multiscale(img)
	for r in rects:
		x = r[1]
		y = r[0]
		w = r[2]
		h = r[3]
		face = img[x:x+w, y:y+h]
		#face = imutils.resize(face, width=50)
		# Localizamos los landmarks de la imagen
		ed = LandmarkEyeDetector(predictor, face)
		ed.detect()
		landmarks = ed.get_landmarks()

		# Obtenemos las coordenadas de cada uno de los elementos
		rex1, rey1 = landmarks[1] # Coordenadas 1 de ojo derecho
		rex2, rey2 = landmarks[0] # Coordenadas 2 de ojo derecho
		lex1, ley1 = landmarks[2] # Coordenadas 1 de ojo izquierdo
		lex2, ley2 = landmarks[3] # Coordenadas 2 de ojo izquierdo
		mx, my = landmarks[4] # Coordenadas de boca
		
		# Calculamos la altura media de los ojos
		ey = int((rey1+rey2+ley1+ley2)/4)
		# Calculamos la posición horizontal central de cada ojo
		lex = int((lex1+lex2)/2)	# Ojo izquierdo
		rex = int((rex1+rex2)/2)	# Ojo derecho
		# Obtenemos tamaño cara
		wf, hf = face.shape

		# La distancia entre ojos ha de ser un 50% de la imagen
		# Calculamos el nuevo ancho en base a esto
		eye_to_eye = rex-lex	# Distancia entre ojos
		new_size = int(eye_to_eye*2) # 100/50 = 2

		#print ("------------")
		#print ("Alto: " + str(hf))
		#print ("Ancho: " + str(wf))
		left_pos   = int((wf-new_size)/2)		# Posición para recortar por la izquierda
		right_pos  = left_pos+new_size			# Posición para recortar por la derecha
		top_pos    = int(ey-(new_size*0.25))	# Posición para recortar por arriba
		bottom_pos = top_pos + new_size			# Posición para recortar por abajo

		# Hay veces que se recorta porque top_pos es mayor que el alto de la imagen
		# En esos casos desplazamos un poco el recorte hacia arriba
		if bottom_pos > hf:
			bottom_pos=hf
			top_pos=bottom_pos-new_size

		face_crop = face[top_pos:bottom_pos, left_pos:right_pos]

		
		#print (face_crop.shape)
		face_crop = imutils.resize(face_crop, width=70)	

		xf, yf = face_crop.shape

		"""
		cv2.circle(face,(lex1,ley1), 2, (0,0,255), 2)
		cv2.circle(face,(lex2,ley2), 2, (0,0,255), 2)

		"""
		# face=cv2.cvtColor(face, cv2.COLOR_GRAY2BGR)
		#for xp, yp in landmarks:
		#	cv2.circle(face,(xp,yp), 2, (0, 0, 255), 2)
		
		# cv2.imshow("image", face)
		# cv2.imshow("image2", face_crop)
		file_name="face_" + str(img_number).zfill(4) + ".png"
		path_faces = home + "/TFM/datasets/training_dataset2/Faces/"
		cv2.imwrite(path_faces + file_name, face_crop)
		img_number+=1
		#cv2.waitKey()

print ("Se han procesado todas las imágenes.")