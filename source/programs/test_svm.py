"""
	Prueba de la clase Detector. 
	Entrena un algoritmo HOG + kNN y localiza las caras en una imagen. Muestra todos los candidatos
	a cara, el mapa de calor y el resultado final tras simplificar el resultado.

"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from ProviderDataset import ProviderDataset
from ProviderVideo import ProviderVideo
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Preprocessor import PreprocessorBasic
from Detector import Detector
from Pipeline import Pipeline
from Processor import Processor

import multiprocessing

import rasprec
import time
import cv2
import numpy as np


if __name__ == "__main__":
	# Vamos a usar características de HOG
	width = 80	
	height = 100
	cell_size = 20 
	block_size = 40
	block_stride = 20

	hog_params = {
		'win_size': (width, height),
		'block_size': (block_size, block_size),
		'block_stride': (block_stride, block_stride),
		'cell_size': (cell_size, cell_size),
		'nbins': 9
	}
	detector_params = {
		'scale_factor': 1.35,
		'min_width': width,
		'min_height': height,
		'disp': 20
	}
	face_size = (width, height)

	svm_params = {
		'kernel_type': cv2.ml.SVM_LINEAR,
		'type': cv2.ml.SVM_C_SVC,
		'gamma': 10.5,
		'c': .15,
		'term_crit': (cv2.TERM_CRITERIA_MAX_ITER, 100, 1e-6),
	}

	hf = FeaturesHOG(verbose=False, params=hog_params)

	train_faces_svm = ProviderDatasetTrainTest("training_faces_rect",  
		seed=0,
		train_percent=1.,
		num_images=500,
		train=True,
		verbose=True)
	train_nfaces_svm = ProviderDatasetTrainTest("training_nfaces_rect", 
		seed=0,
		train_percent=1., 
		num_images=500,
		train=True,
		verbose=True)
	hard_mining_data = ProviderDataset("stanford_bg")
	pp = PreprocessorBasic(face_size=face_size)

	classifier_SVM =  ClassifierSVM(features=hf, params=svm_params, verbose=True)
	classifier_SVM.load_data(train_faces_svm, train_nfaces_svm, preprocessor=pp)
	classifier_SVM.train(hard_mining_provider=hard_mining_data, hard_mining_num_images=50000, hard_mining_width=width, hard_mining_height=height)

	# Inicializamos los componentes
	#provider = ProviderVideo('../data/portatil_01.avi')
	provider = ProviderDataset('helen')
	preprocessor = PreprocessorBasic() 

	detector = Detector(
					params = detector_params,
					classifier = classifier_SVM, 
					verbose=False, 
					#concurrent=None, 
					concurrent='threads',
					fusion_mode=Detector.FUSION_HEATMAP)
	#detector = Detector(classifier = classifier, verbose=False, concurrent='threads')
	processor = Processor()

	# Inicializamos el pipeline
	pipeline = Pipeline(
		provider = provider,
		preprocessor = preprocessor,
		detector = detector,
		face_processor = processor,
		recognizer = None,
		debug = (rasprec.DEBUG_HEATMAP, rasprec.DEBUG_BOUNDING_BOXES)
		)

	start = time.time()
	for img, data in pipeline:
		for d in data:
			x1, y1, x2, y2 = d['rect']
			img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)
		#hm = pipeline.get_heatmap()
		#cv2.imshow('Heatmap', hm)
		cv2.imshow("img", img)
		if cv2.waitKey(100000) & 0xFF == ord('q'):
			break
	end = time.time()
	total_time=end-start
	print ("Tiempo empleado: " + str(total_time))

	"""

	d = Detector(classifier = classifier, verbose=True)

	# Estos son los parámetros por defecto
	d.set_params({
		'scale_factor': 1.25, 
		'min_size': 70,
		'disp': 20
		})

	"""


	"""
	img = cv2.imread('../images/faces.png')
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rects_all = d.detect_multiscale(img, debug='bounding_boxes')
	heatmap = d.detect_multiscale(img, debug='heatmap')
	rects = d.detect_multiscale(img)

	print (rects)

	img_all_boxes = np.copy(img)
	for x1, y1, x2, y2 in rects_all:
		img_all_boxes = cv2.rectangle(img_all_boxes, (x1, y1), (x2, y2), 255, 1)

	for x1, y1, x2, y2 in rects:
		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)



	cv2.imshow("Antes de heatmap", img_all_boxes)
	cv2.imshow("Despues de heatmap", img)
	cv2.imshow("Heatmap", heatmap)
	cv2.waitKey(0)
	"""