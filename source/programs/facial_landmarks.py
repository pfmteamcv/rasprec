import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Processor import Processor
import cv2

p = Processor(verbose=True)

filename = '../images/face05.png'
img = cv2.imread(filename)
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
x1, y1, x2, y2 = p.load(img, method=Processor.EYES_FACIAL_LANDMARKS)

print (str(x1) + ' - ' + str(y1))
print (str(x2) + ' - ' + str(y2))

img = cv2.rectangle(img, (x1, y1), (x1+1, y1+1), 255, 1)
img = cv2.rectangle(img, (x2, y2), (x2+1, y2+1), 255, 1)

cv2.imshow("imagen", img)
cv2.waitKey(0)