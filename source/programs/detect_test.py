"""
	Este programa sirve para comprobar el funcioanmiento de la clase Detector
	Está imcompleto a falta de decidir algún tipo de programación multihilo
"""
import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Detector import Detector
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ClassifierSVM import ClassifierSVM
from Heatmap import Heatmap
import cv2


hf = FeaturesHOG(verbose=True)
cl = ClassifierkNN(features=hf, verbose=True)

train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=100, train_percent=1.0,
	num_images=200, train=True, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=100, train_percent=1., 
	num_images=2000, train=True, verbose=True)

# Entrenamos el clasificador
cl.load_data(train_faces, train_nfaces)
cl.train()


image = cv2.imread("../images/faces.png")
# Esto lo tiene que hacer la clase preprocessor
img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Creamos el detector
d = Detector(cl, verbose=True)
d.detect(img)