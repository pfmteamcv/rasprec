# Módulo únicamente para pruebas, borrar al acabar
import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

print(sys.path)

from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from ProviderDataset import ProviderDataset

from ProviderDatasetTrainTest import ProviderDatasetTrainTest

import cv2
import numpy as np


num_images = 200
seed = 35
train_percent = 0.8
# Preparamos el dataset para entrenamiento y test
train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
test_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)



# Preparamos el generador de características y el clasificador
hog = FeaturesHOG(verbose=True)
classifier = ClassifierSVM(features=hog, verbose=True)

# Cargamos los datos en el clasificador
classifier.load_data(train_faces, train_nfaces)

# Y lo entrenamos
classifier.train()

# Creamos el generador de características
fh = FeaturesHOG()


img = cv2.imread("../images/face01.png")
f = hog.get_features(img)
classifier.is_face(f)


tf = test_faces.get_array()
ltf = []
for i in tf:
	img = cv2.imread(i)
	f = hog.get_features(img)
	ltf.append(f)
classifier.are_faces(np.asarray(ltf))

tnf = test_nfaces.get_array()
ltnf = []
for i in tnf:
	img = cv2.imread(i)
	f = hog.get_features(img)
	ltnf.append(f)
classifier.are_faces(np.asarray(ltnf))

"""
print ("")
print ("FACES")
classifier.are_faces(test_faces)
print ("")
print ("NFACES")
classifier.are_faces(test_nfaces)



img = cv2.imread("img.png")


detector = DetectorHOGkNN(verbose=True)


faces_dp = DatasetProvider("training_faces")
nfaces_dp = DatasetProvider("training_nfaces")
knn.load_data(faces_dp, nfaces_dp, 2, 2) 

#knn.train_and_save()
knn.load_training()
"""

"""
f=hf.get_features("img.png")
print (f)
print (f.shape)
hf.draw_features("img.png")
"""


