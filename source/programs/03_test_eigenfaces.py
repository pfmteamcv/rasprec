import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import cv2
from RecognizerEigenfaces import RecognizerEigenfaces
from pathlib import Path

rec = RecognizerEigenfaces(verbose=True)

home = str(Path.home())
dataset_dir	 = home + "/TFM/datasets/Yale/faces/"

train1 = [
	"subject01.centerlight.pgm",
	"subject01.glasses.pgm",
	"subject01.happy.pgm", 
	"subject01.leftlight.pgm",
	"subject01.noglasses.pgm",
	"subject01.normal.pgm",
	"subject01.rightlight.pgm",
	"subject01.sad.pgm"
	]

train2 = [
	"subject02.centerlight.pgm",
	"subject02.glasses.pgm",
	"subject02.happy.pgm", 
	"subject02.leftlight.pgm",
	"subject02.noglasses.pgm",
	"subject02.normal.pgm",
	"subject02.rightlight.pgm",
	"subject02.sad.pgm"
	]

train3 = [
	"subject03.centerlight.pgm",
	"subject03.glasses.pgm",
	"subject03.happy.pgm", 
	"subject03.leftlight.pgm",
	"subject03.noglasses.pgm",
	"subject03.normal.pgm",
	"subject03.rightlight.pgm",
	"subject03.sad.pgm"
	]

test1 = [
	"subject01.sleepy.pgm",
	"subject01.surprised.pgm",
	"subject01.wink.pgm",
	]

test2 = [
	"subject02.sleepy.pgm",
	"subject02.surprised.pgm",
	"subject02.wink.pgm",
	]

test3 = [
	"subject03.sleepy.pgm",
	"subject03.surprised.pgm",
	"subject03.wink.pgm",
	]


for t in train1:
	file = dataset_dir	 + t
	img = cv2.imread(file)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rec.add_face(img, 1)

for t in train2:
	file = dataset_dir	 + t
	img = cv2.imread(file)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rec.add_face(img, 2)

"""
for t in train3:
	file = dataset_dir	 + t
	img = cv2.imread(file)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	rec.add_face(img, 3)
"""

rec.train()

print ("")
print ("Test sujeto 01")
print ("--------------")
for t in test1:
	file = dataset_dir + t
	face = cv2.imread(file)
	face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
	pred = rec.predict(face)
	print (pred)

print ("")
print ("Test sujeto 02")
print ("--------------")
for t in test2:
	file = dataset_dir + t
	face = cv2.imread(file)
	face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
	pred = rec.predict(face)
	print (pred)

print ("")
print ("Test sujeto 03")
print ("--------------")
for t in test3:
	file = dataset_dir + t
	face = cv2.imread(file)
	face = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
	pred = rec.predict(face)
	print (pred)	

#rec.show_info()
#rec.save()

rec.draw()