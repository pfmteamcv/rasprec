import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")


from FeaturesBoWSIFT import FeaturesBoWSIFT
import cv2

feat = FeaturesBoWSIFT()

img = cv2.imread("../images/face02.png")
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

feat.get_features(img)