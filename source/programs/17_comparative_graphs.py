"""
	Dibuja las gráficas comparativas de precision and recall entre los distintos algoritmos.
"""

import csv
import numpy as np
import matplotlib.pyplot as plt


data = {
	'features': [],
	'classifier': [],
	'precision': [],
	'recall': [],
	'accuracy': [],
	'tp': [],
	'fp': [],
	'fn': [],
	'tn': [],
	'time': [],
}

data_list = [[], [], [], [], [], [], [], [], [], []]

with open('Metricas_PyR_tiempos.csv', mode='r') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	line_count = 0
	for row in csv_reader:
		if line_count != 0:
			data['features'].append(row[0])
			data['classifier'].append(row[1])
			data['precision'].append(float(row[2]))
			data['recall'].append(float(row[3]))
			data['accuracy'].append(float(row[4]))
			data['tp'].append(float(row[5]))
			data['fp'].append(float(row[6]))
			data['fn'].append(float(row[7]))
			data['tn'].append(float(row[8]))
			data['time'].append(float(row[9]))

			data_list[0].append(row[0])
			data_list[1].append(row[1])
			data_list[2].append(float(row[2]))
			data_list[3].append(float(row[3]))
			data_list[4].append(float(row[4]))
			data_list[5].append(float(row[5]))
			data_list[6].append(float(row[6]))
			data_list[7].append(float(row[7]))
			data_list[8].append(float(row[8]))
			data_list[9].append(float(row[9]))

			"""

			voltage = float(row[1])
			current = float(row[2])
			power = voltage*current

			data['time'].append(line_count)
			data['voltage'].append(voltage)
			data['current'].append(current)
			data['power'].append(round(power, 4))
			"""
		line_count += 1
	

data_list = np.asarray(data_list)



def autolabel(ax, rects):
	for rect in rects:
		height = rect.get_height()
		ax.annotate('{0:.3f}'.format(height),
				xy=(rect.get_x() + rect.get_width()/2, height),
				xytext=(0, 3),
				textcoords='offset points',
				ha='center',
				va='bottom')



def graph_hog_pyr():
	"""
	Gráfica precision and recall con características HOG
	"""

	data_hog = data_list[:,0:6]

	labels = data_hog[1]
	precision_hog = data_hog[2]
	recall_hog = data_hog[3]
	accuracy_hog = data_hog[4]

	x = np.arange(len(labels))
	width = 0.20	# Ancho de las barras

	print (precision_hog)

	fig, ax = plt.subplots()

	plt.ylim(top=1.1)
	plt.ylim(bottom=0)

	print("X:    " + str(x))
	print("Prec: " + str(precision_hog))

	rects1 = ax.bar(x=x-width, height=precision_hog.astype(np.float32), 
		width=width, label="Precisión")
	rects2 = ax.bar(x=x, height=recall_hog.astype(np.float32), 
		width=width, label="Exhaustividad")
	rects3 = ax.bar(x=x+width, height=accuracy_hog.astype(np.float32), 
		width=width, label="Exactitud")

	ax.set_ylabel("Valor")
	ax.set_title("Precisión y Exhaustividad - HOG", fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=14, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)
	autolabel(ax, rects3)

	fig.tight_layout()

	plt.show()



def graph_sift_pyr():
	"""
	Gráfica precision and recall con características HOG
	"""

	data_sift = data_list[:,7:13]

	labels = data_sift[1]
	precision_hog = data_sift[2]
	recall_hog = data_sift[3]
	accuracy_hog = data_sift[4]

	x = np.arange(len(labels))
	width = 0.20	# Ancho de las barras

	fig, ax = plt.subplots()

	plt.ylim(top=1.1)
	plt.ylim(bottom=0)

	print("X:    " + str(x))
	print("Prec: " + str(precision_hog))

	rects1 = ax.bar(x=x-width, height=precision_hog.astype(np.float32), 
		width=width, label="Precisión")
	rects2 = ax.bar(x=x, height=recall_hog.astype(np.float32), 
		width=width, label="Exhaustividad")
	rects3 = ax.bar(x=x+width, height=accuracy_hog.astype(np.float32), 
		width=width, label="Exactitud")

	ax.set_ylabel("Valor")
	ax.set_title("Precisión y Exhaustividad - BoW SIFT",
			fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=14, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)
	autolabel(ax, rects3)

	fig.tight_layout()

	plt.show()


def graph_surf_pyr():
	"""
	Gráfica precision and recall con características HOG
	"""

	data_surf = data_list[:,14:20]

	labels = data_surf[1]
	precision_hog = data_surf[2]
	recall_hog = data_surf[3]
	accuracy_hog = data_surf[4]

	x = np.arange(len(labels))
	width = 0.20	# Ancho de las barras

	print (precision_hog)

	fig, ax = plt.subplots()

	plt.ylim(top=1.1)

	print("X:    " + str(x))
	print("Prec: " + str(precision_hog))

	rects1 = ax.bar(x=x-width, height=precision_hog.astype(np.float32), 
		width=width, label="Precisión")
	rects2 = ax.bar(x=x, height=recall_hog.astype(np.float32), 
		width=width, label="Exhaustividad")
	rects3 = ax.bar(x=x+width, height=accuracy_hog.astype(np.float32), 
		width=width, label="Exactitud")

	ax.set_ylabel("Valor")
	ax.set_title("Precisión y Exhaustividad - BoW SURF",
			fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=14, fontweight='demibold')
	ax.legend()

	autolabel(ax, rects1)
	autolabel(ax, rects2)
	autolabel(ax, rects3)

	fig.tight_layout()

	plt.show()



def graph_times():

	data_times = data_list[9:10, :][0]
	data_algo = data_list[0:2, :]

	feats = data_algo[0]
	clas = data_algo[1]
	
	i = 0
	labels=[]
	while i < len(feats):
		cad = feats[i] + " - " + clas[i]
		labels.append(cad)
		i += 1

	x = np.arange(len(labels))
	width = 0.40

	fig, ax = plt.subplots()

	plt.ylim(top=60)

	rect = ax.bar(x=x, height=data_times.astype(np.float32), 
		width=width, label="Tiempo (ms)")

	ax.set_ylabel("Milisegundos")
	ax.set_title("Tiempo de cálculo",
			fontsize=18, fontweight="demibold")
	ax.set_xticks(x)
	ax.set_xticklabels(labels, fontsize=10, rotation=90)
	ax.legend()

	autolabel(ax, rect)

	fig.tight_layout()

	plt.show()
	
	
graph_hog_pyr()
graph_sift_pyr()
graph_surf_pyr()
graph_times()
