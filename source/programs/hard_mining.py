import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Detector import Detector
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from ProviderDataset import ProviderDataset
from FeaturesHOG import FeaturesHOG
from ClassifierSVM import ClassifierSVM
from Heatmap import Heatmap
import cv2


hf = FeaturesHOG(verbose=True)
cl = ClassifierSVM(features=hf, verbose=True)

seed = 100
num_images = 2000
train_percent = 0.60

num_images_train = int(num_images*train_percent)
num_images_test  = int(num_images - num_images_train)

train_faces = ProviderDatasetTrainTest("training_faces",   
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)
test_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
test_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=False, verbose=True)
backgrounds = ProviderDataset("caltech_back", num_images=3)

# Entrenamos el clasificador
cl.load_data(train_faces, train_nfaces)		# Cargamos los datos para el entrenamiento
cl.load_hard_mining_data(backgrounds)		# Cargamos los datos para el hard mining
cl.train()
cl.hard_mining()

"""
tp = cl.are_faces(test_faces)
fp = num_images_test - tp
fn = cl.are_faces(test_nfaces)
tn = num_images_test - fn

print ("True positives:  " + str(tp))
print ("False positives: " + str(fp))
print ("True negatives:  " + str(tn))
print ("False negatives: " +str(fn))
print ("")
print ("Porcentaje falsos positivos: " + str(fp/(tp+fp)))
print ("Porcentaje falsos negativos: " + str(fn/(tn+fn)))
"""
"""
image = cv2.imread("../images/faces.png")
# Esto lo tiene que hacer la clase preprocessor
img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
"""
