"""
	Prueba de la clase Detector. 
	Entrena un algoritmo HOG + kNN y localiza las caras en una imagen. Muestra todos los candidatos
	a cara, el mapa de calor y el resultado final tras simplificar el resultado.

"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ClassifierSVM import ClassifierSVM
from ProviderDataset import ProviderDataset
from DetectorHOGkNN import DetectorHOGkNN
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Detector import Detector
import rasprec
import cv2
import numpy as np


if __name__ == "__main__":
	# Vamos a usar características de HOG
	
	params = {
	'win_size': (40, 60),
	'block_size': (20, 20),
	'block_stride': (10, 10),
	'cell_size': (10, 10),
	'nbins': 9
	}
	hf = FeaturesHOG(verbose=True, params=params)

	classifier_SVM = ClassifierSVM(features=hf, verbose=True)
	classifier_SVM.load_training('hog.rectangular.svm.xml')


	d = Detector(classifier = classifier_SVM, verbose=True)

	# Estos son los parámetros por defecto
	d.set_params({
		'scale_factor': 1.25, 
		'min_size': 70,
		'disp': 20
		})

	img = cv2.imread('../images/faces.png')
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	print (img.shape)
	rects_all = d.detect_multiscale(img)
	#heatmap = d.detect_multiscale(img)
	#rects = d.detect_multiscale(img)


	img_all_boxes = np.copy(img)
	for x1, y1, x2, y2 in rects_all:
		img_all_boxes = cv2.rectangle(img_all_boxes, (x1, y1), (x2, y2), 255, 1)

	#for x1, y1, x2, y2 in rects:
	#	img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)



	cv2.imshow("Antes de heatmap", img_all_boxes)
	cv2.imshow("Despues de heatmap", img)
	#cv2.imshow("Heatmap", heatmap)
	cv2.waitKey(0)