"""
	Pruebas con los diferentes parámetros de CLAHE
"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Preprocessor import PreprocessorBasic
from pathlib import Path
from ProviderVideo import ProviderVideo
import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt
import time


def draw_histo(img, title):

	hist, bins = np.histogram(img.flatten(), 256, [0, 256])
	cdf = hist.cumsum()
	cdf_normalized = cdf * hist.max()/cdf.max()

	plt.plot(cdf_normalized, color="b")
	plt.hist(img.flatten(), 256, [0, 256], color="r")
	plt.xlim([0, 256])
	plt.legend(('cdf', 'Histograma'), loc='upper left')
	plt.title(title)
	plt.show()


provider = ProviderVideo('../data/portatil_01.avi')
ppb = PreprocessorBasic()

for img in provider:
	img = imutils.resize(img, width=480)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	# Imagen original
	cv2.imshow("Original", img)
	#draw_histo(img, "Imagen original")


	# CLAHE 

	for tgs in [(4,4), (8,8), (16,16), (32,32), (64,64)]:
		img = ppb.clahe(img, clip_limit=1.0, tile_grid_size=tgs)
		cv2.imshow("CLAHE " + str(tgs), img)
		draw_histo(img, "CLAHE: " + str(tgs))


	# Descomentar para diferentes valoes de ClipLimit
	#for cl in [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0]:
	#	imgc = ppb.clahe(img, clip_limit=cl)
	#	cv2.imshow("CLAHE " + str(cl), imgc)
	#	draw_histo(imgc, "CLAHE: " + str(cl))

	"""
	# Ecualización de histograma
	start = time.perf_counter()
	imgh = ppb.hist_norm(img)
	end = time.perf_counter()
	t = end - start
	print ("Ecualización de histograma: " + str(t*1000))
	cv2.imshow("Histograma", imgh)
	draw_histo(imgh, "Ecualización de histograma")

	# Normalización de rango
	start = time.perf_counter()
	imgr = ppb.range_norm(img)
	end = time.perf_counter()
	t = end - start
	print ("Normalización de rango: " + str(t*1000))
	cv2.imshow("Normalización de rango", imgr)
	draw_histo(imgr, "Normalización de rango")
	"""
	
	

	
	cv2.waitKey(50)
