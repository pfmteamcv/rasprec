import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from Preprocessor import PreprocessorBasic
from pathlib import Path
from ProviderVideo import ProviderVideo
import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt
import time


def draw_histo(img, title):

	hist, bins = np.histogram(img.flatten(), 256, [0, 256])
	cdf = hist.cumsum()
	cdf_normalized = cdf * hist.max()/cdf.max()

	plt.plot(cdf_normalized, color="b")
	plt.hist(img.flatten(), 256, [0, 256], color="r")
	plt.xlim([0, 256])
	plt.legend(('cdf', 'Histograma'), loc='upper left')
	plt.title(title)
	plt.show()


provider = ProviderVideo('../data/portatil_01.avi')
ppb = PreprocessorBasic()

for img in provider:
	img = imutils.resize(img, width=480)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	# Imagen original
	cv2.imshow("Original", img)
	draw_histo(img, "Imagen original")

	# Ecualización de histograma
	start = time.perf_counter()
	imgh = ppb.hist_norm(img)
	end = time.perf_counter()
	t = end - start
	print ("Ecualización de histograma: " + str(t*1000))
	cv2.imshow("Histograma", imgh)
	draw_histo(imgh, "Ecualización de histograma")

	# Normalización de rango
	start = time.perf_counter()
	imgr = ppb.range_norm(img)
	end = time.perf_counter()
	t = end - start
	print ("Normalización de rango: " + str(t*1000))
	cv2.imshow("Normalización de rango", imgr)
	draw_histo(imgr, "Normalización de rango")

	# CLAHE 
	start = time.perf_counter()
	imgc = ppb.clahe(img)
	end = time.perf_counter()
	t = end - start
	print ("CLAHE: " + str(t*1000))
	cv2.imshow("CLAHE", imgc)
	draw_histo(imgc, "CLAHE")
	

	
	cv2.waitKey(50)
