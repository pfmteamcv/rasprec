import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from DetectorViola import DetectorViola
from Pipeline import Pipeline
from Preprocessor import PreprocessorBasic
from ProviderVideo import ProviderVideo
from Processor import Processor
import cv2
import time
import rasprec
import numpy as np

import matplotlib.pyplot as plt

filename = str(Path.home()) + '/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml'


provider = ProviderVideo('../data/portatil_01.avi')
preprocessor = PreprocessorBasic(width=640)
detector = DetectorViola(filename, scale_factor=1.25)
processor = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_68)
#processor = Processor(eye_detect_method=rasprec.EYES_VIOLA)


# Inicializamos el pipeline
pipeline = Pipeline(
	provider = provider,
	preprocessor = preprocessor,
	detector = detector,
	face_processor = processor,
	recognizer = None,
	debug = []
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)


for img, data in pipeline:
	for d in data:
		x1, y1, x2, y2 = d['rect']

		img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)

	cv2.imshow("video", img)
	cv2.waitKey(1)

pipeline.debug_info()

t = pipeline.get_times()
k=[]
v=[]
for key, value in t.items():
	print ('{:>15}: {:011.7f}'.format(key, float(value)))
	