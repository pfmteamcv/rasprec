"""

	Este programa verifica la detección de caras en un vídeo previamente grabado
	desde el portátil.


"""
import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import cv2

from Pipeline import Pipeline

from ProviderVideo import ProviderVideo
from Preprocessor import PreprocessorBasic
from DetectorHOGkNN import DetectorHOGkNN


provider = ProviderVideo('../data/portatil_01.avi')
preprocessor = PreprocessorBasic()
detector = DetectorHOGkNN(train_file='../train_files/default.knn')

pipeline = Pipeline(
	provider = provider,
	preprocessor = preprocessor,
	detector = None,
	face_processor = None,
	recognizer = None
	)

for img, data in pipeline:
	print (data)
	cv2.imshow("img", img)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break




"""

#REPRODUCE VIDEO SIN MAS
pv = ProviderVideo('../data/portatil_01.avi')

for frame in pv:
	cv2.imshow("video", frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

"""

