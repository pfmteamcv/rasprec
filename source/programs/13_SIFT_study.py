import sys
from pathlib import Path
sys.path.append(str(Path.home()) + "/TFM/rasprec/source")
import cv2
import imutils
import os
from FeaturesBoWSIFT import FeaturesBoWSIFT
from FeaturesBoWSURF import FeaturesBoWSURF
import matplotlib.pyplot as plt



def draw_kp_SIFT():
	home = str(Path.home())
	filename = home + '/TFM/rasprec/resources/people/Victor/victor.jpg'
	img = cv2.imread(filename)
	img = imutils.resize(img, width=320)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	fbs = FeaturesBoWSIFT()

	fbs.draw_features(img)

	cv2.imshow("Image", img)
	cv2.waitKey(0)


def draw_kp_SURF():
	home = str(Path.home())
	filename = home + '/TFM/rasprec/resources/people/Victor/victor.jpg'
	img = cv2.imread(filename)
	img = imutils.resize(img, width=320)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	fbs = FeaturesBoWSURF()

	fbs.draw_features(img)

	cv2.imshow("Image", img)
	cv2.waitKey(0)

"""
def two_faces():

	sift = cv2.xfeatures2d.SIFT_create()

	home = str(Path.home())
	filename01 = home + '/TFM/datasets/train/faces/f00006.png'
	filename02 = home + '/TFM/datasets/train/faces/f00007.png'
	img01 = cv2.imread(filename01)
	img02 = cv2.imread(filename02)
	img01 = imutils.resize(img01, width=320)
	img02 = imutils.resize(img02, width=320)
	#img02 = img02[:213,:]
	img01 = cv2.cvtColor(img01, cv2.COLOR_BGR2GRAY)
	img02 = cv2.cvtColor(img02, cv2.COLOR_BGR2GRAY)

	#img02 = img01.copy()

	kp01, desc01 = sift.detectAndCompute(img01, None)
	kp02, desc02 = sift.detectAndCompute(img02, None)

	bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)

	matches = bf.match(img01, img02)

	# Sort the matches in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)

	# draw the top N matches
	N_MATCHES = min(len(kp01), len(kp02), len(matches)-1)
	N_MATCHES = 100

	matches = matches[:N_MATCHES]

	img03 = img02.copy()


	match_img = cv2.drawMatches(
	    img01, kp01,
	    img02, kp02,	    
	    #matches[:N_MATCHES], img02.copy(), flags=0)
	    matches,
	    img03,
	    flags=0)

	cv2.imshow("matches", match_img)
	cv2.waitKey()
"""


if os.name == 'posix':
	os.system('clear')
else:
	os.system('cls')

print ("Opciones")
print ("--------")
print ("1.- Mostrar keypoints SIFT")
print ("2.- Mostrar keypoints SURF")
print ("3.- xx")
print ("")
op = input("Elige una opción: ")

if op == '1':
	draw_kp_SIFT()
elif op == '2':
	draw_kp_SURF()

elif op == '3':
	pass
