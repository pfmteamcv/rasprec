"""
	Este programa es un entrenador para el reconocedor de caras. 
	Se supone que obtiene una serie de imágenes de entrada junto con una etiqueta y
	las carga en el reconocedor.

	Para la entrada se puede utilizar una carpeta. Todas las imágenes que contiene se suponen que pertenecen a una misma persona.
"""
import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

import cv2

from ProviderFolder import ProviderFolder
from Preprocessor import PreprocessorBasic
from DetectorViola import DetectorViola
from Processor import Processor
from RecognizerEigenfaces import RecognizerEigenfaces
from RecognizerFisherfaces import RecognizerFisherfaces
from RecognizerLBP import RecognizerLBP
import rasprec
from Pipeline import Pipeline
import os
import imutils
import numpy as np
import os

preprocessor = PreprocessorBasic(width=640)
detector     = DetectorViola(scale_factor=1.25)
processor    = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5)

# Menú para elegir el reconocedor
if os.name == 'posix':
	os.system('clear')
else:
	os.system('cls')

print ("Elige un reconocedor")
print ("--------------------")
print ("1.- Eigenfaces")
print ("2.- Fisherfaces")
print ("3.- Local Binary Patterns")
print ("")
op = input("Elige una opción: ")

if op == '1':
	recognizer = RecognizerEigenfaces()
elif op == '2':
	recognizer = RecognizerFisherfaces()
elif op == '3':
	recognizer = RecognizerLBP()


people_dir = str(Path.home()) + "/TFM/rasprec/resources/people/"

images = []		# Lista que contiene las imágenes
labels = []		# Lista que contiene las etiquetas
train_faces  = {}		# Diccionario que contiene las caras

dir, subdirs, f = next(os.walk(people_dir))
print("Actual: ", dir)
print("Subdirectorios: ", subdirs)
print("Archivos: ", f)


for s in subdirs: 
	d, sd, files = next(os.walk(people_dir + s))
	faces = []
	prep_faces = []
	for f in files:
		file = people_dir + s + '/' + f
		img = cv2.imread(file)

		labels.append(s)	# Guardamos la etiqueta

		img2 = preprocessor.prepare(img)
		rects = detector.detect_multiscale(img2)

		for x1, y1, x2, y2 in rects:
			face = img2[y1:y2, x1:x2]
			face = imutils.resize(face, height=100)
			faces.append(face)
			pface = processor.load(face)
			prep_faces.append(pface)

	train_faces[s] = prep_faces

	# Para mostrarlas
	cfaces=np.hstack(faces)
	cpfaces = np.hstack(prep_faces)
	cv2.imshow(s, cfaces)
	cv2.imshow(s + ' Prepared', cpfaces)
	

# Tenemos un diccionario donde cada etiqueta contiene una lista de caras de una persona

for name, faces in train_faces.items():
	for f in faces:
		recognizer.add_face(f, name)

info = recognizer.get_info()
print (info)

recognizer.train()
recognizer.save('test01')

cv2.waitKey(0)




cv2.destroyAllWindows()		


"""

rec  = RecognizerEigenfaces()

provider     = ProviderFolder(folder)
preprocessor = PreprocessorBasic(width=640)
detector     = DetectorViola(scale_factor=1.25)
processor    = Processor(eye_detect_method=rasprec.EYES_FACIAL_LANDMARKS_5)
#processor   = Processor(eye_detect_method=rasprec.EYES_VIOLA)

# Inicializamos el pipeline
pipeline = Pipeline(
	provider       = provider,
	preprocessor   = preprocessor,
	detector       = detector,
	face_processor = processor,
	recognizer     = None,
	debug          = []
	#debug=[rasprec.DEBUG_EYES_DETECTION, rasprec.DEBUG_GEOMETRIC]#, rasprec.DEBUG_COUNT_DETECTIONS, rasprec.DEBUG_GEOMETRIC]
	#, rasprec.DEBUG_GEOMETRIC, rasprec.DEBUG_HISTOGRAM,
	#rasprec.DEBUG_SMOOTHING, rasprec.DEBUG_ELLIPTICAL]
	)


faces = []
for img, data in pipeline:
	#cv2.imshow("full image", img)
	for d in data:
		#x1, y1, x2, y2 = d['rect']
		#face = img[y1:y2, x1:x2]
		if not d['face'] is None:
			faces.append(d['face'])
			print (d['face'].shape)
			#cv2.imshow("faceeeeeeeeee", d['face'])
			#cv2.waitKey(0)

for f in faces:
	rec.add_face(f, 1)
	

rec.train()
rec.save()
rec.draw()

cv2.destroyAllWindows()

"""


