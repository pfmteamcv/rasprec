"""
	Prueba de la clase Detector. 
	Entrena un algoritmo HOG + kNN y localiza las caras en una imagen. Muestra todos los candidatos
	a cara, el mapa de calor y el resultado final tras simplificar el resultado.

"""

import sys
from pathlib import Path

sys.path.append(str(Path.home()) + "/TFM/rasprec/source")

from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
from DetectorHOGkNN import DetectorHOGkNN
from ProviderDatasetTrainTest import ProviderDatasetTrainTest
from Detector import Detector
import cv2
import numpy as np


# Vamos a usar características de HOG
hf = FeaturesHOG(verbose=True)

# ------------------------------
# ENTRENAMIENTO
# ------------------------------
#
# 300 imágenes para entrenamiento y 700 imágenes para test
# Mismas imágenes para cada iteración
num_images = 2000		# Imágenes totales del dataset
seed = 177				# Semilla para que todas las pruebas utilicen las mismas imágenes
train_percent = 0.05	# Porcentaje de imágenes de entrenamiento. 0.05 = 100
num_images_train = int(num_images * train_percent)
num_images_test = num_images - num_images_train

# Preparamos el dataset para entrenamiento y test
train_faces = ProviderDatasetTrainTest("training_faces",  
	seed=seed, train_percent=train_percent,
	num_images=num_images, train=True, verbose=True)
train_nfaces = ProviderDatasetTrainTest("training_nfaces", 
	seed=seed, train_percent=train_percent, 
	num_images=num_images, train=True, verbose=True)

# Inicializamos las características con los parámetros
hf.set_params({
	"cell_size": (14, 14),
	"block_size": (28, 28),
	"block_stride": (14, 14),
	"nbins": 9
	})	

# Clasificador kNN con 7 vecinos
classifier = ClassifierkNN(features=hf, params={"neighs": 7}, verbose=True)

# Carga de datos y entrenamiento
classifier.load_data(train_faces, train_nfaces)
classifier.train()

# --> En este punto tenemos el clasificador ya entrenado.

d = Detector(classifier = classifier, verbose=True)

# Estos son los parámetros por defecto
d.set_params({
	'scale_factor': 1.25, 
	'min_size': 70,
	'disp': 20
	})

img = cv2.imread('../images/faces.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
rects_all = d.detect_multiscale(img, debug='bounding_boxes')
heatmap = d.detect_multiscale(img, debug='heatmap')
rects = d.detect_multiscale(img)

print (rects)

img_all_boxes = np.copy(img)
for x1, y1, x2, y2 in rects_all:
	img_all_boxes = cv2.rectangle(img_all_boxes, (x1, y1), (x2, y2), 255, 1)

for x1, y1, x2, y2 in rects:
	img = cv2.rectangle(img, (x1, y1), (x2, y2), 255, 1)



cv2.imshow("Antes de heatmap", img_all_boxes)
cv2.imshow("Despues de heatmap", img)
cv2.imshow("Heatmap", heatmap)
cv2.waitKey(0)