""" Funciones para el tratamiento de mapas de calor

	Esta clase aglutina todas las funciones necesarias para la 
	depuración de los resultados eliminando falsos positivos y 
	agrupando los verdaderos positivos.
"""


import cv2
import rasprec
import numpy as np
from scipy.ndimage import label

class Heatmap:

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, height, width, verbose=False, debug=(), threshold=3):
		if verbose:
			print ("[INFO] Inicializando la clase Heatmap")

		self.__height = height
		self.__width = width

		self.__map = None
		self.__colormap = None
		self.__thresholded_map = None
		self.__boxes = []		

		self.__threshold = threshold	# Umbral para dar por válido un punto
		self.__bounding_boxes = []
		self.__generated = False 	# Si se ha generado el mapa

		self.__debug = debug


	def __detect_boxes(self):
		# Iteramos sobre cada cara detectada
		for face_number in range(1, self.num_features+1):
			 # Buscar los píxeles de la imagen que tengan ese número
			 nonzero = (self.labeled_map == face_number).nonzero()
			 # Localizar los valores x e y de esos píxeles
			 nonzeroy = np.array(nonzero[0])
			 nonzerox = np.array(nonzero[1])
			 # Definir un rectángulo basado en los valores máximo y mínimo de x e y
			 bbox = (np.min(nonzerox), np.min(nonzeroy), np.max(nonzerox), np.max(nonzeroy))
			 # Guardamos los rectángulos, descartando aquellos menores de 70x70 píxeles
			 if bbox[2] - bbox[0] > 70 and bbox[3] + bbox[1] > 70:
			 	self.__bounding_boxes.append(bbox)
			 

	def __generate(self):
		""" Aplica los rectángulos a la imagen y genera todos los mapas intermedios
		"""
		# Creamos el mapa vacío e incrementamos en uno el valor de cada píxel de cada rectángulo
		self.__map = np.zeros((self.__height, self.__width), np.uint8)
		for (x1, y1, x2, y2) in self.__boxes:
			self.__map[y1:y2, x1:x2] += 1

		# Aplicamos el umbral al mapa
		self.__thresholded_map = np.zeros((self.__height, self.__width), np.uint8)
		self.__thresholded_map[self.__map >= self.__threshold] = 125

		# Aplicamos las etiquetas al mapa
		self.labeled_map, self.num_features = label(self.__thresholded_map)
		self.labeled_map = self.labeled_map.astype(np.uint8)

		# Generamos los rectángulos
		self.__detect_boxes()

		self.__generated = True


	def get_map(self):
		""" Genera el mapa a partir de los rectángulos que se han añadido

		Hay que destacar que, cada vez que se añade un rectángulo, se almacena en una lista
		y es al llamar a esta función cuando se genera el mapa sobre la imagen original.
		"""
		if not self.__generated:
			self.__generate()
		
		return self.__map


	def add(self, coords):
		""" Añade un rectángulo a la imagen
		"""

		self.__boxes.append(coords)
		self.__generated = False


	def add_multiple(self, rects):
		"""
			Añade múltiples rectángulos
		"""
		self.__boxes = rects
		self.__generated = False


	def get_colormap(self):
		""" Devuelve un mapa de calor apto para su visualización

			Como su objetivo es únicamente para su visualización se ha sacado 
			el código de __generate() y se ha incluido aquí para ejecutarlo 
			únicamente cuando sea necesario.
		"""

		if not self.__generated:
			self.__generate()

		# Creamos el mapa de calor (para visualizar)
		m = np.amax(self.__map)
		if m == 0:
			m = 1
		self.__colormap = self.__map * int(250/m)
		# https://www.learnopencv.com/applycolormap-for-pseudocoloring-in-opencv-c-python/
		self.__colormap = cv2.applyColorMap(self.__colormap, cv2.COLORMAP_HOT)		

		return self.__colormap


	def get_thresholded_map(self):
		""" Devuelve el mapa tras aplicar el umbral
		"""
		if not self.__generated:
			self.__generate()

		return self.__thresholded_map


	def get_labeled_map(self):

		if not self.__generated:
			self.__generate()

		return self.labeled_map


	def get_bounding_boxes(self):

		if not self.__generated:
			self.__generate()
		if rasprec.DEBUG_HEATMAP in self.__debug:
			colormap = self.get_colormap()
			cv2.imshow("Debug colormap", colormap)

		return self.__bounding_boxes
	
"""

print ("Probando")
hm = Heatmap(800, 1200)

hm.add((100, 200), (700, 300))
hm.add((100, 200), (200, 300))
hm.add((400, 100), (500, 250))
hm.add((0, 400), (600, 200))
hm.add((700, 600), (850, 650))

cv2.imshow ("Map", hm.get_map())
cv2.imshow ("Heatmap", hm.get_colormap())
cv2.imshow ("Thresolded", hm.get_thresholded_map())

cv2.waitKey(0)
cv2.destroyAllWindows()

print (hm.boxes)

"""

		