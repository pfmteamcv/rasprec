"""
Este programa entrena un clasificador. Necesitamos un menú que pregunte:

	- Características
	- Clasificador
	- Nombre del fichero de salida
	- Parámetros ????? (*)
	- Imágenes para el entrenamiento

(*) Seleccionar aquí los parámetros es más complicado de lo que parece. Un modelo entrenado
con unos determinados parámetros debe utilizarlos también en la fase de detección. Como él
único nexo de unión entre entrenamiento y pruebas es el fichero de entrenamiento, debería 
guardarse aquí, en kNN es relativamente fácil, pero en SVM será mucho más complicado ya que
genera su propio fichero. Hay que pensarlo y darle una vuelta.

"""
from FeaturesHOG import FeaturesHOG
from ClassifierkNN import ClassifierkNN
from ProviderDataset import ProviderDataset
from DetectorHOGkNN import DetectorHOGkNN
import os
import sys


def features_menu():
	feat = {
		"a": "hog",
		"b": "sift",
		"c": "surf"
		}
	op = None

	while not op in [ "a", "b", "c", "q" ]:
		os.system('cls')
		print ("Escoge las características")
		print ("--------------------------")
		print ("a) HOG")
		print ("b) SIFT (Aún no implementado)")
		print ("c) SURF (Aún no implementado")
		print ("q) Quit")
		op = input("Escoge una opción: ")

	return feat[op]
	

def classifier_menu():
	clas = {
		"a": "1nn",
		"b": "knn",
		"c": "svm"
	}
	op = None

	while not op in ["a", "b", "c", "q"]:
		os.system('cls')
		print ("Escoge un clasificador")
		print ("----------------------")
		print ("a) 1NN")
		print ("b) kNN")
		print ("c) SVM")
		print ("q) Quit")
		op =input("Escoge una opción: ")

	return clas[op]


def filename_menu():
	os.system('cls')
	fn = input ("Indica el nombre del fichero: ")
	return fn


feat = features_menu()
clas = classifier_menu()
#fn = filename_menu()



# Clasificador 1NN
if clas == '1nn':
	# Características HOG
	if feat == 'hog':
		# Por ahora pillamos todo el conjunto de entrenamiento
		faces_dp = ProviderDataset("training_faces", num_images=1)
		nfaces_dp = ProviderDataset("training_nfaces", num_images=1)

		features = FeaturesHOG()
		classifier = ClassifierkNN(features=features, params={"neighs": 1}, verbose=True)
		classifier.load_data(faces_dp, nfaces_dp, 2, 2) 

		classifier.train_and_save('default.1nn')
		

