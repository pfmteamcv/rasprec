"""
	Esta clase engloba diferentes utilidades que se invocan desde diferentes lugares

""" 
import imutils
import cv2

class Utils:

	def __init__ (self):
		pass



	@staticmethod
	def get_sliding_windows(image, min_height=70, min_width=70, scale_factor=1.25, disp=20):
		#  Implementa las ventanas deslizantes
		# 	
		# Este método implementa las ventanas deslizantes.
		# A partir de una imagen devuelve una lista donde cada elemento es otro 
		# array que incluye todas las sub-imágenes de un determinado tamaño. Cada
		# elemento de este array contiene a su vez un par de la forma:
		# 	- Array con una subimagen
		# 	- Lista de la forma (x, y, w, h) con las coordenadas, ancho y alto
		# 	  de la subimagen sobre la imagen original
		
		height, width = image.shape

		ratio = 1.
		windows = []
		
		#while width > self.min_size and height > self.min_size:
		while width > min_width and height > min_height:
			#extend / append
			windows.extend(Utils.__get_fixed_size_windows(image, ratio, min_width, min_height, disp))
			width = int(width / scale_factor)
			image = imutils.resize(image, width=width)
			height, width = image.shape
			ratio *= scale_factor
		return windows



	@staticmethod
	def __get_fixed_size_windows(image, ratio, width, height, disp):
		""" Devuelve una lista con todas las subimágenes del tamaño indicado.
		"""

		total_height, total_width = image.shape
		windows = []

		x = 0
		y = 0

		"""
		size = int(self.min_size * ratio)

		while y + self.min_size < total_height:
			while x + self.min_size < total_width:
				i = image[y:y+self.min_size, x:x+self.min_size]
				windows.append ((i, (int(x*ratio), int(y*ratio), int(x*ratio) + size, int(y*ratio) + size)))
				x += self.disp
			y += self.disp
			x = 0
		"""

		print ("Ancho: " + str(width))
		print ("Alto:  " + str(height))

		width = int(width * ratio)
		height = int(height * ratio)

		while y + height < total_height:
			while x + width < total_width:
				i = image[y:y+height, x:x+width]
				windows.append((i, (int(x*ratio), int(y*ratio), int(x*ratio) + width, int(y*ratio) + height)))
				x += disp
			y += disp
			x = 0

		return windows