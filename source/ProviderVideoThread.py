from Provider import Provider
from threading import Thread
import cv2

class ProviderVideoThread(Provider):
	
	def __init__(self, filename):
		self.__video = cv2.VideoCapture(filename)
		self.__next_frame = None
		self.__ready = False		# Indica si está listo para obtener el siguiente frame

		if not self.__video.isOpened():
			raise Exception('[ERROR] No se pudo abrir el fichero' + filename)

		Thread(target=self.update, args=()).start()


	def __iter__(self):

		#self.actual=0
		return self


	def __next__(self):

		if self.__ready:
			self.__ready = False
			self.update()
			return self.__next_frame
		else:
			raise StopIteration()

		

	def update(self):

		if not self.__ready:
			ret, frame = self.__video.read()
			self.__ready = True
			if not frame is None:
				self.__next_frame = frame
			else:
				self.__video.release()
				self.__next_frame = None
				raise StopIteration

		"""
		if self.actual == self.len:
			raise StopIteration()
		else:
			i = self.files[self.actual]
			self.actual += 1
			return i

		"""