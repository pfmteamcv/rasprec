import os
from pathlib import Path
from Provider import Provider
import cv2
import sys

class ProviderDatasetMetadata(Provider):

	def __init__(self, dataset, num_images=0):
		"""
			num_images es el número de imágenes que se tomarán del dataset. Si es 0
			se toman todas las imágenes.
		"""
		self.__path = None		# Ruta del dataset que se utilizará
		self.__files = None		# Listado de imágenes del dataset sobre las que se itera
		self.__length = None		# Número de imágenes en el dataset
		self.__actual = 0

		#self.__metadata = {
		#	'points': [],
		#	'id': []
		#	}
		self.__metadata = []
		self.__files = []

		datasets = {"bioid": "/TFM/datasets/BioID/",
					"helen": "/TFM/datasets/HELEN/images/",
					"caltech_faces": "/TFM/datasets/caltech_faces/tagged/"
					}
		
		dataset=dataset.lower()
		if dataset in datasets:
			home = str(Path.home())
			self.__path = home + datasets[dataset]
		else:
			print ("[ERROR] No se reconoce el dataset indicado.")
			sys.exit()

		# Cada dataset almacena de los metadatos de forma diferente, así que se procesan de forma diferente
		if dataset == 'bioid':
			self.__load_bioid(num_images)
		elif dataset == 'helen':
			self.__load_helen(num_images)
		elif dataset == 'caltech_faces':
			self.__load_caltech_faces()

		

	def __load_caltech_faces(self):

		# Guardamos la ruta de todas las imágenes
		lst_dir = os.walk(self.__path)
		num = 0
		for root, dirs, files in lst_dir:
			for d in dirs:
				# Recorremos las imágenes dentro de cada directorio
				id = d
				sub_lst_dir = os.walk(root+d)
				for sub_root, sub_dirs, sub_files in sub_lst_dir:
					for sf in sub_files:
						self.__files.append(root + d + '/' + sf)
						self.__metadata.append( {
							'id': id
							})
		self.__length = len(self.__files)



	def __load_helen(self, num_images):

		raise NotImplementedError()

	
	def __load_bioid(self, num_images):
		"""
		Carga los datos de BioID
		"""

		# Guardamos la ruta de todas las imágenes
		lst_dir = os.walk(self.__path)
		num = 0
		for root, dirs, files in lst_dir:
			for f in files:
				if f[-3:] in ["pgm"]:
					img_filename = f
					eyes_filename = f[:-3] + 'eye'
					if num_images == 0 or num < num_images:
						# Almacenamos la ruta de la imagen
						self.__files.append(root+f)
						# Procesamos los metadatos

						eyes_filename = root + eyes_filename
						# Leemos el fichero 
						file = open(eyes_filename, 'r')
						lines = list(file)
						# Localizamos la segunda línea (coordenadas)
						coords = lines[1].split()
						lx = coords[0]	# Ojo izquierdo. Coordenada X
						ly = coords[1]	# Ojo izquierdo. Coordenada Y
						rx = coords[2]	# Ojo derecho. Coordenada X
						ry = coords[3]	# Ojo derecho. Coordenada Y

						# Se guardan las coordenadas de la forma ((xl, ly), (rx, ry))
						#self.__metadata['points'].append( ((int(lx), int(ly)), (int(rx), int(ry))) )
						self.__metadata.append( {
							'points': ((int(lx), int(ly)), (int(rx), int(ry)))
							})

						num += 1

		self.__length = len(self.__files)


	def get_array(self):
		"""Devuelve el array con todas las direcciones
		"""
		return self.__files


	def len(self):
		"""Devuelve el número de imágenes en el dataset
		"""
		return self.len


	def __iter__(self):
		self.__actual=0
		return self


	def __next__(self):
		
		if self.__actual >= len(self.__files):
			raise StopIteration()
		else:
			i = self.__files[self.__actual]
			meta = self.__metadata[self.__actual]
			self.__actual += 1
			return i, meta

			
"""

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
lst_dir = os.walk(path + "01_preprocessed/")
detector = HaarDetector()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)
		detected_image = detector.detect(image)
"""