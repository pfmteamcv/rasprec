""" Implementa el clasificador kNN

Esta clase implementa un clasificador kNN

Parámetros
----------


"""


import cv2
from Classifier import Classifier
from Provider import Provider
import numpy as np

class ClassifierkNN(Classifier):
	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, features=None, params=None, verbose=False):

		Classifier.__init__(self, features, params, verbose)

		self.__knn = None					# Clasificador kNN
		self.__neighs = 3					# Número de vecinos. Valor por defecto: 3
		self.__threshold = 2				# Números de vecinos positivos para ser considerada cara
		self._faces_descriptors = []	# Descriptores de las caras de entrenamiento
		self._nfaces_descriptors = []	# Descriptores de las no caras de entrenamiento

		if params:
			self.set_params(params)
		else:
			self.__initialize()

		if self._verbose:
			print ("[INFO] Se ha inicializado la clase kNNClassifier.")


	def __initialize(self):
		# Inicializamos el clasificador kNN
		self.__knn=cv2.ml.KNearest_create()



	def train(self):
		""" Entrena el clasificador a partir de los datos precargados

		Entrena el clasificador kNN tomando los datos que tienen que haber sido cargados
		previamente con la función load_data().

		Devuelve:
			response
				Array de respuesta, cada elemento corresponde con un vector de características
				de train_data e indica si es cara (1.) o no cara (0.)
			train_data
				Array con los vectores de características de las imágenes de entrenamiento

		"""

		num_faces_train = len(self._faces_descriptors)
		num_nfaces_train = len(self._nfaces_descriptors)

		# Cargamos el clasificador. 1 caras, 0 no caras
		response = np.concatenate((np.ones((num_faces_train,1), dtype=np.float32),
								   np.zeros((num_nfaces_train, 1), dtype=np.float32)),
								   axis=0)
		train_data=np.concatenate((self._faces_descriptors, self._nfaces_descriptors), axis=0)

		# https://docs.opencv.org/3.4/d5/d26/tutorial_py_knn_understanding.html
		self.__knn.train(train_data, cv2.ml.ROW_SAMPLE, response)

		self._is_trained = True
		self._num_trained_faces = num_faces_train
		self._num_trained_nfaces = num_nfaces_train
		self._num_dimensions = train_data[0].shape[0]

		if self._verbose:
			print ("[INFO] Entrenado el clasificador kNN. " + \
				str(self._num_trained_faces) + " caras y " + \
				str(self._num_trained_nfaces) + " no caras con " + \
				str(self._num_dimensions) + " dimensiones.")

		return response, train_data


	def train_and_save(self, filename=None):
		""" Entrena el clasificador y lo guarda en el fichero especificado.

		Entrena el clasificador kNN a partir de los datos que han sido precargados y
		posteriormente lo guarda en el fichero indicado.
		Hay que destacar que kNN no requiere ningún tipo de entrenamiento, consistiendo
		este en cargar los vectores de características. Por tanto, el fichero generado 
		es simplemente una lista de todos los vectores de características de las imágenes 
		indicando si son caras o no caras.

		Formato del fichero:
			Es un fichero de texto con extensión aconsejada .knn.
			Cada línea del fichero tiene la siguiente estructura: [[1.], [0.34 0.24 ... 0.64]]
			Representa una lista que contiene dos listas:
				- La primera lista tiene un único elemento que indica si los datos de esta
				  línea corresponden a una cara o una no cara
				- La segunda lista contiene el vector de características de la imagen
				  correspondiente.

		Parámetros:
			filename -- Nombre del fichero donde se guardará

		Devuelve:
			Nada

		"""

		if filename is None:
			filename = self._path + self._default_filename + '.knn'
		else:
			filename = self._path + filename

		response, train_data = self.train()

		# Guardamos las características en el fichero
		resp = 0
		with open(filename, 'w') as f:
			for t in train_data:
				f.write(str([response[resp].tolist(), t.tolist()]) + "\n")
				resp += 1

		if self._verbose:
			print ("[INFO] Se ha guardado el fichero " + str(filename) + ".")


	def load_training(self, filename="train_files/default.knn"):
		""" Carga las características desde un fichero y entrena el clasificador

		Entrena el clasificador a partir de los datos contenidos en un fichero de texto. 
		Este fichero debe tener el formato indicado en la función train_and_save(). 

		Parámetros:
			filename -- Nombre del fichero. Por defecto, usa el nombre default.knn

		Devuelve: 
			Nada

		Excepciones:
			FileNotFoundError
				El fichero pasado como parámetro no existe

		"""

		train_data = []
		response = []

		if filename is None:
			filename = self._path + self._default_filename + '.knn'
		else:
			filename = self._path + filename

		try:
			with open(filename, "r") as f:
				for line in f:
					l = eval(line)
					response.append(l[0])
					train_data.append(l[1])
			response = np.asarray(response, dtype=np.float32)
			train_data = np.asarray(train_data ,dtype=np.float32)

			# Y cargamos los datos en el clasificador
			self.__knn.train(train_data, cv2.ml.ROW_SAMPLE, response)

			self._is_trained = True
			self._num_trained_faces = np.count_nonzero(response == 1.)
			self._num_trained_nfaces = np.count_nonzero(response == 0.)
			self._num_dimensions = train_data[0].shape[0]

			if self._verbose:
				print ("[INFO] Entrenado el clasificador. " + 
					str(self._num_trained_faces) + " caras y " + 
					str(self._num_trained_nfaces) + " no caras con " + 
					str(self._num_dimensions) + " dimensiones.")

		except (FileNotFoundError, IOError):
			print ("[ERROR] El fichero indicado no existe.")


	def test_feat(self, image):
		print (self._features.get_features(image).shape)

	"""
		def train(self):
			self.__knn.train(train_data, cv2.ml.ROW_SAMPLE, response)
	"""


	def set_params(self, params):
		if 'neighs' in params:
			self.__neighs = params['neighs']
		if 'threshold' in params:
			self.__threshold = params['threshold']

		self.__initialize()


	def is_face(self, image):
		""" Indica si una imagen es cara
		"""
		if self._is_trained:
			# Se ha pasado el vector de características
			#   print (type(image))
			#   print ("Vamos a evaluarlo.")

			# Se ha pasado una imagen, por tanto el primer paso es obtener sus características
			feat = self._features.get_features(image)

			# knn recoge un array de vectores de características. Como solo queremos evaluar
			# una cara debemos crear el array y cargar el vector pasado como primer (y único)
			# elemento del mismo
			samples = np.asarray([feat])

			ret, results, neighbours, dist = self.__knn.findNearest(samples, self.__neighs)

			if self.__threshold == 0:
				return results[0][0] == 1
			else:
				return neighbours.sum() > self.__threshold



	def are_faces(self, images):
		"""
			Se le pasa un array de imágenes (cada elemento un ndarray de 70x70) y devuelve otro array indicando
			cuáles son caras
		"""

		""" CÓDIGO TEMPORAL. 
		"""
		if self._is_trained:
			ret = []
			for i in images:
				ret.append(self.is_face(i))

			print("")
			print (ret.count(True))
			print (ret.count(False))
			print ("")

			return ret
		
	
		"""
		if self._is_trained:
			if isinstance(images, tuple):
				# Se ha pasado una lista de imágenes
				samples = []
				# Calculamos las características de todas las imágenes
				for i in images:
					feat = self._features.get_features(i)
					samples.append(feat)


				samples = np.asarray(samples)

				ret, preds = self.__knn.predict(samples)
				preds = np.ravel(preds)
				return (preds)
			
			elif isinstance(images, Provider):
				images = images.get_array()
				samples = []
				for i in images:
					img = cv2.imread(i)
					feat = self._features.get_features(img)
					samples.append(feat)

				samples = np.asarray(samples)

				ret, preds = self.__knn.predict(samples)

				print (ret)

				preds = np.ravel(preds)

				return (preds)

		"""



	def evaluate(self, images, threshold=0):
		""" 
		Se le pasa un iterador (clase Provider) de imágenes y evalua si son caras
		Evaluar posteriormente otros tipos de datos

		Parámetros:
			images -- array de imágenes a analizar
			threshold -- con este parámetro se puede modificar el umbral a partir del que se considera
				que una cara es tal. Si es diferente a 0, en lugar de usar el criterio de la implementación
				de OpenCV de HOG, se considera cara cuando el número de vecinos que son caras es superior
				al valor de este parámetro.

		Devuelve:
			Tres arrays donde cada elemento corresponde a un elemento del array pasado como parámetro. Los elementos
			de estos arrays son:
				results - 1 si se trata de una cara o 0 en caso contrario
				neighs  - array con tantos elementos como vecinos se han evaluado, cada uno de ellos puede tener un
					valor de 1 si el vecino es una cara o 0 en caso contrario.
				dists   - array con las distancias del elemento evaluado a cada uno de sus vecinos

		"""
		samples = []
		if self._is_trained:
			if isinstance(images, Provider):
				images = images.get_array()
				for i in images:
					img = cv2.imread(i)
					feat = self._features.get_features(img)
					samples.append(feat)

				samples = np.asarray(samples)

				ret, results, neighbours, dists = self.__knn.findNearest(samples, 35)
				
				if threshold != 0:
					results = []
					for r in neighbours:
						if np.count_nonzero(r == 1) >= threshold:
							results.append([1.])
						else:
							results.append([0.])
					results = np.asarray(results)

				results = np.ravel(results).astype(np.int32)
				neighs = neighbours.astype(np.int32)

				"""
				print("ret:        " + str(ret))
				print("results:    " + str(results))
				print("neighbours: " + str(neighs))
				print("dist:       " + str(dists))
				print ("")
				"""
				
				n = np.count_nonzero(results == 1)	# Número de imágenes que son caras

				return results, neighs, dists



	def get_num_of_faces(self, images, threshold=0):
		""" Devuelve el número de caras que hay en un conjunto de imágenes

		Parámetros:
			images -- array de imágenes a analizar
			threshold -- con este parámetro se puede modificar el umbral a partir del que se considera
				que una cara es tal. Si es diferente a 0, en lugar de usar el criterio de la implementación
				de OpenCV de HOG, se considera cara cuando el número de vecinos que son caras es superior
				al valor de este parámetro.

		Devuelve:
			Por ahora devuelve el número de imágenes que son caras.

		"""
		samples = []
		if self._is_trained:
			if isinstance(images, Provider):
				images = images.get_array()
				for i in images:
					img = cv2.imread(i)
					feat = self._features.get_features(img)
					samples.append(feat)

				samples = np.asarray(samples)
				ret, results, neighbours, dist = self.__knn.findNearest(samples, 35)
				
				if threshold != 0:
					results = []
					for r in neighbours:
						if np.count_nonzero(r == 1) >= threshold:
							results.append([1.])
						else:
							results.append([0.])
					results = np.asarray(results)

				n = np.count_nonzero(results == 1)	# Número de imágenes que son caras
				
				return n