from Features import Features
import cv2
import numpy as np
import math

#
# https://www.learnopencv.com/histogram-of-oriented-gradients/
#

class FeaturesHOG(Features):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, params=[], verbose=False):
		
		Features.__init__(self, params, verbose)

		if self._verbose:
			print ("[INFO] Se ha inicializado la clase FeaturesHOG.")

		# Generador de características 
		self.__hog = None

		# Parámetros por defecto usados por HOG
		self.__win_size = (70,70)
		self.__block_size = (28,28)
		self.__block_stride = (14,14)
		self.__cell_size = (14,14)
		self.__nbins = 9
		self.__dimensions = None

		self.set_params(params)

		self.__initialize()


	def __initialize(self):
		# Inicializamos HOG para obtener el descriptor de las imágenes que queremos clasificar

		self.derivAperture = 1
		self.winSigma = -1.
		self.histogramNormType = 0
		self.L2HysThreshold = 0.2
		self.gammaCorrection = 1
		self.nlevels = 64
		self.signedGradient = False
		self.__hog = cv2.HOGDescriptor(self.__win_size, self.__block_size,
				self.__block_stride, self.__cell_size, self.__nbins, self.derivAperture,
				self.winSigma, self.histogramNormType, self.L2HysThreshold,
				self.gammaCorrection, self.nlevels, self.signedGradient)


	# --------------------- METODOS PUBLICOS ---------------------

	def get_params(self):
		p = {	'win_size': self.__win_size,
				'block_size': self.__block_size,
				'block_stride': self.__block_stride,
				'cell_size': self.__cell_size,
				'nbins': self.__nbins }
		return p


	def set_params(self, params):
		if 'win_size' in params:
			self.__win_size = params['win_size']
		if 'block_size' in params:
			self.__block_size = params['block_size']
		if 'block_stride' in params:
			self.__block_stride = params['block_stride']
		if 'cell_size' in params:
			self.__cell_size = params['cell_size']
		if 'nbins' in params:
			self.__nbins = params['nbins']

		self.__initialize()


	def get_features(self, image):
		""" Devuelve el vector de características de una imagen

		"""
		if type(image) == np.ndarray:
			hog_descriptor = self.__hog.compute(image)
			hog_descriptor = np.squeeze(hog_descriptor)
		elif type(image) == str:
			img = cv2.imread(image)
			hog_descriptor = self.__hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)

		return hog_descriptor


	def get_dimensions(self, image):
		""" Devuelve el número de dimensiones del vector de características de la imagen.

		TODO: Hay que solucionarlo para no tener que cargar la imagen. Es necesario
		realizar las operaciones sobre cell_size, block_size y block_stride. Como está
		es muy cutre.

		"""

		if type(image) == np.ndarray:
			hog_descriptor = self.__hog.compute(image)
			hog_descriptor = np.squeeze(hog_descriptor)
		elif type(image) == str:
			img = cv2.imread(image)
			hog_descriptor = self.__hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)

			#print ("Real: " + str(hog_descriptor.shape))

			cells_per_block = (self.__block_size[0] / self.__cell_size[0]) * (self.__block_size[1] / self.__cell_size[1])
			num_cells_x = self.__win_size[0] / self.__cell_size[0]
			num_cells_y = self.__win_size[1] / self.__cell_size[1]
			stride = self.__block_stride[0] / self.__cell_size[0]
			num_blocks = (num_cells_x - stride) * (num_cells_y - stride)


			dim = self.__nbins * num_blocks * cells_per_block


			#print ("Numero bloques: " + str(num_blocks))
			#print ("Celdas por bloque: " + str(cells_per_block))
			#print ("Estimado: " + str(dim))

		return hog_descriptor.shape


	# Visualización del descriptor de HOG en C++
	# http://www.juergenbrauer.org/old_wiki/doku.php?id=public:hog_descriptor_computation_and_visualization

	# Visualización de HOG. Solo funciona para bloques de 2x2 celdad y de 1x1 celdas
	def draw_features(self, image):
		""" Representa visualmente las características de una imagen

		A partir de la imagen que se pasa como parámetro dibuja una representación visual 
		de las características de HOG de la misma. El código de esta función está basado en 
		http://www.juergenbrauer.org/old_wiki/doku.php?id=public:hog_descriptor_computation_and_visualization
		que lo implementa en C++.

		Parámetros:
			image
				Imagen cuyas características se van a representar. Se puede pasar como un array
				o la cadena de su ruta en el disco.

		"""
		
		# Cargamos la imagen
		if type(image) == str:
			img = cv2.imread(image)
		else:
			img = image

		# Calculamos HOG
		hog_descriptor = self.__hog.compute(img)
		hog_descriptor = np.squeeze(hog_descriptor)

		#print (hog_descriptor)
		#print (hog_descriptor.shape)

		# Redimensionamos la imagen 
		ratio = 10 # Ratio de ampliación para la visualización
		x_size = self.__win_size[0] # Tamaño de la imagen original
		y_size = self.__win_size[1]
		cell_size = self.__cell_size[0]
		nbins = self.__nbins
		cell_color = [0, 255, 255]
		hog_color = [0, 0, 255]
		index = 0
		rad_range_for_one_bin = math.pi/nbins	# Radianes que cubre un bin

		img = cv2.resize(img,(x_size*ratio,y_size*ratio),fx=0, fy=0, interpolation = cv2.INTER_NEAREST)

		if len(img.shape) == 2:
			img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

		# Dibujamos las celdas
		print ("")
		print (img)
		print (img.shape)
		print ("")
		for x in range(0, x_size*ratio, cell_size*ratio):
			img[:,x] = cell_color
			img[x,:] = cell_color

		# Preparamos las estructuras de datos
		# Cada celda necesita una lista con nbins elementos
		num_x_cells = int(x_size / cell_size)
		num_y_cells = int(y_size / cell_size)
		gradient_strengths = np.zeros((num_x_cells, num_y_cells, nbins))
		cell_update_counter = np.zeros((num_x_cells, num_y_cells))

		# Número de bloques = Número de celdas - 1
		# Hay un nuevo bloque en cada celda salvo en la última
		num_x_blocks = num_x_cells - 1
		num_y_blocks = num_y_cells - 1

		if not self.__block_size[0] == self.__cell_size[0]:
			# Lo habitual, cada bloque es de 2x2 celdas. Se asume que son cuadradas
			# Calculamos la fuerza de los gradientes en cada celda
			# Ojo, que no funciona para bloques que no sean de 2x2 o de 1x1 celdas
			for x_block in range(0, num_x_blocks):
				for y_block in range(0, num_y_blocks):
					for cell in range(0,4):	# 4 celdas por bloque
						# Calculamos cada una de las 4 celdas del bloque
						x_cell = x_block
						y_cell = y_block
						if cell == 1:
							y_cell += 1
						if cell == 2:
							x_cell += 1
						if cell == 3:
							x_cell += 1
							y_cell += 1

						for b in range(0, nbins):
							gradient_strengths[x_cell, y_cell, b] += hog_descriptor[index]
							index += 1

						# Llevamos la cuenta de las veces que se ha actualizado la celda
						# ya que luego hemos de calcular la media
						cell_update_counter[x_cell, y_cell] += 1


			for x_cell in range(0, num_x_cells):
				for y_cell in range(0, num_y_cells):
					num_updates = cell_update_counter[x_cell, y_cell]

					for b in range(0, nbins):
						gradient_strengths[x_cell, y_cell, b] /= num_updates
		else:
			for x_cell in range(0, num_x_cells):
				for y_cell in range(0, num_y_cells):
					for b in range(0, nbins):
						gradient_strengths[x_cell, y_cell, b] += hog_descriptor[index]
						index += 1

		# Ahora vamos a ver como dibujamos esto
		for x_cell in range(0, num_x_cells):
			for y_cell in range(0, num_y_cells):
				# Calculamos el centro de cada celda
				x_center = int((cell_size * ratio)/2 + x_cell * (cell_size * ratio))
				y_center = int((cell_size * ratio)/2 + y_cell * (cell_size * ratio))

				for b in range(0, nbins):
					current_grad_strength = gradient_strengths[x_cell, y_cell, b]

					if current_grad_strength != 0:
						current_grad = b * rad_range_for_one_bin + rad_range_for_one_bin / 2

						dir_vect_x = math.cos(current_grad)
						dir_vect_y = math.sin(current_grad)

						max_vect_len = (cell_size * ratio) / 2

						scale = 2

						# Calculamos las coordenadas
						x1 = int(x_center - dir_vect_x * current_grad_strength * max_vect_len * scale)
						y1 = int(y_center - dir_vect_y * current_grad_strength * max_vect_len * scale)
						x2 = int(x_center + dir_vect_x * current_grad_strength * max_vect_len * scale)
						y2 = int(y_center + dir_vect_y * current_grad_strength * max_vect_len * scale)

						# Lo dibujamos en la imagen
						img = cv2.line(img, (x1, y1), (x2, y2), hog_color, thickness=1 )

		cv2.imshow("imagen", img)
		cv2.waitKey(0)
		cv2.destroyAllWindows()


	# Funciones para dibujar lo que hacemos
	def draw_gradient(self, image):
		if type(image) == str:
			img = cv2.imread(image)
		else:
			img = image

		img = np.float32(img) / 255.0

		# Calcular el gradiente
		gx = cv2.Sobel(img, cv2.CV_32F, 1, 0, ksize=1)
		gy = cv2.Sobel(img, cv2.CV_32F, 0, 1, ksize=1)
		mag, angle = cv2.cartToPolar(gx, gy, angleInDegrees=True)
		
		print (gx)
		print (type(gx))
		print (gy.shape)
		print (mag.shape)
		print (angle.shape)
		cv2.imshow('gx', gx.astype(np.uint8))
		cv2.imshow("gy", gy.astype(np.uint8))
		cv2.imshow("magnitud", mag.astype(np.uint8))
		cv2.imshow("angle", angle.astype(np.uint8))
		cv2.waitKey(0)
		cv2.destroyAllWindows()

