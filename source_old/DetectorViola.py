import cv2
from Detector import Detector
from pathlib import Path

"""
		Detector de caras utilizando el algoritmo implementado en OpenCV2: Viola Jones.
"""


class DetectorViola(Detector):
	def __init__(self):
		print ("Inicializando detector con Viola Jones.")

		# Inicilizando variables de clase
		self.classifier_path = str(Path.home()) + "/TFM/rasprec/resources/haar_files/haarcascade_frontalface_default.xml"
		# self.detector = cv2.CascadeClassifier("haarcascade_frontalface_default")
		self.scale_factor = 1.3
		self.min_neighbors = 5


	def detect_multiscale(self, image):
		self.detector = cv2.CascadeClassifier(self.classifier_path)
		
		rects = self.detector.detectMultiScale(image,
									scaleFactor=self.scale_factor,
									minNeighbors=self.min_neighbors,
									minSize=(30, 30),
									flags=cv2.CASCADE_SCALE_IMAGE)
		return rects
