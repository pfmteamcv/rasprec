""" Clase base para el entrenador de los clasificadores

Esta clase pretende contener los métodos virtuales que deberán
implementar todas las clases heredadas, así como los métodos
con funciones comunes a todas estas clases.


Attributes
---------
faces_dir : str
	Directorio con las imágenes de caras para el entrenamiento
faces_ndir : str
	Directorio con las imágenes de NO caras para el entrenamiento
faces : list
	Lista con rutas de todas las imágenes de caras para el entrenamiento
nfaces : list
	Lista con rutas de todas las imágenes de NO caras para el entrenamiento

Methods
-------
get_files()
	Recorre los directorios de imágenes de entrenamiento y carga todas las imágenes
needs_trainig() : boolean
	Indica si el clasificador requiere entrenamiento o no

"""

import os
from pathlib import Path
from abc import ABCMeta
from abc import abstractmethod

class Trainer:
	__metaclass__ = ABCMeta


	# --------------------- METODOS PRIVADOS ---------------------


	def __init__(self, faces_dir="/TFM/datasets/train/faces/", nfaces_dir="/TFM/datasets/train/nfaces/"):
		"""Inicializa el entrenador

		Inicializa las variables de clase del entrenador. Establece
		los directorios por defecto de los que se obtienen las imágenes
		para el entrenamiento.

		"""

		print ("Inicializando el entrenador")

		# Directorios donde se encuentran las imágenes de entrenamiento
		home=str(Path.home())
		self.faces_dir = home + faces_dir
		self.nfaces_dir = home + nfaces_dir

		self.faces = [] 	
		self.nfaces = [] 


	# -------------------- METODOS VIRTUALES --------------------


	@abstractmethod
	def needs_training(self):
		"""Método virtual. Indica si el algoritmo requiere entrenamiento

		Algunos algoritmos de clasificación requieren entrenamiento (p.e. SVM) mientras
		que otros no (p.e. kNN). Este método virtual devolverá el valor correspondiente
		a cada caso.
		"""

		raise NotImplementedError()


	@abstractmethod
	def train(self):
		""" Método virtual. Entrena el algoritmo y genera los ficheros de salida

		Lanza el proceso de entrenamiento del algoritmo y guarda los resultados
		en los ficheros de salida
		"""
		raise NotImplementedError()


	@abstractmethod
	def is_trained(self):
		"""Verifica si el algoritmo ya ha sido entrenado (existen ficheros)

		Comprueba si ya se han generado los ficheros de entrenamiento del 
		algoritmo
		"""
		raise NotImplementedError()


	@abstractmethod
	def precision_and_recall(self):
		"""Evalua el rendimiento del algoritmo

		Para ello toma el 80% de las imágenes de entrenamiento para entrenar el algoritmo y el resto
		para evaluarlo.
		"""
		raise NotImplementedError()

	# -------------------- METODOS REUTILIZABLES --------------------


	def set_training_dirs(self, faces_dir=None, nfaces_dir=None):
		"""Establece otro directorio de imágenes para entrenamiento

		Parameters
		----------
		faces_dir : str
			Nuevo directorio de imágenes de caras para entrenamiento
		nfaces_dir : str
			Nuevo directorio de imágenes de NO caras para entrenamiento

		"""

		if not faces_dir is None:
			self.faces_dir = faces_dir
		if not nfaces_dir is None:
			self.nfaces_dir = nfaces_dir


	def get_files(self):
		"""Localiza las imágenes para el entrenamiento

		Recorre los directorios de entramiento y recoge las rutas completas
		de todas las imágenes en una lista iterable que podrá ser utilizable
		por todas las clases hija.
		"""

		print ("Recorremos directorios")
		# Obtenemos las rutas de todos los ficheros con caras
		print (self.faces_dir)
		lst_dir = os.walk(self.faces_dir)
		for root, dirs, files in lst_dir:
			for f in files:
				if f[-3:] in ["pgm", "jpg", "png"]:
					self.faces.append(root+f)

		# Obtenemos la ruta de todos los ficheros con no caras
		lst_dir = os.walk(self.nfaces_dir)
		for root, dirs, files in lst_dir:
			for f in files:
				if f[-3:] in ["pgm", "jpg", "png"]:
					self.nfaces.append(root+f)