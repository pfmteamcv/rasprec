import os
from pathlib import Path
from Provider import Provider
import cv2

class DatasetProvider(Provider):
	def __init__(self, dataset, verbose=False):
		Provider.__init__(self, verbose)

		self.path = None		# Ruta del dataset que se utilizará
		self.files = None		# Listado de imágenes del dataset sobre las que se itera
		self.length = None		# Número de imágenes en el dataset

		datasets = {"bioid": "/TFM/datasets/BioID/",
					"yale":  "/TFM/datasets/Yale/faces/",
					"helen": "/TFM/datasets/HELEN/images/",
					"mit":   "/TFM/datasets/MIT_CBCL/train/face/",
					"caltech_back": "/TFM/datasets/caltech/backgrounds/",
					"stanford_bg": "/TFM/datasets/stanford/backgrounds/",
					"training_faces": "/TFM/datasets/train/faces/",
					"training_nfaces": "/TFM/datasets/train/nfaces/"}
		
		dataset=dataset.lower()
		if dataset in datasets:
			home = str(Path.home())
			self.path = home + datasets[dataset]

		# Guardamos la ruta de todas las imágenes
		lst_dir = os.walk(self.path)
		self.files = []
		for root, dirs, files in lst_dir:
			for f in files:
				if f[-3:] in ["pgm", "jpg", "png"]:
					self.files.append(root+f)
		self.length = len(self.files)

		if self.verbose:
			print("[INFO] Se ha inicializado la clase DatasetProvider.")
			print("[INFO] Se está utilizando el dataset " + str(dataset) +
				" ubicado en " + str(datasets[dataset]) + ".")
			print("[INFO] Número de elementos en el dataset: " + str(self.length))


	def get_array(self):
		"""Devuelve el array con todas las direcciones
		"""
		return self.files


	def len(self):
		"""Devuelve el número de imágenes en el dataset
		"""
		return self.len


	def __iter__(self):
		self.actual=0
		return self


	def __next__(self):
		if self.actual == self.len:
			raise StopIteration()
		else:
			i = self.files[self.actual]
			self.actual += 1
			return i
"""

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
lst_dir = os.walk(path + "01_preprocessed/")
detector = HaarDetector()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)
		detected_image = detector.detect(image)
"""