"""
Este fichero genera un dateset de no caras para el entrenamiento
de los clasificadores. 
Simplemente coge todas las imágenes de fondo del dataset de
del MIT, los redimensiona a 70x70 y los guarda en el directorio
de destino.
"""

import cv2
import imutils
from pathlib import Path
from DatasetProvider import DatasetProvider
from Preprocessor import Preprocessor

dp = DatasetProvider("caltech_back")
pp = Preprocessor(color_space="proportional")

img_number=0	# Contador para obtener el nombre de archivo
for file in dp:
	img = cv2.imread(file)
	img = pp.prepare(img)

	# Recortamos para que sea cuadrado
	x, y = img.shape
	if x > y:
		img=img[0:y,:]
	else:
		img=img[:,0:x]
	img = imutils.resize(img, width=70)
	x, y = img.shape
	
	home = str(Path.home())
	file_name="nface_" + str(img_number).zfill(4) + ".png"
	path_faces = home + "/TFM/datasets/training_dataset/non_faces/"
	cv2.imwrite(path_faces + file_name, img)
	img_number+=1

	#cv2.imshow("imagen", img)
	#cv2.waitKey()

