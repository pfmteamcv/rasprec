from Preprocessor import Preprocessor
from DatasetProvider import DatasetProvider
from DetectorViola import DetectorViola
from DetectorHOGkNN import DetectorHOGkNN
from TrainerHOGkNN import TrainerHOGkNN
import cv2
import numpy as np
from time import time
import os
import sys
import time
from pathlib import Path


# Parámetros por defecto para los algoritmos (se puede sacar luego a un fichero de configuración)
hog_winsize = (70, 70)
hog_blocksize = (28, 28)
hog_blockstride = (14, 14)
hog_cellsize = (28, 28)
hog_nbins = 9

# Variables que contendran el entrenador y el detector
detector = None
trainer = None


def precision_and_recall():
	"""Evalúa el rendimiento de un algoritmo
	"""

	home=str(Path.home())
	faces_dir=home + "/TFM/datasets/training_dataset/faces/"
	nfaces_dir=home + "/TFM/datasets/training_dataset/non_faces/"

	#if not trainer.needs_training():


def test_dataset():
	""" Recorre un directorio mostrando las caras en las imágenes
	"""
	dp = DatasetProvider('bioid')
	pp = Preprocessor()
	dhog = DetectorHOGkNN()

	for img_file in dp:
		img = cv2.imread(img_file)
		img = pp.prepare(img)
		print ("--------")
		print (img)

		d = dhog.detect_multiscale(img)
	
		img_color = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

		for (x, y, h, w) in d:
			cv2.rectangle(img_color, (x, y), (x+w, y+h), (0, 255, 255), 1)

		cv2.imshow("image", img_color)
		cv2.waitKey()


def entrenar_clasificador():
	home=str(Path.home())
	faces_dir=home + "/TFM/datasets/training_dataset/faces/"
	nfaces_dir=home + "/TFM/datasets/training_dataset/non_faces/"

	t = TrainerHOGkNN()
	t.train(faces_dir, nfaces_dir)


def menu_principal():
	global detector
	op = None
	ops = ["a", "b", "c"]
	while not op in [ "a", "b", "c", "q" ]:
		os.system('cls')
		print ("Escoge un algoritmo")
		print ("-------------------")
		print ("a) HOG + kNN")
		print ("b) HOG + 1NN")
		print ("c) HOG + SVN")
		print ("q) Quit")
		op = input("Escoge una opción: ")

	if op == "a":
		print ("Inicializando detector")
		detector = DetectorHOGkNN()
	elif op == "q":
		sys.exit(0)

	menu_clasificadores()


def menu_clasificadores():

	salir = False
	while not salir:
		op = None
		while not op in [ "a", "b", "c", "d", "e", "q" ]:
			os.system('cls')
			print ("Escoge una opción")
			print ("-------------------")
			print ("a) Mostrar parámetros")
			print ("b) Cambiar parámetros")
			print ("c) Entrenar")
			print ("d) Precision and recall")
			print ("e) Test")
			print ("q) Volver al menú principal")
			op = input("Escoge una opción: ")

		if op == "a":
			# Mostrar parámetros
			params = detector.get_params()
			print ()
			print ("Parámetros del algoritmo")
			print ("------------------------")
			for p in params:
				print (str(p) + ": " + str(params[p]))
			print ("\nPulsa una tecla para continuar...")
			wait_key()
		elif op == "b":
			# Cambiar parámetros
			params = detector.get_params()
			for p in params:
				if type(params[p]) is int:
					i = input(str(p) + " [" + str(params[p]) + "]: ")
					if i != "":
						params[p] = int(i)
				elif type(params[p]) is float:
					i = input(str(p) + " [" + str(params[p]) + "]: ")
					if i != "":
						params[p] = float(i)
				elif type(params[p]) is tuple:
					print (str(p) + " [" + str(params[p]) + "]")
					i1 = input("    Valor 1: ")
					if i1 != "":
						i2 = input("    Valor 2[" + str(i1) + "]: ")
						if i2 != "":
							params[p] = (int(i1), int(i2))
						else:
							params[p] = (int(i1), int(i1))
			detector.set_params(params)
			print ("--------------")
			print (params)
			print ("Pulsa una tecla para continuar...")
			wait_key()

		elif op == "c":
			entrenar_clasificador()

		elif op == "d":
			precision_and_recall()

		elif op == "e":
			test_dataset()

		elif op == "q":
			salir = True


def wait_key():
    """
    Fuente: https://stackoverflow.com/questions/983354/how-do-i-make-python-to-wait-for-a-pressed-key
    """
    result = None
    if os.name == 'nt':
        import msvcrt
        result = msvcrt.getch()
    else:
        import termios
        fd = sys.stdin.fileno()

        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)

        try:
            result = sys.stdin.read(1)
        except IOError:
            pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)

    return result


while True:
	menu_principal()
	time.sleep(1)



# Lo primero es seleccionar un origen de datos
# Puede ser cámara, base de datos o imagen suelta

"""
print ("Seleccione un base de datos")
print ("1.- Yale")
print ("2.- Helen")
sel = input("Escoge la base de datos: ")

print ("La base escogida es " + sel)
"""

dp = DatasetProvider("helen")	# Origen de los datos
pp_hsv = Preprocessor(color_space="hsv")
pp_pro = Preprocessor(color_space="proportional")
pp_wei = Preprocessor(color_space="weighted")

#dv = DetectorViola()
dhog = DetectorHOGkNN()




for file in dp:
	img = cv2.imread(file)
	img_hsv=pp_hsv.prepare(img)
	print ("*************")
	print (img_hsv.shape)
	img = cv2.cvtColor(img_hsv, cv2.COLOR_GRAY2BGR)
	# img_pro=pp_pro.prepare(img)
	# img_wei=pp_wei.prepare(img)
	
	#rects = dv.detect_multiscale(img_hsv)
	print (img_hsv.shape)
	imgs = dhog.detect_multiscale(img_hsv)
	#print (imgs)
	j = 1
	for i, pos in imgs:
		cv2.imshow("img" + str(j), i)
		j+=1
	cv2.waitKey()
	#for x, y, w, h in rects:
	#	img=cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
	
	#cv2.imshow("hsv", img)
	# cv2.imshow("pro", img_pro)
	# cv2.imshow("wei", img_wei)
	#cv2.waitKey()
	


