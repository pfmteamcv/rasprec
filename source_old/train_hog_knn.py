from DetectorHOGkNN import DetectorHOGkNN
from DatasetProvider import DatasetProvider
from Preprocessor import Preprocessor
import cv2
import threading
from threading import Semaphore
from threading import Lock
import csv
#import matplotlib.pyplot as plt


def test_real_images():
	dp = DatasetProvider("helen")
	d = DetectorHOGkNN()
	pp = Preprocessor()

	params = {
		"win_size": (70, 70),
		"block_size": (35, 35),
		"block_stride": (5, 5),
		"cell_size": (35, 35),
		"nbins": 9
	}
	d.set_params(params)

	d.train2(ratio=0.30)


	for i in dp:
		img = cv2.imread(i)
		img = pp.prepare(img)

		rects=d.detect_multiscale(img)

		overlay=img.copy()
		output=img.copy()

		img=cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
		for (x, y, w, h) in rects:
			overlay=img.copy()
			output=img.copy()
			overlay=cv2.rectangle(overlay, (x,y), (x+w, y+h), (0, 0, 255), -1)
			cv2.addWeighted(overlay, 0.15, output, 1 - 0.15, 0, output)
			img=output

		cv2.imshow("Imagen", img)
		cv2.waitKey()
		print (i)


def precision_and_recall(params = None):
	lock = Lock()
	if params is None:
		params = {
			"win_size": (70, 70),
			"block_size": (35,35),
			"block_stride": (5,5),
			"cell_size": (35,35),
			"nbins": 10,
			"neighs": 5
		}

	d = DetectorHOGkNN(params)
	
	repetitions=3
	results=[]
	acc=0
	rec=0
	pre=0
	tp=0
	tn=0
	fp=0
	fn=0
	for i in range(repetitions):
		values=d.precision_and_recall(ratio=0.20)
		results.append(values)
		acc+=values["accuracy"]
		rec+=values["recall"]
		pre+=values["precision"]
		tp+=values["tp"]
		fp+=values["fp"]
		tn+=values["tn"]
		fn+=values["fn"]


	global final_results
	sem.acquire()
	final_results[params["neighs"]] = values
	sem.release()

	lock.acquire()
	try:
		print ("")
		print ("--------------------------------------------------")
		print ("RESULTADOS")
		print ("**********")
		print ("Aciertos")
		print ("   True positives:  " + str(tp/repetitions))
		print ("   True negatives:  " + str(tn/repetitions))
		print ("Fallos")
		print ("   False positives: " + str(fp/repetitions))
		print ("   False negatives: " + str(fn/repetitions))
		print ("")
		print ("Accuracy:  " + str(acc/repetitions))
		print ("Recall:    " + str(rec/repetitions))
		print ("Precision: " + str(pre/repetitions))
		print ("")
		print ("Dimensiones: " + str(values["dimensions"]))
		print ("Vecinos kNN: " + str(params["neighs"]))
		print ("--------------------------------------------------")
		print ("")
	finally:
		lock.release()



# Menú con las opciones
"""
while not salir:
	op = None
	while not op in [ "a", "b"]:
		os.system('cls')
		print ("Escoge una opción")
		print ("-----------------")
		print ("a) Precision and Recall")
		print ("b) Test sobre imágenes reales")
		print ("q) Volver al menú principal")
		op = input("Escoge una opción: ")

		if op == "a":
"""

def par_thread(params):
	precision_and_recall(params)



iters=10
simplethread=[]
sem=Semaphore(1)

final_results = [None] * iters

for i in range (iters):
	params = {
		"win_size": (70, 70),
		"block_size": (35,35),
		"block_stride": (5,5),
		"cell_size": (35,35),
		"nbins": 10,
		"neighs": i
	}

	simplethread.append(threading.Thread(target=par_thread, args=[params]))
	simplethread[-1].start()

for i in range(iters):
	simplethread[i].join()


print("")
print ("++++++++++++++++++++++++++++++++++++++++++++++")
print ("")
for r in final_results:
	print (r['dimensions'])
	print (r['precision'])

# Copiamos el resultado en un fichero CSV
with open('hogknn_neighbours.csv', 'w') as f:
	print (final_results[0].keys())
	writer = csv.DictWriter(f, final_results[0].keys())

	writer.writeheader()
	for r in final_results:
		print (r)
		writer.writerow(r)



# test_real_images()