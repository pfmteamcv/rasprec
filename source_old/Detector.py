""" Clase base para el detector

Esta clase contiene los métodos virtuales y métodos reutilizables por todas las
clases que hereden de ella. Pretende establecer la estructura común que deberán
tener todos los detectores de caras.

Attributes
---------
faces_dir : str
	Directorio con las imágenes de caras para el entrenamiento
faces_ndir : str
	Directorio con las imágenes de NO caras para el entrenamiento
faces : list
	Lista con rutas de todas las imágenes de caras para el entrenamiento
nfaces : list
	Lista con rutas de todas las imágenes de NO caras para el entrenamiento

Methods
-------
detect_multiscale(image) : 

"""



import cv2
import imutils
from abc import ABCMeta
from abc import abstractmethod

class Detector:
	__metaclass__ = ABCMeta


	# --------------------- METODOS PRIVADOS ---------------------


	def __init__(self, faces_dir="/TFM/datasets/train/faces/", nfaces_dir="/TFM/datasets/train/nfaces/"):
		print ("Inicializando clase Detector")

		# Inicializamos variables
		self.scale_factor = 1.15		# Esto hay que cambiarlo
		self.min_size = 70				# Esto también
		self.disp = 35					# También. Esto es el desplazamiento entre una y otra ventana

		self.faces_dir = faces_dir		# Directorio de caras para el entrenamiento
		self.nfaces_dir = nfaces_dir 	# Directorio de no caras para el entrenamiento


	# -------------------- METODOS VIRTUALES --------------------
	

	@abstractmethod
	def detect_multiscale(self, image):
		raise NotImplementedError()


	@abstractmethod
	def get_params(self):
		raise NotImplementedError()


	@abstractmethod
	def set_params(self):
		raise NotImplementedError()


	@abstractmethod
	def needs_training(self):
		"""Método virtual. Indica si el algoritmo requiere entrenamiento

		Algunos algoritmos de clasificación requieren entrenamiento (p.e. SVM) mientras
		que otros no (p.e. kNN). Este método virtual devolverá el valor correspondiente
		a cada caso.
		"""

		raise NotImplementedError()


	@abstractmethod
	def train(self):
		""" Método virtual. Entrena el algoritmo y genera los ficheros de salida

		Lanza el proceso de entrenamiento del algoritmo y guarda los resultados
		en los ficheros de salida
		"""
		raise NotImplementedError()


	@abstractmethod
	def is_trained(self):
		"""Verifica si el algoritmo ya ha sido entrenado (existen ficheros)

		Comprueba si ya se han generado los ficheros de entrenamiento del 
		algoritmo
		"""
		raise NotImplementedError()


	@abstractmethod
	def precision_and_recall(self):
		"""Evalua el rendimiento del algoritmo

		Para ello toma el 80% de las imágenes de entrenamiento para entrenar el algoritmo y el resto
		para evaluarlo.
		"""
		raise NotImplementedError()



	# --------------------- METODOS PUBLICOS ---------------------


	def get_sliding_windows(self, image):
		"""
			Este método implementa las ventanas deslizantes.
			A partir de una imagen devuelve una lista donde cada elemento contiene
			un par de la forma:
				- Array con una subimagen
				- Lista de la forma (x, y, w, h) con las coordenadas, ancho y alto
				  de la subimagen sobre la imagen original
		"""
		height, width = image.shape

		print ("alto: " +str(height))
		print ("ancho: " +str(width))

		ratio = 1.
		windows = []
		
		while width > self.min_size and height > self.min_size:
			windows.extend(self.__get_fixed_windows(image, ratio))
			width = int(width / self.scale_factor)
			image = imutils.resize(image, width=width)
			height, width = image.shape
			ratio *= self.scale_factor
		return windows


	def __get_fixed_windows(self, image, ratio):
		# Recoge todas las subimágenes para un tamaño determinado
		height, width = image.shape
		w = []

		x = 0
		y = 0

		size = int(self.min_size * ratio)


		while y + self.min_size < height:
			while x + self.min_size < width:
				i = image[y:y+self.min_size, x:x+self.min_size]
				w.append ((i, (int(x*ratio), int(y*ratio), size, size)))
				x += self.disp
			y += self.disp
			x = 0
		return w


	def set_training_dirs(self, faces_dir, nfaces_dir):
		""" Establece los directorios de caras y no caras para el entrenamiento
		"""
		self.faces_dir = faces_dir
		self.nfaces_dir = nfaces_dir
