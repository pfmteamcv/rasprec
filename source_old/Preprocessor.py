import imutils
import cv2
import numpy as np

class Preprocessor:

	""" 
	Las diferentes operaciones de preprocesador pueden ser:
		- Redimensionar: se puede indicar el ancho para un redimensionado proporcional
			o también el alto para estirar la imagen
		- Espacio de color: indica como se convierte a blanco y negro. Las opciones son:
				* proportional: cada componente de color contribuye proporcionalmente
					al valor del gris de cada pixel
				* weighted: se aproxima al ojo humano, teniendo la componente
					verde mayor valor que las otras dos.
				* hsv: se pasa la imagen al espacio de color hsv y se descartan
					los canales de matiz y saturación, que tienen información sobre
					el color, quedándonos únicamente con el canal de valor
		- Ecualización del histograma: si se aplica ecualización del histograma o no
		- Gamma: si se realiza corrección gamma sobre la imagen o no. En caso afirmativo,
			el valor gamma se pasa con el parámetro gamma_value.
	"""

	def __init__(self, width=320, height=None, color_space = "weighted", 
					histogram=False, gamma=False, gamma_value=1.0):
		self.image = None
		self.width = width
		self.height = height
		self.color_space = color_space
		self.histogram = histogram
		self.gamma = gamma
		self.gamma_value = gamma_value

		print ("Inicializando el preprocesador.")


	def prepare(self, image):
		if not image is None:
			# Redimensionamos
			self.image = imutils.resize(image, width=self.width)

			# Convertimos a escala de grises
			if self.color_space == "proportional":
				self.image = np.dot(self.image[...,:3], [0.3333, 0.3333, 0.3333]).astype(np.uint8)
			elif self.color_space == "weighted":
				self.image=cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
			elif self.color_space == "hsv":
				self.image=cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
				self.image=self.image[:,:,2]

			# Aplicamos la corrección gamma
			if self.gamma:
				inv_gamma = 1.0 / self.gamma_value
				table = np.array([((i / 255.0) ** inv_gamma) * 255
					for i in np.arange(0, 256)]).astype("uint8")
				self.image = cv2.LUT(self.image, table)

			# Ecualización del histograma
			if self.histogram:
				self.image = cv2.equalizeHist(self.image)

				
		return self.image


	def configure (self, width=None, height=None, color_space = None, 
					histogram=None, gamma=None, gamma_value=None):
		if not width is None:
			self.width = width
		if not height is None:
			self.height = height
		if not color_space is None:
			self.color_space = color_space
		if not histogram is None:
			self.histogram = histogram
		if not gamma is None:
			self.gamma = gamma
		if not gamma_value is None:
			self.gamma_value = gamma_value


		