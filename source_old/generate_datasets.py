"""Genera automáticamente los datasets de entrenamiento

Genera dos conjuntos de imágenes de entrenamiento: una con caras y otra con no caras

  - La generación del dataset con caras utiliza el código extraido de
    https://jakevdp.github.io/PythonDataScienceHandbook/05.14-image-features.html
    que obtiene las caras del dataset de caras proporcionado por la librería sklearn.
  - La generación del dataset con no caras obtiene las imágenes a partir del dataset 
    de imágenes de fondo de Stanford (http://dags.stanford.edu/projects/scenedataset.html)
    Para obtener un mayor número de imágenes se extraen fragmentos del tamaño deseado
    de las imágenes del dataset mencionado.

Por defecto, se generan un total de 10.189 imágenes de fondo y 13.233 imágenes de caras
con un tamaño de 56x75 píxeles.

"""

import cv2
import numpy as np
from pathlib import Path
import os
from DatasetProvider import DatasetProvider
from Preprocessor import Preprocessor

from sklearn.datasets import fetch_lfw_people
from skimage import data, transform, color
from sklearn.feature_extraction.image import PatchExtractor

faces=fetch_lfw_people(resize=0.6)
positive_patches = faces.images

home = str(Path.home())
base_dir = home + "/TFM/datasets/"
if not os.path.exists(base_dir + "train"):
	os.mkdir (base_dir + "train")
	os.mkdir (base_dir + "train/faces")
	os.mkdir (base_dir + "train/nfaces")

faces_dir = base_dir + "train/faces/"
nfaces_dir = base_dir + "train/nfaces/"


img_number = 1
for img in positive_patches:
	img = img.astype(np.uint8)
	filename = faces_dir + "f" + str(img_number).zfill(5) + ".png"
	cv2.imwrite(filename, img)
	img_number += 1



# Las imágenes de no caras las generamos a partir del dataset de Stanford de fondos
# Son 715 imágenes de 320x240, extraemos fragmentos de ellas.

def extract_patches(image, patch_width=56, patch_height=75):
  global patch_number
  img_height,img_width = image.shape
  
  x=0
  while x+patch_width < img_width:
    y=0
    while y+patch_height < img_height:
      patch=img[y:y+patch_height, x:x+patch_width]
      filename = nfaces_dir + "nf" + str(patch_number).zfill(5) + ".png"
      cv2.imwrite(filename, patch)
      patch_number+=1
      y+=patch_height
    x+=patch_width
  


dp = DatasetProvider('stanford_bg')
pp = Preprocessor()
patch_number=1 

for img_file in dp:
  img = cv2.imread(img_file)
  img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  extract_patches(img)