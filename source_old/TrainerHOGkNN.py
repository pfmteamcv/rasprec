"""Clase que implementar un entrenador con los algoritmos HOG + kNN

Implementa un entrenador utilizando el algoritmo HOG (Histogram of Gradients) como
características y el algoritmo kNN como clasificador.
Hay que tener en cuenta que el método kNN no tiene ningún tipo de entrenamiento ya
que su funcionamiento consisten en localizar los k puntos n-dimensionales más próximos
a la imagen que se quiere clasificar.
Por ello, esta clase únicamente extrae las características de las imágenes de
entrenamiento y guarda estos vectores de características en un fichero.
El objetivo final de esta clase es generar dos ficheros con las características de
las imágenes, uno con caras y otro con no caras

Attributes
----------


Methods
-------
train() : None
	Lanza el proceso de entrenamiento
__read_images() : None
	Obtiene los descriptores HOG de las imágenes y las guarda en un fichero


"""

from Trainer import Trainer
from DatasetProvider import DatasetProvider
import cv2
import imutils
import numpy as np
from pathlib import Path

class TrainerHOGkNN(Trainer):
	def __init__(self, faces_dir="/TFM/datasets/train/faces/", nfaces_dir="/TFM/datasets/train/nfaces/"):
		"""Inicializa el entrenador

		"""
		Trainer.__init__(self, faces_dir, nfaces_dir)

		# Establecemos los ficheros de salida
		self.faces_train_file  = str(Path.home()) + "/TFM/data/hog_desc_faces.out"
		self.nfaces_train_file = str(Path.home()) + "/TFM/data/hog_desc_nfaces.out"
		self.__initialize_HOG()
		

	def precision_and_recall(self, ratio=0.80):
		"""HABRÁ QUE MOVERLO A LA CLASE PADRE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			ratio: porcentaje de imágenes para entrenar, el resto es para test
		"""
		# A lo mejor esto no hace falta
		#self.get_files()


		faces_dp = DatasetProvider("training_faces")
		nfaces_dp = DatasetProvider("training_nfaces")
		print ("Num faces: " + str(faces_dp.length))
		print ("Num nfaces: " + str(nfaces_dp.length))

		# Array que determinará que imágenes se tomarán para entrenar (0) y cuales para test (1)
		faces_type  = np.ones(faces_dp.length, dtype=np.uint8)		# Array que indicará que caras son para entrenar y cuáles no
		nfaces_type = np.ones(nfaces_dp.length, dtype=np.uint8)		# Array que indicará que no caras son para entrenar y cuáles no
		num_faces_train  = int(faces_dp.length*ratio)	# Número de caras para entrenar
		num_nfaces_train = int(nfaces_dp.length*ratio) 	# Número de no caras para entrenar
		print ("num faces: " + str(num_faces_train))
		print ("num nfaces: " + str(num_nfaces_train))
		faces_type[:num_faces_train] = 0
		nfaces_type[:num_nfaces_train] = 0
		np.random.shuffle(faces_type)
		np.random.shuffle(nfaces_type)

		faces_files = faces_dp.get_array()		# Listado de todos los ficheros con caras
		nfaces_files = nfaces_dp.get_array()	# Listado de todos los ficheros con no caras

		faces_train_files = faces_files[faces_type==0]		# Array con los ficheros de caras para entrenar
		faces_test_files = faces_files[faces_type==1]		# Array con los ficheros de caras para test
		nfaces_train_files = nfaces_files[nfaces_type==0]	# Array con los ficheros de no caras para entrenar
		nfaces_test_files = nfaces_files[nfaces_type==0]	# Array con los ficheros de no caras para test

		# Entrenamos el clasificador



	def train(self, faces, nfaces):
		for i in faces:
			img = cv2.imread(i)
			# Obtenemos el descriptor de la imagen
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.faces_descriptors.append(hog_descriptor)

		for i in nfaces:
			img = cv2.imread(i)
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.nfaces_descriptors.append(hog_descriptor)


	"""
	def train(self, faces_dir, nfaces_dir):
		self.get_files()
		self.faces_descriptors = []
		self.nfaces_descriptors = []
		self.__read_images()
	"""

	def needs_training(self):
		return False

	"""
	def __read_images(self):
		# Empezamos con las caras
		for i in self.faces:
			img = cv2.imread(i)

			# Obtenemos el descriptor de la imagen
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.faces_descriptors.append(hog_descriptor)
			
		self.faces_descriptors = np.asarray(self.faces_descriptors)
		np.savetxt(self.faces_train_file, self.faces_descriptors)

		# Y ahora lo mismo con no caras
		x = 0
		for i in self.nfaces:
			img = cv2.imread(i)

			# Obtenemos el descriptor de la imagen
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.nfaces_descriptors.append(hog_descriptor)
			print ("Iteración: " + str(x))
			print (hog_descriptor.shape)
			x += 1
			if x == 1000:
					break
			
		self.nfaces_descriptors = np.asarray(self.nfaces_descriptors)
		np.savetxt(self.nfaces_train_file, self.nfaces_descriptors)
	"""

	def __initialize_HOG(self):
		# Inicializamos HOG
		# Esto para ventana de 50x50 da 144 características
		#self.winSize = (50,50)
		#self.blockSize = (20,20)
		#self.blockStride = (10,10)
		#self.cellSize = (20,20)

		self.winSize = (70,70)
		self.blockSize = (28, 28)
		self.blockStride = (14, 14)
		self.cellSize = (28, 28)

		self.nbins = 9
		self.derivAperture = 1
		self.winSigma = -1.
		self.histogramNormType = 0
		self.L2HysThreshold = 0.2
		self.gammaCorrection = 1
		self.nlevels = 64
		self.signedGradient = True

		self.hog = cv2.HOGDescriptor(self.winSize, self.blockSize,
				self.blockStride, self.cellSize, self.nbins, self.derivAperture,
				self.winSigma, self.histogramNormType, self.L2HysThreshold,
				self.gammaCorrection, self.nlevels, self.signedGradient)


		
		