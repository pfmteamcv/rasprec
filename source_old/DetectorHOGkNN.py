from Detector import Detector
from DatasetProvider import DatasetProvider
from pathlib import Path
import numpy as np
import cv2
import imutils
import time

class DetectorHOGkNN(Detector):

	# --------------------- METODOS PRIVADOS ---------------------

	def __init__(self, params = None):
		print("Inicializando el detector HOG y kNN")
		Detector.__init__(self)

		self.knn=None
		self.faces_descriptors = []
		self.nfaces_descriptors = []
		
		self.winSize = (70,70)
		self.blockSize = (28,28)
		self.blockStride = (14,14)
		self.cellSize = (28, 28)
		self.nbins = 9
		self.neighs = 1

		self.dimensions = None

		if not params is None:
			self.set_params(params)

		self.__initialize()


	def __initialize(self):
		# Inicializamos HOG para obtener el descriptor de las imágenes que queremos clasificar

		self.derivAperture = 1
		self.winSigma = -1.
		self.histogramNormType = 0
		self.L2HysThreshold = 0.2
		self.gammaCorrection = 1
		self.nlevels = 64
		self.signedGradient = True
		self.hog = cv2.HOGDescriptor(self.winSize, self.blockSize,
				self.blockStride, self.cellSize, self.nbins, self.derivAperture,
				self.winSigma, self.histogramNormType, self.L2HysThreshold,
				self.gammaCorrection, self.nlevels, self.signedGradient)


	# --------------------- METODOS PUBLICOS ---------------------


	def get_params(self):
		p = {	'win_size': self.winSize,
				'block_size': self.blockSize,
				'block_stride': self.blockStride,
				'cell_size': self.cellSize,
				'nbins': self.nbins }
		return p


	def set_params(self, params):

		if 'win_size' in params:
			self.winSize = params['win_size']
		if 'block_size' in params:
			self.blockSize = params['block_size']
		if 'block_stride' in params:
			self.blockStride = params['block_stride']
		if 'cell_size' in params:
			self.cellSize = params['cell_size']
		if 'nbins' in params:
			self.nbins = params['nbins']
		if 'neighs' in params:
			self.neighs = params['neighs']

		self.__initialize()


	def detect_multiscale(self, image):
		windows = self.get_sliding_windows(image)
		
		detected = [] 

		for img, coords in windows:
			hd = self.hog.compute(img)
			hd = np.swapaxes(hd, 0, 1)

			ret, results, neighbours, dist = self.knn.findNearest(hd, self.neighs)

			res = int(results[0, 0])
			if res == 1:
				detected.append(coords)
			print ("---------> " + str(res))
			
		print ("******************")
		print (detected)

		# No es esto lo que tiene que devolver, debe ser una lista con las
		# coordenadas de todas las caras encontradas
		return detected


	def is_face(self, image):
		hd = self.hog.compute(image)
		hd = np.swapaxes(hd, 0, 1)

		self.dimensions = hd.shape[1]

		ret, results, neighbours, dist = self.knn.findNearest(hd, 1)
		"""
		print (ret)
		print (" ")
		print (neighbours)
		print (" ")
		print (results)
		print (" ")
		print (dist)
		print ("--------")
		"""
		return int(results[0,0])


	def train2(self, ratio=0.50):
		faces_dp = DatasetProvider("training_faces")
		nfaces_dp = DatasetProvider("training_nfaces")
		faces_type  = np.ones(faces_dp.length, dtype=np.uint8)		# Array que indicará que caras son para entrenar y cuáles no
		nfaces_type = np.ones(nfaces_dp.length, dtype=np.uint8)		# Array que indicará que no caras son para entrenar y cuáles no
		num_faces_train  = int(faces_dp.length*ratio)	# Número de caras para entrenar
		num_nfaces_train = int(nfaces_dp.length*ratio) 	# Número de no caras para entrenar

		faces_type[:num_faces_train] = 0
		nfaces_type[:num_nfaces_train] = 0
		np.random.shuffle(faces_type)
		np.random.shuffle(nfaces_type)

		faces_files = faces_dp.get_array()		# Listado de todos los ficheros con caras
		nfaces_files = nfaces_dp.get_array()	# Listado de todos los ficheros con no caras

		faces_files=np.asarray(faces_files)
		nfaces_files=np.asarray(nfaces_files)
		faces_train_files = faces_files[ faces_type==0 ]	# Array con los ficheros de caras para entrenar
		faces_test_files = faces_files[faces_type==1]		# Array con los ficheros de caras para test
		nfaces_train_files = nfaces_files[nfaces_type==0]	# Array con los ficheros de no caras para entrenar
		nfaces_test_files = nfaces_files[nfaces_type==1]	# Array con los ficheros de no caras para test

		# Entrenamos el clasificador
		self.train(faces_train_files, nfaces_train_files)


	def precision_and_recall(self, ratio=0.80):
		"""HABRÁ QUE MOVERLO A LA CLASE PADRE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			ratio: porcentaje de imágenes para entrenar, el resto es para test
		"""
		faces_dp  = DatasetProvider("training_faces")
		nfaces_dp = DatasetProvider("training_nfaces")
		
		# Array que determinará que imágenes se tomarán para entrenar (0) y cuales para test (1)
		faces_type  = np.ones(faces_dp.length, dtype=np.uint8)		# Array que indicará que caras son para entrenar y cuáles no
		nfaces_type = np.ones(nfaces_dp.length, dtype=np.uint8)		# Array que indicará que no caras son para entrenar y cuáles no
		num_faces_train  = int(faces_dp.length*ratio)	# Número de caras para entrenar
		num_nfaces_train = int(nfaces_dp.length*ratio) 	# Número de no caras para entrenar

		faces_type[:num_faces_train] = 0
		nfaces_type[:num_nfaces_train] = 0
		np.random.shuffle(faces_type)
		np.random.shuffle(nfaces_type)

		faces_files = faces_dp.get_array()		# Listado de todos los ficheros con caras
		nfaces_files = nfaces_dp.get_array()	# Listado de todos los ficheros con no caras

		faces_files=np.asarray(faces_files)
		nfaces_files=np.asarray(nfaces_files)
		faces_train_files = faces_files[ faces_type==0 ]	# Array con los ficheros de caras para entrenar
		faces_test_files = faces_files[faces_type==1]		# Array con los ficheros de caras para test
		nfaces_train_files = nfaces_files[nfaces_type==0]	# Array con los ficheros de no caras para entrenar
		nfaces_test_files = nfaces_files[nfaces_type==1]	# Array con los ficheros de no caras para test

		# Entrenamos el clasificador
		self.train(faces_train_files, nfaces_train_files)
		
		# Vamos a probar el reconocimiento
		faces_tested = 0
		total_faces_time = 0
		tn=0	# True negative. Es negativo y predicho negativo
		tp=0	# True positive. Es positivo y predicho positivo
		fn=0	# False negative. Es positivo y predicho negativo
		fp=0	# False negative. Es negativo y predicho positivo
		for i in faces_test_files:
			faces_tested+=1
			img = cv2.imread(i)
			init_time=time.time()
			if self.is_face(img):
				tp+=1
			else:
				fn+=1
			end_time=time.time()
			total_faces_time+=(end_time-init_time)
		avg_faces_time = total_faces_time / faces_tested

		nfaces_tested = 0
		total_nfaces_time = 0
		for i in nfaces_test_files:
			nfaces_tested+=1
			img=cv2.imread(i)
			init_time=time.time()
			if not self.is_face(img):
				tn+=1
			else:
				fp+=1
			end_time=time.time()
			total_nfaces_time+=(end_time-init_time)
		avg_nfaces_time = total_nfaces_time / nfaces_tested

		total = faces_tested + nfaces_tested
		accuracy = (tp + tn)/total
		recall = tp / ( tp + fn)
		precision = tp / ( tp + fp )

		"""
		print (" ")
		print ("Resultados")
		print ("----------")
		print ("Total caras testadas:    " + str(faces_tested))
		print ("Total no caras testadas: " + str(nfaces_tested))
		print ("Tiempo medio evaluación: " + str((avg_nfaces_time+avg_faces_time)/2))
		print ("Aciertos")
		print ("  True positive:  " + str(tp))
		print ("  True negatives: " + str(tn))
		print ("Fallos")
		print ("  False positive: " + str(fp))
		print ("  False negative: " + str(fn))		
		print (" ")
		print ("Exactitud:    " + str(accuracy))
		print ("Recuperación: " + str(recall))
		print ("Precision:    " + str(precision))
		"""

		#return faces_tested, nfaces_tested, tp, tn, fp, fn, accuracy, recall, precision
		return {
			"faces_tested": faces_tested,
			"nfaces_tested": nfaces_tested,
			"tp": tp,
			"tn": tn,
			"fp": fp,
			"fn": fn,
			"accuracy": accuracy,
			"recall": recall,
			"precision": precision,
			"dimensions": self.dimensions
		}

	def train(self, faces, nfaces):
		for i in faces:
			img = cv2.imread(i)
			# Obtenemos el descriptor de la imagen
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.faces_descriptors.append(hog_descriptor)

		for i in nfaces:
			img = cv2.imread(i)
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.nfaces_descriptors.append(hog_descriptor)


		num_faces_train = len(self.faces_descriptors)
		num_nfaces_train = len(self.nfaces_descriptors)

		# Cargamos el clasificador
		# Clasificador: 1 caras, 0 no caras
		response = np.concatenate((np.ones((num_faces_train,1), dtype=np.float32), np.zeros((num_nfaces_train, 1), dtype=np.float32)),
									axis=0)
		train_data=np.concatenate((self.faces_descriptors, self.nfaces_descriptors), axis=0)

		# https://docs.opencv.org/3.4/d5/d26/tutorial_py_knn_understanding.html
		self.knn=cv2.ml.KNearest_create()
		self.knn.train(train_data, cv2.ml.ROW_SAMPLE, response)
