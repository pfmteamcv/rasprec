from TrainerHOGkNN import TrainerHOGkNN
from DetectorHOGkNN import DetectorHOGkNN
from pathlib import Path 
import os
import cv2


dhk = DetectorHOGkNN()

# ***************************
# Entrenamos el modelo
home=str(Path.home())
train_faces_dir =  home + "/TFM/datasets/training_dataset/faces/"
train_nfaces_dir = home + "/TFM/datasets/training_dataset/non_faces/"

t = TrainerHOGkNN(train_faces_dir, train_nfaces_dir)

# ***************************
# Y ahora lo probamos
test_faces_dir =  home + "/TFM/datasets/test_dataset/faces/"
test_nfaces_dir = home + "/TFM/datasets/test_dataset/non_faces/"


print ("CARAS")
print ("+++++++++++++++++++++++")
# Guardamos la ruta de todas las imágenes
lst_dir = os.walk(test_faces_dir)
files_list = []
for root, dirs, files in lst_dir:
	for f in files:
		if f[-3:] in ["pgm", "jpg", "png"]:
			files_list.append(root+f)

			
for f in files_list:
	image = cv2.imread(f)
	res = dhk.is_face(image)
	print ("Resultado: " + str(res))



print ("NO CARAS")
print ("+++++++++++++++++++++++")
# Guardamos la ruta de todas las imágenes
lst_dir = os.walk(test_nfaces_dir)
files_list = []
for root, dirs, files in lst_dir:
	for f in files:
		if f[-3:] in ["pgm", "jpg", "png"]:
			files_list.append(root+f)

			
for f in files_list:
	image = cv2.imread(f)
	res = dhk.is_face(image)
	print ("Resultado: " + str(res))

