from Detector import Detector
import cv2
import numpy as np

class DetectorHOGSVM(Detector):

	# --------------------- METODOS PRIVADOS ---------------------
	def __init__(self, params = None):
		print("Inicializando el detector HOG y kNN")
		Detector.__init__(self)

		#self.knn=None
		self.faces_descriptors = []		# Descriptores HOG de las caras
		self.nfaces_descriptors = []	# Descriptores HOG de las NO caras
		
		# Parámetros para HOG
		self.winSize = (70,70)			
		self.blockSize = (28,28)
		self.blockStride = (14,14)
		self.cellSize = (28, 28)
		self.nbins = 9
		self.neighs = 1

		self.dimensions = None

		if not params is None:
			self.set_params(params)

		self.__initialize()


	def __initialize(self):
		# Inicializamos HOG para obtener el descriptor de las imágenes que queremos clasificar

		self.derivAperture = 1
		self.winSigma = -1.
		self.histogramNormType = 0
		self.L2HysThreshold = 0.2
		self.gammaCorrection = 1
		self.nlevels = 64
		self.signedGradient = True
		self.hog = cv2.HOGDescriptor(self.winSize, self.blockSize,
				self.blockStride, self.cellSize, self.nbins, self.derivAperture,
				self.winSigma, self.histogramNormType, self.L2HysThreshold,
				self.gammaCorrection, self.nlevels, self.signedGradient)


	# --------------------- METODOS PUBLICOS ---------------------

	# Obtiene los parámetros de HOG
	def get_params(self):
		p = {	'win_size': self.winSize,
				'block_size': self.blockSize,
				'block_stride': self.blockStride,
				'cell_size': self.cellSize,
				'nbins': self.nbins }
		return p


	# Establece los parámetros de HOG
	def set_params(self, params):

		if 'win_size' in params:
			self.winSize = params['win_size']
		if 'block_size' in params:
			self.blockSize = params['block_size']
		if 'block_stride' in params:
			self.blockStride = params['block_stride']
		if 'cell_size' in params:
			self.cellSize = params['cell_size']
		if 'nbins' in params:
			self.nbins = params['nbins']
		#if 'neighs' in params:
		#	self.neighs = params['neighs']

		self.__initialize()




	def train2(self, ratio=0.50):
		faces_dp = DatasetProvider("training_faces")
		nfaces_dp = DatasetProvider("training_nfaces")
		faces_type  = np.ones(faces_dp.length, dtype=np.uint8)		# Array que indicará que caras son para entrenar y cuáles no
		nfaces_type = np.ones(nfaces_dp.length, dtype=np.uint8)		# Array que indicará que no caras son para entrenar y cuáles no
		num_faces_train  = int(faces_dp.length*ratio)	# Número de caras para entrenar
		num_nfaces_train = int(nfaces_dp.length*ratio) 	# Número de no caras para entrenar

		faces_type[:num_faces_train] = 0
		nfaces_type[:num_nfaces_train] = 0
		np.random.shuffle(faces_type)
		np.random.shuffle(nfaces_type)

		faces_files = faces_dp.get_array()		# Listado de todos los ficheros con caras
		nfaces_files = nfaces_dp.get_array()	# Listado de todos los ficheros con no caras

		faces_files=np.asarray(faces_files)
		nfaces_files=np.asarray(nfaces_files)
		faces_train_files = faces_files[ faces_type==0 ]	# Array con los ficheros de caras para entrenar
		faces_test_files = faces_files[faces_type==1]		# Array con los ficheros de caras para test
		nfaces_train_files = nfaces_files[nfaces_type==0]	# Array con los ficheros de no caras para entrenar
		nfaces_test_files = nfaces_files[nfaces_type==1]	# Array con los ficheros de no caras para test

		# Entrenamos el clasificador
		self.train(faces_train_files, nfaces_train_files)


	def train(self, faces, nfaces):
		for i in faces:
			img = cv2.imread(i)
			# Obtenemos el descriptor de la imagen
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.faces_descriptors.append(hog_descriptor)

		for i in nfaces:
			img = cv2.imread(i)
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			self.nfaces_descriptors.append(hog_descriptor)


		num_faces_train = len(self.faces_descriptors)
		num_nfaces_train = len(self.nfaces_descriptors)

		# Cargamos el clasificador
		# Clasificador: 1 caras, 0 no caras
		response = np.concatenate((np.ones((num_faces_train,1), dtype=np.float32), np.zeros((num_nfaces_train, 1), dtype=np.float32)),
									axis=0)
		train_data=np.concatenate((self.faces_descriptors, self.nfaces_descriptors), axis=0)

		# https://docs.opencv.org/3.4/d5/d26/tutorial_py_knn_understanding.html
		self.knn=cv2.ml.KNearest_create()
		self.knn.train(train_data, cv2.ml.ROW_SAMPLE, response)









	def __init__(self):
		print("Inicializando el detector HOG/SVM")
		Detector.__init__(self)

		# Inicializamos HOG
		# Esto para ventana de 50x50 da 144 características
		self.winSize = (50,50)
		self.blockSize = (20,20)
		self.blockStride = (10,10)
		self.cellSize = (20,20)
		self.nbins = 9
		self.derivAperture = 1
		self.winSigma = -1.
		self.histogramNormType = 0
		self.L2HysThreshold = 0.2
		self.gammaCorrection = 1
		self.nlevels = 64
		self.signedGradient = True
		self.hog = cv2.HOGDescriptor(self.winSize, self.blockSize,
				self.blockStride, self.cellSize, self.nbins, self.derivAperture,
				self.winSigma, self.histogramNormType, self.L2HysThreshold,
				self.gammaCorrection, self.nlevels, self.signedGradient)


	def detect_multiscale(self, image):
		print ("Esto debe devolver coordenadas de rectángulos con las caras detectadas.")
		windows = self.get_sliding_windows(image)
		for img, coords in windows:
			hog_descriptor = self.hog.compute(img)
			hog_descriptor = np.squeeze(hog_descriptor)
			print ("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
			print (hog_descriptor)
			print (len(hog_descriptor))
			print ("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

		return windows

