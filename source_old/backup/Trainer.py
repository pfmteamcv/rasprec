# -*- coding: utf-8 -*- 
import cv2
import numpy as np



class Trainer():
	def __init__(self, tfaces, label):
		"""
		Inicializa el entrenador.

		Parámetros:
			tfaces - array con todas las caras para el entrenamiento
			label  - cadena de texto asociada a esa cara. 
		"""
		print "Inicializando el entrenador"

		self.tfaces = tfaces
		self.recognizer = None
		self.labels = np.zeros(len(tfaces), dtype=int)
		self.labels[:] = 25	# Este número hay que cambiarlo y guardarlo en un fichero asociado a la etiqueta
		print type(self.labels)
		print len(self.tfaces)


	def train(self):
		print "Comenzando entrenamiento"
		self.recognizer.train(self.tfaces, self.labels)
		print "Ya está entrenado"



class EigenTrainer(Trainer):
	def __init__(self):
		Trainer.__init__(self)
		self.recognizer = cv2.face.createEigenFaceRecognizer()


	


class FisherTrainer(Trainer):
	def __init__(self, tfaces, label):
		Trainer.__init__(self, tfaces, label)
		self.recognizer = cv2.face.createFisherFaceRecognizer()


class LBPHTrainer(Trainer):
	def __init__(self):
		Trainer.__init__(self)
		self.recognizer = cv2.face.createLBPHFaceRecognizer()