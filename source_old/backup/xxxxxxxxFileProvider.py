# -*- coding: utf-8 -*- 
import cv2
import os
from Provider import Provider
from PIL import Image

class FileProvider(Provider):
	"""
	Proveeodr de imágenes a partir de un directorio
	Recoge todas las imágenes que se encuentran en dicho directorio
	"""

	def __init__(self, path):
		"""
		Args:
			path - Relativo al fichero del proyecto
		"""
		self.next = None		# Siguiente imagen
		self.has_next = False	# Almacena si quedan más imágenes
		self.files_left = None	# Ficheros que quedan por procesar
		self.paht = ""			# Ruta del proyecto
		self.files_list = []	# Listado de ficheros del directorio

		project_path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/"
		self.path = project_path + path
		lst_dir = os.walk(self.path)
		
		for root, dirs, files in lst_dir:
			for file in files:
				self.files_list.append(root + "/" + file)
					
		self.files_left = len(self.files_list)
		print "Se han detectado {} ficheros".format(self.files_left)

		# Dejamos la primera imagen preparada
		self.prepare_next()





	def next(self):
		""" Devuelve la siguiente imagen
		"""
		return self.next


	def prepare_next(self):
		print len(self.files_list)
		if len(self.files_list) > 0:
			file = self.files_list.pop()
			image = cv2.imread(file)
			print "Quedan {} imágenes".format(len(self.files_list))
			if image is None:
				print "No se ha podido cargar la imagen"
				self.has_next = False
				self.prepare_next()
			else:
				print "Imagen reconocida: {}".format(file)
				self.has_next = True
				self.next = image