# -*- coding: utf-8 -*- 

class Processor:
	"""
	Clase base para el procesador de imágenes que prepara las caras antes de su reconocimiento
	"""
	def __init__(self):
		# Almacenamos los frames intermedios para documentación
		self.face = None					# Frame original
		self.face_geom_rotate = None		# Tras aplicar rotación en transformaciones geométricas 
		self.face_geom_scale = None			# Tras aplicar escalado en transformaciones geométricas 
		self.face_geom_translate = None		# Tras aplicar traslación en transformaciones geométricas 
		self.face_geom_crop = None			# Tras aplicar recorte en transformaciones geométricas 
		self.face_hist = None				# Tras ecualización de histograma independiente
		self.face_smooth = None				# Tras suavizado
		self.face_mask = None				# Tras aplicar la máscara
		self.face_processed = None			# Frame tras aplicar todas las operaciones

		self.desired_width = 70				# Ancho de la cara ya recortada
		self.desired_height = 70			# Alto de la cara ya recortada

		self.left_eye_coords = None
		self.right_eye_coords = None

		self.ed = None						# Detector 

		print "Inicializado el procesador de caras"


	def load (self, face):
		self.face = face


		# Detectamos la posición de los ojos
		self.ed = EyeDetector(self.frame)
		self.ed.detect(method="HAAR_EYES_AREA")		# El método es utilizar cascadas Haar sobre el área donde se esperan los ojos
		end = time.time()
		self.times["initialization"] = (end-start)*1000

		# Si no se han detectado 2 ojos
		if self.ed.detected == 2:
			(self.x1, self.y1), (self.x2, self.y2) = self.ed.get_eyes_centers()
			self.eyes_detected = True
			#print "Se han detectado los ojos correctamente"
			return True
		else:
			self.eyes_detected = False
			#print "No se han podido detectar los ojos"
			return False




	def initialize(self):
		"""
		Este método debe ser heredado por las clases hijo.
		Su objetivo es localizar los ojos en la imagen.
		"""

	def prepare(self):
		if self.eyes_detected is None:
			self.initialize()
		if self.eyes_detected:
			self.do_geometric()
			self.do_histogram()
			self.do_smoothing()
			self.do_elliptical_mask()
			self.frame_processed = self.frame_mask


	def do_geometric(self):
		slope = self.ed.get_slope()
		if not slope is None:
			start = time.time()
			# Primer paso: rotación de la imagen para que los ojos estén en la horizontal
			self.frame_geom_rotate = imutils.rotate(self.frame, math.degrees(slope))

			# Segundo paso: se redimensiona la imagen para que los ojos se encuentren en las posiciones deseadas
			# Calculamos el ratio para que la distancia entre ambos ojos sea 0.68
			ratio = float(self.desired_right_eye_x - self.desired_left_eye_x)/(self.x2-self.x1)
			if self.width <= self.height:
				self.frame_geom_scale = imutils.resize(self.frame_geom_rotate, width=int(self.width*ratio))
			else: 
				self.frame_geom_scale = imutils.resize(self.frame_geom_rotate, height=int(self.height*ratio))
				
			if self.verbose:
				print "Distancia entre ojos actual: " + str(self.x2-self.x1)
				print "Distancia entre ojos deseada: " + str(self.desired_right_eye_x - self.desired_left_eye_x)
				print "Ancho actual: " + str(self.width)
				print "Nuevo ancho: " + str(int(self.width*ratio))
				print "Alto actual: " + str(self.height)
				print "Nuevo alto: " + str(int(self.height*ratio))
				print "Ratio: " + str(ratio)

			new_width = int(self.width*ratio)
			new_height = int(self.height*ratio)

			# Recortamos la imagen para que el ojo izquierdo se encuentre en (0.16, 0.14)
			new_x1 = self.x1 * ratio
			new_y1 = self.y1 * ratio

			crop_left = int(new_x1 - self.desired_left_eye_x)					# Lo que ha que recortar por la izquierda
			crop_top = int(new_y1 - self.desired_eyes_y)							# Lo que hay que recortar por arriba
			crop_right = int(new_width - self.desired_width - crop_left)	# Lo que hay que recortar por la derecha
			crop_down = int(new_height - self.desired_height - crop_top) 	# Lo que hay que recortar por debajo

			# Hay ocasiones en que para centrar los ojos nos salimos de la cara. Con esto se soluciona aunque quede
			# ligeramente descentrado
			"""
			if new_height-crop_down-crop_top < 70:
				print "Corrigiendo"
				crop_down-=1
			"""
			if crop_down <= 0:
				crop_top += crop_down - 1
				crop_down = 1
			if crop_right <= 0:
				crop_left += crop_right - 1
				crop_right = 1

			#self.frame_geom_crop = self.frame_geom_scale[crop_left:self.width-crop_right, crop_top:self.width-crop_down]
			#self.frame_geom_crop = self.frame_geom_scale[crop_top:new_height-crop_down, crop_left:new_width-crop_right]
			self.frame_geom_crop = self.frame_geom_scale[crop_top:crop_top+self.desired_height, crop_left:crop_left+self.desired_width]
			if self.verbose:
				print "Desired left eye x: " + str(self.desired_left_eye_x)
				print "Crop left: " + str(crop_left)
				print "Crop right: " + str(crop_right)
				print "Crop top: " + str(crop_top)
				print "Crop down: " + str(crop_down)

			end = time.time()
			self.times["geometric"] = (end-start)*1000


	def do_histogram(self):
		""" Realiza el histograma por partes sobre la imagen
		"""
		# Se ecualiza la cara completa y también las mitades por separado
		self.face_hist = cv2.equalizeHist(self.face_geom_crop)
		face_hist_tmp = self.face_hist 	# Solo a efectos de generación de capturas para documentación
		face_left = self.face_geom_crop[:, :int(self.desired_width/2)]
		face_right = self.face_geom_crop[:, int(self.desired_width/2):]

		face_left_hist = cv2.equalizeHist(face_left)
		face_right_hist = cv2.equalizeHist(face_right)

		# Y se combinan
		# ---> Comparar velocidad con np.item y np.itemset
		div_left   = int(self.desired_width*0.25)	# Punto donde comienza la transición entre izquierda y centro
		div_center = int(self.desired_width*0.50)	# Punto central
		div_right  = int(self.desired_width*0.75)	# Punto donde comienza la transición entre centro y derecha
		step = 1./(div_center-div_left)				# Cuánto se debe incrementar o decremenar blend en la parte central
		blend = 0									# Cuánto se deben mezclar las imágenes
		for x in range (0, self.desired_width):
			# Calculamos el ratio de mezcla en cada columna
			if x < div_left or x > div_right:
				blend = 0
			else:
				blend += step * (-1 if x > div_center else 1)

			for y in range (0, self.desired_height):
				# Parte izquierda
				if x < div_center:
					self.face_hist[y, x] = int(face_left_hist[y, x]*(1. - blend) + self.face_hist[y, x]*blend)
				else:
					self.face_hist[y, x] = int(face_right_hist[y, x-div_center]*(1. - blend) + self.face_hist[y, x]*blend)

		return face_left, face_right, face_left_hist, face_right_hist, face_hist_tmp


	def do_smoothing(self):
		""" Aplica el suavizado sobre la imagen para eliminar los ruidos introducidos por el histograma.
		"""

		# Los valores son los indicados en el libro (hay que verificarlo y probar con otros tipos de filtro
		self.face_smooth = cv2.bilateralFilter(self.face_hist, 0, 20., 2.)


	def do_elliptical_mask(self):
		""" Aplica una máscara elíptica para eliminar los elementos periféricos.
		"""
		center = (int(self.desired_width*0.5), int(self.desired_height*0.4))
		size = (int(self.desired_width * 0.5), int(self.desired_height * 0.8))
		mask = np.zeros((self.desired_width, self.desired_height))
		mask = cv2.ellipse(mask, center, size,0,0,360,175,-1)
		self.face_mask =  self.face_smooth
		self.face_mask[mask==0] = 96

		

class HaarProcessor(Processor):
	"""
	Cascadas de Haar para detectar los ojos
	"""

	def __init__(self):
		""" Inicializa el procesador de caras
		"""
		



	def load(img):
		""" Carga una imagen en el procesador de caras












