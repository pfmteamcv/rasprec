# -*- coding: utf-8 -*- 

import cv2
import numpy as np
import imutils

class Interface():
	def __init__(self):
		print "Inicializando la interfaz"

		self.width = 510
		self.height = 390
		self.background_color = (69, 56, 40)

		self.left = 0		# Si la imagen a mostrar es más pequeña la distancia hasta el borde izquierdo
		self.top = 0		# Si la imagen a mostrar es más pequeña la distancia hasta el borde superior

		# Creamos el frame en blanco
		self.frame = np.zeros((self.height, self.width, 3), np.uint8)
		# Y lo ponemos de color azul
		self.frame[:320, :] = self.background_color

		# Botón de debug
		#self.draw_buttons()

		# Mostramos la imagen por pantalla
		cv2.imshow("imagen", self.frame)
		cv2.waitKey(100)


	def draw_buttons(self):
		self.frame[330:370, 10:100] = (155, 0, 0)
		self.frame = cv2.putText(self.frame, "Debug", (15, 355), cv2.FONT_HERSHEY_COMPLEX, 0.75, (255, 255, 255))

	def set_image(self, img):
		"""
		Establece la imagen que se mostrará en la interfaz
		La imagen de entrada siempre tiene 340 de ancho
		"""
		(h, w, c) = img.shape

		# Rellenamos el área no cubierta por la imagen de negro
		self.frame[10:310, 10:350] = (0, 0, 0)
		
		if w > 340:
			# Si el ancho es > 340 la reducimos
			img = imutils.resize(img, 340)
		elif w < 340:
			# Si es menor se deja espacio a los lados.
			self.left = int((340 - w)/2)+10
			self.top = int((300 - h)/2)+10
			self.frame[self.top:self.top+h, self.left:self.left+w] = img
		else:
			# Lo habitual es que tenga 340 de ancho
			self.top = int((300 - h)/2)
			self.frame[10+self.top:10+self.top+h, 10:350] = img

		# Dibujamos el espacio para las caras
		# Las coordenadas de las caras son:
		#   Cara detectada: (360, 10), (500, 150)
		#self.frame = cv2.rectangle(self.frame, (360, 10), (500, 150), (57, 44, 32), 5)
		self.frame[10:150, 360:500] = (57, 44, 32)

		#   Cara procesada: (360, 160), (500, 300)
		#self.frame = cv2.rectangle(self.frame, (360, 160), (500, 310), (57, 44, 32), 5)
		self.frame[160:310, 360:500] = (57, 44, 32)


	def set_training_faces(self, tfaces):
		"""
		Dibuja las imágenes seleccionadas para entrenamiento en la parte inferior de la interfaz
		"""
		print "***"
		print type(tfaces[0])
		print "****"
		x = 0
		for i in tfaces[-7:]:
			self.frame[320:390, 10+(70*x):80+(70*x), 0] = i
			self.frame[320:390, 10+(70*x):80+(70*x), 1] = i
			self.frame[320:390, 10+(70*x):80+(70*x), 2] = i
			x += 1


	def set_face_rect(self, coords):
		"""
		Establece las coordenadas en que se encontró la cara para dibujarla
		"""
		((x1, y1), (x2, y2)) = coords
		self.frame = cv2.rectangle(self.frame, (x1+10, y1+10+self.top), (x2+10, y2+10+self.top), (255, 0, 0), 2)


	def set_eyes(self, eyes, areas, landmarks):
		"""
		Establece los centros de los ojos para dibujarlos
		Puede que se detecte 1, 2 o ninguno
		"""
		if not eyes is None:
			if len(eyes) == 2:
				(x1, y1) = eyes[0]
				(x2, y2) = eyes[1]
				self.frame = cv2.line(self.frame, (x1+360, y1+10), (x2+360, y2+10), (0, 255, 255), 1 )
			for (x, y) in eyes:
				self.frame = cv2.circle(self.frame, (x+360, y+10), 2, (0, 255, 255))

		if not areas is None:
			for ((x1, y1), (x2, y2)) in areas:
				self.frame = cv2.rectangle(self.frame, ((x1+360, y1+10), (x2+360, y2+10)), (255, 0, 255), 1)

		if not landmarks is None:
			for (x, y) in landmarks:
				if (x>0) and (x<139) and (y>0) and (y<139):
					self.frame = cv2.circle(self.frame, (x+360, y+10), 1, (255, 255, 0))
		


	def set_face (self, face):
		"""
		Establece la cara detectada en la esquina superior derecha
		"""
		(h, w) = face.shape
		left = self.width - w - 20
		top = 20

		self.frame[10:150,360:500, 0] = face
		self.frame[10:150,360:500, 1] = face
		self.frame[10:150,360:500, 2] = face
		

	def set_processed_faces(self, cface, hface, sface, pface):
		"""
		Dibuja todos los pasos intermedios del procesamiento
		Estos pasos son:
			- cface: Cara tras aplicar las transfomaciones
			- hface: Cara tras aplicar el histograma
			- sface: Cara tras aplicar el suavizado
			- pface: Cara tras finalizar el procesamiento
		Todas deberían medir 70x70

		"""
		# Todas tienen las mismas dimesiones
		(h, w) = pface.shape
	

		# Cara tras las trasformaciones (parte superior izquierda)
		self.frame[160:230, 360:430, 0] = cface
		self.frame[160:230, 360:430, 1] = cface
		self.frame[160:230, 360:430, 2] = cface
		# Cara tras aplicar el histograma (parte superior derecha)
		self.frame[160:230, 430:500, 0] = hface
		self.frame[160:230, 430:500, 1] = hface
		self.frame[160:230, 430:500, 2] = hface
		# Cara tras aplicar suavizado (parte inferior izquierda)
		self.frame[230:300, 360:430, 0] = sface
		self.frame[230:300, 360:430, 1] = sface
		self.frame[230:300, 360:430, 2] = sface
		# Cara una vez finalizado el procesamiento (parte inferior derecho)
		self.frame[230:300, 430:500, 0] = pface
		self.frame[230:300, 430:500, 1] = pface
		self.frame[230:300, 430:500, 2] = pface



	def draw(self):
		cv2.imshow("imagen", self.frame)
		cv2.waitKey(1)

