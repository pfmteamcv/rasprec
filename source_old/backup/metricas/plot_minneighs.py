# -*- coding: utf-8 -*- 
import matplotlib.pyplot as plt
import json
import numpy as np

file = open('./pi/min_neighbors.json', 'r')
j = file.read()
data = json.loads(j)
sf_data = data[0]
results = sf_data["results"]	# Esto es una lista

average_time = []
multiple_faces = []
value = []
detected_faces = []
total_images = int(sf_data["num_images"])

for r in results:
	average_time.append(r["average_time"])
	multiple_faces.append(r["multiple_faces"])
	value.append(r["value"])
	detected_faces.append(r["detected_faces"])

detected_faces = np.array(detected_faces, dtype=np.float)
multiple_faces = np.array(multiple_faces, dtype=np.float)

detected_percent = detected_faces/total_images
multiple_percent = multiple_faces/total_images
print detected_percent
print multiple_percent

#plt.plot(value, average_time)
plt.plot(value, detected_percent, label='Detectada')
plt.plot(value, multiple_percent, label='Falsos positivos')
plt.legend()

# Vamos a sacar máximos y mínimos
# Detectados
index_max_detected = np.argmax(detected_percent) # Índice del valor máximo
index_min_detected = np.argmin(detected_percent) # Índice del valor mínimo
# Coordenadas del valor máximo de detectados
max_detected = (value[index_max_detected], detected_percent[index_max_detected])
min_detected = (value[index_min_detected], detected_percent[index_min_detected])

plt.axhline(max_detected[1], linestyle='dashed', linewidth=1, color='#009999')
plt.axhline(min_detected[1], linestyle='dashed', linewidth=1, color='#009999')


# Colocamos texto cerca del valor donde se encuentra el mínimo
plt.text(max_detected[0], max_detected[1]-0.05,
			"{0:.2f}".format(max_detected[0]),
			#str(detected_percent[pos_max_detected]),
			fontsize = 10, horizontalalignment='center', verticalalignment='center')  
plt.text(min_detected[0], min_detected[1]-0.05,
			"{0:.2f}".format(min_detected[0]),
			#str(detected_percent[pos_max_detected]),
			fontsize = 10, horizontalalignment='center', verticalalignment='center')  


# plt.legend(handles=[detected_plt, false_plt])
#print average_time
plt.xlabel("Factor de escala")
plt.ylabel("Porcentaje de deteccion")
plt.show()