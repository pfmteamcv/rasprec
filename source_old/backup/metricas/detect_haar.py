# -*- coding: utf-8 -*- 
import cv2
import os
from Detector import HaarDetector
from time import time
import numpy as np
import json
import psutil
from datetime import datetime
import platform

""""
	Cálculo de las métricas de detección sobre la base de datos BioID

"""


"""
	Pruebas sobre el detector.
	Leemos las imágenes de prueba ya preprocesadas de la BD de Yale y detectamos la cara
"""


def detect(path):
	total_images = 0
	total_time = 0
	detected_faces = 0
	multiple_faces = 0
	cpu_avg = [0, 0, 0, 0]

	lst_dir = os.walk(path)

	for root, dirs, files in lst_dir:
		for file in files:
			if file[-3:] == 'pgm':
				total_images += 1
				image = cv2.imread(root + file)

				# Este es el tiempo que hay que medir
				# La primera llamada a psutil inicializa el contador. La segunda recoge el porcentaje de procesador
				# empleado desde la primera llamada.
				start_time = time()
				psutil.cpu_percent(percpu=True, interval=0)
				# La primera llamada inicializa el contador. La segunda 
				detected_image = detector.detect(image)
				cpu = psutil.cpu_percent(percpu=True, interval=0)
				end_time = time()
				t = (end_time - start_time)*1000
				total_time += end_time - start_time

				cpu_avg = [cpu_avg[0]+cpu[0],cpu_avg[1]+cpu[1],cpu_avg[2]+cpu[2],cpu_avg[3]+cpu[3],]


				#cpu_user = float(end_cpu.user)-float(start_cpu.user)

				# psutils.cpu_times() devuelve el tiempo empleado por el procesador. La unidad de medida es USER_HZ y está
				# explicado en el primer enlace. Esta medida puede variar con la arquitectura, habitualmente es una centésima
				# de segundo pero se puede comprobar con la llamada a sysconf (segundo enlace)
				# Aquí se muestra en milisegundos (por eso se divide por 1000)
				# https://stackoverflow.com/questions/27039537/what-is-the-meaning-of-every-paramater-returned-by-psutil-cpu-times-in-python
				# https://docs.python.org/2/library/os.html
				"""
				cpu1 = (end_cpu[0].user - start_cpu[0].user) # *os.sysconf('SC_CLK_TCK')/1000
				cpu2 = (end_cpu[1].user - start_cpu[1].user) # *os.sysconf('SC_CLK_TCK')/1000
				cpu3 = (end_cpu[2].user - start_cpu[2].user) # *os.sysconf('SC_CLK_TCK')/1000
				cpu4 = (end_cpu[3].user - start_cpu[3].user) # *os.sysconf('SC_CLK_TCK')/1000

				print "CPU 1: " + str(cpu1)
				print "CPU 2: " + str(cpu2)
				print "CPU 3: " + str(cpu3)
				print "CPU 4: " + str(cpu4)
				print "Time: " + str(t)

				print " "
				"""
				rects = detector.get_rects_as_coords()

				if len(rects) > 0:
					if len(rects) == 1:
						detected_faces += 1
					else:
						multiple_faces += 1
					for f in detector.faces:
						cv2.imshow("face", f)
						#cv2.waitKey(1)

		cpu_avg = [cpu_avg[0]/total_images, cpu_avg[1]/total_images,cpu_avg[2]/total_images,cpu_avg[3]/total_images]
		data = {}
		#data.update({'total_images': total_images})
		data.update({'detected_faces': detected_faces})
		data.update({'multiple_faces': multiple_faces})
		data.update({'average_time': (total_time/total_images)*1000})
		data.update({'average_cpu': cpu_avg})

		return data


def init_dictionary(stage, parameter, database, algorithm):
	d = {}
	d.update({'stage': stage})
	d.update({'algorithm': algorithm})
	d.update({'parameter': parameter})
	d.update({'database': database})
	d.update({'timestamp': str(datetime.today())})
	if database == "BioID":
		d.update({'num_images': '1521'})	# Este número habría que automatizarlo
	elif database == "Yale":
		d.update({'num_images': '165'})	# Este número habría que automatizarlo
	d.update({'processor': platform.processor()})
	d.update({'memory': psutil.virtual_memory().total})
	os = platform.dist()
	d.update({'operative_system': os[0] + " " + os[1] + " - " + os[2]})

	return d

#path = os.getenv("HOME") + "/resources/facial_databases/Yale_Face_Database/faces_pgm/"
#path = os.getenv("HOME") + "/resources/facial_databases/BioID_Face_Database/"


# -----------------------------------------------------
# Iteraciones sobre minSize y Yale
# -----------------------------------------------------
results_final = []
path = os.getenv("HOME") + "/resources/facial_databases/Yale_Face_Database/faces_pgm/"
path = os.getenv("HOME") + "/resources/facial_databases/BioID_Face_Database/"
results_param = init_dictionary(stage="detection", parameter="min_size", database="BioID", algorithm="Viola Jones")

detector = HaarDetector()
results_param_list = []

for ms in range(10, 150, 5):
	print "Tamaño de ventana: (" + str(ms) + ", " + str(ms) + ")"
	detector.set_parameters(min_size = (ms, ms))

	data = detect(path)
	data.update({'value': ms})
	
	results_param_list.append(data)
	results_param.update({'results': results_param_list})

results_final.append(results_param)

file = open('min_size.json', 'w')
file.write(json.dumps(results_final))
file.close()

print " "
# -----------------------------------------------------
# Iteraciones sobre scaleFactor y Yale
# -----------------------------------------------------
results_final = []
path = os.getenv("HOME") + "/resources/facial_databases/Yale_Face_Database/faces_pgm/"
results_param = init_dictionary(stage="detection", parameter="scale_factor", database="Yale", algorithm="Viola Jones")

detector = HaarDetector()
results_param_list = []

for sf in np.arange(1.05, 2.5, 0.05):
	print "Factor de escala: " + str(sf)
	detector.set_parameters(scale_factor = sf)

	data = detect(path)
	data.update({'value': sf})
	
	results_param_list.append(data)
	results_param.update({'results': results_param_list})

results_final.append(results_param)

file = open('scale_factor.json', 'w')
file.write(json.dumps(results_final))
file.close()

print " "
# -----------------------------------------------------
# Iteraciones sobre minNeighbors y Yale
# -----------------------------------------------------
results_final = []
path = os.getenv("HOME") + "/resources/facial_databases/Yale_Face_Database/faces_pgm/"
results_param = init_dictionary(stage="detection", parameter="minNeighbors", database="Yale", algorithm="Viola Jones")

detector = HaarDetector()
results_param_list = []

for mn in range(1, 20):
	print "Mínimo vecinos: " + str(mn)
	detector.set_parameters(min_neighbors = mn)

	data = detect(path)
	data.update({'value': mn})
	
	results_param_list.append(data)
	results_param.update({'results': results_param_list})

results_final.append(results_param)

file = open('min_neighbors.json', 'w')
file.write(json.dumps(results_final))
file.close()


