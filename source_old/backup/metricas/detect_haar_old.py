# -*- coding: utf-8 -*- 
import cv2
import os
from Detector import HaarDetector
from time import time

""""
	Cálculo de las métricas de detección sobre la base de datos BioID

"""


"""
	Pruebas sobre el detector.
	Leemos las imágenes de prueba ya preprocesadas de la BD de Yale y detectamos la cara
"""

results = []


path = os.getenv("HOME") + "/resources/facial_databases/BioID_Face_Database/"
path = os.getenv("HOME") + "/resources/facial_databases/Yale_Face_Database/faces_pgm/"
lst_dir = os.walk(path)

# Valores que se deben variar
#	Tamaño de la imagen original
#	scaleFactor
# 	minNeighbors
#	minSize

detector = HaarDetector()

detector.set_parameters(scale_factor = 1.5, min_size=(30, 30), min_neighbors=3)

total_images = 0
total_time = 0
detected_faces = 0
multiple_faces = 0

for root, dirs, files in lst_dir:
	for file in files:
		if file[-3:] == 'pgm':
			total_images += 1
			image = cv2.imread(root + file)
			start = time()
			detected_image = detector.detect(image)
			end = time()
			total_time += end - start

			rects = detector.get_rects_as_coords()

			if len(rects) > 0:
				if len(rects) == 1:
					detected_faces += 1
				else:
					multiple_faces += 1
				for f in detector.faces:
					cv2.imshow("face", f)
					cv2.waitKey(10)
cv2.waitKey(2000)
print "Total de imágenes: " + str(total_images)
print "Caras detectadas: " + str(detected_faces)
print "Caras múltiples: " + str(multiple_faces)
print "Tiempo total: " +  str(total_time)
print "Tiempo medio: " + str((total_time/total_images)*1000) + " ms."

"""

path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/yale_tests/"
#path = os.getenv("HOME") + "/facial-recognition-for-raspberry-pi-3/rasprec/facial_databases/bioid_tests/"
lst_dir = os.walk(path + "01_preprocessed/")
detector = HaarDetector()


for root, dirs, files in lst_dir:
	for file in files:
		image = cv2.imread(root + file)
		detected_image = detector.detect(image)

		rects = detector.get_rects_as_coords()

		# Si se ha detectado alguna cara
		if len(rects) > 0:
			#img_rgb = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
			# Iteramos sobre las caras encontradas

			for f in detector.faces:
				cv2.imwrite(path + "02_detected/" + file, f)

			for r in rects:
				# Mostramos la posición de la cara detectada
				cv2.rectangle(image, r[0], r[1], (0, 255, 0), 2)

			#cv2.imshow("text2", image)
			#cv2.waitKey(0)
"""