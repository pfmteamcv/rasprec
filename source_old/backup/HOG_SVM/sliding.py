# -*- coding: utf-8 -*-

import cv2
import numpy as np


def get_hog() : 
	# Esto para ventana de 150x150 da 144 características
	winSize = (150,150)
	blockSize = (60,60)
	blockStride = (30,30)
	cellSize = (60,60)
	nbins = 9
	derivAperture = 1
	winSigma = -1.
	histogramNormType = 0
	L2HysThreshold = 0.2
	gammaCorrection = 1
	nlevels = 64
	signedGradient = True

	hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradient)

	return hog
	affine_flags = cv2.WARP_INVERSE_MAP|cv2.INTER_LINEAR


def svmInit(C=12.5, gamma=0.50625):
	"""
	model = cv2.ml.SVM_create()
	model.setGamma(gamma)
	model.setC(C)
	model.setKernel(cv2.ml.SVM_RBF)
	model.setType(cv2.ml.SVM_C_SVC)
	"""
	# https://stackoverflow.com/questions/38182132/how-to-load-svm-data-from-file-in-opencv-3-1
	model = cv2.ml.SVM_load('digits_svm_model.yml')
	return model


def sliding_window(image, stepSize, windowSize):
	print image.shape
	h, w, c = image.shape
	ww, wh = windowSize 	
	for y in xrange(0, h-wh+1, stepSize):
		for x in xrange(0, w-ww+1, stepSize):
			yield (x, y, image[y:y+wh, x:x+ww])


img = cv2.imread('people.jpg')


# Inicializamos HOG
hog = get_hog()

model = svmInit()


superposition = 75
cell_size = (150, 150)

hog_descriptors = []
images = []

for (x1, y1, i) in sliding_window(img, superposition, cell_size):
	# Obtenemos el HOG de la ventana
	i = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
	images.append(i)
	hog_descriptor = hog.compute(i)
	hog_descriptor = np.squeeze(hog_descriptor)
	
	hog_descriptors.append(hog_descriptor)

images = np.array(images)
print "images shape"
print images.shape

hog_descriptors = np.array(hog_descriptors)
print hog_descriptors
print hog_descriptors.shape
prediction = model.predict(hog_descriptors)
prediction = prediction[1]

masc =  np.squeeze((prediction == 1))
print "masc shape"
print masc.shape
print masc


faces = images[masc]
print faces

for i in faces:
	cv2.imshow("f", i)
	cv2.waitKey(5000)

cv2.destroyAllWindows()


"""
	img2 = np.copy(img)
	img2 = cv2.rectangle(img2, (x1, y1), (x1+cell_size[0], y1+cell_size[1]), (255, 0, 0), 2)

	cv2.imshow("img", img2)
	cv2.imshow("f", i)

	cv2.waitKey(500)


cv2.destroyAllWindows()
"""