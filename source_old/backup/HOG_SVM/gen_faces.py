# -*- coding: utf-8 -*-
import cv2
import os
import imutils

path_images = './../../facial_databases/bioid_tests/00_original/'
path_dest = './train/faces'

li = os.listdir(path_images)
images = []

count = 0
# Leemos las imágenes
for i in li:
	filename = path_images + i
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


	d = cv2.CascadeClassifier('/home/victor/fr/rasprec/source/haar_files/haarcascade_frontalface_alt.xml')
	rects = d.detectMultiScale(img ,
				scale_factor=1.2,
				minNeighbors=5,
				minSize=(30, 30),
				flags=cv2.CASCADE_SCALE_IMAGE)
	# Se almacenan las coordenadas de las caras detectadas en la imagen
	for x, y , w, h in rects:
		image = img[y:y+h, x:x+w]
		image = imutils.resize(image, width=70, height=70)
		fn = "./train/faces/bioid"+str(count)+".jpg"
		print "Guardando ... " + fn
		cv2.imwrite(fn, image)
		count += 1
		#cv2.imshow("asd", image)
		#cv2.waitKey(1000)