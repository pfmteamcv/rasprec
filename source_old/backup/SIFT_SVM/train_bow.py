# -*- coding: utf-8 -*-
#from os import listdir

"""
Explicación HOG (muy buena)
http://www.learnopencv.com/histogram-of-oriented-gradients/

Como se ha hecho
http://www.learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/

Codigo fuente
https://github.com/spmallick/learnopencv
"""

import os
import cv2
import numpy as np
import itertools as it
import imutils

 
 # Crea la estructura para SIFT
def get_sift():
	sift = cv2.xfeatures2d.SIFT_create()
	return sift


def svmInit(C=12.5, gamma=0.50625):
	model = cv2.ml.SVM_create()
	model.setGamma(gamma)
	model.setC(C)
	model.setKernel(cv2.ml.SVM_RBF)
	model.setType(cv2.ml.SVM_C_SVC)
	return model



def svmTrain(model, samples, responses):
	model.train(samples, cv2.ml.ROW_SAMPLE, responses)
	return model

def svmPredict(model, samples):
	print samples.shape
	m = model.predict(samples)
	"""
		predict devuelve un array con 3 datos:
			- Un float que no se lo que es (por ahora es 0.0)
			- Array con las predicciones
			- Tipo de datos de las predicciones (dtype=float32) 
	"""
	return m[1].ravel() # Se convierte a array unidimensional


def svmEvaluate(model, images, samples, labels):
	# Samples son los descriptores
    predictions = svmPredict(model, samples)
    accuracy = (labels == predictions).mean()
    print('Percentage Accuracy: %.2f %%' % (accuracy*100))

    confusion = np.zeros((2, 2), np.int32)
    for i, j in zip(labels, predictions):
        confusion[int(i), int(j)] += 1
    print('confusion matrix:')
    print(confusion)

    print "Etiquetas"
    print labels
    print "predicciones"
    print predictions

    vis = []
    #for img, flag in zip(images, predictions != labels):
    #	if flag:
	#		cv2.imshow("Error", img)
	#		cv2.waitKey(1000)
    for img, flag in zip(images, predictions):
    	if flag == 1:
    		print "OK"
    		cv2.imshow("Face", img)
    		cv2.waitKey(1000)
    	else:
    		print "NO OK"
    		cv2.imshow("Non Face", img)
    		cv2.waitKey(1000)
		

home = os.getenv("HOME")
if home is None:
	home = os.path.expanduser('~')	# Windows
path_faces = home + '/resources/train/faces/'
path_non_faces = home + '/resources/train/non_faces/'
lf = os.listdir(path_faces)
lnf = os.listdir(path_non_faces)

images = []
labels = []

# Leemos las imágenes que son cara
for p in lf:
	filename = path_faces + p
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	#img = imutils.resize(img, width=70, height=70)
	img = cv2.resize(img, (70, 70))
	img = cv2.equalizeHist(img)

	cv2.imshow("img", img)
	cv2.waitKey(1)
	images.append(img)
	labels.append(1)

# Leemos las imágenes que NO son cara
for p in lnf:
	filename = path_non_faces + p
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	# Redimensionamos a lo cutre
	img = cv2.resize(img, (70, 70))
	y, x = img.shape
	cv2.imshow("img", img)
	cv2.waitKey(1)

	images.append(img)
	labels.append(0)

print "Se han leido " + str(len(images)) + " imágenes"

# Mezclamos los datos de entrada
rand = np.random.RandomState(75)
shuffle = rand.permutation(len(images))

images = np.array(images)
labels = np.array(labels)
images = images[shuffle]
labels = labels[shuffle]


# PASO 1: Construir el diccionario
# Inicializamos SIFT
sift = get_sift()

bow = cv2.BOWKMeansTrainer(5)

print 'Calculando HOG para cada imagen ...'
sift_keypoints = []
sift_descriptors = []
for img in images:
	kp, desc = sift.detectAndCompute(img, None)
	#descriptor = sift.compute(img, kp)
	d = np.array(desc)
	print d.shape
	sift_descriptors.append(desc)
	if not desc is None:
		bow.add(desc)
#sift_descriptors = np.squeeze(sift_descriptors)

# Calculando el clúster
dictionary = bow.cluster()
print "-----------------"
print "DICCIONARIO"
print dictionary
# En este punto hemos creado un diccionario de palabras
# En este caso es un conjunto de 5 puntos (tamaño del diccionario)
# en un espacio de 128 dimensiones (tamaño del descriptor)
# En el ejemplo lo guarda en disco pero creo que no hace falta. Se puede poner una captura en la doc

# PASO 2: Obtener el descriptor BOF de una imagen
# Se parte del vocabulario del punto anterior




FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params,search_params)
sift2 = cv2.DescriptorExtractor_create("SIFT")
bowDiction = cv2.BOWImgDescriptorExtractor(sift2, cv2.BFMatcher(cv2.NORM_L2))
bowDiction.setVocabulary(dictionary)
print "bow dictionary", np.shape(dictionary)


"""
#returns descriptor of image at pth
def feature_extract(pth):
    im = cv2.imread(pth, 1)
    gray = cv2.cvtColor(im, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    return bowDiction.compute(gray, sift.detect(gray))

train_desc = []
train_labels = []
i = 0
for p in training_paths:
    train_desc.extend(feature_extract(p))
    if names_path[i]=='Chair':
        train_labels.append(1)
    if names_path[i]=='Doge':
        train_labels.append(2)
    if names_path[i]=='Football':
        train_labels.append(3)
    if names_path[i]=='Watch':
        train_labels.append(4)
    if names_path[i]=='Wrench': 
        train_labels.append(5)
    i = i+1

print "svm items", len(train_desc), len(train_desc[0])
count=0
svm = cv2.SVM()
svm.train(np.array(train_desc), np.array(train_labels))
i=0
j=0

confusion = np.zeros((5,5))
def classify(pth):
    feature = feature_extract(pth)
    p = svm.predict(feature)
    confusion[train_labels[count]-1,p-1] = confusion[train_labels[count]-1,p-1] +1
    
    

for p in training_paths:
    classify(p)
    count+=1

def normalizeRows(M):
    row_sums = M.sum(axis=1)
    return M / row_sums
    
confusion = normalizeRows(confusion)

confusion = confusion.transpose()
    
print confusion

"""








"""

# Dividimos los datos entre entrenamiento (90%) y test (10%)
train_n = int(0.9*len(sift_descriptors))
print "train_n"
print train_n
images_train, images_test = np.split(images, [train_n])
hog_descriptors_train, hog_descriptors_test = np.split(sift_descriptors, [train_n])
labels_train, labels_test = np.split(labels, [train_n])


# Entrenamos el modelo svm
model = svmInit(C=6.5)

print "Entrenando modelo"
svmTrain(model, hog_descriptors_train, np.array(labels_train))

model.save("faces_model.yml");

print "Evaluando el modelo"
svmEvaluate(model, images_test, hog_descriptors_test, labels_test)


#cv2.imwrite("classification.jpg",vis)
#cv2.imshow("Vis", vis)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

"""