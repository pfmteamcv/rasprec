# -*- coding: utf-8 -*-

import cv2
import numpy as np 
import matplotlib.pyplot as plt

img = cv2.imread('face.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gray = cv2.resize(gray, (250, 250))
sift = cv2.xfeatures2d.SIFT_create()
kps = sift.detect(gray, None)

# Con esto también obtenemos el descriptor en cada keypoint
kps, desc = sift.detectAndCompute(gray, None)
# Se supone que el 4 de flags es = que DRAW_RICH_KEYPOINTS 
# https://docs.opencv.org/2.4/modules/features2d/doc/drawing_function_of_keypoints_and_matches.html
img = cv2.drawKeypoints(gray, kps, img, flags=4)

print "Se ha detectado " + str(len(kps)) + " keypoints"
# Escogemos uno cualquiera
print len (kps)
kp = kps[0]

print "Angulo:" + str(kp.angle)
print "Clase:" + str(kp.class_id)
print "Octave:" + str(kp.octave)
print "pt:" + str(kp.pt)
print "response:" + str(kp.response)
print "Tamaño:" + str(kp.size)

kp = kps[1]
print " "
print "Angulo:" + str(kp.angle)
print "Clase:" + str(kp.class_id)
print "Octave:" + str(kp.octave)
print "pt:" + str(kp.pt)
print "response:" + str(kp.response)
print "Tamaño:" + str(kp.size)


print " "
print desc[0]
print len(desc)
print len(desc[0])

plt.imshow(desc[0].reshape(16, 8), interpolation=None)
plt.show()
"""
img2 = cv2.imread('face2.png')
gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
gray2 = cv2.resize(gray2, (250, 250))
sift2 = cv2.xfeatures2d.SIFT_create()
kp2 = sift.detect(gray2, None)
img2 = cv2.drawKeypoints(gray2, kp2, img2)
"""

cv2.imshow('SIFT', img)
#cv2.imshow('SIFT2', img2)
cv2.waitKey(0)

