# -*- coding: utf-8 -*-
#from os import listdir

"""
Explicación HOG (muy buena)
http://www.learnopencv.com/histogram-of-oriented-gradients/

Como se ha hecho
http://www.learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/

Codigo fuente
https://github.com/spmallick/learnopencv
"""

import os
import cv2
import numpy as np
import itertools as it
import imutils


def get_hog() : 
	# Esto para ventana de 150x150 da 144 características
	winSize = (70,70)
	blockSize = (30,30)
	blockStride = (10,10)
	cellSize = (15,15)
	nbins = 9
	derivAperture = 1
	winSigma = -1.
	histogramNormType = 0
	L2HysThreshold = 0.2
	gammaCorrection = 1
	nlevels = 64
	signedGradient = True

	hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradient)

	return hog
	affine_flags = cv2.WARP_INVERSE_MAP|cv2.INTER_LINEAR
 
 # Crea la estructura para SIFT
def get_sift():
	sift = cv2.xfeatures2d.SIFT_create()
	return sift


def svmInit(C=12.5, gamma=0.50625):
	model = cv2.ml.SVM_create()
	model.setGamma(gamma)
	model.setC(C)
	model.setKernel(cv2.ml.SVM_RBF)
	model.setType(cv2.ml.SVM_C_SVC)
	return model



def svmTrain(model, samples, responses):
	model.train(samples, cv2.ml.ROW_SAMPLE, responses)
	return model

def svmPredict(model, samples):
	print samples.shape
	m = model.predict(samples)
	"""
		predict devuelve un array con 3 datos:
			- Un float que no se lo que es (por ahora es 0.0)
			- Array con las predicciones
			- Tipo de datos de las predicciones (dtype=float32) 
	"""
	return m[1].ravel() # Se convierte a array unidimensional


def svmEvaluate(model, images, samples, labels):
	# Samples son los descriptores
    predictions = svmPredict(model, samples)
    accuracy = (labels == predictions).mean()
    print('Percentage Accuracy: %.2f %%' % (accuracy*100))

    confusion = np.zeros((2, 2), np.int32)
    for i, j in zip(labels, predictions):
        confusion[int(i), int(j)] += 1
    print('confusion matrix:')
    print(confusion)

    print "Etiquetas"
    print labels
    print "predicciones"
    print predictions

    vis = []
    #for img, flag in zip(images, predictions != labels):
    #	if flag:
	#		cv2.imshow("Error", img)
	#		cv2.waitKey(1000)
    for img, flag in zip(images, predictions):
    	if flag == 1:
    		print "OK"
    		cv2.imshow("Face", img)
    		cv2.waitKey(1000)
    	else:
    		print "NO OK"
    		cv2.imshow("Non Face", img)
    		cv2.waitKey(1000)
		

home = os.getenv("HOME")
if home is None:
	home = os.path.expanduser('~')	# Windows
path_faces = home + '/resources/train/faces/'
path_non_faces = home + '/resources/train/non_faces/'
lf = os.listdir(path_faces)
lnf = os.listdir(path_non_faces)

images = []
labels = []

# Leemos las imágenes que son cara
for p in lf:
	filename = path_faces + p
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	#img = imutils.resize(img, width=70, height=70)
	img = cv2.resize(img, (70, 70))
	img = cv2.equalizeHist(img)

	cv2.imshow("img", img)
	cv2.waitKey(1)
	images.append(img)
	labels.append(1)

# Leemos las imágenes que NO son cara
for p in lnf:
	filename = path_non_faces + p
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	# Redimensionamos a lo cutre
	img = cv2.resize(img, (70, 70))
	y, x = img.shape
	cv2.imshow("img", img)
	cv2.waitKey(1)

	images.append(img)
	labels.append(0)


print "Se han leido " + str(len(images)) + " imágenes"


# Mezclamos los datos de entrada
rand = np.random.RandomState(75)
shuffle = rand.permutation(len(images))

images = np.array(images)
labels = np.array(labels)
images = images[shuffle]
labels = labels[shuffle]

# Inicializamos SIFT
sift = get_sift()

print 'Calculando SIFT para cada imagen ...'
sift_descriptors = []
for img in images:
	#kp = sift.detect(img, None)				# Se detectan los keypoints
	#descriptor = sift.compute(img, kp)		# Generamos el descriptor
	kp, dsc = sift.detectAndCompute(img, None)	# El segundo parámetro creo que es una máscara
	# kp: lista de keypoints
	# dsc: lista de descriptores
	sift_descriptors.append(dsc)

#sift_descriptors = np.squeeze(sift_descriptors)

print 'Dividimos entre entrenamiento y test'
# Dividimos los datos entre entrenamiento (90%) y test (10%)
train_n = int(0.9*len(sift_descriptors))
print "train_n"
print train_n
images_train, images_test = np.split(images, [train_n])
hog_descriptors_train, hog_descriptors_test = np.split(sift_descriptors, [train_n])
labels_train, labels_test = np.split(labels, [train_n])

# Entrenamos el modelo svm
model = svmInit(C=6.5)

print "Entrenando modelo"
svmTrain(model, hog_descriptors_train, np.array(labels_train))

model.save("faces_model.yml");

print "Evaluando el modelo"
svmEvaluate(model, images_test, hog_descriptors_test, labels_test)


#cv2.imwrite("classification.jpg",vis)
#cv2.imshow("Vis", vis)
#cv2.waitKey(0)
#cv2.destroyAllWindows()