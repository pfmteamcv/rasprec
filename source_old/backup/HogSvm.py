# -*- coding: utf-8 -*- 
import cv2
import numpy as np
import imutils
import time

class HogSvm:
	"""
	Esta clase encapsula un detector de imágenes basado en características HOG y clasificación SVM
	"""

	def __init__(self, image):
		"""
		Realiza los pasos necesarios para inicializar todo
		"""
		self.image = image
		self.hog = self.get_hog()
		self.model = self.svmInit()

		# Configuración para las pirámides
		self.scale = 1.5 
		self.min_size = (70, 70)


	def svmInit(self):
		model = cv2.ml.SVM_load('./HOG_SVM/faces_model.yml')
		return model 
		
		"""
		model = cv2.ml.SVM_create()
		model.setGamma(gamma)
		model.setC(C)
		model.setKernel(cv2.ml.SVM_RBF)
		model.setType(cv2.ml.SVM_C_SVC)
		return model
		"""


	def get_hog(self) : 
		# Esto para ventana de 150x150 da 144 características
		winSize = (70,70)
		blockSize = (30,30)
		blockStride = (10,10)
		cellSize = (15,15)
		nbins = 9
		derivAperture = 1
		winSigma = -1.
		histogramNormType = 0
		L2HysThreshold = 0.2
		gammaCorrection = 1
		nlevels = 64
		signedGradient = True

		hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradient)

		return hog
		affine_flags = cv2.WARP_INVERSE_MAP|cv2.INTER_LINEAR


	def get_slides(self, image, stepSize, windowSize):
		h, w = image.shape
		ww, wh = windowSize 	
		for y in xrange(0, h-wh+1, stepSize):
			for x in xrange(0, w-ww+1, stepSize):
				yield (x, y, image[y:y+wh, x:x+ww])


	def detect (self):
		"""
		Detecta las caras que hay en la imagen de entrada
		"""

		start = time.time()

		# Paso 1: Pyramids
		img = np.copy(self.image)
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		#img = cv2.equalizeHist(img)
		ratio = 1
		pyramids = []	# Imágenes en diversos tamaños
		ratios = []		# Ratios de reducción para cada imagen
		pyramids.append(img)
		ratios.append(ratio)
		while True:
			w = int(img.shape[1]/self.scale)
			ratio *= self.scale
			img = imutils.resize(img, width = w)
			pyramids.append(img)
			ratios.append(ratio)
			if img.shape[0]<self.min_size[1] or img.shape[1]<self.min_size[0]:
				break

		# En este punto pyramids es una lista de imágenes con diferentes tamaños de la imagen original
		rects = []				# Coordenadas de las caras encontradas: ((x1, y1), (x2, y2))
		stepSize = 10			# Salto para las ventanas deslizantes
		windowSize = (70, 70)   # Tamaño de la ventana deslizante
		for i in range(len(pyramids)-1):
			p = pyramids[i]
			r = ratios[i]

			hog_descriptors = []
			coords = []			
			for (x, y, cell) in self.get_slides(p, stepSize, windowSize):
				# En cell tenemos un fragmento de la imagen de 70x70
				# Calculamos el descriptor HOG
				hog_descriptor = self.hog.compute(cell)
				hog_descriptor = np.squeeze(hog_descriptor)
				hog_descriptors.append(hog_descriptor)
				coords.append((x, y))
				
			# Realizamos la predicción para esta ventana
			preds = self.model.predict(np.array(hog_descriptors))
			p = preds[1].ravel()

			# Recorremos las detecciones positivas
			for (x, y), flag in zip(coords, p == 1):
				if flag:
					x1 = int(x*r)
					y1 = int(y*r)
					x2 = int((x+windowSize[0])*r)
					y2 = int((y+windowSize[1])*r)
					rects.append(((x1, y1), (x2, y2)))
		
		end = time.time()
		t = (end-start)*1000
		print "Tiempo: " + str(t)

		# Tenemos todas las caras detectadas almacenadas en rects.
		# Aquí hay que llamar a mapas de calor o a NMS

		faces = np.copy(self.image)
		for (p1, p2) in rects:			
			faces = cv2.rectangle(faces, p1, p2, (0, 0, 255), 1)
		cv2.imshow("Final", faces)
		cv2.waitKey(50)

		rects = self.heat_map(rects)




			#cv2.imshow ("a"+str(c), i)
		cv2.waitKey(0)

		# Paso 2: Sliding
		

		# Paso 3: HOG

		# Paso 4: SVM

		# Paso 5: Devolver coordenadas


	def heat_map(self, rects):
		"""
		Se le pasan una serie de detecciones positivas en la imagen, genera el mapa de calor. 
		Unifica las detecciones solapadas y elimina falsos positivos.
		"""

		threshold = 1	# Umbral para los falsos positivos

		w, h, c = self.image.shape

		hm = np.zeros((w, h), dtype=np.uint8)		# Array para el mapa de calor
		hmd = np.zeros((w, h), dtype=np.uint8)		# Array para limpiar el mapa de 


		for ((x1, y1), (x2, y2)) in rects:
			for x in range(x1, x2):
				for y in range(y1, y2):
					hm[y, x] += 50

		print hm
		hm_draw = cv2.applyColorMap(hm, cv2.COLORMAP_JET)
		cv2.imshow("hm", hm_draw)

		# Eliminamos las partes con baja tasa de positivos
		hm[hm <=50] = 0
		
		cv2.imshow("hm2", hm)
		
		cv2.waitKey(5000)
		print hm


img = cv2.imread('./HOG_SVM/yo.jpg')
img = imutils.resize(img, width=340)
#img = cv2.imread('./HOG_SVM/train/faces/subject01.sleepy.pgm')
cv2.imshow("imagen", img)
cv2.waitKey(500)

hs = HogSvm(img)
hs.detect()