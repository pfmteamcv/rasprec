# -*- coding: utf-8 -*- 


class ImageFragment(object):
	""" Almacena un fragmento de una imagen

	El objetivo de esta clase es encapsular fragmentos de imágenes (en concreto serán caras) recordando
	sus coordenadas respecto a la imagen inicial.
	"""
	def __init__(self, img, x, y, w, h):
		self.image = img		# Imagen
		self.coords = (x, y)	# Coordenadas de la esquina superior izquierda: (x, y)
		self.dims = (w, h)		# Dimesiones de la imagen: (ancho, alto)


	
		