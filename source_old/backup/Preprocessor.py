# -*- coding: utf-8 -*- 
import imutils
import cv2
import ConfigParser

class Preprocessor:
	"""
	Clase base para el preprocesador de imágenes
	"""
	def __init__(self):
		config = ConfigParser.ConfigParser()
		config.read("../source/config.ini")
		options = config.options("Preprocessor")
		print config.options("Preprocessor")
		if "initialwidth" in config.options("Preprocessor"):
			self.width = int(config.get("Preprocessor", "InitialWidth"))
		else:
			self.width = 320	

		print "Inicializado el preprocesador"


	def prepare(self, image):
		print "Preparando imagen"




class BasicPreprocessor(Preprocessor):
	"""
	Preprocesador básico. Realiza las siguientes operaciones:
	- Convertir a escala de grises
	- Redimensionar la imagen

	"""
	def prepare(self, original_image):
		image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)		# Escala de grises
		image = imutils.resize(image, width=self.width)
		#image = cv2.equalizeHist(image)

		return image




