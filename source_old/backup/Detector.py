# -*- coding: utf-8 -*- 
import cv2
import imutils
import ConfigParser
import os
 
class Detector(object):
	"""
	Clase base para los detectores faciales
	Por ahora sus clases heredadas son:
		- HaarDetector ???
	"""
	def __init__(self):
		print "Inicializando el detector ..."

		# Preparamos el parser del fichero de configuración	
		self.project_path = os.getenv("HOME") + "/fr/rasprec/"

		self.config = ConfigParser.ConfigParser()
		self.config.read(self.project_path + "source/config.ini")
		
		try:
			self.face_width = int(self.config.get('Detector', 'FaceWidth'))
		except:
			print "Error leyendo el fichero de configuración"
			raise


	def detect(self):
		print "Detector"




class HaarDetector(Detector):
	""" Detector facial clásico utilizando Viola Jones con cascadas de Haar
	"""

	def __init__(self):
		#super(HaarDetector, self).__init__()
		Detector.__init__(self)

		try:
			self.classifierPath = self.config.get("HaarDetector", "ClassifierPath")
			self.scale_factor = float(self.config.get("HaarDetector", "scale_factor"))
			self.minNeighbors = int(self.config.get("HaarDetector", "MinNeighbors"))
			minxsize = self.config.get("HaarDetector", "MinXSize")
			minysize = self.config.get("HaarDetector", "MinYSize")
			if minxsize == "None" or minysize == "None":
				self.minSize = None
			else:
				self.minSize = (int(minxsize), int(minysize))
			maxxsize = self.config.get("HaarDetector", "MaxXSize")
			maxysize = self.config.get("HaarDetector", "MaxYSize")
			if maxxsize == "None" or maxysize == "None":
				self.maxSize = None
			else:
				self.maxSize = (int(maxxsize), int(maxysize))
		except:
			print "Error leyendo el fichero de configuración"
			print self.config.options("Detector")
			raise 


		self.faces = []		# Lista con las caras que se han encontrado
		self.rects = []
		self.face_index = 0


	def set_parameters(self, scale_factor=None, min_neighbors=None, min_size=None):
		if not scale_factor is None:
			self.scale_factor = scale_factor

		if not min_neighbors is None:
			self.minNeighbors = min_neighbors

		if not min_size is None:
			self.minSize = min_size


	def get_rects_as_coords(self):
		"""
			Devuelve los rectángulos donde se han encontrado caras como coordenadas de la forma:
			[[[x1, y1], [x2, y2]], ...]
		"""
		rc = []
		for r in self.rects:
			(x1, x2), (y1, y2) = r
			rc.append([(x1, y1), (x2, y2)])

		return rc
			


	def detect(self, image):
		# Se detectan las caras
		d = cv2.CascadeClassifier(self.classifierPath)
		
		rects = d.detectMultiScale(image ,
					scaleFactor=self.scale_factor,
					minNeighbors=self.minNeighbors,
					minSize=(30, 30),
					flags=cv2.CASCADE_SCALE_IMAGE)
		
		# Se almacenan las coordenadas de las caras detectadas en la imagen
		self.faces = [image[y:y+h, x:x+w] for (x, y, w, h) in rects]
		self.rects = [((int(x), int(y)), (int(x+w), int(y+h))) for (x, y, w, h) in rects]
		# Las coordenadas tienen la forma ((x1, y1), (x2, y2))
		
		# Recogemos la imagen de la cara y sus coordenadas dentro de la imagen original
		#self.f = [((int(x), int(x+w), int(y), int(y+h)),image[y:y+h, x:x+w]) for (x, y, w, h) in rects]

		# Redimensionamos las caras al tamaño de salida
		for i in range(len(self.faces)):
			self.faces[i] = imutils.resize(self.faces[i], width=self.face_width)

		self.num_faces = len(self.faces)
		if self.num_faces == 0:
			self.more_faces = False
		else:
			self.face_index = 0
			self.more_faces = True


	def next_face(self):
		""" Itera sobre las caras encontradas

		Devuelve:
			f: imagen con la cara que se ha detectado
			r: coordenadas de la cara encontrada de la forma (x1, x2, y1, y2)
		"""
		f = self.faces[self.face_index]
		r = self.rects[self.face_index]
		self.face_index += 1
		if self.face_index >= len(self.faces):
			self.more_faces = False
		return (f, r)
