# -*- coding: utf-8 -*- 
import cv2
import os
import ConfigParser
import imutils
import time
#from picamera import PiCamera
#from picamera.array import PiRGBArray

from WebcamVideoStream import WebcamVideoStream



class Provider():
	def __init__(self):
		print "Inicializado el proveedor ..."

		self.next_image = None	# Siguiente imagen
		self.next_label = None	# Etiqueta de la siguiente imagen
		self.has_next = False	# Almacena si quedan más imágenes

		config = ConfigParser.ConfigParser()
		config.read("../source/config.ini")
		options = config.options("Preprocessor")
		#print config.options("Preprocessor")
		if "initialwidth" in config.options("Preprocessor"):
			self.width = int(config.get("Preprocessor", "InitialWidth"))
		else:
			self.width = 320


	def next(self):
		""" Devuelve la siguiente imagen

		Las clases que hereden de ésta deben implementar el método prepare_next()
		"""
		n = self.next_image
		self.prepare_next()
		return n



class FileProvider(Provider):
	"""
	Proveedor de imágenes a partir de un directorio. Debe ser una RUTA ABSOLUTA
	Recoge todas las imágenes que se encuentran en dicho directorio
	"""

	def __init__(self, path):
		"""
		Args:
			path - Ruta ABSOLUTA de las imágenes
		"""
		Provider.__init__(self)
		
		self.files_left = None	# Ficheros que quedan por procesar
		self.path = path		# Ruta del proyecto
		self.files_list = []	# Listado de ficheros del directorio

		lst_dir = os.walk(self.path)
		
		for root, dirs, files in lst_dir:
			for file in files:
				self.files_list.append(root + "/" + file)
					
		self.files_left = len(self.files_list)

		print "Se han detectado {} ficheros".format(self.files_left)

		# Dejamos la primera imagen preparada
		self.prepare_next()


	def prepare_next(self):
		if len(self.files_list) == 0:
			self.has_next = False
		else:
			file = self.files_list.pop()
			print file
						
			image = cv2.imread(file)
			print "Quedan {} imágenes".format(len(self.files_list))
			if image is None:
				#print "No se ha podido cargar la imagen"
				self.has_next = False
				self.prepare_next()
			else:
				# print "Imagen reconocida: {}".format(file)
				self.has_next = True
				self.next_image = imutils.resize(image, width=self.width)
				#self.next_image = image
				# Utilizamos el nombre del fichero para asignar la etiqueta
				filename = os.path.basename(file)

				#self.next_label = int(filename[2])
				#print self.next_label



class VideoStream:
	def __init__(self, src=0, usePiCamera=False, resolution=(320, 240), framerate=32):
		if usePiCamera:
			from PiVideoStream import PiVideoStream

			self.stream = PiVideoStream(resolution=resolution, framerate=framerate)
		else:
			self.stream = WebcamVideoStream(src=src)

	def start(self):
		return self.stream.start()

	def update(self):
		self.stream.update()

	def read(self):
		return self.stream.read()

	def stop(self):
		self.stream.stop()




class CameraProvider(Provider):
	"""
		Proveedor de imágenes a partir de la cámara de la Raspberry Pi

	"""
	def __init__(self):
		"""
		Args:
			path - Ruta ABSOLUTA de las imágenes
		"""
		Provider.__init__(self)
		
		"""
		self.camera = PiCamera()
		self.camera.resolution = (640, 480)
		self.camera.framerate = 32
		"""
		#self.rawCapture = PiRGBArray(self.camera, size=(640, 480))

		self.stream = VideoStream(usePiCamera=True).start()

		# Permitimos que la cámara caliente
		time.sleep(2.0)

		self.prepare_next()


	def prepare_next(self):
		# El parámetro use_video_port=True hace que no se utilice el puerto de imágenes
		# de la cámara. Peor calidad de imagen pero mayor velocidad.
		# http://picamera.readthedocs.io/en/release-1.13/api_camera.html
		"""
		self.camera.capture(self.rawCapture, format="bgr", use_video_port=True)
		image = self.rawCapture.array
		self.has_next = True
		self.next_image = image
		"""

		self.next_image = self.stream.read()
		self.has_next = True




