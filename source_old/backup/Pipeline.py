# -*- coding: utf-8 -*- 
import cv2
import os
import time
import numpy as np
import Trainer

from Interface import Interface


class Pipeline: 
	"""
	Esta es la clase que encapsula todo el proceso de procesamiento de la imagen.
	Los pasos son:
		1.- Obtención de las imágenes. 	Instancia de la clase Provider
		2.- Preprocesamiento.			Instancia de la clase Preprocessor
		3.- Detección de las caras.		Instancia de la clase Detector
		4.- Procesamiento de las caras.	Instancia de la clase FaceProcessor
		5.- Reconocimiento facial.		
				5.1.- Entrenamiento. 	Instancia de la clase Trainer
				5.2.- Reconocimiento. 	Instancia de la clase Recognizer
	"""

	def __init__ (self, provider, preprocessor=None, detector=None, face_processor=None, trainer=None, recognizer=None, train_mode=True):

		# Etapas del procesado de imágenes
		self.provider = provider
		self.preprocessor = preprocessor
		self.detector = detector
		self.face_processor = face_processor
		self.trainer = trainer
		self.recognizer = recognizer

		self.train_mode = train_mode
		self.train_faces = []
		self.train_labels = []

		# Variables para medir los tiempos empleados
		self.preprocessor_time = []
		self.detector_time = []
		self.processor_time = []

		# Inicializamos la interfaz
		self.gui = Interface()


		self.original_image = None		# Imagen original
		self.preprocessed_image = None	# Imagen tras el preprocesado
		self.output = None				# Imagen que se mostrará al final
		self.output_face = None			# Imagen que mostrará el procesamiento de la cara

		if preprocessor is None:
			print "Warning: No se ha definido un preprocesador"
		if detector is None:
			print "Warning: No se ha definido un detector"
		if face_processor is None:
			print "Warning: No se ha definido un procesador facial"
		if recognizer is None:
			print "Warning: No se ha definido un reconocedor"

		print "Inicializamos el pipeline"


	def start(self):
		img_number=0
		#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
		while self.provider.has_next:			
			# Obtenemos la siguiente imagen
			start = time.time()
			self.original_image = self.provider.next()
			self.gui.set_image(self.original_image)

			self.output = self.original_image

			# Preprocesamiento
			self.preprocessed_image = self.preprocessor.prepare(self.original_image)
			
			# Detección
			self.detector.detect(self.preprocessed_image)
			end = time.time()
			t = (end-start)*1000
			print "Tiempo de detección: " + str(t) + " ms."

			while self.detector.more_faces:
				start = time.time()
				f, r = self.detector.next_face()	# Imagen de la cara y coordenadas donde se encuentra
				(face_x1, face_y1), (face_x2, face_y2) = r
				
				self.gui.set_face(f)
				self.gui.set_face_rect(r)

				self.output_face = cv2.cvtColor(f, cv2.COLOR_GRAY2BGR)

				# Preprocesado
				self.output = cv2.rectangle(self.output, (face_x1, face_y1), (face_x2, face_y2), (255, 0, 0), 2)

				img_number += 1
				# Esto itera por cada cara encontrada en la imagen

				method = "HAAR"
				method = "FACIAL_LANDMARKS"
				eyes, areas, landmarks = self.face_processor.load(f, method)
				self.gui.set_eyes(eyes, areas, landmarks)
				print "---------------"

				cface, hface, sface, pface = self.face_processor.get_processed_faces()
				self.gui.set_processed_faces(cface, hface, sface, pface)
				end = time.time()
				t = (end - start)*1000
				print "Tiempo de procesado: " + str(t) + " ms."
				print " "

				if self.train_mode:
					# Modo entrenamiento
					# Por ahora recogemos las caras a procesar
					lbl = self.provider.next_label

					if pface is not None:
						self.train_faces.append(pface)
						self.train_labels.append(lbl)
					else:
						print "Cara no válida"

				# Mostramos las caras seleccionadas en la interface
				self.gui.set_training_faces(self.train_faces)

			
			self.gui.draw()

			#cv2.imshow("Output", self.output)
			#cv2.imshow("Face", self.output_face)
			#cv2.waitKey(1500)

		# Se han leido todas las imágenes, se puede proceder al entrenamiento
		if self.train_mode:
			trainer = Trainer.FisherTrainer(self.train_faces, "Victor")
			trainer.train()

		cv2.waitKey(0)
		cv2.destroyAllWindows()
			
	

