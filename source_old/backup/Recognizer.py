# -*- coding: utf-8 -*- 
import cv2
import numpy as np
import time



class Recognizer:
	"""
	Clase base para los reconocedores faciales
	"""
	def __init__(self):
		print "Inicializando el reconocedor"


class EigenfacesRecognizer(Recognizer):
	"""
		Reconocedor facial basado en EigenFaces
	"""
	def __init__(self):
		Recognizer.__init__(self)

		#self.method = method		# EIGENFACES, FISHERFACES o ANN	(tal vez alguno más o menos)
		self.training_set = []		# Caras para el entrenamiento
		self.training_labels = []	# Etiquetas para el entrenamiento
		self.faceRecognizer = None	# Sistema de reconocimiento facial

		self.faceRecognizer = cv2.face.createEigenFaceRecognizer()
		print dir(self.faceRecognizer)


	def add_face(self, face, label):
		""" Añade una cara al conjunto de entrenamiento

		Se puede añadir una cara de la clase Face, una imagen o la ruta de un fichero en disco
		"""
		self.training_set.append(face)
		self.training_labels.append(label)


	def train(self):
		self.faceRecognizer.train(self.training_set, np.array(self.training_labels))
				


	def predict(self, image):
		prediction = self.faceRecognizer.predict(image)
		print "++++++++++++"
		print self.faceRecognizer.getEigenVectors().shape
		print "++++++++++++"
		return prediction

