# -*- coding: utf-8 -*- 
import cv2
import imutils
import numpy as np
import dlib
from imutils import face_utils


class LandmarkEyeDetector:
	"""
	Implementa un detector de rasgos faciales utilizando Facial Landmarks
	"""
	def __init__(self, predictor, face=None):
		self.face = face
		self.landmarks = None
		self.predictor = predictor


	def get_landmarks(self):
		return self.landmarks

	def set_face(self, face):
		""" Carga un frame en el objeto y aplica las operaciones a realizar
		"""
		self.face = face


	def detect(self):
		"""
		Detecta los rasgos faciales y devuelve una lista con las coordenadas de los rasgos encontrados
		"""

		w, h = self.face.shape
		rect = dlib.rectangle(0, 0, w-1, h-1)

		landmarks = self.predictor(self.face, rect)
		self.landmarks = face_utils.shape_to_np(landmarks)
		
		










class HaarEyeDetector:
	"""
		Implementa un detector de ojos en la imagen de una cara
	"""
	def __init__(self, face=None):
		self.face = None

		self.rects = None				# El formato es: [[x1, x2, y1, y2], ...]
		self.left_eye_rect = None		# Solo se almacena un elemento detectado: [x1, x2, y1, y2]
		self.right_eye_rect = None

		# Posiciones donde localizar los ojos
		# El ojo izquierdo lo busca en el rectangulo dado por [(left_eye_x1, left_eye_y1), (left_eye_x21, left_eye_y2)]
		# El ojo derecho lo busca en el rectangulo dado por [(right_eye_x1, right_eye_y1), (right_eye_x21, right_eye_y2)]
		self.width = None
		self.height = None
		self.left_eye_x1 = None			
		self.left_eye_x2 = None			
		self.right_eye_x1 = None		
		self.right_eye_x2 = None		
		self.left_eye_y1 = None
		self.left_eye_y2 = None
		self.right_eye_y1 = None
		self.right_eye_y2 = None
		self.left_eye_area = None
		self.right_eye_area = None
		self.left_eye_center = None
		self.right_eye_center = None
		self.detected = 0				# Número de ojos que se han detectado

		# Parámetros para el clasificador
		self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/src/haar_files/haarcascade_eye.xml'
		self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/src/haar_files/haarcascade_eye_tree_eyeglasses.xml'
		self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/src/haar_files/haarcascade_righteye_2splits.xml'
		self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/src/haar_files/haarcascade_lefteye_2splits.xml'
		self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/source/haar_files/haarcascade_eye.xml'
		
		self.scale_factor = 1.04	
		self.minNeighbors = 3
		self.minSize = (20, 20)
		self.maxSize = None

		if face is not None:
			self.set_face(face)


	def set_face(self, face):
		""" Carga un frame en el objeto y aplica las operaciones a realizar
		"""
		self.face = face

		# Determinamos las posiciones donde hay que buscar los ojos
		# Según la bibliografía estos valores son:
		# x1=0.16; x2=0.46; y1=0.26; y2=0.54
		# para el ojo derecho y normalizando los valores
		self.width, self.height = self.face.shape
		x1 = 0.14	# Original 0.16
		x2 = 0.48	# Original 0.46
		y1 = 0.22	# Original 0.26
		y2 = 0.60	# Original 0.54
		self.left_eye_x1 = int(self.width * x1)
		self.left_eye_x2 = int(self.width * x2)
		self.right_eye_x1 = int(self.width * (1-x2))
		self.right_eye_x2 = int(self.width * (1-x1))
		self.left_eye_y1 = int(self.height * y1)
		self.left_eye_y2 = int(self.height * y2)
		self.right_eye_y1 = int(self.left_eye_y1)
		self.right_eye_y2 = int(self.left_eye_y2)

		self.left_eye_area =  [(self.left_eye_x1, self.left_eye_y1), (self.left_eye_x2, self.left_eye_y2)]
		self.right_eye_area = [(self.right_eye_x1, self.right_eye_y1), (self.right_eye_x2, self.right_eye_y2)]


	def get_eyes_area(self):
		""" Devuelve las coordenadas donde se buscan los ojos
		Es un array de la forma [[xl1, xl2, yl1, yl2], [xr1, xr2, yr1, yr2]]
		"""
		return [self.left_eye_area, self.right_eye_area]


	def get_eyes_centers(self):
		""" Devuelve los centros de ambos ojos
		Es un array de la forma [[xl, yl], [xr, yr]]
		"""
		return [self.left_eye_center, self.right_eye_center]


	def get_slope(self):
		""" Devuelve la inclinación de la imagen
		Es decir, la pendiente de la recta que une ambos ojos. Lo devuelve en radianes.
		"""
		if self.detected == 2:
			x1, y1 = self.left_eye_center
			x2, y2 = self.right_eye_center
			return float(y2-y1)/(x2-x1)
		else:
			return None


	def get_marked_image(self):
		""" Devuelve la imagen original con indicando la posición esperada y real de los ojos
		"""




	def set_parameters(self, scale_factor=None, minNeighbors=None, minSize=None, maxSize=None):
		""" Establece los parámetros del detector facial
		"""
		if not scale_factor is None:
			self.scale_factor = scale_factor
		if not minNeighbors is None:
			self.minNeighbors = minNeighbors
		if not minSize is None:
			if type(minSize) is tuple:
				self.minSize = minSize
			else:
				self.minSize = (minSize, minSize)
		if not maxSize is None:
			if type(maxSize) is tuple:
				self.maxSize = maxSize
			else:
				self.maxSize = (maxSize, maxSize)		


	def detect(self, xml_file=None):
		""" Detecta los ojos y devuelve una lista con las coordenadas de los ojos encontrados
		"""
		
		if xml_file == "HAARCASCADE_RIGHTEYE_2SPLITS":
			self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/source/haar_files/haarcascade_righteye_2splits.xml'
		elif xml_file == "HAARCASCAE_LEFTEYE_2SPLITS":
			self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/source/haar_files/haarcascade_lefteye_2splits.xml'
		elif xml_file == "HAARCASCADE_EYE_TREE_EYEGLASSES":
			self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/source/haar_files/haarcascade_eye_tree_eyeglasses.xml'
		else:			
			self.classifier_path = '/home/victor/facial-recognition-for-raspberry-pi-3/rasprec/source/haar_files/haarcascade_eye.xml'
		
		# Inicializamos el detector con el clasificador seleccionado
		eye_detector = cv2.CascadeClassifier(self.classifier_path)

		# Localizamos el ojo izquierdo en las coordenadas en que se debería encontrar
		left_eye_pos = self.face[self.left_eye_y1:self.left_eye_y2, self.left_eye_x1:self.left_eye_x2]
		left_eye_pos = cv2.equalizeHist(left_eye_pos)
		left_eye_rect = eye_detector.detectMultiScale(left_eye_pos,
			scale_factor=self.scale_factor,
			minNeighbors=self.minNeighbors,
			minSize=self.minSize,
			maxSize=self.maxSize,
			flags=cv2.CASCADE_SCALE_IMAGE)
		if np.array(left_eye_rect).size > 0:
			self.detected += 1
			self.left_eye_rect = [(int(x)+self.left_eye_x1, int(x+w)+self.left_eye_x1, int(y)+self.left_eye_y1, int(y+h)+self.left_eye_y1) for (x, y, w, h) in left_eye_rect]
			self.left_eye_rect = self.left_eye_rect[0]
			# Calculamos el centro del ojo
			x1, x2, y1, y2 = self.left_eye_rect
			self.left_eye_center = (x1 + (x2-x1)/2, y1 + (y2-y1)/2)

		# Localizamos el ojo derecho en las coordenadas en que se debería encontrar
		right_eye_pos = self.face[self.right_eye_y1:self.right_eye_y2, self.right_eye_x1:self.right_eye_x2]
		right_eye_pos = cv2.equalizeHist(right_eye_pos)
		right_eye_rect = eye_detector.detectMultiScale(right_eye_pos,
			scale_factor=self.scale_factor,
			minNeighbors=self.minNeighbors,
			minSize=self.minSize,
			maxSize=self.maxSize,
			flags=cv2.CASCADE_SCALE_IMAGE)
		if np.array(right_eye_rect).size > 0:
			self.detected += 1
			self.right_eye_rect = [(int(x)+self.right_eye_x1, int(x+w)+self.right_eye_x1, int(y)+self.right_eye_y1, int(y+h)+self.right_eye_y1) for (x, y, w, h) in right_eye_rect]
			self.right_eye_rect = self.right_eye_rect[0]
			# Calculamos el centro del ojo
			x1, x2, y1, y2 = self.right_eye_rect
			self.right_eye_center = (x1 + (x2-x1)/2, y1 + (y2-y1)/2)

