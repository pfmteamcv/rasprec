import cv2
import imutils
from pathlib import Path
import os




home = str(Path.home())
faces_path = home + "/TFM/datasets/train/faces/"
nfaces_path = home + "/TFM/datasets/train/nfaces/"

# Primero las caras
lst_dir = os.walk(faces_path)
faces_files = []
for root, dirs, files in lst_dir:
	for f in files:
		if f[-3:] in ["pgm", "jpg", "png"]:
			faces_files.append(root+f)

faces_dir = home + "/TFM/datasets/train2/faces/"
img_number=1
for i in faces_files:
	img=cv2.imread(i)
	img=imutils.resize(img, width=70)
	img=img[13:83,:,:]
	filename = faces_dir + "f" + str(img_number).zfill(5) + ".png"
	cv2.imwrite(filename, img)
	img_number+=1

	
# Y luego las no caras
lst_dir = os.walk(nfaces_path)
nfaces_files = []
for root, dirs, files in lst_dir:
	for f in files:
		if f[-3:] in ["pgm", "jpg", "png"]:
			nfaces_files.append(root+f)

nfaces_dir = home + "/TFM/datasets/train2/nfaces/"
img_number=1
for i in nfaces_files:
	img=cv2.imread(i)
	img=imutils.resize(img, width=70)
	img=img[13:83,:,:]
	filename = nfaces_dir + "f" + str(img_number).zfill(5) + ".png"
	cv2.imwrite(filename, img)
	img_number+=1
	

